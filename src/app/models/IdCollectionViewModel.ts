export class IdCollectionModel{
    id: number;
    userId: number;
    currencyId: number;
    countryId: number;
    billingId: number;
    Email: string;
    rStatus: string;
    status: number;
    operatorId:number;
  }
