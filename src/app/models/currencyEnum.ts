export enum CurrencyEnum {
  "GBP" = "£",
  "USD" = "$",
  "XOF" = "CFA",
  "EUR" = "€",
  "KES" = "Ksh"
}
