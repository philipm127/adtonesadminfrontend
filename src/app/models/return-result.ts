export interface ReturnResult{
  result:number;
  error:string;
  body:object;
  recordcount:number;
}
