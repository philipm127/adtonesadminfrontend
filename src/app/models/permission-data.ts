export class PermissionData{
  pageName:string;
  visible:boolean;
  elements:element[];
}

export class element{
  name:string;
  enabled:boolean;
  visible:boolean;
  route:string;
  type:string;
  description:string;
  arrayId:number[];
  elements:element[];
}

export class permissionList{
  pageData:PermissionData[];
}

