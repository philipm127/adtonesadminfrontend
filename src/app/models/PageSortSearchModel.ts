export class PageSearchModel {
  name: string;
  email: string;
  status: string;
  operator: string;
  country: string;
  typeName: string;
  dateFrom: string;
  dateTo: string;
  numberFrom: number;
  numberTo: number;
  numberFrom2: number;
  numberTo2: number;
  numberFrom3: number;
  numberTo3: number;
  client: string;
  fullName: string;
  payment: string;
  responseFrom: string;
  responseTo: string;
}

export class PageSortSearchModel {
  elementId: number;
  page: number;
  pageSize: number;
  sort: string;
  direction: string;
  search: string;
}

export class ManagementReportSearchModel {
  operators: number[];
  dateFrom: string;
  dateTo: string;
  country: number;
  currency: number;
}
