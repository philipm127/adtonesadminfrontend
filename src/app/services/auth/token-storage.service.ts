import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { ReturnResult } from 'src/app/models/return-result';


const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})

export class TokenStorageService {
  private loggedIn = new BehaviorSubject<boolean>(this.tokenAvailable());
  retModel: ReturnResult;

  constructor(private router: Router) { }

  signOut() {
    // console.log("Entered SigOut: ",this.getToken());
    window.sessionStorage.removeItem(TOKEN_KEY);
    sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.clear();
    sessionStorage.clear();
    // console.log("token-storeage-service gettoken after clear: ",this.getToken());
    this.loggedIn.next(false);
    this.router.navigate(["login"]);
  }

  public saveToken(token: string) {
    console.log('live token ', token)
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
    this.loggedIn.next(true);
  }

  public getToken(): string {
    /// TODO: Need to remove this on live.
    /// For testing if don't have token use the one set up in environment
    let token = sessionStorage.getItem(TOKEN_KEY);
    console.log(' test token ', token)
    return token;
    // if(token !=null && token.length>5)
    //   return token;
    // else
    //   return environment.token;
  }


  public saveUser(user) {
    console.log("LOGIN user model : ",user);
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser() {
    /// TODO: Need to remove this on live.
    /// For testing if don't have user get from local storage creted on app.component
    let user = JSON.parse(sessionStorage.getItem(USER_KEY));
    // console.log("token-storeage-service getuser the session stored, what is value of user: ",user);
    if (user != null)
      return user;
    else
      user = JSON.parse(sessionStorage.getItem("userdata"));

    // console.log("token-storeage-service getuser, what is value of user: ",user);
    return user;
  }

  private tokenAvailable(): boolean {
    // console.log(localStorage.getItem('token'));
    return !!sessionStorage.getItem(TOKEN_KEY);
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
}
