import { Injectable } from '@angular/core';
import { TokenStorageService } from './token-storage.service';
import { AuthenticationService } from '../../components/Authentication/authentication.service';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';

@Injectable({
  providedIn: 'root'
})
export class TokenRefreshService {

  retModel:ReturnResult;

  constructor(private tokenService: TokenStorageService, private authService: AuthenticationService) { }

  public refreshToken(){
    let user = this.tokenService.getUser();
    this.authService.RefreshToken(user.email)
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retModel = res.body
        if (this.retModel.result == 1) {
          this.tokenService.saveToken(this.retModel.body.toString());
        }
      });
  }

}
