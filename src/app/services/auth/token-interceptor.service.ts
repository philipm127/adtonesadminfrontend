import { HTTP_INTERCEPTORS, HttpEvent } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { TokenStorageService } from './token-storage.service';
import { environment } from 'src/environments/environment';


const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  constructor(
    private injector: Injector, private tokenStorage: TokenStorageService
  ) { }


  intercept(req: HttpRequest<any>, next: HttpHandler) {
    let authReq = req;
    let token: string = null;

    token = this.tokenStorage.getToken();

    // console.log("What is in token: ", token);
    if (token != null) {
      authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token) });
    }
    return next.handle(authReq);
  }
}

export const authInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true }
];
