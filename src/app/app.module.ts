import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PageNotFoundComponent } from './components/shared/page-not-found/page-not-found.component';

import { DatePipe } from '@angular/common';
import { FinancialModule } from './components/financials/financial.module';
import { UsermanagementModule } from './components/usermanagement/usermanagement.module';
import { NavigationModule } from './components/navigation/navigation.module';
import { ProfilematchModule } from './components/profilematch/profilematch.module';
import { CampaignModule } from './components/camapign/campaign.module';

import { authInterceptorProviders } from './services/auth/token-interceptor.service';
import { SharedModule } from './components/shared/shared.module';
import { CountryModule } from './components/country/country.module';
import { OperatorModule } from './components/operator/operator.module';
import { ConfigSystemModule } from './components/config-system/config-system.module';
import { TicketsModule } from './components/tickets/tickets.module';
import { AppRoutingModule } from './app-routing.module';
import { AuthenticationModule } from './components/Authentication/authentication.module';
import { CamapaignPromoModule } from './components/campaign-promo/camapaign-promo.module';
import { HttpErrorInterceptor } from './components/shared/services/http-error.interceptor.service';
import { PermissionManagementModule } from './components/permission-management/permission-management.module';
import { ManagementReportsModule } from './components/management-reports/management-reports.module';
import { SalesManagementModule } from './components/sales-management/sales-management.module';
import { CrmModule } from './components/crm/crm.module';
import { VisitorService } from './components/Authentication/visitor.service';
import { AdvertisersModule } from './components/advertisers/advertisers.module';
import { AdvertsModule } from './components/adverts/adverts.module';
import { CampaignCreateEditModule } from './components/campaign-create-edit/campaign-create-edit.module';




@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    UsermanagementModule,
    FinancialModule,
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    NavigationModule,
    ProfilematchModule,
    CampaignModule,
    CamapaignPromoModule,
    SharedModule,
    CountryModule,
    OperatorModule,
    ConfigSystemModule,
    TicketsModule,
    AuthenticationModule,
    PermissionManagementModule,
    ManagementReportsModule,
    SalesManagementModule,
    AdvertisersModule,
    AdvertsModule,
    CampaignCreateEditModule,
    CrmModule,
    AppRoutingModule // Always needs to be last
  ],
  providers: [
    DatePipe,
    VisitorService,
    authInterceptorProviders,
    {
      provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
