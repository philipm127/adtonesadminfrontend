import { Component, OnInit } from '@angular/core';
import { CountryService } from '../country.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ReturnResult } from 'src/app/models/return-result';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { AreaModel } from '../models/area-form-model';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-add-area',
  templateUrl: './add-area.component.html',
  styleUrls: ['./add-area.component.css']
})
export class AddAreaComponent implements OnInit {

  pagename: string = "AddArea";
  areaId: string = "0";

  form = new FormGroup({
    areaId: new FormControl(0),
    areaName: new FormControl("", Validators.required),
    countryId: new FormControl("", Validators.required)
  });


  editMode: boolean = false;
  userModel: AreaModel;
  retModel: ReturnResult;
  retResult: ReturnResult;
  countrySelect: BasicSelectModel[];
  destroy$: Subject<BasicSelectModel> = new Subject();

  submitEnabled: boolean = true;
  areaNameVisible: boolean = true;
  countryIdVisible: boolean = true;

  userData: any;

  successMessage: string;
  failMessage: string;

  inputvalue = "";


  constructor(private formBuilder: FormBuilder, private countryService: CountryService,
    private accessService: AccessServicesService, private listService: SelectServicesService,
    private route: ActivatedRoute, private router: Router) {

    this.areaId = this.route.snapshot.paramMap.get('id');
    if (parseInt(this.areaId) > 0) {
      this.pagename = "EditArea";
      this.editMode = true;
    }

    this.userData = this.accessService.getUser();
    this.getCountry();

  }

  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();

    if (this.editMode) {
      this.countryService.getAreaDetails(this.areaId)
        .subscribe((res: HttpResponse<ReturnResult>) => {
          this.retModel = res.body;
          if (this.retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <AreaModel>this.retModel.body;
            // console.log("returned userModel: ", this.userModel);
            this.initForm();
          }
        });
    }
  }

  getCountry() {
    this.listService.getCountryList()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        console.log("returned result from get country list: ", this.retResult);
        if (this.retResult.result == 1) {

          this.countrySelect = <BasicSelectModel[]>this.retResult.body;
        }
        else
          this.countrySelect = <BasicSelectModel[]>this.retResult.body;
      });
  }


  private initForm() {
    if (this.editMode) {
      this.form.patchValue({
        areaId: this.userModel.areaId,
        areaName: this.userModel.areaName,
        countryId: this.userModel.countryId
      });
      this.form.get('countryId').setValue(this.userModel.countryId.toString());
      this.setPermissions();
    }
    else
      this.setPermissions();
  }

  setPermissions() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    // console.log("matching value of permissions: ",element.name);
    permListData["elements"].forEach(element => {
      if (element.type == "element" && element.name == this.accessService.getName(this.form.get(element.name))) {
        this[element.name.concat('Visible')] = element.visible;
        console.log("what is enabled/disabled : ",element);
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
      else if (element.type == "button") {
        this.submitEnabled = element.enabled;
      }
    });
  }

  onSubmit() {
    // this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log("What is this form data: ", this.form.value);
    let serviceResult = null;
    if (this.editMode)
      serviceResult = this.countryService.updateArea(this.form.value);
    else
      serviceResult = this.countryService.addArea(this.form.value);
    serviceResult
      .subscribe(resp => {
        this.retModel = resp

        if (this.retModel.result == 1) {
          this.successMessage = "The Details Were Updated Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the details";
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
