import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { CountryModel } from 'src/app/components/country/models/country-form-model';
import { CountryService } from 'src/app/components/country/country.service';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import _ from 'lodash';


@Component({
  selector: 'app-country-table',
  templateUrl: './country-table.component.html',
  styleUrls: ['./country-table.component.css']
})

export class CountryTableComponent implements OnDestroy {
  pagename: string = "CountryTable";
  userData: any;
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];

  destroy$: Subject<CountryModel> = new Subject();
  dataSource: MatTableDataSource<CountryModel>;
  modelSource: CountryModel[];
  isLoading: boolean = true;

  countrySelect: BasicSelectModel[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  retResult: ReturnResult = null;

  tableSearchModel: TableSearchModel;

  constructor(private countryService: CountryService, private accessService: AccessServicesService,
    private popSearch: PopSearchDdService, private datePipe: DatePipe) {

    this.userData = this.accessService.getUser();
    this.PermissionAssignment();

    this.countryService.getCountryList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <CountryModel[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<CountryModel>(this.modelSource);
          this.isLoading = false;
        }
        this.countrySelect = this.popSearch.popCountry(this.modelSource);
        this.dataSource.filterPredicate = ((data, filter) => {
          const a = !filter.country || data.name.trim().toLowerCase().includes(filter.country.trim().toLowerCase());
          const b = !filter.dateFrom || data.createdDate >= this.datePipe.transform(filter.dateFrom, 'yyyy-MM-dd');
          const c = !filter.dateTo || data.createdDate <= this.datePipe.transform(filter.dateTo, 'yyyy-MM-dd');
          return a && b && c;
        }) as (CountryElement, string) => boolean;

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}

