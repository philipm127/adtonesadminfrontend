import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { CountryService } from 'src/app/components/country/country.service';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { AreaModel } from 'src/app/components/country/models/area-form-model';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';

@Component({
  selector: 'app-area-table',
  templateUrl: './area-table.component.html',
  styleUrls: ['./area-table.component.css']
})

export class AreaTableComponent implements OnDestroy {
  pagename: string = "AreaTable";
  userData: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = ['areaName', 'countryName', 'action'];

  destroy$: Subject<AreaModel> = new Subject();
  dataSource: MatTableDataSource<AreaModel>;
  modelSource: AreaModel[];
  isLoading: boolean = true;

  countrySelect: BasicSelectModel[] = [];

  retResult: ReturnResult = null;
  retResultUpdate: ReturnResult = null;

  tableSearchModel: TableSearchModel;
  // This is to appear in Search Box of numberFrom/numberTo
  // numberName:string="Credit Limit";

  constructor(private countryService: CountryService, private accessService: AccessServicesService,
    private popSearch: PopSearchDdService, private datePipe: DatePipe) {

    this.userData = this.accessService.getUser();
    this.PermissionAssignment();
    this.countryService.getAreaList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <AreaModel[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<AreaModel>(this.modelSource);
          this.isLoading = false;
        }

        this.countrySelect = this.popSearch.popCountry(this.modelSource);
        this.dataSource.filterPredicate = ((data, filter) => {
          const a = !filter.country || data.countryName.trim().toLowerCase().includes(filter.country.trim().toLowerCase());
          const b = !filter.name || data.areaName.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          return a && b;
        }) as (AreaElement, string) => boolean;

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }


  DeleteArea(id: number) {
    this.countryService.deleteArea(id)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResultUpdate = res.body;

        if (this.retResultUpdate.result == 1) {
          for (let i in this.modelSource) {
            if (this.modelSource[i].areaId == id) {
              this.modelSource.splice(parseInt(i), 1);
              break;
            }
          }
          this.dataSource = new MatTableDataSource<AreaModel>(this.modelSource);
        }
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}

