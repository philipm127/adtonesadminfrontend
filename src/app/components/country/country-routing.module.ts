import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CountryTableComponent } from './country-table/country-table.component';
import { AreaTableComponent } from './area-table/area-table.component';
import { AddAreaComponent } from './add-area/add-area.component';
import { AddCountryComponent } from './add-country/add-country.component';


const countryRoutes: Routes = [
  { path: 'countries', component: CountryTableComponent },
  { path: 'areas', component: AreaTableComponent },
  { path: 'areadetails/:id', component: AddAreaComponent },
  { path: 'countrydetails/:id', component: AddCountryComponent }
];

@NgModule({
  imports: [RouterModule.forChild(countryRoutes)],
  exports: [RouterModule]
})
export class CountryRoutingModule { }
