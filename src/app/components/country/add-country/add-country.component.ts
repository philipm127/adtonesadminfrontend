import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ReturnResult } from 'src/app/models/return-result';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { CountryService } from '../country.service';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { CountryModel } from '../models/country-form-model';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.css']
})
export class AddCountryComponent implements OnInit {
  pagename: string = "AddCountry";
  areaId: string = "0";
  public files: Set<File> = new Set();

  userData: any;

  @ViewChild('file', { static: false }) file;

  form: FormGroup;


  editMode: boolean = false;
  userModel: CountryModel;

  countrySelect: BasicSelectModel[];
  destroy$: Subject<BasicSelectModel> = new Subject();

  submitEnabled: boolean = true;
  areaNameVisible: boolean = true;
  countryIdVisible: boolean = true;



  successMessage: string;
  failMessage: string;

  inputvalue = "";


  constructor(private formBuilder: FormBuilder, private countryService: CountryService,
    private accessService: AccessServicesService, private listService: SelectServicesService,
    private route: ActivatedRoute, private router: Router) {

    this.userData = this.accessService.getUser();
    this.areaId = this.route.snapshot.paramMap.get('id');
    if (parseInt(this.areaId) > 0) {
      this.pagename = "EditCountry";
      this.editMode = true;
    }

    this.form = new FormGroup({
      id: new FormControl("0"),
      name: new FormControl("", Validators.required),
      shortName: new FormControl("", Validators.required),
      countryCode: new FormControl("", Validators.required),
      taxPercentage: new FormControl("", Validators.required),
      minBid: new FormControl("", Validators.required),
      termAndConditionFileName: new FormControl("")
    });

    let permListData = this.accessService.getPermissionList(this.pagename);
    permListData["elements"].forEach(element => {
      if (element.type == "element" && element.name == this.accessService.getName(this.form.get(element.name))) {
        this[element.name.concat('Visible')] = element.visible;
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
      else if (element.type == "button") {
        console.log("matching value of submit type: ", element.name);
        this.submitEnabled = element.enabled;
      }
    });
  }

  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();

    if (this.editMode) {
      this.countryService.getCountryDetails(this.areaId)
        .subscribe((res: HttpResponse<ReturnResult>) => {
          let retModel = res.body;
          if (retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <CountryModel>retModel.body;
            // console.log("returned userModel: ", this.userModel);
            this.initForm();
          }
        });
    }
  }


  private initForm() {
    if (this.editMode) {
      this.form.patchValue({
        id: this.userModel.id,
        name: this.userModel.name,
        countryCode: this.userModel.countryCode,
        shortName: this.userModel.shortName,
        taxPercentage: this.userModel.taxPercentage,
        minBid: this.userModel.minBid,
        termAndConditionFileName: this.userModel.termAndConditionFileName
      });
    }
  }


  onFilesAdded() {
    console.log("Entered onFilesAdded()");
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (let key in files) {
      if (!isNaN(parseInt(key))) {
        this.files.add(files[key]);
      }
    }
    this.failMessage = "";
  }

  addFiles() {
    this.file.nativeElement.click();
  }


  onSubmit() {
    let fs: any;
    if (this.files.size > 0) {
      this.files.forEach(file => {
        fs = file;
      });
    }

    // Until we know I've not added the file as compulsory

    // if (fs == null) {
    //   this.failMessage = "No file added";
    // }
    // else {
    let serviceResult = null;
    console.log("Submitted value: ", this.form.getRawValue());
    if (this.editMode)
      serviceResult = this.countryService.updateCountryWFile(this.form.getRawValue(), fs);
    else
      serviceResult = this.countryService.addCountryWFile(this.form.getRawValue(), fs);
    serviceResult
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          this.successMessage = "The Details Were Updated Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the details";
        }
      });
    //}
  }



  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
