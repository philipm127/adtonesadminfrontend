export interface AreaModel {
  areaId:number;
  areaName:string;
  countryId:number;
  countryName:string;
  isActive:boolean;
}
