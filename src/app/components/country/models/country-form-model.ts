export interface CountryModel {

  id: number;
  name: string;
  shortName: string;
  countryCode: string;
  taxPercentage: number;
  createdDate: string;
  minBid: number;
  status: number;
  userId: number;
  termAndConditionFileName: string;
  file: File;
}
