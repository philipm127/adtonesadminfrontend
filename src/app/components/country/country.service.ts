import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ReturnResult } from '../../models/return-result';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryService {
  myAppUrl: string;
  controller: string = "country/";
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  };

  constructor(private http: HttpClient) {
    this.myAppUrl = (environment.appUrl + this.controller);
  }

  public getCountryList() {
    let ctrAction = "v1/GetCountryData"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  public getCountryDetails(Id: string) {
    let id = parseInt(Id);
    let ctrAction = "v1/GetCountry/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public updateCountry(model: any) {
    let ctrAction = "v1/UpdateCountry";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public addCountry(model: any) {
    let ctrAction = "v1/AddCountry";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public addCountryWFile(model: any, file: File): Observable<ReturnResult> {
    let ctrAction = "v1/AddCountry";
    const formData: FormData = new FormData();
    if (file != null || file != undefined)
      formData.append('file', file, file.name);
    formData.append("Id", model.id);
    formData.append("Name", model.name);
    formData.append("ShortName", model.shortName);
    formData.append("CountryCode", model.countryCode);
    formData.append("TaxPercentage", model.taxPercentage);
    formData.append("MinBid", model.minBid);

    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, formData);

  }

  public updateCountryWFile(model: any, file: File): Observable<ReturnResult> {
    let ctrAction = "v1/UpdateCountry";
    const formData: FormData = new FormData();
    if (file != null || file != undefined)
      formData.append('file', file, file.name);
    formData.append("Id", model.id);
    formData.append("Name", model.name);
    formData.append("ShortName", model.shortName);
    formData.append("CountryCode", model.countryCode);
    formData.append("TaxPercentage", model.taxPercentage);
    formData.append("TermAndConditionFileName", model.termAndConditionFileName);
    formData.append("MinBid", model.minBid);


    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, formData);

  }

  public getAreaList() {
    let ctrAction = "v1/GetAreaData"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getAreaDetails(Id: string) {
    let id = parseInt(Id);
    let ctrAction = "v1/GetAreaById/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public deleteArea(id: number) {
    let ctrAction = "v1/DeleteAreaById/";
    return this.http.delete<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public updateArea(model: any) {
    let ctrAction = "v1/UpdateArea";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public addArea(model: any) {
    let ctrAction = "v1/AddArea";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public getMinCampaignBid(id: number) {
    // let id = parseInt(Id);
    let ctrAction = "v1/GetMinimumBid/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

}
