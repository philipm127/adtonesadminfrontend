import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CountryRoutingModule } from './country-routing.module';
import { AreaTableComponent } from './area-table/area-table.component';
import { CountryTableComponent } from './country-table/country-table.component';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatTableExporterModule } from 'mat-table-exporter';
import { SharedModule } from '../shared/shared.module';
import { AddCountryComponent } from './add-country/add-country.component';
import { AddAreaComponent } from './add-area/add-area.component';


@NgModule({
  declarations: [
    AreaTableComponent,
    CountryTableComponent,
    AddCountryComponent,
    AddAreaComponent
  ],
  imports: [
    CommonModule,
    CountryRoutingModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule,
    SharedModule
  ]
})
export class CountryModule { }
