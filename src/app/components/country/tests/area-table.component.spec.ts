import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AreaTableComponent } from '../area-table/area-table.component';

describe('AreaTableComponent', () => {
  let component: AreaTableComponent;
  let fixture: ComponentFixture<AreaTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
