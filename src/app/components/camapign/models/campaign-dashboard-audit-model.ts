export interface CampaignDashboardAuditModel
    {
        averageBid: number;
        totalSpend: number;
        totalPlayed: number;
        campaignName:string;
        advertName:string;
        currencyCode:string;
        averagePlayTime:number;
        freePlays:number;
        totalBudget:number;
        maxPlayLength:string;
        totalReach:number;
        reach:number;
    }
