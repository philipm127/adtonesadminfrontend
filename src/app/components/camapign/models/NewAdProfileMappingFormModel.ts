export interface NewAdProfileMappingFormModel{

  Id : number;
  CampaignProfileId : number;
  CountryId : number;
  OperatorId : number;
  AdtoneServerCampaignProfilePrefId? : number;
}
