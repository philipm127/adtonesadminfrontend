export interface CampaignAdminResult {
  userId: number;
  email: string;
  userName: string;
  clientId: number;
  clientName: string;
  advertId: number;
  advertName: string;
  campaignProfileId: number;
  campaignName: string;
  categoryName: string;
  totalBudget: number;
  finaltotalplays: number;
  fundsAvailable: number;
  avgBidValue: number;
  totalSpend: number;
  status: number;
  rStatus: string;
  createdDate: string;
  reach:number;
  ticketCount: number;
  isAdminApproval: boolean;
  countryId: number;
  operatorId: number;
  operatorName: string;
  organisation: string;
  mobileNumber: string;
  countryName: string;
  salesExec: string;
  sUserId: number;

}
