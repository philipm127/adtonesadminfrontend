export interface DashboardPlayListModel
    {
        totalCost: number;
        playCost: number;
        emailCost: number;
        smsCost: number;
        startTime: string;
        endTime: string;
        playLength: number;
        emailMsg: string;
        sms: string;
        userId: number;
        currencyCode: string;
        advertName: string;
        campaignProfileId:number;
    }
