import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ReturnResult } from 'src/app/models/return-result';

@Injectable({
  providedIn: 'root'
})
export class CampaignListService {
  myAppUrl: string;
  // controller: string = "campaign/";

  constructor(private http: HttpClient) {
    this.myAppUrl = (environment.appUrl + "campaign/")
  }

  public getCampaignList(id: number) {
    let ctrAction = "v1/GetCampaignDataTable/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getCampaignListForExec(id: number) {
    let ctrAction = "v1/GetCampaignDataTableForSalesExec/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public getCampaignListForAdvertiser(id: number) {
    let ctrAction = "v1/GetCampaignDataTableForAdvertiser/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getCampaignListSingle(id: number) {
    let ctrAction = "v1/GetCampaignDataTableByCampId/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public updateCampaignStatus(model: any) {
    let ctrAction = "v1/UpdateCampaignStatus";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

}
