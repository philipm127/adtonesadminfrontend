import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { ReturnResult } from '../../models/return-result';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {

  myAppUrl: string;

  repAppUrl: string;
  controllerRep: string = "campaignaudit/";

  createUrl: string;
  controllerCreate: string = "createupdatecampaign/";


  constructor(private http: HttpClient) {
    this.myAppUrl = (environment.appUrl + "campaign/"),
      this.repAppUrl = (environment.appUrl + this.controllerRep),
      this.createUrl = (environment.appUrl + this.controllerCreate)
  }


  public getDashboardPlayList(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/GetPlayDetailsForOperatorByCampaign"
    return this.http.put<ReturnResult>(this.repAppUrl + ctrAction, model);
  }


  public getCampaignDashboardAudit(id: number) {
    let ctrAction = "v1/GetDashboardSummaryByCampaign/";
    return this.http.get<ReturnResult>(this.repAppUrl + ctrAction + id, { observe: "response" });
  }


  public getCampaignDashboardAuditByOperator(operatorId: string) {
    if (operatorId != null && operatorId != "0") {
      let ctrAction = "v1/GetDashboardSummaryByOperator/";
      let id = parseInt(operatorId);
      return this.http.get<ReturnResult>(this.repAppUrl + ctrAction + id, { observe: "response" });
    }
    else
      return { result: 0, error: 'Not valid', body: {}, recordcount: 0 }
  }

  public getCampaignDashboardByAdvertiser(id: number) {
    let ctrAction = "v1/GetDashboardSummaryAdvertiser/";
    return this.http.get<ReturnResult>(this.repAppUrl + ctrAction + id, { observe: "response" });
  }


  public getCampaignDashboardAuditBySalesManager() {
    let ctrAction = "v1/GetCampDashboardSummarySalesManager";
    return this.http.get<ReturnResult>(this.repAppUrl + ctrAction, { observe: "response" });
  }


  public getCampaignDashboardAuditBySalesExec(id: number) {
    if (id != null && id != 0) {
      let ctrAction = "v1/GetCampDashboardSummarySalesExec/";
      return this.http.get<ReturnResult>(this.repAppUrl + ctrAction + id, { observe: "response" });
    }
    else
      return { result: 0, error: 'Not valid', body: {}, recordcount: 0 }

  }

  public updateCampaignCategory(model: any) {
    let ctrAction = "v1/UpdateCampaignCategory";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public addCampaignCategory(model: any) {
    let ctrAction = "v1/AddCampaignCategory";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public getCampaignCategoryDetails(Id: string) {
    let id = parseInt(Id);
    let ctrAction = "v1/GetCampaignCategoryDetails/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getCampaignList(adUserId: string) {
    // console.log("entered getCampaignList: ", adUserId);
    let id = 0;
    if (adUserId != null)
      id = parseInt(adUserId);
    let ctrAction = "v1/GetCampaignDataTable/";
    // console.log("Url submitted for getCampaignList: ", this.myAppUrl + ctrAction + id);
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getCampaignListForExec(id: number) {
    let ctrAction = "v1/GetCampaignDataTableForSalesExec/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public getCampaignListForAdvertiser(id: number) {
    let ctrAction = "v1/GetCampaignDataTableForAdvertiser/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getCampaignListSingle(campId: string) {
    console.log("entered getCampaignListSingle: ", campId);
    if (campId == null)
      campId = "0";
    let ctrAction = "v1/GetCampaignDataTableByCampId/";
    let id = parseInt(campId);

    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getCampaignCreditList(Id: string) {
    let id = parseInt(Id);
    let ctrAction = "v1/GetCampaignCreditDataTable/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public updateCampaignStatus(model: any) {
    let ctrAction = "v1/UpdateCampaignStatus";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }
}
