import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-campaign-audit-detail',
  templateUrl: './campaign-audit-detail.component.html',
  styleUrls: ['./campaign-audit-detail.component.css']
})
export class CampaignAuditDetailComponent {
  campId: string;
  dashboardType: string = "campaign";

  constructor(private route: ActivatedRoute,
    private router: Router) {

    this.campId = this.route.snapshot.paramMap.get('id');
  }


}
