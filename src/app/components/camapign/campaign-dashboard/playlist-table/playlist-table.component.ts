import { Component, OnDestroy, ViewChild, AfterViewInit, Input } from '@angular/core';
import { Subject, merge, of as observableOf } from 'rxjs';
import { DashboardPlayListModel } from '../../models/dashboard-playlist-model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { CampaignService } from 'src/app/components/camapign/campaign.service';
import { DatePipe } from '@angular/common';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { MatDialog } from '@angular/material/dialog';
import { ScriptDialogueComponent } from '../../../adverts/advert-table/script-dialogue/script-dialogue.component';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { PageSortSearchModel } from 'src/app/models/PageSortSearchModel';
import { tap } from 'rxjs/operators';
import * as XLSX from 'xlsx';


@Component({
  selector: 'app-playlist-table',
  templateUrl: './playlist-table.component.html',
  styleUrls: ['./playlist-table.component.css']
})

export class PlaylistTableComponent implements AfterViewInit, OnDestroy {

  pagename: string = "DashboardPlayList";
  permData: any;
  retResult: ReturnResult = null;
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];

  userData: any;
  miscName: string = "Campaign";
  numberName: string = "Play Cost";
  numberName2: string = "Play Length";
  numberName3: string = "Total cost";
  responseName: string = "Play Date"
  @Input() campId: string = "0";

  destroy$: Subject<DashboardPlayListModel> = new Subject();
  // dataSource: MatTableDataSource<CampaignAdminResult>;
  modelSource: DashboardPlayListModel[];
  exportModelSource: DashboardPlayListModel[];
  isLoading: boolean = true;
  resultsLength: number;
  isLoadingResults: boolean = false;


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  filterValues: string;

  tableSearchModel: TableSearchModel;

  constructor(private campaignService: CampaignService,
    private datePipe: DatePipe, private popSearch: PopSearchDdService,
    private accessService: AccessServicesService, public dialog: MatDialog) {

    this.userData = this.accessService.getUser();
    this.PermissionAssignment();

  }

  filtered(filter: string) {
    // console.log("What is in filters: ",filter);
    this.filterValues = filter;
    this.paginator.pageIndex = 0;
    // console.log("What is in filters: ",this.filterValues);
    this.loadData();
  }



  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    // console.log("In Permission assignment what is value of permPageData: ", permPageData);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }


  ngAfterViewInit() {

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.loadData();


    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadData())
      )
      .subscribe();
  }

  exportToExcel() {
    this.isLoading = true;
    let resLength = this.resultsLength;
    if (this.resultsLength > 1000000)
      resLength = 1000000;
    let paging = new PageSortSearchModel();
    paging.elementId = parseInt(this.campId);
    paging.page = 0;
    paging.pageSize = resLength;
    paging.direction = this.sort.direction;
    paging.sort = this.sort.active;
    paging.search = JSON.stringify(this.filterValues);
    // console.log("paging model : ", paging);
    this.campaignService.getDashboardPlayList(paging)
      .subscribe(
        data => {

          this.retResult = data
          if (this.retResult.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.exportModelSource = <DashboardPlayListModel[]>this.retResult.body;
            console.log("returned Export Model: ", this.exportModelSource);
            const workSheet = XLSX.utils.json_to_sheet(this.exportModelSource);
            this.isLoading = false;
            const workBook: XLSX.WorkBook = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(workBook, workSheet, 'Sheet1');
            XLSX.writeFile(workBook, 'calllistdata.xlsx');
          }
        });
  }


  loadData() {
    let paging = new PageSortSearchModel();
    paging.elementId = parseInt(this.campId);
    paging.page = this.paginator.pageIndex;
    paging.pageSize = this.paginator.pageSize;
    paging.direction = this.sort.direction;
    paging.sort = this.sort.active;
    paging.search = JSON.stringify(this.filterValues);

    // console.log("paging model : ", paging);

    this.campaignService.getDashboardPlayList(paging)
      .subscribe(
        data => {
          this.isLoading = false;
          this.retResult = data
          if (this.retResult.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.modelSource = <DashboardPlayListModel[]>this.retResult.body;
            console.log("returned userModel: ", this.modelSource);
            this.resultsLength = this.retResult.recordcount;
          }
        });

  }

  getscripts(type: string, advert: string, script: string) {
    this.dialog.open(ScriptDialogueComponent, {
      data: {
        type: type,
        advertName: advert,
        script: script
      }
    });
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
