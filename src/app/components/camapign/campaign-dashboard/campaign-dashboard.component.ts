import { HttpResponse } from "@angular/common/http";
import { AfterViewInit, Component, Input } from "@angular/core";
import { ReturnResult } from "src/app/models/return-result";
import { AccessServicesService } from "../../shared/services/access-services.service";
import { CampaignService } from "../campaign.service";
import { CampaignDashboardAuditModel } from "../models/campaign-dashboard-audit-model";


@Component({
  selector: 'app-campaign-dashboard',
  templateUrl: './campaign-dashboard.component.html',
  styleUrls: ['./campaign-dashboard.component.css']
})

export class CampaignDashboardComponent implements AfterViewInit {
  pagename: string = "CampaignDashboard";
  permData: any;
  userData: any;
  @Input() advertiserName: string;
  retResult: ReturnResult = null;
  @Input() advertiserId: number = 0;
  @Input() campaignId: number = 0;
  @Input() dashboardType: string;
  @Input() salesExecName: string;
  @Input() salesId: number = 0;
  @Input() routeType: string;
  modelSource: CampaignDashboardAuditModel;
  isLoading: boolean = true;
  playData: number[] = [];
  budgetData: number[] = [];
  reachData: number[] = [];


  constructor(private campaignService: CampaignService, private accessService: AccessServicesService) {
    this.userData = this.accessService.getUser();
  }


  ngAfterViewInit() {
    let campaignData: any = null;
    console.log('this ID ', this.campaignId)
    if (this.campaignId > 0)
      campaignData = this.campaignService.getCampaignDashboardAudit(this.campaignId);
    else if (this.userData.roleId == 6)
      campaignData = this.campaignService.getCampaignDashboardAuditByOperator(this.userData.operatorId);
    else if (this.userData.roleId == 3)
      campaignData = this.campaignService.getCampaignDashboardByAdvertiser(this.advertiserId);
    else if (this.userData.roleId == 8) {
      if (this.salesExecName == null || this.salesExecName.length == 0)
        campaignData = this.campaignService.getCampaignDashboardAuditBySalesManager();
      else
        campaignData = this.campaignService.getCampaignDashboardAuditBySalesExec(this.salesId);
    }
    else if (this.userData.roleId == 9)
      campaignData = this.campaignService.getCampaignDashboardAuditBySalesExec(this.salesId);

    campaignData
      .pipe()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.isLoading = false;
        this.retResult = res.body;
        if (this.retResult.result == 1) {
          this.modelSource = <CampaignDashboardAuditModel>this.retResult.body;
          this.playData.push(this.modelSource.freePlays);
          this.playData.push((this.modelSource.totalPlayed - this.modelSource.freePlays))

          this.budgetData.push(this.modelSource.totalSpend);
          this.budgetData.push((this.modelSource.totalBudget - this.modelSource.totalSpend))

          this.reachData.push(this.modelSource.reach);
          this.reachData.push((this.modelSource.totalReach - this.modelSource.reach))

        }
      });

  }
}
