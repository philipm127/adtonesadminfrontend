import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CamapaignTableComponent } from '../campaign-table/campaign-table.component';

describe('CamapaignTableComponent', () => {
  let component: CamapaignTableComponent;
  let fixture: ComponentFixture<CamapaignTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CamapaignTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamapaignTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
