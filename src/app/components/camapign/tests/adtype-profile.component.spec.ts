import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdtypeProfileComponent } from '../createcampaign/profilesection/adtype-profile/adtype-profile.component';

describe('AdtypeProfileComponent', () => {
  let component: AdtypeProfileComponent;
  let fixture: ComponentFixture<AdtypeProfileComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdtypeProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdtypeProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
