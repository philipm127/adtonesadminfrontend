import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AirtimeProfileComponent } from '../createcampaign/profilesection/airtime-profile/airtime-profile.component';

describe('AirtimeProfileComponent', () => {
  let component: AirtimeProfileComponent;
  let fixture: ComponentFixture<AirtimeProfileComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AirtimeProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirtimeProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
