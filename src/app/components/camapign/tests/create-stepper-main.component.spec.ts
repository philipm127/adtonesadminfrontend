import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CreateStepperMainComponent } from '../createcampaign/create-stepper-main/create-stepper-main.component';

describe('CreateStepperMainComponent', () => {
  let component: CreateStepperMainComponent;
  let fixture: ComponentFixture<CreateStepperMainComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateStepperMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateStepperMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
