import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddCampaigncategoryComponent } from '../add-campaigncategory/add-campaigncategory.component';

describe('AddCampaigncategoryComponent', () => {
  let component: AddCampaigncategoryComponent;
  let fixture: ComponentFixture<AddCampaigncategoryComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCampaigncategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCampaigncategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
