import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdvertTableComponent } from '../advert/advert-table/advert-table.component';

describe('AdvertTableComponent', () => {
  let component: AdvertTableComponent;
  let fixture: ComponentFixture<AdvertTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
