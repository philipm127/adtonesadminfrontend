import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DemographicProfileComponent } from '../createcampaign/profilesection/demographic-profile/demographic-profile.component';

describe('DemographicProfileComponent', () => {
  let component: DemographicProfileComponent;
  let fixture: ComponentFixture<DemographicProfileComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DemographicProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemographicProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
