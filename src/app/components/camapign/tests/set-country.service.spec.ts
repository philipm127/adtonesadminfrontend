import { TestBed } from '@angular/core/testing';

import { SetCountryService } from '../../campaign-create-edit/set-country.service';

describe('SetCountryService', () => {
  let service: SetCountryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SetCountryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
