import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddCampaignCreditComponent } from '../add-campaign-credit/add-campaign-credit.component';

describe('AddCampaignCreditComponent', () => {
  let component: AddCampaignCreditComponent;
  let fixture: ComponentFixture<AddCampaignCreditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCampaignCreditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCampaignCreditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
