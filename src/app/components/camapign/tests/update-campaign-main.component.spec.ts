import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UpdateCampaignMainComponent } from '../updatecampaign/update-campaign-main/update-campaign-main.component';

describe('UpdateCampaignMainComponent', () => {
  let component: UpdateCampaignMainComponent;
  let fixture: ComponentFixture<UpdateCampaignMainComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCampaignMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCampaignMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
