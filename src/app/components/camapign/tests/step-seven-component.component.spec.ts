import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StepSevenComponentComponent } from '../createcampaign/step-seven-component/step-seven-component.component';

describe('StepSevenComponentComponent', () => {
  let component: StepSevenComponentComponent;
  let fixture: ComponentFixture<StepSevenComponentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ StepSevenComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepSevenComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
