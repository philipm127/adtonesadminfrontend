import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ScriptDialogueComponent } from '../advert/advert-table/script-dialogue/script-dialogue.component';

describe('ScriptDialogueComponent', () => {
  let component: ScriptDialogueComponent;
  let fixture: ComponentFixture<ScriptDialogueComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ScriptDialogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScriptDialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
