import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CampaignAuditDetailComponent } from '../campaign-dashboard/campaign-audit-detail/campaign-audit-detail.component';

describe('CampaignAuditDetailComponent', () => {
  let component: CampaignAuditDetailComponent;
  let fixture: ComponentFixture<CampaignAuditDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignAuditDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignAuditDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
