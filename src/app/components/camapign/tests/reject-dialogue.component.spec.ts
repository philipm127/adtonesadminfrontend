import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RejectDialogueComponent } from '../advert/advert-table/reject-dialogue/reject-dialogue.component';

describe('RejectDialogueComponent', () => {
  let component: RejectDialogueComponent;
  let fixture: ComponentFixture<RejectDialogueComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectDialogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectDialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
