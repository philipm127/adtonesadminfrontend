import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdvertDetailsComponent } from '../advert/advert-details/advert-details.component';

describe('AdvertDetailsComponent', () => {
  let component: AdvertDetailsComponent;
  let fixture: ComponentFixture<AdvertDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
