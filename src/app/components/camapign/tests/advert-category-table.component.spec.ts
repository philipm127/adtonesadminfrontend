import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdvertCategoryTableComponent } from '../advert/advert-category-table/advert-category-table.component';

describe('AdvertCategoryTableComponent', () => {
  let component: AdvertCategoryTableComponent;
  let fixture: ComponentFixture<AdvertCategoryTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertCategoryTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertCategoryTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
