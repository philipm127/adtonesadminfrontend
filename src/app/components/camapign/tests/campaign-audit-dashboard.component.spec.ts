import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CampaignAuditDashboardComponent } from '../campaign-dashboard/campaign-audit-dashboard/campaign-audit-dashboard.component';

describe('CampaignAuditDashboardComponent', () => {
  let component: CampaignAuditDashboardComponent;
  let fixture: ComponentFixture<CampaignAuditDashboardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignAuditDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignAuditDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
