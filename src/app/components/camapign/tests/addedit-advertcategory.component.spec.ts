import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddeditAdvertcategoryComponent } from '../advert/addedit-advertcategory/addedit-advertcategory.component';

describe('AddeditAdvertcategoryComponent', () => {
  let component: AddeditAdvertcategoryComponent;
  let fixture: ComponentFixture<AddeditAdvertcategoryComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditAdvertcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditAdvertcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
