import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CampaignTablesComponent } from '../campaign-tables/campaign-tables.component';

describe('CampaignTablesComponent', () => {
  let component: CampaignTablesComponent;
  let fixture: ComponentFixture<CampaignTablesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignTablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
