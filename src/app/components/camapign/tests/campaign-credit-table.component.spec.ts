import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CampaignCreditTableComponent } from '../campaign-credit-table/campaign-credit-table.component';

describe('CampaignCreditTableComponent', () => {
  let component: CampaignCreditTableComponent;
  let fixture: ComponentFixture<CampaignCreditTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignCreditTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignCreditTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
