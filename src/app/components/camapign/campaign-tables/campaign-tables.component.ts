import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-campaign-tables',
  templateUrl: './campaign-tables.component.html',
  styleUrls: ['./campaign-tables.component.css']
})
export class CampaignTablesComponent {
  @Input() displayedColumns: string[] = [];
  @Input() dataSource: MatTableDataSource<any>;

  @Output() sorted: EventEmitter<any> = new EventEmitter();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  customSort(event) {
    const filter = { ...event } as any;
    console.log("Is anything in the sort event? ", filter);
    this.sorted.emit(filter);
  }

}
