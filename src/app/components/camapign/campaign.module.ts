import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignRoutingModule } from './campaign-routing.module';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatTableExporterModule } from 'mat-table-exporter';
import { CampaignTableComponent } from './campaign-table/campaign-table.component';
import { SharedModule } from '../shared/shared.module';
import { CampaignTablesComponent } from './campaign-tables/campaign-tables.component';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { PlaylistTableComponent } from './campaign-dashboard/playlist-table/playlist-table.component';
import { CampaignAuditDetailComponent } from './campaign-dashboard/campaign-audit-detail/campaign-audit-detail.component';
import { CampaignAuditDashboardComponent } from './campaign-dashboard/campaign-audit-dashboard/campaign-audit-dashboard.component';
import { AddCampaigncategoryComponent } from './add-campaigncategory/add-campaigncategory.component';
import { CampaignOverviewComponent } from './campaign-overview/campaign-overview.component';
import { CampaignDashboardComponent } from './campaign-dashboard/campaign-dashboard.component';



@NgModule({
  declarations: [
    CampaignTableComponent,
    CampaignTablesComponent,
    PlaylistTableComponent,
    CampaignAuditDetailComponent,
    CampaignAuditDashboardComponent,
    AddCampaigncategoryComponent,
    CampaignOverviewComponent,
    CampaignDashboardComponent
  ],
  imports: [
    CommonModule,
    CampaignRoutingModule,
    SharedModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule
  ],
  exports: [
    CampaignTableComponent
  ],
  providers: [
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } }
  ]
})
export class CampaignModule { }
