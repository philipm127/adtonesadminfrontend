import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { CampaignListService } from '../campaign-list.service';
import { CampaignService } from '../campaign.service';

@Component({
  selector: 'app-campaign-overview',
  templateUrl: './campaign-overview.component.html',
  styleUrls: ['./campaign-overview.component.css']
})
export class CampaignOverviewComponent {
  routeType: string = '';
  // For advertiser
  advertiserId: number = 0;
  advertiserName: string;
  // For advertiser
  // For Operator
  Id: number = 0;
  campId: number = 0;
  dashboardType: string = "campaign";
  // For Operator
  // For Sales
  salesId: number = 0;
  salesExecName: string;
  // For Sales
  userData: any;

  constructor(private route: ActivatedRoute, private accessService: AccessServicesService,
    private campaignService: CampaignService,
    private campaignListService: CampaignListService) {

    this.userData = this.accessService.getUser();
    this.routeType = this.route.snapshot.paramMap.get('type');
    let routeUrl = this.route.pathFromRoot.map(r => r.snapshot.url).filter(f => !!f[0]).map(([f]) => f.path).join('/');
    this.Id = this.route.snapshot.paramMap.get('id') == null ? 0 : parseInt(this.route.snapshot.paramMap.get('id'));
    // Admin
    if (this.routeType == 'playlist')
      this.campId = this.Id;
    else if (this.routeType == 'sales' || this.userData.roleId == 9) {
      this.routeType == 'sales';
      this.salesId = this.Id == 0 ? this.userData.userId : this.Id;
      this.salesExecName = this.route.snapshot.paramMap.get('name');
    }
    else if (this.routeType == 'advertiser' || routeUrl == 'advertisercampaign') {
      // For advertiser
      this.routeType == 'advertiser';
      this.advertiserId = this.Id == 0 ? this.userData.userId : this.Id;
      this.advertiserName = this.route.snapshot.paramMap.get('name');
      // For advertiser
    }
  }

}
