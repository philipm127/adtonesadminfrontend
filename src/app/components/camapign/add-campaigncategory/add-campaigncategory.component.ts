import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { CampaignService } from '../campaign.service';
import { CampaignCategoryTableModel } from '../../adverts/models/advert-category-table';

@Component({
  selector: 'app-add-campaigncategory',
  templateUrl: './add-campaigncategory.component.html',
  styleUrls: ['./add-campaigncategory.component.css']
})
export class AddCampaigncategoryComponent implements OnInit {

  pagename: string = "AddCampaignCategory";
  catId: string = "0";
  countrySelect: BasicSelectModel[];

  form = new FormGroup({
    campaignCategoryId: new FormControl(0),
    categoryName: new FormControl("", Validators.required),
    description: new FormControl(""),
    active: new FormControl(true),
    countryId: new FormControl("", Validators.required)
  });


  editMode: boolean = false;
  userModel: any;

  destroy$: Subject<CampaignCategoryTableModel> = new Subject();

  userData: any;

  successMessage: string;
  failMessage: string;


  constructor(private formBuilder: FormBuilder, private campService: CampaignService,
    private accessService: AccessServicesService, private listService: SelectServicesService,
    private route: ActivatedRoute, private router: Router) {

    this.catId = this.route.snapshot.paramMap.get('id');
    if (parseInt(this.catId) > 0) {
      this.pagename = "EditCampaignCategory";
      this.editMode = true;
    }

    this.userData = this.accessService.getUser();
    this.getCountry();
  }


  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();

    if (this.editMode) {
      this.campService.getCampaignCategoryDetails(this.catId)
        .subscribe((res: HttpResponse<ReturnResult>) => {
          let retModel = res.body;
          if (retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <CampaignCategoryTableModel>retModel.body;
            // console.log("returned userModel: ", this.userModel);
            this.initForm();
          }
        });
    }
  }

  getCountry() {
    this.listService.getCountryList()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        console.log("returned result from get country list: ", retResult);
        if (retResult.result == 1) {

          this.countrySelect = <BasicSelectModel[]>retResult.body;
        }
        else
          this.countrySelect = <BasicSelectModel[]>retResult.body;
      });
  }


  private initForm() {
    if (this.editMode) {
      this.form.patchValue({
        campaignCategoryId: this.userModel.campaignCategoryId,
        categoryName: this.userModel.categoryName
      });

    }
  }

  onSubmit() {
    this.successMessage = "";
    this.failMessage = "";
    // console.log("What is this form data: ", this.form.value);
    let serviceResult = null;
    if (this.editMode)
      serviceResult = this.campService.updateCampaignCategory(this.form.value);
    else
      serviceResult = this.campService.addCampaignCategory(this.form.value);
    serviceResult
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          this.successMessage = "The Details Were Updated Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the details";
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
