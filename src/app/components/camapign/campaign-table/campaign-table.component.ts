import { Component, OnDestroy, ViewChild, AfterViewInit, Input } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';

import { BasicSelectModel } from 'src/app/models/basic-select';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { CampaignAdminResult } from '../models/campaign-table-model';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { CurrencyEnum } from 'src/app/models/currencyEnum';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CampaignListService } from '../campaign-list.service';


@Component({
  selector: 'app-campaign-table',
  templateUrl: './campaign-table.component.html',
  styleUrls: ['./campaign-table.component.css']
})

export class CampaignTableComponent implements AfterViewInit {
  pagename: string = "CampaignList";

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @Input() advertiserId: number = 0;
  @Input() salesId: number = 0;
  @Input() routeType: string;
  advertiserName: string;
  permData: any;
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];
  userData: any;

  adUserId: string = "0";
  campId: number = 0;
  campaignLink: boolean = false;
  advertLink: boolean;

  // Dashboard for Operator
  dashboardType: string = "operator";
  operatorId: string = "0";

  dataSource: MatTableDataSource<CampaignAdminResult>;
  modelSource: CampaignAdminResult[];
  isLoading: boolean = true;

  statusSelect: BasicSelectModel[] = [];
  clientSelect: BasicSelectModel[] = [];
  operatorSelect: BasicSelectModel[] = [];
  countrySelect: BasicSelectModel[] = [];

  filterValues = {};


  sharedModel: TableSearchModel;
  currencySymbol = CurrencyEnum;

  minDate = new Date(2016, 1, 1);
  maxDate = new Date();


  formControl: FormGroup;


  constructor(private campaignService: CampaignListService, private datePipe: DatePipe,
    private popSearch: PopSearchDdService, private route: ActivatedRoute,
    private router: Router, private accessService: AccessServicesService,
    private formBuilder: FormBuilder) {
    this.formControl = formBuilder.group({
      name: '',
      email: '',
      status: '',
      operator: '',
      country: '',
      typeName: '',
      dateFrom: '',
      dateTo: '',
      numberFrom: '',
      numberTo: '',
      numberFrom2: '',
      numberTo2: '',
      numberFrom3: '',
      numberTo3: '',
      client: '',
      fullName: '',
      salesExec: '',
      payment: '',
      responseFrom: '',
      responseTo: ''
    });

    this.userData = this.accessService.getUser();
    this.operatorId = this.userData.operatorId;
    this.PermissionAssignment();

    this.formControl.valueChanges.subscribe(value => {
      Object.keys(value).forEach(key => value[key] === '' ? delete value[key] : key);
      const filter = { ...value } as string;
      this.filtered(filter);
    });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  ngAfterViewInit() {
    this.getData()
      .pipe()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.isLoading = false;
        let retResult = res.body;
        console.log('camp data res ', retResult.body)
        if (retResult.result == 1) {
          this.modelSource = <CampaignAdminResult[]>retResult.body;
          this.dataSource = new MatTableDataSource<CampaignAdminResult>(this.modelSource);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
        this.setupSearchSelect();

        this.dataSource.filterPredicate = ((data, filter) => {
          const a = !filter.fullName || data.userName.trim().toLowerCase().includes(filter.fullName.trim().toLowerCase());
          const b = !filter.client || data.clientName.trim().toLowerCase().includes(filter.client.trim().toLowerCase());
          const c = !filter.status || data.rStatus.trim().toLowerCase().includes(filter.status.trim().toLowerCase());
          const d = !filter.dateFrom || data.createdDate >= this.datePipe.transform(filter.dateFrom, 'yyyy-MM-dd');
          const e = !filter.dateTo || data.createdDate <= this.datePipe.transform(filter.dateTo, 'yyyy-MM-dd');
          const f = !filter.name || data.campaignName.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          const g = !filter.operator || data.operatorName.trim().toLowerCase().includes(filter.operator.trim().toLowerCase());
          const h = !filter.country || data.countryName.trim().toLowerCase().includes(filter.country.trim().toLowerCase());
          const i = !filter.numberFrom || data.totalBudget >= parseInt(filter.numberFrom);
          const j = !filter.numberTo || data.totalBudget <= parseInt(filter.numberTo);
          const k = !filter.numberFrom2 || data.totalSpend >= parseInt(filter.numberFrom2);
          const l = !filter.numberTo2 || data.totalSpend <= parseInt(filter.numberTo2);
          return a && b && c && d && e && f && g && h && i && j && k && l;
        }) as (CampaignElement, string) => boolean;

      });
  }

  getData() {
    let campaignData = null;
    let iD = 0;
    if (this.campId > 0)
      campaignData = this.campaignService.getCampaignListSingle(this.campId);
    else if (this.routeType == 'advertiser')
      //   // For advertiser
      campaignData = this.campaignService.getCampaignListForAdvertiser(this.advertiserId);
    else if (this.userData.roleId == 6 || this.userData.roleId == 1)
      campaignData = this.campaignService.getCampaignList(iD);
    else if (this.userData.roleId == 3)
      campaignData = this.campaignService.getCampaignList(this.advertiserId);
    else if (this.userData.roleId == 8 || this.userData.roleId == 9)
      campaignData = this.campaignService.getCampaignList(this.salesId);


    // else if (this.userData.roleId == 6 || this.userData.roleId == 1)
    //   campaignData = this.campaignService.getCampaignList('0');
    // else if (this.userData.roleId == 3)
    //   campaignData = this.campaignService.getCampaignListForAdvertiser(this.advertiserId);
    // else if (this.userData.roleId == 8 || this.userData.roleId == 9)
    //   campaignData = this.campaignService.getCampaignListForExec(this.salesId);
    // else if (this.routeType == 'advertiser')
    //   // For advertiser
    //   campaignData = this.campaignService.getCampaignListForAdvertiser(this.advertiserId);
    // else if (!this.route.snapshot.paramMap.get('name') == null) {

    // }

    return campaignData;
  }

  setupSearchSelect() {
    this.statusSelect = this.popSearch.popStatus(this.modelSource);
    this.clientSelect = this.popSearch.popClient(this.modelSource);
    this.operatorSelect = this.popSearch.popOperator(this.modelSource);
    this.countrySelect = this.popSearch.popCountry(this.modelSource);
  }


  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.sharedModel = this.accessService.FilterPermissionAssignment(permPageData);
    let campFind = permPageData["elements"].filter(j => j.name == "campaignName" && j.type == "element");
    this.campaignLink = campFind[0].enabled;
    let adFind = permPageData["elements"].filter(j => j.name == "advertName" && j.type == "element");
    this.advertLink = adFind[0].enabled;
  }


  public updateStatus(Id: number, event: any) {
    let status: number = event.target.value;
    for (let i in this.modelSource) {
      if (this.modelSource[i].campaignProfileId == Id) {
        this.modelSource[i].status = status;
        break;
      }
    }

    let newModel = {
      id: Id,
      status: status
    }

    this.campaignService.updateCampaignStatus(newModel)
      .subscribe(resp => {
        let retResultUpdate = resp

        if (retResultUpdate.result == 1) {
          console.log("returned result retResultUpdate: ", retResultUpdate.result);
        }
      });
    console.log("What is status after update: ", this.modelSource);
  }

  updateCampaign(userId: number, campid: number) {
    this.router.navigate(['updatecampaign/' + userId + '/' + campid]);
  }

  fundCampaign(campId: number, userId: number) {
    this.router.navigate(['buycredit/' + campId + '/' + userId]);
  }
}
