import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CampaignTableComponent } from './campaign-table/campaign-table.component';
import { CampaignAuditDetailComponent } from './campaign-dashboard/campaign-audit-detail/campaign-audit-detail.component';
import { AuthguardService } from 'src/app/services/auth/authguard.service';
import { AddCampaigncategoryComponent } from './add-campaigncategory/add-campaigncategory.component';
import { CampaignOverviewComponent } from './campaign-overview/campaign-overview.component';


const campignRoutes: Routes = [
  { path: 'campaigntable', component: CampaignOverviewComponent, canActivate: [AuthguardService] },
  { path: 'campaigntable/:id', component: CampaignOverviewComponent },
  { path: 'campaigntablesingle/:campid', component: CampaignOverviewComponent },
  { path: 'advertisercampaign', component: CampaignOverviewComponent },
  { path: 'campaigntable/:id/:type', component: CampaignOverviewComponent },
  { path: 'salescampaign', component: CampaignOverviewComponent },
  { path: 'salescampaign/:id/:name/:type', component: CampaignOverviewComponent },
  { path: 'campaigncategory/:id', component: AddCampaigncategoryComponent },
  { path: 'campaignauditdetail/:id', component: CampaignAuditDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(campignRoutes)],
  exports: [RouterModule]
})
export class CampaignRoutingModule { }
