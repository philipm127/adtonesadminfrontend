import { HttpResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { SelectServicesService } from 'src/app/components/shared/services/select-services.service';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';

@Component({
  selector: 'app-step-four-component',
  templateUrl: './step-four-component.html',
  styleUrls: ['./step-four-component.css']
})
export class StepFourComponent implements OnInit {
  @Input() editCampaign: boolean;
  @Input() campaignData: any;
  @Input() minBid: number;
  @Input() budgetSuccess: boolean = false;
  @Input() budgetFail: boolean = false;
  @Input() hasCampaignError: boolean = false;
  @Input() campaignErrorMessage: string;
  accountUpdate: boolean = false;
  frmStepFour: FormGroup;
  frmFourSubmitting = false;
  currencySelect$: Observable<BasicSelectModel[]>;

  @Input() advertiserId: string;
  @Output() fourthStepData = new EventEmitter<any>();
  @Output() changeErrorStatus = new EventEmitter<boolean>();


  destroy$: Subject<BasicSelectModel> = new Subject();

  constructor(private formBuilder: FormBuilder, private listService: SelectServicesService) {

  }

  ngOnInit() {
    this.frmStepFour = this.formBuilder.group({
      maxMonthBudget: new FormControl("", [Validators.required, Validators.pattern(/^\d*\.?\d*$/)]),
      maxWeeklyBudget: new FormControl("", [Validators.required, Validators.pattern(/^\d*\.?\d*$/)]),
      maxDailyBudget: new FormControl("", [Validators.required, Validators.pattern(/^\d*\.?\d*$/)]),
      maxHourlyBudget: new FormControl("", [Validators.required, Validators.pattern(/^\d*\.?\d*$/)]),
      maxBid: new FormControl("", [Validators.required, Validators.pattern(/^\d*\.?\d*$/), Validators.min(this.minBid)]),
      currencyId: new FormControl("", Validators.required)
    });
    this.getCurrency();
    // this.frmStepFour.controls['maxBid'].setValidators([Validators.required, Validators.pattern(/^[1-9]\d*$/), Validators.min(this.minBid)]);
    if (this.editCampaign)
      this.initForm();
    // else
    //   this.frmStepFour.controls['maxBid'].setValidators([Validators.required, Validators.pattern(/^[1-9]\d*$/), Validators.min(this.minBid)]);
    if (this.minBid != undefined) {
      if (typeof this.minBid === "number") {
        console.log("Type of minNumber is number ", this.minBid);
      }
      else {
        console.log("Type of minNumber is NOT A number ", this.minBid);
      }
    }
  }

  initForm() {
    this.frmStepFour.patchValue({
      maxMonthBudget: this.campaignData.maxMonthBudget,
      maxWeeklyBudget: this.campaignData.maxWeeklyBudget,
      maxDailyBudget: this.campaignData.maxDailyBudget,
      maxHourlyBudget: this.campaignData.maxHourlyBudget,
      maxBid: this.campaignData.maxBid,
      campaignProfileId: this.campaignData.campaignProfileId
    });

    this.frmStepFour.get('currencyId').setValue(this.campaignData.currencyId.toString());
  }

  getCurrency() {
    this.currencySelect$ = this.listService.getCurrencyList()
    .pipe(
      takeUntil(this.destroy$),
      map((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        if (retResult.result == 1) {
          return <BasicSelectModel[]>retResult.body;
        } else {
          return [];
        }
      })
    );
}


  form4() {
    this.accountUpdate = true;
    this.fourthStepData.emit(this.frmStepFour.getRawValue());
  }

  clearError() {
    this.changeErrorStatus.emit(false);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }



}
