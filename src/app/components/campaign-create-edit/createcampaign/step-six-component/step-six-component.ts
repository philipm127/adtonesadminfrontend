import { HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SelectServicesService } from 'src/app/components/shared/services/select-services.service';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';
import { CampaignCreateEditService } from '../../campaign-create-edit.service';

@Component({
  selector: 'app-step-six-component',
  templateUrl: './step-six-component.html',
  styleUrls: ['./step-six-component.css']
})
export class StepSixComponent {
  @Input() editCampaign: boolean;
  frmStepSix: FormGroup;
  @Input() profileSelectionModel: any;
  @Input() countryId: number;
  @Input() profileSuccess: boolean = false;
  @Input() profileFail: boolean = false;

  profileUpdate: boolean = false;

  @Input() advertiserId: string;
  @Output() sixthStepData = new EventEmitter<any>();


  constructor(private formBuilder: FormBuilder, private listService: SelectServicesService,
    private campaignService: CampaignCreateEditService) {

  }


  form6() {

    this.profileUpdate = true;
    this.profileSelectionModel.campaignProfileGeographicModel.countryId = this.countryId;
    this.profileSelectionModel.campaignProfileSkizaFormModel.countryId = this.countryId;
    this.sixthStepData.emit(this.profileSelectionModel);

  }

}
