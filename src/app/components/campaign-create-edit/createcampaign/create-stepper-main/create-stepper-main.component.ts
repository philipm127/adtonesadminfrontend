import { HttpResponse } from '@angular/common/http';
import { ChangeDetectorRef, EventEmitter } from '@angular/core';
import { AfterViewChecked } from '@angular/core';
import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';
import { data } from 'jquery';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CountryService } from 'src/app/components/country/country.service';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { ReturnResult } from 'src/app/models/return-result';
import { CampaignCreateEditService } from '../../campaign-create-edit.service';
import { CampaignInfoModel, NewClientFormModel } from '../../models/campaigncreateupdate-models';
import { StepFiveComponent } from '../step-five-component/step-five-component';
import { StepFourComponent } from '../step-four-component/step-four-component';
import { StepOneComponent } from '../step-one-component/step-one-component';
import { StepSixComponent } from '../step-six-component/step-six-component';
import { StepThreeComponent } from '../step-three-component/step-three-component';
import { StepTwoComponent } from '../step-two-component/step-two-component';
import { SetCountryService } from '../../set-country.service';
import { AdvertService } from '../../../adverts/advert.service';

// step-one-component
@Component({
  selector: 'app-create-stepper-main',
  templateUrl: './create-stepper-main.component.html',
  styleUrls: ['./create-stepper-main.component.css']
})
export class CreateStepperMainComponent implements OnInit, AfterViewChecked {
  pagename: string = "CreateCampaign";
  editCampaign: boolean = false;
  userData: any;
  countryId: number;
  operatorId: number
  advertiserId: number;
  frmOneData: any;
  frmTwoData: any;
  frmThreeData: any;
  frmFourData: any;
  frmFiveData: any;
  frmSixData: any;
  adFile: File;
  scriptFile: File;
  campaignId: number;
  isClient: boolean = false;
  minBid: number;
  campaignName: string;
  formSubmitted: boolean = false;

  hasCampaignError: boolean = false;
  campaignErrorMessage: string;
  destroy$: Subject<ReturnResult> = new Subject();

  @ViewChild(StepOneComponent) stepOneComponent: StepOneComponent;
  @ViewChild(StepTwoComponent) stepTwoComponent: StepTwoComponent;
  @ViewChild(StepThreeComponent) stepThreeComponent: StepThreeComponent;
  @ViewChild(StepFourComponent) stepFourComponent: StepFourComponent;
  @ViewChild(StepFiveComponent) stepFiveComponent: StepFiveComponent;
  @ViewChild(StepSixComponent) stepSixComponent: StepSixComponent;

  // permisiions
  stepOneVisible: boolean = false;
  stepTwoVisible: boolean = false;
  stepThreeVisible: boolean = false;
  stepFourVisible: boolean = false;
  stepFiveVisible: boolean = false;
  stepSixVisible: boolean = false;
  brandVisible: boolean = false;
  campaignCategoryVisible: boolean = false;

  profileSelectionModel: any;

  get frmStepOne() {
    return this.stepOneComponent ? this.stepOneComponent.frmStepOne : null;
  }

  get frmStepTwo() {
    return this.stepTwoComponent ? this.stepTwoComponent.frmStepTwo : null;
  }

  get frmStepThree() {
    return this.stepThreeComponent ? this.stepThreeComponent.frmStepThree : null;
  }


  get frmStepFour() {
    return this.stepFourComponent ? this.stepFourComponent.frmStepFour : null;
  }

  get frmStepFive() {
    return this.stepFiveComponent ? this.stepFiveComponent.frmStepFive : null;
  }

  get frmStepSix() {
    return this.stepSixComponent ? this.stepSixComponent.frmStepSix : null;
  }

  constructor(private setService: SetCountryService, private accessService: AccessServicesService,
    private route: ActivatedRoute, private router: Router, private changeDetect: ChangeDetectorRef,
    private campaignService: CampaignCreateEditService, private countryService: CountryService, private advertService: AdvertService) {
    this.userData = this.accessService.getUser();
    this.advertiserId = parseInt(this.route.snapshot.paramMap.get('id'));

    if (this.advertiserId == null || this.advertiserId == 0 || Number.isNaN(this.advertiserId))
      this.advertiserId = this.userData.userId;
  }

  ngOnInit() {
    this.PermissionAssignment();
  }



  getProfileInfo(countryId: number) {
    // console.log("getProfileInfo - Country Id: ", countryId);

    this.campaignService.getProfileTemplate(countryId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        if (retResult.result == 1) {
          this.profileSelectionModel = retResult.body;
          // console.log("his.geographicModel ", this.profileSelectionModel);
        }
      });
  }


  getMinimumBid(countryId: number) {
    // console.log("getProfileInfo - Country Id: ", countryId);

    this.countryService.getMinCampaignBid(countryId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        if (retResult.result == 1) {
          this.minBid = parseFloat(retResult.body.toString());
          // console.log("this.minBid = parseFloat(retResult.body.toString()); ", this.minBid);
          this.frmStepFour.controls['maxBid'].setValidators([Validators.required, Validators.pattern(/^\d*\.?\d*$/), Validators.min(this.minBid)]);
          // this.getMyData();
        }
      });
  }

  setCountry(countryId: number) {
    this.countryId = countryId;
  }

  isForClient(data: any) {
    this.isClient = data;
  }

  firstStepData(data: any) {

    this.frmOneData = data;
    this.campaignName = this.frmOneData.campaignName;
    this.countryId = this.frmOneData.countryId;
    this.setService.setData(this.frmOneData.countryId);

    if (this.stepSixVisible)
      this.getProfileInfo(this.frmOneData.countryId);
    if (this.stepFourVisible)
      this.getMinimumBid(this.frmOneData.countryId)
  }

  secondStepData(data: any) {
    this.frmTwoData = data;
  }

  thirdStepData(data: any) {
    this.frmThreeData = data;
    this.operatorId = this.frmThreeData.operatorId;
    if (this.frmThreeData) {
      let model = ({
        advertiserId: this.advertiserId,
        advertName: this.frmThreeData.advertName
      })
      this.advertService.checkIfAdvertNameExists(model)
        .subscribe(
          data => {
            const retResult = data;
            // console.log("Returned body checkIfAdvertNameExists is: ", retResult);
            if (retResult.result == 0) {
              this.campaignErrorMessage = "The Advert Name already Exists please go back and change";
              this.hasCampaignError = true;
            }
          });
    }
  }

  fourthStepData(data: any) {
    this.frmFourData = data;
  }

  fifthStepData(data: any) {
    this.frmFiveData = data;
  }

  sixthStepData(data: any) {
    this.frmSixData = data;
  }

  advertFile(file: File) {
    this.adFile = file;
  }

  smsFile(file: File) {
    this.scriptFile = file;
  }


  changeErrorStatus(data: boolean) {
    this.hasCampaignError = data;
    this.campaignErrorMessage = null;
  }


  frmDataComplete(data: boolean) {
    let subbed = data;
    if (subbed) {
      let model: CampaignInfoModel = new CampaignInfoModel();
      model.userId = this.advertiserId;
      model.campaignName = this.frmOneData.campaignName;
      model.campaignDescription = this.frmOneData.campaignDescription;
      model.countryId = this.frmOneData.countryId;
      model.startDate = this.frmOneData.startdate;
      model.endDate = this.frmOneData.endDate;
      if (this.campaignCategoryVisible)
        model.campaignCategoryId = this.frmOneData.campaignCategoryId;

      // model.clientId = this.frmTwoData.clientId;
      if (this.stepTwoVisible && this.isClient && this.frmTwoData != null) {

        let clData = this.frmTwoData;

        if (clData.id != 0 && clData.id != null && clData != "")
          model.clientId = clData.id;
        else {
          let clModel: NewClientFormModel = new NewClientFormModel();
          clModel.contactPhone = clData.contactPhone;
          clModel.countryId = this.countryId;
          clModel.description = clData.description;
          clModel.email = clData.email;
          clModel.name = clData.name;
          clModel.userId = this.advertiserId;
          model.newClientFormModel = clModel;
          console.log("Submitted for 2 data model.newClientFormModel: ", model.newClientFormModel);
        }
      }

      if (this.frmFourData != null) {

        model.currencyId = this.frmFourData.currencyId;
        model.maxBid = this.frmFourData.maxBid;
        model.maxDailyBudget = this.frmFourData.maxDailyBudget;
        model.maxHourlyBudget = this.frmFourData.maxHourlyBudget;
        model.maxMonthBudget = this.frmFourData.maxMonthBudget;
        model.maxWeeklyBudget = this.frmFourData.maxWeeklyBudget;

      }

      if (this.frmFiveData != null) {
        console.log("if (this.frmFiveData != null) {: ", this.frmFiveData);
        model.emailSubject = this.frmFiveData.emailSubject !== "" ? this.frmFiveData.emailSubject : null;
        model.emailBody = this.frmFiveData.emailContent !== "" ? this.frmFiveData.emailContent : null;
        model.smsOriginator = this.frmFiveData.smsOriginator !== "" ? this.frmFiveData.smsOriginator : null;
        model.smsBody = this.frmFiveData.smsMessage !== "" ? this.frmFiveData.smsMessage : null;
        console.log("Form 5 data is: ", this.frmFiveData);
      }
      this.addcampaign(model);
    }
  }


  addcampaign(model: CampaignInfoModel) {
    model.operatorId = this.operatorId;
    this.campaignService.createCampaign(model)
      .subscribe(resp => {
        const retModel = resp;
        if (retModel.result == 1) {
          console.log("addcampaign(model:CampaignInfoModel) ; ", retModel.body);
          this.campaignId = parseInt(retModel.body.toString());
          // this.frmThreeData.campaignProfileId = this.campaignId;
          if (this.stepSixVisible)
            this.addProfileData();
          else
            this.addAdvert();

        }
        else {
          this.hasCampaignError = true;
          // console.log("addcampaign(model:CampaignInfoModel) Error Message; ", retModel.error);
          this.campaignErrorMessage = "There was a problem inserting the Campaign please consult an Admin";// + retModel.error;
        }
      });
  }

  addAdvert() {
    // let z = this.frmThreeData;
    // console.log("Third step value is: ", z);
    this.frmThreeData.campaignProfileId = this.campaignId;
    this.advertService.createCampaign_Advert(this.frmThreeData)
      .subscribe(resp => {
        const retModel = resp
        if (retModel.result == 1) {
          this.formSubmitted = true;
          this.hasCampaignError = false;
        }
        else {
          this.formSubmitted = true;
          this.hasCampaignError = true;
          // console.log("addAdvert() Error Message; ", retModel.error);
          this.campaignErrorMessage = "There was a problem inserting the Advert please consult an Admin";// + retModel.error;
        }
      });
  }


  addProfileData() {
    let z = this.frmSixData;
    z.id = 0;
    z.campaignProfileId = this.campaignId;
    z.countryId = this.countryId;
    z.operatorId = this.operatorId;
    z.campaignProfileGeographicModel.campaignProfileId = this.campaignId;
    z.campaignProfileAd.campaignProfileId = this.campaignId;
    z.campaignProfileDemographicsmodel.campaignProfileId = this.campaignId;
    z.campaignProfileMobileFormModel.campaignProfileId = this.campaignId;
    z.campaignProfileSkizaFormModel.campaignProfileId = this.campaignId;
    z.campaignProfileTimeSettingModel.campaignProfileId = this.campaignId;

    // console.log("Sixth step value is: ", z);
    this.campaignService.insertAdvertProfileData(z)
      .subscribe(resp => {
        const retModel = resp
        if (retModel.result == 1) {
          this.addAdvert();
        }
        else {
          this.hasCampaignError = true;
          // console.log("addProfileData() Error Message; ", retModel.error);
          this.campaignErrorMessage = "There was a problem inserting the Campaign Profile Data please consult an Admin";
        }
      });
  }




  ngAfterViewChecked(): void {
    this.changeDetect.detectChanges();
  }


  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    // console.log("PermissionAssignment(): ", permPageData);
    this.stepOneVisible = permPageData["elements"].filter(j => j.name == "stepOne")[0].visible;
    this.stepTwoVisible = permPageData["elements"].filter(j => j.name == "stepTwo")[0].visible;
    this.stepThreeVisible = permPageData["elements"].filter(j => j.name == "stepThree")[0].visible;
    this.stepFourVisible = permPageData["elements"].filter(j => j.name == "stepFour")[0].visible;
    this.stepFiveVisible = permPageData["elements"].filter(j => j.name == "stepFive")[0].visible;
    this.stepSixVisible = permPageData["elements"].filter(j => j.name == "stepSix")[0].visible;

    this.brandVisible = permPageData["elements"].filter(j => j.name == "brand")[0].visible;
    this.campaignCategoryVisible = permPageData["elements"].filter(j => j.name == "campaignCategory")[0].visible;
  }


  checkCampaign() {
    return true;
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
