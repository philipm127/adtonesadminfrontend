import { HttpResponse } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, Subscription } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { ConfigSystemModule } from 'src/app/components/config-system/config-system.module';
import { SelectServicesService } from 'src/app/components/shared/services/select-services.service';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';
import { AdvertService } from '../../../adverts/advert.service';
import { SetCountryService } from '../../set-country.service';

@Component({
  selector: 'app-step-three-component',
  templateUrl: './step-three-component.html',
  styleUrls: ['./step-three-component.css']
})

export class StepThreeComponent implements OnInit {
  private sub: Subscription;
  @Input() editCampaign: boolean;
  advertData: any;
  frmStepThree: FormGroup;
  audioFileError: string;
  uploadedAudioURL: string;
  existingAudioURL: string;

  categorySelect$: Observable<BasicSelectModel[]>;
  operatorSelect$: Observable<BasicSelectModel[]>;
  advertUpdate: boolean = false;
  @Input() brandVisible: boolean = false;

  @Input() advertiserId: string;
  @Input() countryId: number;
  @Input() campaignMessage: boolean = false;
  @Input() campaignErrorMessage: string;
  @Output() thirdStepData = new EventEmitter<any>();
  @Output() advertFile = new EventEmitter<File>();
  @Output() smsFile = new EventEmitter<File>();
  @Input() hasCampaignError: boolean = false;
  @Input() campaignId: number;
  @Output() changeErrorStatus = new EventEmitter<boolean>();
  fileName = '';
  mediaURL = '';
  fileSizeKB: number = 0;


  destroy$: Subject<BasicSelectModel> = new Subject();

  constructor(private countryService: SetCountryService, private formBuilder: FormBuilder,
    private listService: SelectServicesService, private advertService: AdvertService,
    private route: ActivatedRoute,
    private router: Router) {
    let seeIfRouted = parseInt(this.route.snapshot.paramMap.get('id'));
    if (seeIfRouted !== null && seeIfRouted > 0) {
      this.advertiserId = this.route.snapshot.paramMap.get('id');
      this.campaignId = parseInt(this.route.snapshot.paramMap.get('camp'));
      this.countryId = parseInt(this.route.snapshot.paramMap.get('country'));
      this.editCampaign = true;
    }
  }

  ngOnInit() {

    if (this.countryId != undefined)
      this.getCategory(this.countryId);
    this.frmStepThree = this.formBuilder.group({
      advertName: new FormControl("", Validators.required),
      clientId: new FormControl(null),
      advertCategoryId: new FormControl("", Validators.required),
      brand: new FormControl(""),
      campaignName: new FormControl(""),
      operatorId: new FormControl("", Validators.required),
      campaignProfileId: new FormControl(""),
      scriptFile: new FormControl(""),
      mediaFile: new FormControl(""),
      audioUpload: [null, [Validators.required, this.audioFileSizeValidator]],
      fileUpload: new FormControl("",),
      mediaFileLocation: new FormControl(""),
      script: new FormControl(""),
      advertiserId: new FormControl(this.advertiserId)
    });

    if (this.editCampaign) {
      this.getAdvertData();
    }
    else {
      this.frmStepThree.get('mediaFile').setValidators([
        Validators.required,
        this.sizeValidator()
      ]);
      this.frmStepThree.get('fileUpload').setValidators([
        Validators.required,
        this.sizeValidator()
      ]);
      this.frmStepThree.get('mediaFile').updateValueAndValidity();
      this.frmStepThree.get('fileUpload').updateValueAndValidity();
    }

  }

  getAdvertData() {
    this.advertService.getAdvertDetailsByCampaignId(this.campaignId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        const retResult = res.body;
        if (retResult.result == 1) {
          this.advertData = retResult.body;
          this.getOperator();
          this.initForm();
          console.log("The Advert DataModel ", this.advertData);
        }
      });
  }

  initForm() {
    this.frmStepThree.patchValue({
      advertName: this.advertData.advertName,
      clientId: this.advertData.clientId,
      advertCategoryId: this.advertData.advertCategoryId,
      brand: this.advertData.brand,
      operatorId: this.advertData.operatorId,
      campaignProfileId: this.advertData.campaignProfileId,
      campaignName:this.advertData.campaignName,
      scriptFile: this.advertData.scriptFile,
      script: this.advertData.script
      //advertiserId: this.campaignData.smsBody
    });
    this.frmStepThree.get('advertCategoryId').setValue(this.advertData.advertCategoryId.toString());
    this.frmStepThree.get('operatorId').setValue(this.advertData.operatorId.toString())
    this.frmStepThree.get('operatorId').disable();
    this.frmStepThree.get('campaignName').disable();
    console.log('oprator id ', this.advertData.operatorId)
    this.existingAudioURL = this.advertData.mediaFileLocation;
    var n = this.mediaURL.lastIndexOf('/');

  }

  onChange() {
    this.getOperator();
  }


  getCategory(coId: number) {
    this.categorySelect$ = this.listService.getAdvertCategoryList(coId)
    .pipe(
      takeUntil(this.destroy$),
      map((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        if (retResult.result == 1) {
          return <BasicSelectModel[]>retResult.body;
        } else {
          return [];
        }
      })
    );
}


  getOperator() {
    this.operatorSelect$ = this.listService.getOperatorList(this.countryId)
    .pipe(
      takeUntil(this.destroy$),
      map((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        if (retResult.result == 1) {
          if(retResult.body[0].length == 1){
          this.frmStepThree.get('operatorId').setValue(retResult.body[0].value);
          this.frmStepThree.get('operatorId').disable;
          }
          return <BasicSelectModel[]>retResult.body;
        } else {
          return [];
        }
      })
    );
  }


  form3() {
    this.thirdStepData.emit(this.frmStepThree.getRawValue())
  }

  updateAdvert() {
    this.advertUpdate = true;
    let bob = this.frmStepThree.getRawValue()
    console.log("updatedAdvert ", bob);
    const formData: FormData = new FormData();
    formData.append("campaignProfileId", bob.campaignProfileId);
    formData.append("advertName", bob.advertName);
    formData.append("advertCategoryId", bob.advertCategoryId);
    // formData.append("clientId", null);
    formData.append("advertiserId", bob.advertiserId);
    formData.append("brand", bob.brand);
    formData.append("operatorId", bob.operatorId);
    formData.append("script", bob.script);
    formData.append("mediaFile", bob.audioUpload);
    formData.append("scriptFile", bob.scriptFile);
    formData.append("advertId", this.advertData.advertId);

    this.advertService.updateAdvert(formData)
      .subscribe(resp => {
        const retModel = resp
        if (retModel.result == 1) {
          // this.formSubmitted = true;
          this.hasCampaignError = false;
        }
        else {
          // this.formSubmitted = true;
          this.hasCampaignError = true;
          // console.log("addAdvert() Error Message; ", retModel.error);
          this.campaignErrorMessage = "There was a problem inserting the Advert please consult an Admin";// + retModel.error;
        }
      });
  }

  @ViewChild("smsfileUpload", { static: false }) smsfileUpload: ElementRef; smsfile: File;
  @ViewChild("fileUpload", { static: false }) fileUpload: ElementRef; file: File;

  onSMSClick() {
    const smsfileUpload = this.smsfileUpload.nativeElement; smsfileUpload.onchange = () => {
      this.frmStepThree.patchValue({
        scriptFile: smsfileUpload.files[0]
      });

    };
    smsfileUpload.click();
  }


  audioFileSizeValidator(control) {
    const maxSize = 1024 * 1024; // 1MB
    if (control.value && control.value.size > maxSize) {
      return { sizeValidator: true };
    }
    return null;
  }

  onAudioFileChange(event) {
    const file = event.target.files[0];
    this.audioFileError = null;
    this.uploadedAudioURL = null;

    if (file) {
      // Validate the file type
      if (file.type !== 'audio/wav') {
        this.audioFileError = 'Invalid file type. Please upload a WAV file.';
        return;
      }

      // Validate the file size
      if (file.size > 1024 * 1024) {
        this.audioFileError = 'The file is too big. Please upload a file with a maximum size of 1MB.';
        return;
      }

      const reader = new FileReader();
      reader.onload = () => {
        this.uploadedAudioURL = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  clearError() {
    this.changeErrorStatus.emit(false);
  }




  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }


}
