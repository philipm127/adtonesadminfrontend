import { HttpResponse } from '@angular/common/http';
import { EventEmitter, Input, Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SelectServicesService } from 'src/app/components/shared/services/select-services.service';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';
import { CampaignCreateEditService } from '../../campaign-create-edit.service';
import { CampaignInfoModel } from '../../models/campaigncreateupdate-models';


@Component({
  selector: 'app-step-one-component',
  templateUrl: './step-one-component.html',
  styleUrls: ['./step-one-component.css'],
  providers: [
    {
      provide: 'ngOnDestroy',
      useFactory: ngOnDestroyFactory,
      deps: []
    },
  ],
})

export class StepOneComponent {
  @Input() editCampaign: boolean;
  @Input() campaignData: any;
  @Input() campSuccess: boolean = false;
  @Input() campFail: boolean = false;
  frmStepOne: FormGroup;
  countrySelect: BasicSelectModel[];
  categorySelect: BasicSelectModel[];
  campaignSelect: BasicSelectModel[];

  frmOneSubmitting = false;
  errorMessage: string = '';

  @Input() advertiserId: string;
  @Input() campaignCategoryVisible: boolean;
  @Output() firstStepData = new EventEmitter<any>();
  @Output() isForClient = new EventEmitter<any>();

  destroy$: Subject<BasicSelectModel> = new Subject();

  constructor(private formBuilder: FormBuilder, private listService: SelectServicesService,
    private campaignService: CampaignCreateEditService) {
    this.getCountry();
    this.frmStepOne = this.formBuilder.group({
      campaignName: new FormControl("", Validators.required),
      campaignDescription: new FormControl(""),
      countryId: new FormControl("", Validators.required),
      userId: new FormControl(this.advertiserId),
      startDate: new FormControl(""),
      endDate: new FormControl(""),
      campaignProfileId: new FormControl(0),
      campaignCategoryId: new FormControl("")
    });
  }

  async form1() {
    this.frmOneSubmitting = true;
    const modelfrmStepOne = this.frmStepOne.getRawValue();

    if (this.frmStepOne) {
      let model: CampaignInfoModel = new CampaignInfoModel();

      model.userId = parseInt(this.advertiserId);
      model.campaignName = modelfrmStepOne.campaignName;
      model.campaignDescription = modelfrmStepOne.campaignDescription;
      model.countryId = modelfrmStepOne.countryId;

      this.campaignService.checkIfCampaignNameExists(model)
        .subscribe(
          data => {
            const retResult = data;
            console.log("Returned body checkIfCampaignNameExists is: ", retResult);

          });


    const retResult = await this.campaignService.checkIfCampaignNameExists(model).toPromise();

  // Check the condition
  if (retResult.result == 0) {
    this.errorMessage = 'The campaign name already exists.';
  } else {
    // Clear the error message and emit the firstStepData event
    this.errorMessage = '';
    this.firstStepData.emit(modelfrmStepOne);
  }
    this.frmOneSubmitting = false;
  }
}


  initForm() {
    // console.log("campaign data for update step one: ", this.campaignData);
    this.frmStepOne.patchValue({
      campaignName: this.campaignData.campaignName,
      campaignDescription: this.campaignData.campaignDescription,
      userId: this.advertiserId,
      startDate: this.campaignData.startDate,
      endDate: this.campaignData.endDate,
      campaignProfileId: this.campaignData.campaignProfileId
    });

    this.frmStepOne.get('countryId').setValue(this.campaignData.countryId.toString());
    this.frmStepOne.get('countryId').disable();
    this.frmStepOne.get('campaignName').disable();

  }


  getCountry() {
    this.listService.getCountryList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        const retResult = res.body;
        if (retResult.result == 1) {
          this.countrySelect = <BasicSelectModel[]>retResult.body;

          if (this.editCampaign && this.campaignData) {
            this.frmStepOne.get('countryId').setValue(this.campaignData.countryId.toString());
            this.initForm();
            this.getCampaignCategory(this.campaignData.countryId);
          }

        }
      });
  }


  getCampaignCategory(countryId: string) {
    if (this.campaignCategoryVisible)
      this.listService.getCampaignCategoryList(parseInt(countryId))
        .pipe(takeUntil(this.destroy$))
        .subscribe((res: HttpResponse<ReturnResult>) => {
          const retResult = res.body;
          if (retResult.result == 1) {
            this.categorySelect = <BasicSelectModel[]>retResult.body;
            if (this.editCampaign && this.campaignCategoryVisible && this.campaignData.campaignCategoryId != null)
              this.frmStepOne.get('campaignCategoryId').setValue(this.campaignData.campaignCategoryId.toString());
          }
        });
  }


  OnChange(event: any) {
    // console.log("Is For Client", event);
    this.isForClient.emit(event);
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}

function ngOnDestroyFactory() {
  return function ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  };
}
