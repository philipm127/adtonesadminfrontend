import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CampaignCreateEditService } from '../../../campaign-create-edit.service';

@Component({
  selector: 'app-adtype-profile',
  templateUrl: './adtype-profile.component.html',
  styleUrls: ['./adtype-profile.component.css']
})
export class AdtypeProfileComponent implements OnInit {
  frmAdType: FormGroup;
  allChecked: boolean = false;

  // @Input() frmStepSix
  @Input() adTypeModel: any;
  constructor(private campaignService: CampaignCreateEditService) { }

  ngOnInit(): void {

  }


  isFewSelected(): boolean {
    return this.adTypeModel.foodQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.sweetSaltySnacksQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.alcoholicDrinksQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.nonAlcoholicDrinksQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.householdproductsQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.toiletriesCosmeticsQuestion.filter(t => t.selected).length > 0 &&

      this.adTypeModel.pharmaceuticalChemistsProductsQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.tobaccoProductsQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.petsPetFoodQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.shoppingRetailClothingQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.diyGardeningQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.electronicsOtherPersonalItemsQuestion.filter(t => t.selected).length > 0 &&

      this.adTypeModel.communicationsInternetQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.financialServicesQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.holidaysTravelQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.sportsLeisureQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.motoringQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.newspapersQuestion.filter(t => t.selected).length > 0 &&

      this.adTypeModel.tvQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.cinemaQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.socialNetworkingQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.shoppingQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.fitnessQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.environmentQuestion.filter(t => t.selected).length > 0 &&

      this.adTypeModel.goingOutQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.religionQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.musicQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.businessOrOpportunitiesQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.gamblingQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.restaurantsQuestion.filter(t => t.selected).length > 0 &&

      this.adTypeModel.insuranceQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.furnitureQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.informationTechnologyQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.energyQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.supermarketsQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.healthcareQuestion.filter(t => t.selected).length > 0 &&

      this.adTypeModel.jobsAndEducationQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.giftsQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.advocacyOrLegalQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.datingAndPersonalQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.realEstateQuestion.filter(t => t.selected).length > 0 &&
      this.adTypeModel.gamesQuestion.filter(t => t.selected).length > 0 &&

      !this.allChecked;
  }

  // Update Master and list checkboxes
  setAll(selected: boolean) {
    this.allChecked = selected;
    if (this.adTypeModel.foodQuestion == null &&
      this.adTypeModel.sweetSaltySnacksQuestion == null &&
      this.adTypeModel.alcoholicDrinksQuestion == null &&
      this.adTypeModel.nonAlcoholicDrinksQuestion == null &&
      this.adTypeModel.householdproductsQuestion == null &&
      this.adTypeModel.toiletriesCosmeticsQuestion == null &&

      this.adTypeModel.pharmaceuticalChemistsProductsQuestion == null &&
      this.adTypeModel.tobaccoProductsQuestion == null &&
      this.adTypeModel.petsPetFoodQuestion == null &&
      this.adTypeModel.shoppingRetailClothingQuestion == null &&
      this.adTypeModel.diyGardeningQuestion == null &&
      this.adTypeModel.electronicsOtherPersonalItemsQuestion == null &&

      this.adTypeModel.communicationsInternetQuestion == null &&
      this.adTypeModel.financialServicesQuestion == null &&
      this.adTypeModel.holidaysTravelQuestion == null &&
      this.adTypeModel.sportsLeisureQuestion == null &&
      this.adTypeModel.motoringQuestion == null &&
      this.adTypeModel.newspapersQuestion == null &&

      this.adTypeModel.tvQuestion == null &&
      this.adTypeModel.cinemaQuestion == null &&
      this.adTypeModel.socialNetworkingQuestion == null &&
      this.adTypeModel.shoppingQuestion == null &&
      this.adTypeModel.fitnessQuestion == null &&
      this.adTypeModel.environmentQuestion == null &&

      this.adTypeModel.goingOutQuestion == null &&
      this.adTypeModel.religionQuestion == null &&
      this.adTypeModel.musicQuestion == null &&
      this.adTypeModel.businessOrOpportunitiesQuestion == null &&
      this.adTypeModel.gamblingQuestion == null &&
      this.adTypeModel.restaurantsQuestion == null &&

      this.adTypeModel.insuranceQuestion == null &&
      this.adTypeModel.furnitureQuestion == null &&
      this.adTypeModel.informationTechnologyQuestion == null &&
      this.adTypeModel.energyQuestion == null &&
      this.adTypeModel.supermarketsQuestion == null &&
      this.adTypeModel.healthcareQuestion == null &&

      this.adTypeModel.jobsAndEducationQuestion == null &&
      this.adTypeModel.giftsQuestion == null &&
      this.adTypeModel.advocacyOrLegalQuestion == null &&
      this.adTypeModel.datingAndPersonalQuestion == null &&
      this.adTypeModel.realEstateQuestion == null &&
      this.adTypeModel.gamesQuestion == null
    ) {
      return;
    }
    this.adTypeModel.foodQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.sweetSaltySnacksQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.alcoholicDrinksQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.nonAlcoholicDrinksQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.householdproductsQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.toiletriesCosmeticsQuestion.forEach(t => t.selected = selected);

    this.adTypeModel.pharmaceuticalChemistsProductsQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.tobaccoProductsQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.petsPetFoodQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.shoppingRetailClothingQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.diyGardeningQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.electronicsOtherPersonalItemsQuestion.forEach(t => t.selected = selected);

    this.adTypeModel.communicationsInternetQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.financialServicesQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.holidaysTravelQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.sportsLeisureQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.motoringQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.newspapersQuestion.forEach(t => t.selected = selected);

    this.adTypeModel.tvQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.cinemaQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.socialNetworkingQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.shoppingQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.fitnessQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.environmentQuestion.forEach(t => t.selected = selected);

    this.adTypeModel.goingOutQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.religionQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.musicQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.businessOrOpportunitiesQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.gamblingQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.restaurantsQuestion.forEach(t => t.selected = selected);

    this.adTypeModel.insuranceQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.furnitureQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.informationTechnologyQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.energyQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.supermarketsQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.healthcareQuestion.forEach(t => t.selected = selected);

    this.adTypeModel.jobsAndEducationQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.giftsQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.advocacyOrLegalQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.datingAndPersonalQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.realEstateQuestion.forEach(t => t.selected = selected);
    this.adTypeModel.gamesQuestion.forEach(t => t.selected = selected);

  }

  // Check master checkbox if all items selected
  updateAllComplete() {
    this.allChecked = this.adTypeModel.foodQuestion != null && this.adTypeModel.foodQuestion.every(t => t.selected) &&
      this.adTypeModel.sweetSaltySnacksQuestion != null && this.adTypeModel.sweetSaltySnacksQuestion.every(t => t.selected) &&
      this.adTypeModel.alcoholicDrinksQuestion != null && this.adTypeModel.alcoholicDrinksQuestion.every(t => t.selected) &&
      this.adTypeModel.nonAlcoholicDrinksQuestion != null && this.adTypeModel.nonAlcoholicDrinksQuestion.every(t => t.selected) &&
      this.adTypeModel.householdproductsQuestion != null && this.adTypeModel.householdproductsQuestion.every(t => t.selected) &&
      this.adTypeModel.toiletriesCosmeticsQuestion != null && this.adTypeModel.toiletriesCosmeticsQuestion.every(t => t.selected) &&

      this.adTypeModel.pharmaceuticalChemistsProductsQuestion != null && this.adTypeModel.pharmaceuticalChemistsProductsQuestion.every(t => t.selected) &&
      this.adTypeModel.tobaccoProductsQuestion != null && this.adTypeModel.tobaccoProductsQuestion.every(t => t.selected) &&
      this.adTypeModel.petsPetFoodQuestion != null && this.adTypeModel.petsPetFoodQuestion.every(t => t.selected) &&
      this.adTypeModel.shoppingRetailClothingQuestion != null && this.adTypeModel.shoppingRetailClothingQuestion.every(t => t.selected) &&
      this.adTypeModel.diyGardeningQuestion != null && this.adTypeModel.diyGardeningQuestion.every(t => t.selected) &&
      this.adTypeModel.electronicsOtherPersonalItemsQuestion != null && this.adTypeModel.electronicsOtherPersonalItemsQuestion.every(t => t.selected) &&

      this.adTypeModel.communicationsInternetQuestion != null && this.adTypeModel.communicationsInternetQuestion.every(t => t.selected) &&
      this.adTypeModel.financialServicesQuestion != null && this.adTypeModel.financialServicesQuestion.every(t => t.selected) &&
      this.adTypeModel.holidaysTravelQuestion != null && this.adTypeModel.holidaysTravelQuestion.every(t => t.selected) &&
      this.adTypeModel.sportsLeisureQuestion != null && this.adTypeModel.sportsLeisureQuestion.every(t => t.selected) &&
      this.adTypeModel.motoringQuestion != null && this.adTypeModel.motoringQuestion.every(t => t.selected) &&
      this.adTypeModel.newspapersQuestion != null && this.adTypeModel.newspapersQuestion.every(t => t.selected) &&

      this.adTypeModel.tvQuestion != null && this.adTypeModel.tvQuestion.every(t => t.selected) &&
      this.adTypeModel.cinemaQuestion != null && this.adTypeModel.cinemaQuestion.every(t => t.selected) &&
      this.adTypeModel.socialNetworkingQuestion != null && this.adTypeModel.socialNetworkingQuestion.every(t => t.selected) &&
      this.adTypeModel.shoppingQuestion != null && this.adTypeModel.shoppingQuestion.every(t => t.selected) &&
      this.adTypeModel.fitnessQuestion != null && this.adTypeModel.fitnessQuestion.every(t => t.selected) &&
      this.adTypeModel.environmentQuestion != null && this.adTypeModel.environmentQuestion.every(t => t.selected) &&

      this.adTypeModel.goingOutQuestion != null && this.adTypeModel.goingOutQuestion.every(t => t.selected) &&
      this.adTypeModel.religionQuestion != null && this.adTypeModel.religionQuestion.every(t => t.selected) &&
      this.adTypeModel.musicQuestion != null && this.adTypeModel.musicQuestion.every(t => t.selected) &&
      this.adTypeModel.businessOrOpportunitiesQuestion != null && this.adTypeModel.businessOrOpportunitiesQuestion.every(t => t.selected) &&
      this.adTypeModel.gamblingQuestion != null && this.adTypeModel.gamblingQuestion.every(t => t.selected) &&
      this.adTypeModel.restaurantsQuestion != null && this.adTypeModel.restaurantsQuestion.every(t => t.selected) &&

      this.adTypeModel.insuranceQuestion != null && this.adTypeModel.insuranceQuestion.every(t => t.selected) &&
      this.adTypeModel.furnitureQuestion != null && this.adTypeModel.furnitureQuestion.every(t => t.selected) &&
      this.adTypeModel.informationTechnologyQuestion != null && this.adTypeModel.informationTechnologyQuestion.every(t => t.selected) &&
      this.adTypeModel.energyQuestion != null && this.adTypeModel.energyQuestion.every(t => t.selected) &&
      this.adTypeModel.supermarketsQuestion != null && this.adTypeModel.supermarketsQuestion.every(t => t.selected) &&
      this.adTypeModel.healthcareQuestion != null && this.adTypeModel.healthcareQuestion.every(t => t.selected) &&

      this.adTypeModel.jobsAndEducationQuestion != null && this.adTypeModel.jobsAndEducationQuestion.every(t => t.selected) &&
      this.adTypeModel.giftsQuestion != null && this.adTypeModel.giftsQuestion.every(t => t.selected) &&
      this.adTypeModel.advocacyOrLegalQuestion != null && this.adTypeModel.advocacyOrLegalQuestion.every(t => t.selected) &&
      this.adTypeModel.datingAndPersonalQuestion != null && this.adTypeModel.datingAndPersonalQuestion.every(t => t.selected) &&
      this.adTypeModel.realEstateQuestion != null && this.adTypeModel.realEstateQuestion.every(t => t.selected) &&
      this.adTypeModel.gamesQuestion != null && this.adTypeModel.gamesQuestion.every(t => t.selected);
  }


}
