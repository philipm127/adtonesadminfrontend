import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CampaignCreateEditService } from '../../../campaign-create-edit.service';

@Component({
  selector: 'app-airtime-profile',
  templateUrl: './airtime-profile.component.html',
  styleUrls: ['./airtime-profile.component.css']
})
export class AirtimeProfileComponent implements OnInit {
  frmAirTime: FormGroup;
  allChecked: boolean = false;

  // @Input() frmStepSix
  @Input() airtimeModel: any;
  constructor(private campaignService: CampaignCreateEditService) { }

  ngOnInit(): void {

  }


  isFewSelected(): boolean {
    return this.airtimeModel.contractTypeQuestion.filter(t => t.selected).length > 0 &&
      this.airtimeModel.spendQuestion.filter(t => t.selected).length > 0
      && !this.allChecked;
  }

  // Update Master and list checkboxes
  setAll(selected: boolean) {
    this.allChecked = selected;
    if (this.airtimeModel.contractTypeQuestion == null &&
      this.airtimeModel.spendQuestion == null) {
      return;
    }
    this.airtimeModel.contractTypeQuestion.forEach(t => t.selected = selected);
    this.airtimeModel.spendQuestion.forEach(t => t.selected = selected);
  }

  // Check master checkbox if all items selected
  updateAllComplete() {
    this.allChecked = this.airtimeModel.contractTypeQuestion != null && this.airtimeModel.contractTypeQuestion.every(t => t.selected) &&
      this.airtimeModel.spendQuestion != null && this.airtimeModel.spendQuestion.every(t => t.selected);
  }


}
