import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GeographicProfileComponent } from './geographic-profile.component';

describe('GeographicProfileComponent', () => {
  let component: GeographicProfileComponent;
  let fixture: ComponentFixture<GeographicProfileComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GeographicProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeographicProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
