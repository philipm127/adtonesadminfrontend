import { HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';
import { CampaignCreateEditService } from '../../../campaign-create-edit.service';


@Component({
  selector: 'app-geographic-profile',
  templateUrl: './geographic-profile.component.html',
  styleUrls: ['./geographic-profile.component.css']
})
export class GeographicProfileComponent implements OnInit {
  allChecked: boolean = false;
  @Input() geographicModel: any;
  destroy$: Subject<BasicSelectModel> = new Subject();

  constructor(private campaignService: CampaignCreateEditService) { }

  ngOnInit(): void {

  }

  isFewSelected(): boolean {
    return this.geographicModel.locationQuestion.filter(t => t.selected).length > 0 && !this.allChecked;
  }

  // Update Master and list checkboxes
  setAll(selected: boolean) {
    this.allChecked = selected;
    if (this.geographicModel.locationQuestion == null) {
      return;
    }
    this.geographicModel.locationQuestion.forEach(t => t.selected = selected);
  }

  // Check master checkbox if all items selected
  updateAllComplete() {
    this.allChecked = this.geographicModel.locationQuestion != null && this.geographicModel.locationQuestion.every(t => t.selected);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}

