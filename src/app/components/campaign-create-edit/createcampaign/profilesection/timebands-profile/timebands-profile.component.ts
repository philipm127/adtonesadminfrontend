import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CampaignCreateEditService } from '../../../campaign-create-edit.service';

@Component({
  selector: 'app-timebands-profile',
  templateUrl: './timebands-profile.component.html',
  styleUrls: ['./timebands-profile.component.css']
})
export class TimebandsProfileComponent implements OnInit {

  frmTimeSettings: FormGroup;
  allChecked: boolean = false;

  // @Input() frmStepSix
  @Input() timesettingsModel: any;
  constructor(private campaignService: CampaignCreateEditService) { }

  ngOnInit(): void {

  }

  isFewSelected(): boolean {
    return this.timesettingsModel.mondaySelectedTimes.filter(t => t.isSelected).length > 0 &&
      this.timesettingsModel.tuesdaySelectedTimes.filter(t => t.isSelected).length > 0 &&
      this.timesettingsModel.wednesdaySelectedTimes.filter(t => t.isSelected).length > 0 &&
      this.timesettingsModel.thursdaySelectedTimes.filter(t => t.isSelected).length > 0 &&
      this.timesettingsModel.fridaySelectedTimes.filter(t => t.isSelected).length > 0 &&
      this.timesettingsModel.saturdaySelectedTimes.filter(t => t.isSelected).length > 0 &&
      this.timesettingsModel.sundaySelectedTimes.filter(t => t.isSelected).length > 0
      && !this.allChecked;
  }

  // Update Master and list checkboxes
  setAll(selected: boolean) {
    this.allChecked = selected;
    if (this.timesettingsModel.mondaySelectedTimes == null &&
      this.timesettingsModel.tuesdaySelectedTimes == null &&
      this.timesettingsModel.wednesdaySelectedTimes == null &&
      this.timesettingsModel.thursdaySelectedTimes == null &&
      this.timesettingsModel.fridaySelectedTimes == null &&
      this.timesettingsModel.saturdaySelectedTimes == null &&
      this.timesettingsModel.sundaySelectedTimes == null) {
      return;
    }
    this.timesettingsModel.mondaySelectedTimes.forEach(t => t.isSelected = selected);
    this.timesettingsModel.tuesdaySelectedTimes.forEach(t => t.isSelected = selected);
    this.timesettingsModel.wednesdaySelectedTimes.forEach(t => t.isSelected = selected);
    this.timesettingsModel.thursdaySelectedTimes.forEach(t => t.isSelected = selected);
    this.timesettingsModel.fridaySelectedTimes.forEach(t => t.isSelected = selected);
    this.timesettingsModel.saturdaySelectedTimes.forEach(t => t.isSelected = selected);
    this.timesettingsModel.sundaySelectedTimes.forEach(t => t.isSelected = selected);
  }

  // Check master checkbox if all items selected
  updateAllComplete() {
    this.allChecked = this.timesettingsModel.mondaySelectedTimes != null && this.timesettingsModel.mondaySelectedTimes.every(t => t.isSelected) &&
      this.timesettingsModel.tuesdaySelectedTimes != null && this.timesettingsModel.tuesdaySelectedTimes.every(t => t.isSelected) &&
      this.timesettingsModel.wednesdaySelectedTimes != null && this.timesettingsModel.wednesdaySelectedTimes.every(t => t.isSelected) &&
      this.timesettingsModel.thursdaySelectedTimes != null && this.timesettingsModel.thursdaySelectedTimes.every(t => t.isSelected) &&
      this.timesettingsModel.fridaySelectedTimes != null && this.timesettingsModel.fridaySelectedTimes.every(t => t.isSelected) &&
      this.timesettingsModel.saturdaySelectedTimes != null && this.timesettingsModel.saturdaySelectedTimes.every(t => t.isSelected) &&
      this.timesettingsModel.sundaySelectedTimes != null && this.timesettingsModel.sundaySelectedTimes.every(t => t.isSelected);
  }


}
