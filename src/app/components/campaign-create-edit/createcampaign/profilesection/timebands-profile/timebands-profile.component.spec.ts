import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TimebandsProfileComponent } from './timebands-profile.component';

describe('TimebandsProfileComponent', () => {
  let component: TimebandsProfileComponent;
  let fixture: ComponentFixture<TimebandsProfileComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TimebandsProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimebandsProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
