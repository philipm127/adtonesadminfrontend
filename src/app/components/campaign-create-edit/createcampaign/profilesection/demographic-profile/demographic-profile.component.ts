import { HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';
import { CampaignCreateEditService } from '../../../campaign-create-edit.service';

@Component({
  selector: 'app-demographic-profile',
  templateUrl: './demographic-profile.component.html',
  styleUrls: ['./demographic-profile.component.css']
})
export class DemographicProfileComponent implements OnInit {
  frmGeographic: FormGroup;
  allChecked: boolean = false;

  // @Input() frmStepSix
  @Input() demographicModel: any;
  constructor(private campaignService: CampaignCreateEditService) { }

  ngOnInit(): void {

  }

  isFewSelected(): boolean {
    return this.demographicModel.ageQuestion.filter(t => t.selected).length > 0 &&
    this.demographicModel.genderQuestion.filter(t => t.selected).length > 0 &&
    this.demographicModel.householdStatusQuestion.filter(t => t.selected).length > 0 &&
    this.demographicModel.workingStatusQuestion.filter(t => t.selected).length > 0 &&
    this.demographicModel.relationshipStatusQuestion.filter(t => t.selected).length > 0 &&
    this.demographicModel.educationQuestion.filter(t => t.selected).length > 0
    && !this.allChecked;
  }

  // Update Master and list checkboxes
  setAll(selected: boolean) {
    this.allChecked = selected;
    if (this.demographicModel.ageQuestion == null &&
      this.demographicModel.genderQuestion == null &&
      this.demographicModel.householdStatusQuestion == null &&
      this.demographicModel.workingStatusQuestion == null &&
      this.demographicModel.relationshipStatusQuestion == null &&
      this.demographicModel.educationQuestion == null) {
      return;
    }
    this.demographicModel.ageQuestion.forEach(t => t.selected = selected);
    this.demographicModel.genderQuestion.forEach(t => t.selected = selected);
    this.demographicModel.householdStatusQuestion.forEach(t => t.selected = selected);
    this.demographicModel.workingStatusQuestion.forEach(t => t.selected = selected);
    this.demographicModel.relationshipStatusQuestion.forEach(t => t.selected = selected);
    this.demographicModel.educationQuestion.forEach(t => t.selected = selected);
  }

  // Check master checkbox if all items selected
  updateAllComplete() {
    this.allChecked = this.demographicModel.ageQuestion != null && this.demographicModel.ageQuestion.every(t => t.selected) &&
                      this.demographicModel.genderQuestion != null && this.demographicModel.genderQuestion.every(t => t.selected) &&
                      this.demographicModel.householdStatusQuestion != null && this.demographicModel.householdStatusQuestion.every(t => t.selected) &&
                      this.demographicModel.workingStatusQuestion != null && this.demographicModel.workingStatusQuestion.every(t => t.selected) &&
                      this.demographicModel.relationshipStatusQuestion != null && this.demographicModel.relationshipStatusQuestion.every(t => t.selected) &&
                      this.demographicModel.educationQuestion != null && this.demographicModel.educationQuestion.every(t => t.selected);
  }


}
