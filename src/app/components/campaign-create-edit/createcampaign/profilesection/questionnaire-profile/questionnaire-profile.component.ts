import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatRadioButton } from '@angular/material/radio';
import { CampaignCreateEditService } from '../../../campaign-create-edit.service';

@Component({
  selector: 'app-questionnaire-profile',
  templateUrl: './questionnaire-profile.component.html',
  styleUrls: ['./questionnaire-profile.component.css']
})
export class QuestionnaireProfileComponent implements OnInit {
  frmQuestionnaire: FormGroup;
  allChecked: boolean = false;

  // @Input() frmStepSix
  @Input() questionnaireModel: any;
  constructor(private campaignService: CampaignCreateEditService) { }

  ngOnInit(): void {

  }

  isFewSelected(): boolean {
    return this.questionnaireModel.hustlersQuestion.filter(t => t.selected).length > 0 &&
      this.questionnaireModel.youthQuestion.filter(t => t.selected).length > 0 &&
      this.questionnaireModel.discerningProfessionalsQuestion.filter(t => t.selected).length > 0 &&
      this.questionnaireModel.massQuestion.filter(t => t.selected).length > 0;
  }


  changedSel(mrChange: any) {
    let mrButton: MatRadioButton = mrChange.source;
    let objIndex = 0;
    if (mrButton.name == 'hustlersQuestion') {
      this.questionnaireModel.hustlersQuestion.forEach(t => t.selected = false);
      objIndex = this.questionnaireModel.hustlersQuestion.findIndex((obj => obj.questionValue == mrChange.value));
      this.questionnaireModel.hustlersQuestion[objIndex].selected = true;
    }
    else if (mrButton.name == 'youthQuestion') {
      this.questionnaireModel.youthQuestion.forEach(t => t.selected = false);
      objIndex = this.questionnaireModel.youthQuestion.findIndex((obj => obj.questionValue == mrChange.value));
      this.questionnaireModel.youthQuestion[objIndex].selected = true;
    }
    else if (mrButton.name == 'discerningProfessionalsQuestion') {
      this.questionnaireModel.discerningProfessionalsQuestion.forEach(t => t.selected = false);
      objIndex = this.questionnaireModel.discerningProfessionalsQuestion.findIndex((obj => obj.questionValue == mrChange.value));
      this.questionnaireModel.discerningProfessionalsQuestion[objIndex].selected = true;
    }
    else if (mrButton.name == 'massQuestion') {
      this.questionnaireModel.massQuestion.forEach(t => t.selected = false);
      objIndex = this.questionnaireModel.massQuestion.findIndex((obj => obj.questionValue == mrChange.value));
      this.questionnaireModel.massQuestion[objIndex].selected = true;
    }
  }
}
