import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { SelectServicesService } from 'src/app/components/shared/services/select-services.service';
import { BasicSelectModel } from 'src/app/models/basic-select';

@Component({
  selector: 'app-step-five-component',
  templateUrl: './step-five-component.html',
  styleUrls: ['./step-five-component.css']
})
export class StepFiveComponent implements OnInit {
  @Input() editCampaign: boolean;
  @Input() campaignData: any;
  @Input() smsSuccess: boolean = false;
  @Input() smsFail: boolean = false;
  frmStepFive: FormGroup;
  smsUpdate: boolean = false;
  @Input() advertiserId: string;
  @Output() fifthStepData = new EventEmitter<any>();


  destroy$: Subject<BasicSelectModel> = new Subject();

  constructor(private formBuilder: FormBuilder, private listService: SelectServicesService) {

  }

  ngOnInit() {
    this.frmStepFive = this.formBuilder.group({

      smsOriginator: new FormControl("", Validators.maxLength(500)),
      smsMessage: new FormControl("", Validators.maxLength(160)),
      emailSubject: new FormControl("", Validators.maxLength(1000)),
      emailContent: new FormControl("", Validators.maxLength(1000))
    });
    if (this.editCampaign)
      this.initForm();
  }

  get smsOriginator() {
    return this.frmStepFive.get('smsOriginator');
  }

  get smsMessage() {
    return this.frmStepFive.get('smsMessage');
  }

  get emailContent() {
    return this.frmStepFive.get('emailContent');
  }

  get emailSubject() {
    return this.frmStepFive.get('emailSubject');
  }

  initForm() {
    this.frmStepFive.patchValue({
      smsOriginator: this.campaignData.smsOriginator,
      smsMessage: this.campaignData.smsBody,
      emailSubject: this.campaignData.emailSubject,
      emailContent: this.campaignData.emailBody
    });
  }

  form5() {
    this.smsUpdate = true;
    this.fifthStepData.emit(this.frmStepFive.getRawValue());
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }


}
