import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'src/app/components/shared/services/message.service';

@Component({
  selector: 'app-step-seven-component',
  templateUrl: './step-seven-component.component.html',
  styleUrls: ['./step-seven-component.component.css']
})
export class StepSevenComponentComponent {
  @Output() frmDataComplete = new EventEmitter<boolean>();
  @Output() changeErrorStatus = new EventEmitter<boolean>();
  @Input() formSubmitted: boolean = false;
  @Input() hasCampaignError: boolean = false;
  @Input() campaignErrorMessage: string;
  @Input() campaignName: string;
  @Input() userData: any;

  constructor(private messService: MessageService, private route: Router) { }
  submit() {
    console.log('submitted');
    this.frmDataComplete.emit(true);
    // this.formSubmitted = true;
  }

  clearError() {
    this.changeErrorStatus.emit(false);
  }

  doneFinally(id: number) {
    let message: string;
    if (id == 1)
      message = "Succesfully Created Campaign: " + this.campaignName;
    else
      message = "There was a problem creating Campaign: " + this.campaignName + " Please contact an Admin";
    this.messService.sendMessage({ text: message.toString(), category: 'success' });
    if (this.userData.roleId == 7)
      this.route.navigate(['/campaigntable']);
    else
      this.route.navigate(['/salescampaign']);
  }

}
