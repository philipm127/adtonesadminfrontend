import { HttpResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { SelectServicesService } from 'src/app/components/shared/services/select-services.service';
import { ClientInfoModel } from 'src/app/components/usermanagement/models/clientInfo-model';
import { UmanagementService } from 'src/app/components/usermanagement/umanagement.service';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';

@Component({
  selector: 'app-step-two-component',
  templateUrl: './step-two-component.html',
  styleUrls: ['./step-two-component.css']
})
export class StepTwoComponent implements OnInit {
  @Input() editCampaign: boolean;
  @Input() campaignData: any;
  public frmStepTwo: FormGroup;

  clientModel: ClientInfoModel;
  frmTwoSubmitting = false;

  clientSelect$: Observable<BasicSelectModel[]>;
  @Input() stepTwoVisible: boolean;
  @Input() advertiserId: number;
  @Output() secondStepData = new EventEmitter<any>();
  @Input() hasCampaignError: boolean = false;
  @Input() campaignErrorMessage: string;
  @Output() changeErrorStatus = new EventEmitter<boolean>();


  destroy$: Subject<BasicSelectModel> = new Subject();

  constructor(private formBuilder: FormBuilder, private listService: SelectServicesService,
    private userService: UmanagementService) {

  }

  ngOnInit() {
    this.frmStepTwo = this.formBuilder.group({
      id: new FormControl(""),
      name: new FormControl("", Validators.required),
      description: new FormControl(""),
      email: new FormControl("", Validators.required),
      contactPhone: new FormControl(""),
      userId: new FormControl("")
    });
    if (this.stepTwoVisible)
      this.getClient();
  }


  onClientChange(event: any) {
    this.userService.getClientInfo(event.value)
      .subscribe(
        data => {
          const retModel = data;
          if (retModel.result == 1) {
            this.clientModel = <ClientInfoModel>retModel.body;
            this.initForm();
          }
        });
  }


  initForm() {
    this.frmStepTwo.patchValue({
      id: this.clientModel.id,
      name: this.clientModel.name,
      description: this.clientModel.description,
      email: this.clientModel.email,
      contactPhone: this.clientModel.contactPhone,
      userId: this.clientModel.userId
    });
  }


  getClient() {
    this.clientSelect$ = this.listService.getClientList(this.advertiserId)
    .pipe(
      takeUntil(this.destroy$),
      map((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        if (retResult.result == 1) {
          return <BasicSelectModel[]>retResult.body;
        } else {
          return [];
        }
      })
    );
}


  form2() {
    this.frmTwoSubmitting = true;
    this.secondStepData.emit(this.frmStepTwo.getRawValue());
    this.frmTwoSubmitting = false;
  }

  clearError() {
    this.changeErrorStatus.emit(false);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
