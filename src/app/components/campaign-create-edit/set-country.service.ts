import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SetCountryService {

  public dataChange = new Subject<number>();

  setData(data: number) {
    this.dataChange.next(data);
  }
}
