import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { StepThreeComponent } from '../createcampaign/step-three-component/step-three-component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { AdvertService } from '../../adverts/advert.service';
import { SelectServicesService } from 'src/app/components/shared/services/select-services.service';
import { of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';

describe('StepThreeComponent', () => {
  let component: StepThreeComponent;
  let fixture: ComponentFixture<StepThreeComponent>;
  let advertService: AdvertService;
  let selectServicesService: SelectServicesService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule
      ],
      declarations: [StepThreeComponent],
      providers: [AdvertService, SelectServicesService],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepThreeComponent);
    component = fixture.componentInstance;
    advertService = TestBed.inject(AdvertService);
    selectServicesService = TestBed.inject(SelectServicesService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render form with all form fields', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('form')).toBeTruthy();
    expect(compiled.querySelector('[formControlName="advertCategoryId"]')).toBeTruthy();
    expect(compiled.querySelector('[formControlName="advertName"]')).toBeTruthy();
    expect(compiled.querySelector('[formControlName="brand"]')).toBeTruthy();
    expect(compiled.querySelector('[formControlName="operatorId"]')).toBeTruthy();
    expect(compiled.querySelector('[formControlName="script"]')).toBeTruthy();
    expect(compiled.querySelector('[formControlName="audioUpload"]')).toBeTruthy();
    expect(compiled.querySelector('[formControlName="fileUpload"]')).toBeTruthy();
  });

  it('should handle audio upload for creating a new advert', () => {
    const audioFile = new File(['audio'], 'test.wav', { type: 'audio/wav' });
    const event = { target: { files: [audioFile] } };

    component.onAudioFileChange(event);
    expect(component.uploadedAudioURL).toBeTruthy();
    expect(component.audioFileError).toBeNull();
  });

  it('should handle audio upload error for an invalid file type', () => {
    const audioFile = new File(['audio'], 'test.mp3', { type: 'audio/mp3' });
    const event = { target: { files: [audioFile] } };

    component.onAudioFileChange(event);
    expect(component.uploadedAudioURL).toBeNull();
    expect(component.audioFileError).toBeTruthy();
  });

  it('should handle audio upload error for a file too big', () => {
    const audioFile = new File(['audio'.repeat(1024 * 1024)], 'test.wav', { type: 'audio/wav' });
    const event = { target: { files: [audioFile] } };

    component.onAudioFileChange(event);
    expect(component.uploadedAudioURL).toBeNull();
    expect(component.audioFileError).toBeTruthy();
  });

  it('should handle existing advert audio', () => {
    spyOn(advertService, 'getAdvertDetailsByCampaignId').and.returnValue(of(new HttpResponse({
      status: 200,
      body: {
        result: 1,
        error: null,
        body: { mediaFileLocation: 'existing-audio-url' },
        recordcount: 0
      }
    })));

    component.editCampaign = true;
    component.ngOnInit();
    component.getAdvertData();

    expect(component.existingAudioURL).toEqual('existing-audio-url');
  });

  it('should handle file upload for creating a new advert', () => {
    const file = new File(['file'], 'test.txt', { type: 'text/plain' });
    const fileUpload = component.fileUpload.nativeElement;
    fileUpload.files = [file];

    component.onSMSClick();
    expect(component.frmStepThree.get('scriptFile').value).toEqual(file);
  });

  it('should handle updates for existing advert', () => {
    spyOn(advertService, 'updateAdvert').and.returnValue(of({
      result: 1,
      error: null,
      body: null,
      recordcount: 0
    }));

    component.editCampaign = true;
    component.updateAdvert();

    expect(component.advertUpdate).toBeTrue();
    expect(component.hasCampaignError).toBeFalse();
  });

  it('should handle update error for existing advert', () => {
    spyOn(advertService, 'updateAdvert').and.returnValue(of({
      result: 0,
      error: 'Error message',
      body: null,
      recordcount: 0
    }));

    component.editCampaign = true;
    component.updateAdvert();

    expect(component.hasCampaignError).toBeTrue();
    expect(component.campaignErrorMessage).toEqual('There was a problem inserting the Advert please consult an Admin');
  });

});

