export interface CampaignCreationModel {


}


export class CampaignInfoModel {
  userId: number;
  campaignName: string;
  campaignId: number;
  campaignDescription: string;
  countryId: number;
  operatorId: number;
  campaignProfileId: number;
  campaignCategoryId: number;
  totalBudget: number;
  maxDailyBudget: number;
  maxBid: number;
  maxMonthBudget: number;
  maxWeeklyBudget: number;
  maxHourlyBudget: number;
  currencyId: number;

  clientId: number;

  startDate: string;
  endDate: string;
  smsOriginator: string;
  smsBody: string;
  emailSubject: string;
  emailBody: string;
  newClientFormModel: NewClientFormModel;
}

export class NewClientFormModel {
  id: number;
  name: string;
  description: string;
  email: string;
  contactPhone: string;
  userId: number;
  countryId: number;
}


export interface AdvertInfoModel {
  advertId: number;
  adId: number;
  advertName: string;
  clientId: number;
  brand: string;
  advertCategoryId: number;
  mediaFile: string;
  scriptFile: string;
  scriptText: string;
  operatorId: number;
  campaignProfileId: number;
  advertiserId: number;
}

