import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { ReturnResult } from '../../models/return-result';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CampaignCreateEditService {

  createUrl: string;
  controllerCreate: string = "createupdatecampaign/";


  constructor(private http: HttpClient) {
      this.createUrl = (environment.appUrl + this.controllerCreate)
  }


  public checkIfCampaignNameExists(model: any): Observable<ReturnResult> {
    // console.log("checkIfCampaignNameExists: ", model);
    let ctrAction = "v1/CheckIfCampaignNameExists";
    return this.http.post<ReturnResult>(this.createUrl + ctrAction, model);
  }


  public createCampaign(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/CreateNewCampaign";
    return this.http.post<ReturnResult>(this.createUrl + ctrAction, model);
  }


  public updateCampaignDetails(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/UpdateCampaignDetails";
    return this.http.post<ReturnResult>(this.createUrl + ctrAction, model);
  }


  // Loads up empty models for profile forms
  // id is countryId
  public getProfileTemplate(id: number) {
    // console.log("Entered getProfileTemplate");
    let ctrAction = "v1/GetInitialData/";
    return this.http.get<ReturnResult>(this.createUrl + ctrAction + id, { observe: "response" });
  }


  // Loads up populated models for profile forms
  // id is campaignProfileId
  public getProfileTemplateData(id: number) {
    // console.log("Entered getProfileTemplate");
    let ctrAction = "v1/GetProfileData/";
    return this.http.get<ReturnResult>(this.createUrl + ctrAction + id, { observe: "response" });
  }


  public getCampaignData(id: number) {
    let ctrAction = "v1/GetCampaignData/";
    return this.http.get<ReturnResult>(this.createUrl + ctrAction + id, { observe: "response" });
  }


  public insertAdvertProfileData(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/InsertProfileInformation";
    return this.http.post<ReturnResult>(this.createUrl + ctrAction, model);
  }

}
