import { HttpResponse } from '@angular/common/http';
import { AfterViewChecked, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { ReturnResult } from 'src/app/models/return-result';
import { CampaignCreateEditService } from '../../campaign-create-edit.service';
import { StepFiveComponent } from '../../createcampaign/step-five-component/step-five-component';
import { StepFourComponent } from '../../createcampaign/step-four-component/step-four-component';
import { StepOneComponent } from '../../createcampaign/step-one-component/step-one-component';
import { StepSixComponent } from '../../createcampaign/step-six-component/step-six-component';
import { StepThreeComponent } from '../../createcampaign/step-three-component/step-three-component';
import { StepTwoComponent } from '../../createcampaign/step-two-component/step-two-component';
import { CampaignInfoModel } from '../../models/campaigncreateupdate-models';

@Component({
  selector: 'app-update-campaign-main',
  templateUrl: './update-campaign-main.component.html',
  styleUrls: ['./update-campaign-main.component.css']
})
export class UpdateCampaignMainComponent implements AfterViewChecked {
  pagename: string = "UpdateCampaign";
  editCampaign: boolean = true;
  userData: any;
  countryId: number;
  operatorId: number;
  advertiserId: number = 0;
  frmOneData: any;
  frmTwoData: any;
  frmThreeData: any;
  frmFourData: any;
  frmFiveData: any;
  frmSixData: any;
  adFile: File;
  scriptFile: File;
  campaignId: number;
  minBid: number;

  budgetForm: FormGroup;

  campaignErrorMessage: string;
  hasCampaignError: boolean = false;
  destroy$: Subject<ReturnResult> = new Subject();

  @ViewChild(StepOneComponent) stepOneComponent: StepOneComponent;
  @ViewChild(StepTwoComponent) stepTwoComponent: StepTwoComponent;
  @ViewChild(StepThreeComponent) stepThreeComponent: StepThreeComponent;
  @ViewChild(StepFourComponent) stepFourComponent: StepFourComponent;
  @ViewChild(StepFiveComponent) stepFiveComponent: StepFiveComponent;
  @ViewChild(StepSixComponent) stepSixComponent: StepSixComponent;

  // permisiions
  stepOneVisible: boolean = false;
  stepTwoVisible: boolean = false;
  stepThreeVisible: boolean = false;
  stepFourVisible: boolean = false;
  stepFiveVisible: boolean = false;
  stepSixVisible: boolean = false;
  brandVisible: boolean = false;
  campaignCategoryVisible: boolean = false;

  profileSelectionModel: any;
  campaignDataModel: any;
  advertDataModel: any;

  messageAvailable: number = 0;
  campSuccess: boolean;
  campFail: boolean;
  budgetSuccess: boolean;
  budgetFail: boolean;
  profileSuccess: boolean;
  profileFail: boolean;
  smsSuccess: boolean;
  smsFail: boolean;

  constructor(private accessService: AccessServicesService,
    private route: ActivatedRoute, private router: Router, private changeDetect: ChangeDetectorRef,
    private campaignService: CampaignCreateEditService) {
    this.budgetForm = this.frmStepFour;
    this.userData = this.accessService.getUser();
    this.advertiserId = parseInt(this.route.snapshot.paramMap.get('id'));
    this.campaignId = parseInt(this.route.snapshot.paramMap.get('profid'));
    if (this.advertiserId == null || this.advertiserId == 0 || Number.isNaN(this.advertiserId))
      this.advertiserId = this.userData.userId;

    this.PermissionAssignment();
    if (this.editCampaign)
      this.getCampaignData();


  }


  firstStepData(data: any) {

    this.frmOneData = data;
    this.countryId = this.frmOneData.countryId;
    this.messageAvailable = 1;
    console.log("firstStepData: ", this.frmOneData);
    this.frmDataComplete(true);

  }

  secondStepData(data: any) {
    this.frmTwoData = data;
  }

  thirdStepData(data: any) {
    this.frmThreeData = data;

    if (this.frmOneData) {
      let model: CampaignInfoModel = new CampaignInfoModel();

      model.userId = this.advertiserId;
      model.campaignName = this.frmOneData.campaignName;
      model.campaignDescription = this.frmOneData.campaignDescription;
      model.countryId = this.frmOneData.countryId;
      if (!this.editCampaign)
        this.campaignService.checkIfCampaignNameExists(model)
          .subscribe(
            data => {
              const retResult = data;
              // console.log("Returned body checkIfCampaignNameExists is: ", retResult);
              if (retResult.result == 0) {
                this.hasCampaignError = true;
              }
            });
    }

    if (this.frmThreeData && !this.editCampaign) {
      let model = ({
        advertiserId: this.advertiserId,
        advertName: this.frmThreeData.advertName
      })
      // this.campaignService.checkIfAdvertNameExists(model)
      //   .subscribe(
      //     data => {
      //       const retResult = data;
      //       // console.log("Returned body checkIfAdvertNameExists is: ", retResult);
      //       if (retResult.result == 0) {
      //         this.hasCampaignError = true;
      //       }
      //     });
    }
  }

  fourthStepData(data: any) {
    this.frmFourData = data;
    this.messageAvailable = 4;
    this.frmDataComplete(true);
  }

  fifthStepData(data: any) {
    this.frmFiveData = data;
    this.messageAvailable = 5;
    this.frmDataComplete(true);
  }

  sixthStepData(data: any) {
    this.frmSixData = data;
    this.messageAvailable = 6;
    if (this.stepSixVisible)
      this.addProfileData();
  }

  advertFile(file: File) {
    this.adFile = file;
  }

  smsFile(file: File) {
    this.scriptFile = file;
  }

  get frmStepOne() {
    return this.stepOneComponent ? this.stepOneComponent.frmStepOne : null;
  }

  get frmStepTwo() {
    return this.stepTwoComponent ? this.stepTwoComponent.frmStepTwo : null;
  }

  get frmStepThree() {
    return this.stepThreeComponent ? this.stepThreeComponent.frmStepThree : null;
  }


  get frmStepFour() {
    return this.stepFourComponent ? this.stepFourComponent.frmStepFour : null;
  }

  get frmStepFive() {
    return this.stepFiveComponent ? this.stepFiveComponent.frmStepFive : null;
  }

  get frmStepSix() {
    return this.stepSixComponent ? this.stepSixComponent.frmStepSix : null;
  }

  // The adverts profiles
  getProfileInfo() {
    console.log("getProfileInfo - Campaign Id: ", this.campaignId);

    this.campaignService.getProfileTemplateData(this.campaignId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        const retResult = res.body;
        if (retResult.result == 1) {
          this.profileSelectionModel = retResult.body;
          console.log("his.geographicModel ", this.profileSelectionModel);
          // this.getMyData();
        }
      });
  }


  getCampaignData() {
    this.campaignService.getCampaignData(this.campaignId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        const retResult = res.body;
        if (retResult.result == 1) {
          this.campaignDataModel = retResult.body;
          this.countryId = this.campaignDataModel.countryId;
          this.operatorId = this.campaignDataModel.operatorId;
          console.log("The Campaign DataModel ", this.campaignDataModel);
          this.minBid = this.campaignDataModel.minBid;
          if (this.stepSixVisible)
            this.getProfileInfo();
        }
      });
  }

  updateMessages() {
    // this.messageAvailable = 0;
    this.campSuccess = false;
    this.campFail = false;
    this.budgetSuccess = false;
    this.budgetFail = false;
    this.profileSuccess = false;
    this.profileFail = false;
    this.smsSuccess = false;
    this.smsFail = false;
  }


  frmDataComplete(data: boolean) {
    this.updateMessages();
    let subbed = data;
    if (subbed) {
      // let model: CampaignInfoModel = new CampaignInfoModel();
      if (this.frmOneData != null) {
        // this.campaignDataModel.userId = this.advertiserId;
        this.campaignDataModel.campaignName = this.frmOneData.campaignName;
        this.campaignDataModel.campaignDescription = this.frmOneData.campaignDescription;
        // this.campaignDataModel.countryId = this.frmOneData.countryId;
        this.campaignDataModel.startDate = this.frmOneData.startdate;
        this.campaignDataModel.endDate = this.frmOneData.endDate;
        if (this.campaignCategoryVisible)
          this.campaignDataModel.campaignCategoryId = this.frmOneData.campaignCategoryId;
        this.campaignDataModel.campaignProfileId = this.campaignId;
        // this.campaignDataModel.clientId = this.frmTwoData.clientId;
      }

      if (this.frmFourData != null) {

        // this.campaignDataModel.currencyId = this.frmFourData.currencyId;
        this.campaignDataModel.maxBid = this.frmFourData.maxBid;
        this.campaignDataModel.maxDailyBudget = this.frmFourData.maxDailyBudget;
        this.campaignDataModel.maxHourlyBudget = this.frmFourData.maxHourlyBudget;
        this.campaignDataModel.maxMonthBudget = this.frmFourData.maxMonthBudget;
        this.campaignDataModel.maxWeeklyBudget = this.frmFourData.maxWeeklyBudget;
      }

      if (this.frmFiveData != null) {
        this.campaignDataModel.emailSubject = this.frmFiveData.emailSubject != null && this.frmFiveData.emailSubject.length > 1 ? this.frmFiveData.emailSubject : null;
        this.campaignDataModel.emailBody = this.frmFiveData.emailContent != null && this.frmFiveData.emailContent.length > 1 ? this.frmFiveData.emailContent : null;
        this.campaignDataModel.smsOriginator = this.frmFiveData.smsOriginator != null && this.frmFiveData.smsOriginator.length > 1 ? this.frmFiveData.smsOriginator : null;
        this.campaignDataModel.smsBody = this.frmFiveData.smsMessage != null && this.frmFiveData.smsMessage.length > 1 ? this.frmFiveData.smsMessage : null;

      }

      console.log("this.campaignService.createCampaign(model): ", this.campaignDataModel);

      this.campaignService.updateCampaignDetails(this.campaignDataModel)
        .subscribe(resp => {
          const retModel = resp;
          if (retModel.result == 1) {
            if (this.messageAvailable == 4)
              this.budgetSuccess = true;
            else if (this.messageAvailable == 1)
              this.campSuccess = true;
            else if (this.messageAvailable == 5)
              this.smsSuccess = true;

          }
          else {
            if (this.messageAvailable == 4)
              this.budgetFail = true;
            else if (this.messageAvailable == 1)
              this.campFail = true;
            else if (this.messageAvailable == 5)
              this.smsFail = true;
          }

        });
    }
  }

  // addAdvert() {
  //   // let z = this.frmThreeData;
  //   // console.log("Third step value is: ", z);
  //   this.campaignService.createCampaign_Advert(this.frmThreeData)
  //     .subscribe(resp => {
  //       const retModel = resp

  //       // if (this.retModel.result == 1) {
  //       //   //this.successMessage = "The Details Were Updated Successfully";
  //       // }
  //       // else {
  //       //   //this.failMessage = "There was a problem updating the details";
  //       // }
  //     });
  // }


  addProfileData() {
    // let z = this.frmSixData;
    // z.id = 1;
    // z.campaignProfileId = this.campaignId;
    // z.countryId = this.countryId;
    // z.campaignProfileGeographicModel.campaignProfileId = this.campaignId;
    // z.campaignProfileAd.campaignProfileId = this.campaignId;
    // z.campaignProfileDemographicsmodel.campaignProfileId = this.campaignId;
    // z.campaignProfileMobileFormModel.campaignProfileId = this.campaignId;
    // z.campaignProfileSkizaFormModel.campaignProfileId = this.campaignId;
    // z.campaignProfileTimeSettingModel.campaignProfileId = this.campaignId;
    this.profileSelectionModel.operatorId = this.operatorId;
    console.log("Sixth step value is: ", this.profileSelectionModel);
    this.updateMessages();
    this.campaignService.insertAdvertProfileData(this.profileSelectionModel)
      .subscribe(resp => {
        const retModel = resp;
        if (retModel.result == 1) {
          if (this.messageAvailable == 6)
            this.profileSuccess = true;
        }
        else {
          if (this.messageAvailable == 6)
            this.profileFail = true;
        }
      });
  }


  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    // console.log("PermissionAssignment(): ", permPageData);
    this.stepOneVisible = permPageData["elements"].filter(j => j.name == "stepOne")[0].visible;
    this.stepTwoVisible = permPageData["elements"].filter(j => j.name == "stepTwo")[0].visible;
    this.stepThreeVisible = permPageData["elements"].filter(j => j.name == "stepThree")[0].visible;
    this.stepFourVisible = permPageData["elements"].filter(j => j.name == "stepFour")[0].visible;
    this.stepFiveVisible = permPageData["elements"].filter(j => j.name == "stepFive")[0].visible;
    this.stepSixVisible = permPageData["elements"].filter(j => j.name == "stepSix")[0].visible;

    this.brandVisible = permPageData["elements"].filter(j => j.name == "stepFive")[0].visible;
    this.campaignCategoryVisible = permPageData["elements"].filter(j => j.name == "campaignCategory")[0].visible;


    // if (this.stepSixVisible)
    //   this.getCampaignData();
  }


  checkCampaign() {
    return true;
  }


  ngAfterViewChecked(): void {
    this.changeDetect.detectChanges();
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
