import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthguardService } from 'src/app/services/auth/authguard.service';

import { CreateStepperMainComponent } from './createcampaign/create-stepper-main/create-stepper-main.component';
import { UpdateCampaignMainComponent } from './updatecampaign/update-campaign-main/update-campaign-main.component';

const campignRoutes: Routes = [
  { path: 'createcampaign/:id', component: CreateStepperMainComponent },
  { path: 'createcampaign', component: CreateStepperMainComponent },
  { path: 'updatecampaign/:id/:profid', component: UpdateCampaignMainComponent }
];

@NgModule({
  imports: [RouterModule.forChild(campignRoutes)],
  exports: [RouterModule]
})

export class CampaignCreateEditRoutingModule { }
