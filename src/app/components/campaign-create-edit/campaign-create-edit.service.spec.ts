import { TestBed } from '@angular/core/testing';

import { CampaignCreateEditService } from './campaign-create-edit.service';

describe('CampaignCreateEditService', () => {
  let service: CampaignCreateEditService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampaignCreateEditService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
