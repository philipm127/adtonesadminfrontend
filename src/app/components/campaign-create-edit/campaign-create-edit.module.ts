import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { MatTableExporterModule } from 'mat-table-exporter';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { SharedModule } from '../shared/shared.module';
import { CampaignCreateEditRoutingModule } from './campaign-create-edit-routing.module';
import { CreateStepperMainComponent } from './createcampaign/create-stepper-main/create-stepper-main.component';
import { AdtypeProfileComponent } from './createcampaign/profilesection/adtype-profile/adtype-profile.component';
import { AirtimeProfileComponent } from './createcampaign/profilesection/airtime-profile/airtime-profile.component';
import { DemographicProfileComponent } from './createcampaign/profilesection/demographic-profile/demographic-profile.component';
import { GeographicProfileComponent } from './createcampaign/profilesection/geographic-profile/geographic-profile.component';
import { QuestionnaireProfileComponent } from './createcampaign/profilesection/questionnaire-profile/questionnaire-profile.component';
import { TimebandsProfileComponent } from './createcampaign/profilesection/timebands-profile/timebands-profile.component';
import { StepFiveComponent } from './createcampaign/step-five-component/step-five-component';
import { StepFourComponent } from './createcampaign/step-four-component/step-four-component';
import { StepOneComponent } from './createcampaign/step-one-component/step-one-component';
import { StepSevenComponentComponent } from './createcampaign/step-seven-component/step-seven-component.component';
import { StepSixComponent } from './createcampaign/step-six-component/step-six-component';
import { StepThreeComponent } from './createcampaign/step-three-component/step-three-component';
import { StepTwoComponent } from './createcampaign/step-two-component/step-two-component';
import { UpdateCampaignMainComponent } from './updatecampaign/update-campaign-main/update-campaign-main.component';



@NgModule({
  declarations: [
    CreateStepperMainComponent,
    StepOneComponent,
    StepTwoComponent,
    StepThreeComponent,
    StepFourComponent,
    StepFiveComponent,
    StepSixComponent,
    StepSevenComponentComponent,
    UpdateCampaignMainComponent,
    GeographicProfileComponent,
    DemographicProfileComponent,
    AdtypeProfileComponent,
    QuestionnaireProfileComponent,
    TimebandsProfileComponent,
    AirtimeProfileComponent
  ],
  imports: [
    CommonModule,
    CampaignCreateEditRoutingModule,
    SharedModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule
  ],
  providers: [
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } }
  ]
})
export class CampaignCreateEditModule { }
