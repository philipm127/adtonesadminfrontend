import { Injectable } from '@angular/core';
import { TokenRefreshService } from '../../../services/auth/token-refresh.service';
import { PermissionService } from './permission-data.service';
import { TokenStorageService } from '../../../services/auth/token-storage.service';
import { AbstractControl, FormGroup } from '@angular/forms';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { PermissionData } from 'src/app/models/permission-data';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccessServicesService {
  testPermissionInput: boolean = environment.testPermissionInput;

  constructor(private refreshService: TokenRefreshService,
    private permService: PermissionService, private tokenStorage: TokenStorageService) { }

  getUser() {
    return this.tokenStorage.getUser();
  }

  refreshToken() {
    this.refreshService.refreshToken();
  }

  getPermissionList(pagename: string) {
    if (this.testPermissionInput)
      return this.getPermissionListFile(pagename)
    else
      return this.getPermissionListDB(pagename);
  }

  // Used to test getting permissions from user data when all set up change name to
  // getPermissionList and change its name to something else.
  getPermissionListDB(pagename: string) {
    let user = this.getUser();
    // console.log("let user = this.getUser(): ",user);
    const permU: PermissionData[] = JSON.parse(user.permissions);

    // console.log("CONST permU: PermissionData[]: ",permU);
    let permPageData = permU.find(i => i.pageName === pagename);
    if (permPageData == undefined) {
      // console.log("value of permData here should be null: ",permPageData);
      this.logOut();
    }
    else
      return permPageData;
  }

  getPermissionListFile(pagename: string): PermissionData {
    let user = this.getUser();
    console.log("Entered getPermissionList looking for page name: ", pagename);
    console.log("Entered getPermissionList looking for role: ", user.role);
    let permPageData = this.permService.getJsonPermissionDataListsByRole(pagename, user.role);
    console.log("Entered getPermissionList looking for perm values: ", permPageData);
    if (permPageData == undefined || !permPageData.visible) {
      // console.log("value of permData here should be null: ",permPageData);
      this.logOut();
    }
    else
      return permPageData;
  }

  FilterPermissionAssignment(permPageData: any): TableSearchModel {
    return this.permService.FilterPermissionAssignment(permPageData);
  }

  TablePermissionAssignment(permPageData: any) {
    return this.permService.TablePermissionAssignment(permPageData)
  }

  logOut() {
    this.tokenStorage.signOut();
  }

  getName(control: AbstractControl): string | null {
    // console.log(" what is get name doing ", control);
    let group = <FormGroup>control.parent;
    if (!group) {
      return null;
    }
    let name: string;
    Object.keys(group.controls).forEach(key => {
      let childControl = group.get(key);

      if (childControl !== control) {
        return;
      }
      name = key;
    });
    // console.log(" what is get name doing with name ", name);
    return name;
  }

}
