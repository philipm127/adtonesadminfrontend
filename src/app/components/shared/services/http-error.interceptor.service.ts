import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest,
  HttpResponse, HttpErrorResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { AccessServicesService } from './access-services.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private router: Router, private accessService: AccessServicesService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request)
      .pipe(
        retry(3),
        catchError((error: HttpErrorResponse) => {
          let errorMessage = '';
          // console.log("The error status is: ",error.status);
          if (error.status === 401) {
            errorMessage = `Error Code: ${error.status}\nMessage: Your session has expired.\n You are required to login again.`;
            this.accessService.logOut();
          }
          else if (error.message.includes('0 Unknown Error')) {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}\n\nTo Resolve: Contact Your Network Administrator Due To Firewall Or Related Issue`;
          }
          else if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
          } else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
            // console.log("Error ststus message: ",error.status);
            // console.log("Error ststus message: ",error.message);
          }

          window.alert(errorMessage);
          return throwError(errorMessage);
        })

      )
  }
}
