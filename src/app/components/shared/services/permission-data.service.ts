import { Injectable } from '@angular/core';
import *  as  admin from '../../../../assets/tempPermissionJson/menu-main-admin.json';
import *  as  ticketList from '../../../../assets/tempPermissionJson/ticketList.json';
import *  as  ticketListOp from '../../../../assets/tempPermissionJson/ticketListOp.json';
import * as advertiserList from '../../../../assets/tempPermissionJson/advertiserList.json';
import * as advertiserListOp from '../../../../assets/tempPermissionJson/advertiserListOp.json';
import * as userDetails from '../../../../assets/tempPermissionJson/contactUserCompanyEdit.json';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import * as advertsList from '../../../../assets/tempPermissionJson/advertsList.json';
import * as advertsListOp from '../../../../assets/tempPermissionJson/advertsListOp.json';
import * as campaignList from '../../../../assets/tempPermissionJson/campaignList.json';
import * as campaignListOp from '../../../../assets/tempPermissionJson/campaignListOp.json';
import * as campaignDashListOp from '../../../../assets/tempPermissionJson/campaignDashboardPlayListOp.json';
import * as defaultRoute from '../../../../assets/tempPermissionJson/login.json';
import * as advertisercredit from '../../../../assets/tempPermissionJson/advertiser-credit-details.json';
import * as myProfile from '../../../../assets/tempPermissionJson/myprofile.json';
import * as myProfileOp from '../../../../assets/tempPermissionJson/myprofileOp.json';
import * as config from '../../../../assets/tempPermissionJson/config-system.json';
import * as country from '../../../../assets/tempPermissionJson/country-area.json';
import * as profilematch from '../../../../assets/tempPermissionJson/profile-match.json';
import * as operators from '../../../../assets/tempPermissionJson/operator.json';
import * as financials from '../../../../assets/tempPermissionJson/financials.json';
import * as promoplaylist from '../../../../assets/tempPermissionJson/promo-campaign-playlist.json';

import * as menu from '../../../../assets/tempPermissionJson/menu-main.json';
import { Observable, of } from 'rxjs';
import { PermissionData } from 'src/app/models/permission-data';

import * as masteradmin from '../../../../assets/newPermissionJson/MasterAdmin.json';
import * as masteroperator from '../../../../assets/newPermissionJson/MasterOperator.json';
import * as mastersalesmgr from '../../../../assets/newPermissionJson/MasterSalesManager.json';
import * as mastersalesexec from '../../../../assets/newPermissionJson/MasterSalesExecutive.json';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  getAllpermissionData(): Observable<PermissionData[]> {
    let permData: any;
    permData = (masteradmin as any).default;
    return of(permData);
  }

  getAllpermissionDataOperator(): Observable<PermissionData[]> {
    let permData: any;
    permData = (masteroperator as any).default;
    return of(permData);
  }

  getJsonPermissionDataListsByRole(pagename: string, role: string): any {
    if (role.toLowerCase() === "admin")
      return this.getJsonPermissionDataListsAdmin(pagename);
    else if (role.toLowerCase().indexOf("operator") !== -1)
      return this.getJsonPermissionDataListsOperator(pagename);
    else if (role.toLowerCase().indexOf("salesman") !== -1)
      return this.getJsonPermissionDataListsSalesMgr(pagename);
    else if (role.toLowerCase().indexOf("salesex") !== -1)
      return this.getJsonPermissionDataListsSalesExec(pagename);

  }

  getJsonPermissionDataListsAdmin(pagename: string): any {
    let permData: any;
    permData = (masteradmin as any).default;
    let permPageData = permData.find(i => i.pageName === pagename);
    return permPageData;

  }

  getJsonPermissionDataListsOperator(pagename: string): any {
    let permData: any;
    permData = (masteroperator as any).default;
    let permPageData = permData.find(i => i.pageName === pagename);
    return permPageData;
  }


  getJsonPermissionDataListsSalesMgr(pagename: string): any {
    let permData: any;
    permData = (mastersalesmgr as any).default;
    let permPageData = permData.find(i => i.pageName === pagename);
    return permPageData;
  }

  getJsonPermissionDataListsSalesExec(pagename: string): any {
    let permData: any;
    console.log("yes have permission by role");
    permData = (mastersalesexec as any).default;
    let permPageData = permData.find(i => i.pageName === pagename);
    return permPageData;
  }

  // getJsonPermissionDataListsAdminOLD(pagename: string): any {
  //   let permData: any;
  //   let d1 = (advertiserList as any).default;
  //   let d2 = (advertsList as any).default;
  //   let d3 = (ticketList as any).default;
  //   let d4 = (userDetails as any).default;
  //   let d5 = (campaignList as any).default;
  //   let d6 = (defaultRoute as any).default;
  //   let d7 = (myProfile as any).default;
  //   let d8 = (advertisercredit as any).default;
  //   let d9 = (config as any).default;
  //   let da = (country as any).default;
  //   let db = (profilematch as any).default;
  //   let dc = (operators as any).default;
  //   let dd = (financials as any).default;
  //   let de = (promoplaylist as any).default;
  //   let df = (menu as any).default;
  //   permData = d1.concat(d2).concat(d3).concat(d4).concat(d5)
  //     .concat(d6).concat(d7).concat(d8).concat(d9).concat(da)
  //     .concat(db).concat(dc).concat(dd).concat(de).concat(df);

  //   // console.log("Permdata for concat is: ",permData);
  //   // console.log("Permdata pagename : ",pagename);
  //   let permPageData = permData.find(i => i.pageName === pagename);
  //   // console.log("let permPageData = permData.find(i => i.pageName === pagename): ",permPageData);
  //   return permPageData;

  // }



  // // Used to test getting data from user
  // getAllpermissionDataOLD(): Observable<PermissionData[]> {
  //   let permData: any;
  //   let d1 = (advertiserList as any).default;
  //   let d2 = (advertsList as any).default;
  //   let d3 = (ticketList as any).default;
  //   let d4 = (userDetails as any).default;
  //   let d5 = (campaignList as any).default;
  //   let d6 = (defaultRoute as any).default;
  //   let d7 = (myProfile as any).default;
  //   let d8 = (advertisercredit as any).default;
  //   let d9 = (config as any).default;
  //   let da = (country as any).default;
  //   let db = (profilematch as any).default;
  //   let dc = (operators as any).default;
  //   let dd = (financials as any).default;
  //   let de = (promoplaylist as any).default;
  //   let df = (menu as any).default;
  //   permData = d1.concat(d2).concat(d3).concat(d4).concat(d5)
  //     .concat(d6).concat(d7).concat(d8).concat(d9).concat(da)
  //     .concat(db).concat(dc).concat(dd).concat(de).concat(df);

  //   return of(permData);

  // }


  // getJsonPermissionDataListsOperatorOLD(pagename: string): any {
  //   let permData: any;
  //   let d1 = (advertiserListOp as any).default;
  //   let d2 = (advertsListOp as any).default;
  //   let d3 = (ticketListOp as any).default;
  //   let d4 = (myProfileOp as any).default;
  //   let d5 = (campaignListOp as any).default;
  //   let d6 = (defaultRoute as any).default;
  //   let d7 = (campaignDashListOp as any).default;
  //   let df = (menu as any).default;

  //   permData = d1.concat(d2).concat(d3).concat(d4).concat(d5).concat(d6).concat(d7).concat(df);

  //   // console.log("Permdata for concat is: ",permData);
  //   let permPageData = permData.find(i => i.pageName === pagename);

  //   return permPageData;

  // }


  // getAllpermissionDataOperatorOLD(): Observable<PermissionData[]> {
  //   let permData: any;
  //   let d1 = (advertiserListOp as any).default;
  //   let d2 = (advertsListOp as any).default;
  //   let d3 = (ticketListOp as any).default;
  //   let d4 = (myProfileOp as any).default;
  //   let d5 = (campaignListOp as any).default;
  //   let d6 = (defaultRoute as any).default;
  //   let d7 = (campaignDashListOp as any).default;
  //   let df = (menu as any).default;

  //   permData = d1.concat(d2).concat(d3).concat(d4).concat(d5).concat(d6).concat(d7).concat(df);

  //   return of(permData);

  // }


  getDefaultRoute(pagename: string, role: string): any[] {
    // console.log("The passed value of role is: ", role.toLowerCase());
    let permData: any;

    permData = (defaultRoute as any).default;

    let permPageData = permData.find(i => i.pageName === pagename);

    return permPageData;
  }


  // Which columns and their order appearondashboard.
  TablePermissionAssignment(permPageData: any) {
    let displayedColumns: string[] = [];
    permPageData.elements.forEach(element => {
      if (element.visible && element.type == "element")
        displayedColumns.push(element.name);
    });
    // displayedColumns.push('noRecord');
    return displayedColumns;
  }

  // The filter panel on dashboards. Sets their visibility.
  FilterPermissionAssignment(permPageData: any): TableSearchModel {
    let tableSearchModel = new TableSearchModel()
    let searchData = null;
    // Pulls out the data from the page elements that relate to the search bar elements
    searchData = permPageData.elements.filter(j => j.type == "search")

    // has a function on the class that sets the visibility of the filter elements
    tableSearchModel.setFromJSON(searchData);
    //console.log("permPageData assigned values to this.tableSearchModel : ", this.tableSearchModel);
    return tableSearchModel;
  }


}
