import { Injectable } from '@angular/core';
import { uniqBy } from 'lodash';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PopSearchDdService {
  clientSelect: BasicSelectModel[] = [];
  statusSelect: BasicSelectModel[] = [];
  paymentSelect: BasicSelectModel[] = [];
  countrySelect: BasicSelectModel[] = [];
  operatorSelect: BasicSelectModel[] = [];
  typeSelect: BasicSelectModel[] = [];


  popClient(modelSource: any) {
    this.clientSelect = [];
    let uniqueClientData$: any[] = uniqBy(modelSource, 'clientName');
    // console.log("What is in client ",this.uniqueClientData$);

    for (let i = 0; i < uniqueClientData$.length; i++) {
      let tmp = { text: uniqueClientData$[i].clientName, value: uniqueClientData$[i].clientName };
      // console.log("What is in client select ", tmp);
      this.clientSelect.push(tmp);
    }
    return this.clientSelect;
  }

  popStatus(modelSource: any) {
    this.statusSelect = [];
    let uniqueStatusData$: any[] = uniqBy(modelSource, 'rStatus');
    // console.log("What is in status ",uniqueStatusData$);
    for (let i = 0; i < uniqueStatusData$.length; i++) {
      let tmp = { text: uniqueStatusData$[i].rStatus, value: uniqueStatusData$[i].rStatus };
      this.statusSelect.push(tmp);
    }

    return this.statusSelect;
  }

  popPayment(modelSource: any) {
    this.paymentSelect = [];
    let uniqueStatusData$: any[] = uniqBy(modelSource, 'paymentMethod');

    for (let i = 0; i < uniqueStatusData$.length; i++) {
      let tmp = { text: uniqueStatusData$[i].paymentMethod, value: uniqueStatusData$[i].paymentMethod };
      // console.log("What is in PAYMENT status select ", tmp);
      this.paymentSelect.push(tmp);
    }
    return this.paymentSelect;
  }

  popCountry(modelSource: any) {
    this.countrySelect = [];
    let uniqueCountryData$: any[] = uniqBy(modelSource, 'countryName');
    for (let i = 0; i < uniqueCountryData$.length; i++) {
      let tmp = { text: uniqueCountryData$[i].countryName, value: uniqueCountryData$[i].countryName };
      // console.log("What is in status select ", tmp);
      this.countrySelect.push(tmp);
    }
    return this.countrySelect;
  }

  popOperator(modelSource: any) {
    this.operatorSelect = [];
    let uniqueStatusData$: any[] = uniqBy(modelSource, 'operatorName');

    for (let i = 0; i < uniqueStatusData$.length; i++) {
      let tmp = { text: uniqueStatusData$[i].operatorName, value: uniqueStatusData$[i].operatorName };
      // console.log("What is in PAYMENT status select ", tmp);
      this.operatorSelect.push(tmp);
    }
    return this.operatorSelect;
  }

  // Used in Ticket list as a dropdown, using typeSelect as only happens once
  popSubjectType(modelSource: any) {
    this.typeSelect = [];
    let uniqueStatusData$: any[] = uniqBy(modelSource, 'questionSubject');

    for (let i = 0; i < uniqueStatusData$.length; i++) {
      let tmp = { text: uniqueStatusData$[i].questionSubject, value: uniqueStatusData$[i].questionSubject };
      // console.log("What is in PAYMENT status select ", tmp);
      this.typeSelect.push(tmp);
    }
    return this.typeSelect;
  }

}



