import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ReturnResult } from '../../../models/return-result';
import { environment } from 'src/environments/environment';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SelectServicesService {
  retResult: ReturnResult;
  countrySelect: BasicSelectModel[];

  myAppUrl: string;
  controller: string = "sharedlist/";


  constructor(private http: HttpClient) {
    this.myAppUrl = (environment.appUrl + this.controller);
  }

  public getRoleList() {
    let ctrAction = "v1/GetRoleList"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  // Users that sre not subscribers
  public getRealUsersRoles() {
    let ctrAction = "v1/GetUsersnRoles";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getCountryList() {
    let ctrAction = "v1/GetCountryList";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  public getCountryListForAdvertiserSignUp() {
    let ctrAction = "v1/GetCountryListForAdvertiserSignUp";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  public getOperatorList(id: number) {
    if (id == null)
      id = 0;
    let ctrAction = "v1/GetOperatorList/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getCurrencyList() {
    let ctrAction = "v1/GetCurrencyList/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + 0, { observe: "response" });
  }


  public getUserCreditDetailList() {
    let ctrAction = "v1/GetUserDetailCreditList";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getUserPaymentList() {
    let ctrAction = "v1/FillUserPaymentDropdown";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getUserStatusList() {
    let ctrAction = "v1/GetUserStatusList";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getCampaignList(id: number) {
    if (id == null)
      id = 0;
    let ctrAction = "v1/FillCampaignDropdown/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public getUserPermissionList() {
    let ctrAction = "v1/GetUserPermissionList";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  public getOrganisationTypes() {
    let ctrAction = "v1/FillOrganisationTypeDropDown";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getClientList(id: number) {
    if (id == null)
      id = 0;
    console.log("getClientList(id: number) AdvertiserId is: ", id);
    let ctrAction = "v1/GetClientList/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getAdvertCategoryList(id: number) {
    let ctrAction = "v1/FillAdvertCategoryDropDown/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getCampaignCategoryList(id: number) {
    let ctrAction = "v1/FillCampaignCategoryDropDown/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public getPaymentType() {
    let ctrAction = "v1/FillPaymentTypeDropDown";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getTicketStatus() {
    let ctrAction = "v1/GetTicketStatusList";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getTicketSubject() {
    let ctrAction = "v1/GetTicketSubjectList";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  downloadPDF(url: any): Observable<any> {
    return this.http.get(url, { responseType: 'blob' });//.subscribe((res) => {
    // var file = new Blob([res], { type: 'application/pdf' });
    //return URL.createObjectURL(file);
    // return fileURL;
    // window.open(fileURL);
    // })
  }


}
