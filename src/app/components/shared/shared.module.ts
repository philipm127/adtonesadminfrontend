import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';



import { TableSearchComponent } from './table-search/table-search.component';
import { LogTestComponent } from './log-test/log-test.component';
import { SelectServicesService } from './services/select-services.service';
import { AccessServicesService } from './services/access-services.service';
import { LabelControlDirective } from './label-control.directive';
import { PdfModalComponent } from './pdf-modal/pdf-modal.component';
import { PieChartComponent } from '../camapign/campaign-dashboard/pie-chart/pie-chart.component';
import { CustomMaxValidatorDirective } from './custom-max-validator.directive';
import { TwoDigitDecimalNumberDirective } from './directives/two-digit-decimal-number';


@NgModule({
  declarations: [
    TableSearchComponent,
    LogTestComponent,
    LabelControlDirective,
    PdfModalComponent,
    PieChartComponent,
    CustomMaxValidatorDirective,
    TwoDigitDecimalNumberDirective
  ],
  imports: [
    CommonModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    TableSearchComponent,
    LogTestComponent,
    LabelControlDirective,
    PieChartComponent,
    TwoDigitDecimalNumberDirective
  ],
  providers: [
    SelectServicesService,
    AccessServicesService
  ]
})
export class SharedModule { }
