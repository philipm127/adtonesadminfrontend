export class TableSearchModel {
  fullName: boolean = false;
  salesExec: boolean = false;
  name: boolean = false;
  email: boolean = false;
  status: boolean = false;
  typeName: boolean = false;
  country: boolean = false;
  dateRange: boolean = false;
  responseRange: boolean = false;
  numberRange: boolean = false;
  numberRange2: boolean = false;
  numberRange3: boolean = false;
  client: boolean = false;
  operator: boolean = false;
  payment: boolean = false;

  constructor() { }

  // Sets the values of the properties to what is included in the permission object
  setFromJSON(obj: Array<{ name: string, visible: boolean }>) {
    obj.forEach(element => {
      // console.log("setFromJSON(obj: Array name ",element.name);
      // console.log("setFromJSON(obj: Array value ",element.visible);
      this[element.name] = element.visible;
    });
  }

  toString() {
    return `{name:${this.name},fullName:${this.fullName},salesExec:${this.salesExec},email:${this.email},status:${this.status},
    typeName:${this.typeName},country:${this.country},dateRange:${this.dateRange},responseRange:${this.responseRange},
    numberRange:${this.numberRange},numberRange2:${this.numberRange2},numberRange3:${this.numberRange3},
    client:${this.client},operator:${this.operator},payment:${this.payment}`;
  }
}

export class ReportTableSearchModel {
  currency: boolean = false;
  country: boolean = false;
  dateRange: boolean = false;
  operators: boolean = false;

  constructor() { }

  // Sets the values of the properties to what is included in the permission object
  setFromJSON(obj: Array<{ name: string, visible: boolean }>) {
    obj.forEach(element => {
      // console.log("setFromJSON(obj: Array name ",element.name);
      // console.log("setFromJSON(obj: Array value ",element.visible);
      this[element.name] = element.visible;
    });
  }

  toString() {
    return `{currency:${this.currency},country:${this.country},dateRange:${this.dateRange},operators:${this.operators}`;
  }
}
