import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { TableSearchModel } from '../table-search-model';
import { BasicSelectModel } from 'src/app/models/basic-select';



@Component({
  selector: 'app-table-search',
  templateUrl: './table-search.component.html',
  styleUrls: ['./table-search.component.css']
})

export class TableSearchComponent {
  @Input() sharedModel: TableSearchModel;
  @Input() countrySelect: BasicSelectModel[] = [];
  @Input() statusSelect: BasicSelectModel[] = [];
  @Input() typeSelect: BasicSelectModel[] = [];
  @Input() clientSelect: BasicSelectModel[] = [];
  @Input() operatorSelect: BasicSelectModel[] = [];
  @Input() paymentSelect: BasicSelectModel[] = [];

  @Input() fullName: string = 'Users Name';
  @Input() salesExec: string = 'Sales Exec';
  @Input() numberName: string = '';
  @Input() numberName2: string = '';
  @Input() numberName3: string = '';
  @Input() createdDateName: string = 'Created';
  @Input() responseName: string = 'Last Response';
  @Input() miscName: string = 'Name';
  @Input() typeName: string = 'Type';


  @Output() filtered = new EventEmitter<string>();

  minDate = new Date(2013, 1, 1);
  maxDate = new Date();


  formControl: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.formControl = formBuilder.group({
      name: '',
      email: '',
      status: '',
      operator: '',
      country: '',
      typeName: '',
      dateFrom: '',
      dateTo: '',
      numberFrom: '',
      numberTo: '',
      numberFrom2: '',
      numberTo2: '',
      numberFrom3: '',
      numberTo3: '',
      client: '',
      fullName: '',
      salesExec: '',
      payment: '',
      responseFrom: '',
      responseTo: ''
    });


    this.formControl.valueChanges.subscribe(value => {
      Object.keys(value).forEach(key => value[key] === '' ? delete value[key] : key);
      const filter = { ...value } as string;
      this.filtered.emit(filter);
    });


  }

}

