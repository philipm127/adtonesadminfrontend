import { AbstractControl } from '@angular/forms';

export function FileUploadSize(control: AbstractControl,size:number) {
  if (!control.value.startsWith('https') || !control.value.includes('.io')) {
    return { invalidUrl: true };
  }
  return null;
}
