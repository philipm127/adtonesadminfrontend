import { TestBed } from '@angular/core/testing';

import { PopSearchDdService } from '../../../components/shared/services/pop-search-dd.service';

describe('PopSearchDdService', () => {
  let service: PopSearchDdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PopSearchDdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
