import { TestBed } from '@angular/core/testing';

import { LogPublishers } from './log-publishers.service';

describe('LogPublishersService', () => {
  let service: LogPublishers;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LogPublishers);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
