import { TestBed } from '@angular/core/testing';

import { LogPublisher } from '../services/log-publisher.txt';

describe('LogPublishersService', () => {
  let service: LogPublisher;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LogPublisher);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
