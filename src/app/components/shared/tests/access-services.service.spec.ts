import { TestBed } from '@angular/core/testing';

import { AccessServicesService } from '../../../components/shared/services/access-services.service';

describe('AccessServicesService', () => {
  let service: AccessServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccessServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
