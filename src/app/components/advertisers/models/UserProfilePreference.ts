export class UserProfilePreference {
  skizaProfileDto: SkizaProfileDto;
  userProfileDemographicsDto: UserProfileDemographicsDto;
}

export class UserProfileDemographicsDto {
  age_Demographics: string;
  age: string;
  gender_Demographics: string;
  IncomeBracket_Demographics: string;
  WorkingStatus_Demographics: string;
  Education_Demographics: string;
  householdStatus_Demographics: string;
  location_Demographics: string;

}

export class SkizaProfileDto {
  hustlers_AdType: string;
  mass_AdType: string;
  youth_AdType: string;
  discerningProfessionals_AdType: string;
  countryId: number;
}
