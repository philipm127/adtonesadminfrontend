import { DatePipe } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AdvertService } from 'src/app/components/adverts/advert.service';
import { AdvertTableModel } from 'src/app/components/adverts/models/advert-table-model';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';
import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';

@Component({
  selector: 'app-advertisers-advert-table',
  templateUrl: './advertisers-advert-table.component.html',
  styleUrls: ['./advertisers-advert-table.component.css']
})
export class AdvertisersAdvertTableComponent implements AfterViewInit, OnDestroy {
  pagename: string = "AdvertsList";
  permData: any;
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];

  userData: any;
  salesId: number = 0;
  salesExecName: string;
  miscName: string = "Advert Name";
  fullName: string = "Advertiser";
  // Current UserId
  currentUser: number;
  emailLink: boolean = false;
  rejectAdvertId: number;
  rejectReason: string;

  destroy$: Subject<AdvertTableModel> = new Subject();
  isLoading: boolean = true;
  dataSource: MatTableDataSource<AdvertTableModel>;
  modelSource: AdvertTableModel[];

  statusSelect: BasicSelectModel[] = [];
  clientSelect: BasicSelectModel[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  tableSearchModel: TableSearchModel;


  constructor(private advertService: AdvertService,
    private datePipe: DatePipe,
    private popSearch: PopSearchDdService, private accessService: AccessServicesService,
    public dialogo: MatDialog) {
    this.userData = this.accessService.getUser();
    this.salesId = this.userData.userId;

    this.currentUser = this.userData.userId;
    // console.log("The current user is: ", this.currentUser);
    this.PermissionAssignment();
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  ngAfterViewInit() {
    this.loadData();
  }

  loadData() {
    this.advertService.GetAdvertListForAdvertisers(this.userData.userId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        this.isLoading = false;
        console.log("Returned body is: ", retResult);
        if (retResult.result == 1) {
          this.modelSource = <AdvertTableModel[]>retResult.body;
          this.dataSource = new MatTableDataSource<AdvertTableModel>(this.modelSource);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
        this.statusSelect = this.popSearch.popStatus(this.modelSource);
        this.clientSelect = this.popSearch.popClient(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("What is in filters: ",filter);
          // Object.keys(filter).forEach(key => console.log("What is key value: ",this.datePipe.transform(filter[key], 'yyyy-MM-dd')));
          const a = !filter.fullName || data.userName.trim().toLowerCase().includes(filter.fullName.trim().toLowerCase());
          const b = !filter.client || data.clientName.trim().toLowerCase().includes(filter.client.trim().toLowerCase());
          const c = !filter.status || data.rStatus.trim().toLowerCase().includes(filter.status.trim().toLowerCase());
          const d = !filter.dateFrom || data.createdDate >= this.datePipe.transform(filter.dateFrom, 'yyyy-MM-dd');
          const e = !filter.dateTo || data.createdDate <= this.datePipe.transform(filter.dateTo, 'yyyy-MM-dd');
          const f = !filter.name || data.advertName.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          return a && b && c && d && e && f;
        }) as (AdvertElement, string) => boolean;
      });
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    // console.log("In Permission assignment what is value of permPageData: ", permPageData);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    // console.log("this.displayedColumns: ", this.displayedColumns);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }


  updateStatus(Id: number, event: any) {
    console.log("What is the submitted status: ", status);
    let newModel: AdvertTableModel;
    let newStatus = event.target.value;
    for (let i in this.modelSource) {
      if (this.modelSource[i].advertId == Id) {
        this.modelSource[i].prevStatus = this.modelSource[i].status;
        this.modelSource[i].status = newStatus;
        this.modelSource[i].updatedBy = this.userData.userId;
        newModel = this.modelSource[i];
        break;
      }
    }

    console.log("What is the newModel data: ", newModel);
    this.advertService.updateAdvertStatus(newModel)
      .subscribe(resp => {
        let retResultUpdate = resp

        if (retResultUpdate.result == 1) {

          this.loadData();
          console.log("returned result retResultUpdate: ", retResultUpdate.result);
        }
      });
    console.log("What is status after update: ", this.modelSource);
  }

  mostrarDialogo(advertId:number): void {
    this.dialogo
      .open(ConfirmationModalComponent)
      .afterClosed()
      .subscribe((confirmado: Boolean) => {
        if (confirmado) {
          alert("Advert Deleted");
        }
      });
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}

