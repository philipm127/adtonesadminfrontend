import { HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AdvertService } from 'src/app/components/adverts/advert.service';
import { ReturnResult } from 'src/app/models/return-result';

@Component({
  selector: 'app-advert-dashboard',
  templateUrl: './advert-dashboard.component.html',
  styleUrls: ['./advert-dashboard.component.css']
})
export class AdvertDashboardComponent implements AfterViewInit {
  @Input() advertiserId: number;
  NoOfPlays: number;
  AvgPlayTime: number;
  NoOfSMS: number;
  NoOfEmail: number;
  destroy$: Subject<number> = new Subject();

  constructor(private advertService: AdvertService) { }

  ngAfterViewInit() {
    this.loadData();
  }

  loadData() {
    // console.log("Advertiser ID is ",this.advertiserId)
    this.advertService.GetAdvertListForAdvertisersSummary(this.advertiserId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        // console.log("Returned body is: ", retResult);
        if (retResult.result == 1) {
          let dash = retResult.body;
          // console.log("retResult.body ",dash)
          this.NoOfPlays = dash['totalPlays'];
          this.NoOfSMS = dash['totalSMS'];
          this.NoOfEmail = dash['totalEmail'];
          this.AvgPlayTime = dash['avgPlayLength'];
        }
      });
  }

}
