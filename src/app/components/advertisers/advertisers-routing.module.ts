import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StepThreeComponent } from '../campaign-create-edit/createcampaign/step-three-component/step-three-component';
// import { SalesCampaignDashboardComponent } from '../sales-management/campaign/sales-campaign-dashboard/sales-campaign-dashboard.component';
import { AdvertisersAdvertTableComponent } from './advert/advertisers-advert-table/advertisers-advert-table.component';
import { CampaignManagementComponent } from './campaign/campaign-management/campaign-management.component';
import { AdvertiserInvoiceTableComponent } from './finances/advertiser-invoice-table/advertiser-invoice-table.component';
import { ConfirmRegistrationComponent } from './signup/confirm-registration/confirm-registration.component';
import { SignupComponent } from './signup/signup.component';
import { VerifyEmailComponent } from './signup/verify-email/verify-email.component';
import { SubscriberProfileComponent } from './subscriberprofile/subscriber-profile/subscriber-profile.component';
import { AdvertiserTicketTableComponent } from './tickets/advertiser-ticket-table/advertiser-ticket-table.component';

const routes: Routes = [
  //{ path: 'advertisertickettable/:id/:name', component: AdvertiserTicketTableComponent },
  { path: 'advertisertickettable', component: AdvertiserTicketTableComponent },
  //{ path: 'advertiseradverttable/:id/:name', component: AdvertisersAdvertTableComponent },
  //{ path: 'advertisercampaign', component: CampaignManagementComponent },
  { path: 'advertiserbilling', component: AdvertiserInvoiceTableComponent },
  { path: 'advertiseradverttable', component: AdvertisersAdvertTableComponent },
  { path: 'advertisereditadvert/:id/:camp/:country', component: StepThreeComponent},
  { path: 'subscriberprofile/:id/:camp', component: SubscriberProfileComponent},
  { path: 'advertisersignup', component: SignupComponent},
  { path: 'confirmsignup', component: ConfirmRegistrationComponent},
  { path: 'VerifyAdvertiser/:activationCode', component: VerifyEmailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdvertisersRoutingModule { }
