import { HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ProfileMatchService } from 'src/app/components/profilematch/profile-match.service';
import { ReturnResult } from 'src/app/models/return-result';
import { SkizaProfileDto, UserProfileDemographicsDto, UserProfilePreference } from '../../models/UserProfilePreference';

@Component({
  selector: 'app-subscriber-profile',
  templateUrl: './subscriber-profile.component.html',
  styleUrls: ['./subscriber-profile.component.css']
})
export class SubscriberProfileComponent implements OnInit {
  campId: any;
  userId: any;
  profilePreference: UserProfilePreference;
  skizaProfile: SkizaProfileDto;
  demoProfile: UserProfileDemographicsDto;

  constructor(private profileService: ProfileMatchService, private route: ActivatedRoute,
    private router: Router) {
      this.userId = this.route.snapshot.paramMap.get('id');
      this.campId = this.route.snapshot.paramMap.get('camp');
     }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    console.log("user ",this.userId);
    console.log("camp ",this.campId);
    let params = { 'id': this.campId, 'userid': this.userId }
    this.profileService.getUserPreferences(params)
    .subscribe(resp => {
        let retResult = <ReturnResult>resp;
        console.log("retResult ",retResult.result)
        if (retResult.result == 1) {
          let model = <UserProfilePreference>retResult.body;
          console.log("model ",model)
          this.demoProfile = <UserProfileDemographicsDto>model.userProfileDemographicsDto;
          this.skizaProfile = <SkizaProfileDto>model.skizaProfileDto;
          console.log("this.demoProfile ",this.demoProfile)
          console.log("his.skizaProfile ",this.skizaProfile)
        }
      });
  }

}
