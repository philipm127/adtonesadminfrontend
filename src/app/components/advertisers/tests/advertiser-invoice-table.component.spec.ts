import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertiserInvoiceTableComponent } from '../finances/advertiser-invoice-table/advertiser-invoice-table.component';

describe('AdvertiserInvoiceTableComponent', () => {
  let component: AdvertiserInvoiceTableComponent;
  let fixture: ComponentFixture<AdvertiserInvoiceTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvertiserInvoiceTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserInvoiceTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
