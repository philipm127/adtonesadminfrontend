import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertisersAdvertTableComponent } from '../advert/advertisers-advert-table/advertisers-advert-table.component';

describe('AdvertisersAdvertTableComponent', () => {
  let component: AdvertisersAdvertTableComponent;
  let fixture: ComponentFixture<AdvertisersAdvertTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvertisersAdvertTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertisersAdvertTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
