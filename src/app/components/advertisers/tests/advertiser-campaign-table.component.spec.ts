import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertiserCampaignTableComponent } from '../campaign/advertiser-campaign-table/advertiser-campaign-table.component';

describe('AdvertiserCampaignTableComponent', () => {
  let component: AdvertiserCampaignTableComponent;
  let fixture: ComponentFixture<AdvertiserCampaignTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvertiserCampaignTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserCampaignTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
