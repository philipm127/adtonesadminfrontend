import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertiserTicketTableComponent } from '../tickets/advertiser-ticket-table/advertiser-ticket-table.component';

describe('AdvertiserTicketTableComponent', () => {
  let component: AdvertiserTicketTableComponent;
  let fixture: ComponentFixture<AdvertiserTicketTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvertiserTicketTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserTicketTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
