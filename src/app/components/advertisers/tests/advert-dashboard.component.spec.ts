import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertDashboardComponent } from '../advert/advert-dashboard/advert-dashboard.component';

describe('AdvertDashboardComponent', () => {
  let component: AdvertDashboardComponent;
  let fixture: ComponentFixture<AdvertDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvertDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
