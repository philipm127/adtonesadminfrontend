import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdvertisersRoutingModule } from './advertisers-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatTableExporterModule } from 'mat-table-exporter';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { SharedModule } from '../shared/shared.module';
import { CampaignDashboardComponent } from './campaign/campaign-dashboard/campaign-dashboard.component';
import { CampaignManagementComponent } from './campaign/campaign-management/campaign-management.component';
import { AdvertiserCampaignTableComponent } from './campaign/advertiser-campaign-table/advertiser-campaign-table.component';
import { AdvertisersAdvertTableComponent } from './advert/advertisers-advert-table/advertisers-advert-table.component';
import { AdvertiserTicketTableComponent } from './tickets/advertiser-ticket-table/advertiser-ticket-table.component';
import { ConfirmationModalComponent } from './advert/confirmation-modal/confirmation-modal.component';
import { AdvertDashboardComponent } from './advert/advert-dashboard/advert-dashboard.component';
import { CreditCardPaymentComponent } from './payments/credit-card-payment/credit-card-payment.component';
import { SubscriberProfileComponent } from './subscriberprofile/subscriber-profile/subscriber-profile.component';
import { AdvertiserInvoiceTableComponent } from './finances/advertiser-invoice-table/advertiser-invoice-table.component';
import { SignupComponent } from './signup/signup.component';
import { ConfirmRegistrationComponent } from './signup/confirm-registration/confirm-registration.component';
import { VerifyEmailComponent } from './signup/verify-email/verify-email.component';
import { TableTemplateComponent } from './table-template/table-template.component';
import {DataPropertyGetterPipe} from './table-template/data-property-getter-pipe/data-property-getter.pipe';
import { OrdersComponent } from './orders/orders.component';


@NgModule({
  declarations: [
    CampaignDashboardComponent,
    CampaignManagementComponent,
    AdvertiserCampaignTableComponent,
    AdvertisersAdvertTableComponent,
    AdvertiserTicketTableComponent,
    ConfirmationModalComponent,
    AdvertDashboardComponent,
    CreditCardPaymentComponent,
    SubscriberProfileComponent,
    AdvertiserInvoiceTableComponent,
    SignupComponent,
    ConfirmRegistrationComponent,
    VerifyEmailComponent,
    DataPropertyGetterPipe,
    TableTemplateComponent,
    OrdersComponent
  ],
  imports: [
    CommonModule,
    AdvertisersRoutingModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule,
    SharedModule
  ]
})
export class AdvertisersModule { }
