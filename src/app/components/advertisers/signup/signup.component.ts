import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';
import { PermissionManagementService } from '../../permission-management/permission-management.service';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { AdvertiserUser } from '../../usermanagement/models/users-applied-models';
import { MyErrorStateMatcher } from '../../usermanagement/userdetails/update-password/update-password.component';
import { NewUserModel } from '../../usermanagement/models/new-user-model';
import { UmanagementService } from '../../usermanagement/umanagement.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  pagename: string = "AdvertiserSignUp"
  roleId: any = 0;
  matcher = new MyErrorStateMatcher();
  isValidFormSubmitted = null;
  form: FormGroup;


  userModel: AdvertiserUser;
  organisationSelect: BasicSelectModel[];
  countrySelect: BasicSelectModel[];

  destroy$: Subject<BasicSelectModel> = new Subject();

  isSubmitted: boolean = false;
  successMessage: string;
  failMessage: string;


  constructor(private fb: FormBuilder, private userService: UmanagementService,
    private listService: SelectServicesService, private route: ActivatedRoute, private router: Router) {

    this.form = this.fb.group({
      email: new FormControl("", Validators.required),
      firstName: new FormControl("", Validators.required),
      lastName: new FormControl("", Validators.required),
      organisation: new FormControl("", Validators.required),
      organisationTypeId: new FormControl("0"),
      mailSuppression: [true],
      roleId: new FormControl(3),
      userId: new FormControl(0),
      phoneNumber: new FormControl(""),
      countryId: new FormControl("", Validators.required),
      password: ['', [Validators.required,
      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&^])[A-Za-z\d$@$!%*?&].{7,}')
      ]
      ],
      confirmPassword: new FormControl("")
    }, { validator: this.checkPasswords });
  }


  ngOnInit() {
    this.getCountry();
    this.getOrganisationTypes();
    // this.form.get('roleId').disable();//this.userModel.roleId.toString());
  }

  get password() {
    return this.form.get('password');
  }

  getOrganisationTypes() {
    this.listService.getOrganisationTypes()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {
          // console.log("returned result from get role list: ", this.retResult);
          this.organisationSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }


  getCountry() {
    this.listService.getCountryListForAdvertiserSignUp()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {
          this.countrySelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }


  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true }
  }

  onSubmit() {
    this.isSubmitted = true;
    this.successMessage = "";
    this.failMessage = "";

    this.isValidFormSubmitted = false;
    if (this.form.valid) {
      this.isValidFormSubmitted = true;

      // console.log("What is this form data: ", this.form.getRawValue());
      let fm = this.form.getRawValue();
      let newModel = new NewUserModel();
      newModel.firstName = fm.firstName;
      newModel.lastName = fm.lastName;
      newModel.email = fm.email;
      newModel.organisation = fm.organisation;
      newModel.organisationTypeId = fm.organisationTypeId;
      newModel.countryId = parseInt(fm.countryId);
      newModel.mobileNumber = fm.phoneNumber;
      newModel.password = fm.password;
      newModel.roleId = 3;
      newModel.operatorId = 0;
      newModel.mailSuppression = false;
      // console.log("what is new model: ", newModel);
      this.userService.newAdvertiserSignUp(newModel)
        .subscribe(resp => {
          let retModel = resp

          if (retModel.result == 1) {
            this.successMessage = retModel.body.toString();
          }
          else {
            let erMsg = retModel.body == null ? retModel.error : retModel.body;
            this.failMessage = "There was a problem updating the details " + erMsg;
            this.isSubmitted = false;
          }
        });
    } else {
      return;
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}

