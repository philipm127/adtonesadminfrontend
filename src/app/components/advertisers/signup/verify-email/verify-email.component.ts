import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UmanagementService } from 'src/app/components/usermanagement/umanagement.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent implements OnInit {
failed:boolean = false;
success:boolean = false;
isSubmitted: boolean = false;
  successMessage: string;
  failMessage: string;
email:string;

  constructor(private route: ActivatedRoute, private router: Router, private userService: UmanagementService) {
    this.email = this.route.snapshot.paramMap.get('activationCode');
   }

  ngOnInit(): void {
    console.log("email ",this.email);
    this.success = true;
    this.userService.verifyEmail(this.email)
        .subscribe(resp => {
          let retModel = resp

          if (retModel.result == 1) {
            this.successMessage = retModel.body.toString();
          }
          else {
            let erMsg = retModel.body == null ? retModel.error : retModel.body;
            this.failMessage = "There was a problem updating the details " + erMsg;
            this.isSubmitted = false;
          }
        });
  }

}
