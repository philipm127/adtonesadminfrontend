import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';

@Component({
  selector: 'app-campaign-management',
  templateUrl: './campaign-management.component.html',
  styleUrls: ['./campaign-management.component.css']
})
export class CampaignManagementComponent implements OnInit {
  advertiserId: number = 0;
  advertiserName: string;
  userData: any;

  constructor(private route: ActivatedRoute, private accessService: AccessServicesService) {
    this.userData = this.accessService.getUser();
    let sid = this.route.snapshot.paramMap.get('id');
    this.advertiserId = this.userData.userId;
    // console.log("What is after  this.route.snapshot.paramMap.get('id'): ", this.salesId);
    this.advertiserName = this.route.snapshot.paramMap.get('name');
  }

  ngOnInit(): void {
  }

}
