import { HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CampaignService } from 'src/app/components/camapign/campaign.service';
import { CampaignAdminResult } from 'src/app/components/camapign/models/campaign-table-model';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { IMessage, MessageService } from 'src/app/components/shared/services/message.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { CurrencyEnum } from 'src/app/models/currencyEnum';
import { ReturnResult } from 'src/app/models/return-result';

@Component({
  selector: 'app-advertiser-campaign-table',
  templateUrl: './advertiser-campaign-table.component.html',
  styleUrls: ['./advertiser-campaign-table.component.css']
})
export class AdvertiserCampaignTableComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() advertiserId: number = 0;
  pagename: string = "CampaignList";
  permData: any;
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];
  userData: any;
  miscName: string = "Campaign";
  numberName: string = "Budget";
  numberName2: string = "Spend";
  typeName: string = "Advert";

  campaignLink: boolean = false;
  advertLink: boolean;

  // Dashboard for Operator
  dashboardType: string = "operator";
  operatorId: string = "0";

  destroy$: Subject<CampaignAdminResult> = new Subject();
  dataSource: MatTableDataSource<CampaignAdminResult>;
  modelSource: CampaignAdminResult[];
  isLoading: boolean = true;
  retResult: ReturnResult = null;

  statusSelect: BasicSelectModel[] = [];
  clientSelect: BasicSelectModel[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  filterValues = {};


  tableSearchModel: TableSearchModel;
  currencySymbol = CurrencyEnum;

  messages: IMessage[] = [];
  subscription: Subscription;
  successMessage: string;


  constructor(private campaignService: CampaignService,
    private popSearch: PopSearchDdService, private accessService: AccessServicesService, private router: Router,
    private messageService: MessageService) {
    this.userData = this.accessService.getUser();

  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }


  ngOnInit() {
    this.subscription = this.messageService.getMessage().subscribe(message => {
      if (message) {
        this.messages.push(message.data);
        if (this.messages[0] != undefined) {
          console.log("this.messages.push(message.data); ", this.messages[0].text);
          this.successMessage = this.messages[0].text;
        }
      } else {
        // clear messages when empty message received
        this.messages = [];
      }
    });
    this.PermissionAssignment();
  }


  ngAfterViewInit() {
    let campaignData = null;
    if (this.advertiserId == null)
      this.advertiserId = 0;
    //campaignData = this.campaignService.getCampaignList("0");
    //else
    // campaignData = this.campaignService.getCampaignListForAdvertiser(this.advertiserId);

    campaignData
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.isLoading = false;
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <CampaignAdminResult[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<CampaignAdminResult>(this.modelSource);
          console.log("Campaign Data returned is: ", this.modelSource);
        }
        this.statusSelect = this.popSearch.popStatus(this.modelSource);
        this.clientSelect = this.popSearch.popClient(this.modelSource);
      }, () => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this.dataSource.filterPredicate = ((data, filter) => {
          console.log("What is in filters: ", filter);
          // Object.keys(filter).forEach(key => console.log("What is key value: ",this.datePipe.transform(filter[key], 'yyyy-MM-dd')));
          const b = !filter.client || data.clientName.trim().toLowerCase().includes(filter.client.trim().toLowerCase());
          const c = !filter.status || data.rStatus.trim().toLowerCase().includes(filter.status.trim().toLowerCase());
          const f = !filter.name || data.campaignName.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          const i = !filter.numberFrom || data.totalBudget >= parseInt(filter.numberFrom);
          const j = !filter.numberTo || data.totalBudget <= parseInt(filter.numberTo);
          const k = !filter.numberFrom2 || data.totalSpend >= parseInt(filter.numberFrom2);
          const l = !filter.numberTo2 || data.totalSpend <= parseInt(filter.numberTo2);
          return b && c && f && i && j && k && l;
        }) as (CampaignElement, string) => boolean;

      });
  }


  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    // console.log("In Permission assignment what is value of permPageData: ",permPageData);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
    let campFind = permPageData["elements"].filter(j => j.name == "campaignName" && j.type == "element");
    this.campaignLink = campFind[0].enabled;
    let adFind = permPageData["elements"].filter(j => j.name == "advertName" && j.type == "element");
    this.advertLink = adFind[0].enabled;
    // console.log("what is this.emailLink: ", this.emailLink);
  }


  public updateStatus(Id: number, event: any) {
    console.log("What is UpdateCampaignStatus ID: ", Id);
    console.log("What is UpdateCampaignStatus Status: ", event.target.value);
    let status: number = event.target.value;
    for (let i in this.modelSource) {
      if (this.modelSource[i].campaignProfileId == Id) {
        this.modelSource[i].status = status;
        // console.log("What is userData for this id: ", this.modelSource[i]);
        break;
      }
    }

    let newModel = {
      id: Id,
      status: status
    }

    // this.campaignService.updateCampaignStatus(newModel)
    //   .subscribe(resp => {
    //     let retResultUpdate = resp

    //     if (retResultUpdate.result == 1) {
    //       console.log("returned result retResultUpdate: ", retResultUpdate.result);
    //     }
    //   });
    console.log("What is status after update: ", this.modelSource);
  }


  fundCampaign(campId: number, userId: number) {
    this.router.navigate(['buycredit/' + campId + '/' + userId]);
  }

  updateCampaign(userId: number, campid: number) {
    this.router.navigate(['updatecampaign/' + userId + '/' + campid]);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
