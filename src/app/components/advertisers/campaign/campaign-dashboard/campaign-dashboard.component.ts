import { HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CampaignService } from 'src/app/components/camapign/campaign.service';
import { CampaignDashboardAuditModel } from 'src/app/components/camapign/models/campaign-dashboard-audit-model';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { ReturnResult } from 'src/app/models/return-result';

@Component({
  selector: 'app-campaign-dashboard',
  templateUrl: './campaign-dashboard.component.html',
  styleUrls: ['./campaign-dashboard.component.css']
})
export class CampaignDashboardComponent implements AfterViewInit {
  pagename: string = "CampaignDashboardAudit";
  permData: any;
  @Input() advertiserName: string;
  retResult: ReturnResult = null;
  @Input() advertiserId: number = 0;
  @Input() campaignId: number = 0;
  destroy$: Subject<CampaignDashboardAuditModel> = new Subject();
  modelSource: CampaignDashboardAuditModel;
  isLoading: boolean = true;
  playData: number[] = [];
  budgetData: number[] = [];
  reachData: number[] = [];


  constructor(private campaignService: CampaignService, private accessService: AccessServicesService) { }


  ngAfterViewInit() {
    let campaignData: any = null;
      campaignData = this.campaignService.getCampaignDashboardByAdvertiser(this.campaignId);

    campaignData
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.isLoading = false;
        // console.log("Returned res result for campaign audit dashboard is: ", res);
        this.retResult = res.body;
        // console.log("Returned body for campaign audit dashboard is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <CampaignDashboardAuditModel>this.retResult.body;
          // console.log("campaign audit this.modelSource is: ", this.modelSource);
          this.playData.push(this.modelSource.freePlays);
          this.playData.push((this.modelSource.totalPlayed - this.modelSource.freePlays))

          this.budgetData.push(this.modelSource.totalSpend);
          this.budgetData.push((this.modelSource.totalBudget - this.modelSource.totalSpend))

          this.reachData.push(this.modelSource.reach);
          this.reachData.push((this.modelSource.totalReach - this.modelSource.reach))

        }
      });

  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
