import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {TableColumn} from "./TableColumn";
import {MatSort, Sort} from "@angular/material/sort";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
/*
Column interface
First of all, we have to define the column interface, in order to pass
the column information to the table from a parent component:
--  export interface TableColumn {--

Table
Secondly, we create our reusable table component with ng generate component table command, and define configurable input and output paramaters in the .ts file, which are being passed from parent component.
Input from the parent component:
isPageable: should the table have pagination?
isSortable: should the table have sorting?
isFilterable: should there be a filter, operating on the whole table?
tableColumns: columns of type interface TableColumn👆🏻 with parameters (name, dataKey, position, isSortable)
rowActionIcon: name of the mat-icon, that is going to be embedded in each row.
paginationSizes: options for rows number on a page
defaultPageSize: default number of rows on a page
tableData: the actual data, which must have a setter, to dynamically get changes of the from parent component (e.g. after sorting it in parent). If we want to apply pipes or services on this data, we should do it in the parent component beforehand, and pass the “ready to display” data in the table.
Output events to the parent component:
sort: Outputs the key to sort with, and the direction(ascending, descending). The sorting logic must be implemented in the parent component, in order to keep the table abstract and simple. Moreover, sorting of different datatypes, e.g. Date and string has different logic, and it is impossible to foresee all sorting strategies in the table.
rowAction: Outputs the whole row, when a user clicks on the action-icon in the given row.


Table template
Thirdly, we define a template for the table component,
and make almost everything optional with *ngIf directive,
for instance: filter, row-action, sorting and pagination:


Example
We have created customers-table and orders-table components, which use our generic table component and pass data to it.
customers-table component only uses basic table features, therefore it only passes the data array and columns definition to the table.

<custom-table
  [tableData]="customers"
  [tableColumns]="customersTableColumns">
</custom-table>

orders-table uses all features of table and explicitly passes all optional parameters,
to turn on the pagination, filtering and sorting.

*/

@Component({
  selector: 'app-table-template',
  templateUrl: './table-template.component.html',
  styleUrls: ['./table-template.component.css']
})
export class TableTemplateComponent implements OnInit, AfterViewInit {

  public tableDataSource = new MatTableDataSource([]);
  public displayedColumns: string[];
  @ViewChild(MatPaginator, {static: false}) matPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) matSort: MatSort;

  @Input() isPageable = false;
  @Input() isSortable = false;
  @Input() isFilterable = false;
  @Input() tableColumns: TableColumn[];
  @Input() rowActionIcon: string;
  @Input() paginationSizes: number[] = [5, 10, 15];
  @Input() defaultPageSize = this.paginationSizes[1];

  @Output() sort: EventEmitter<Sort> = new EventEmitter();
  @Output() rowAction: EventEmitter<any> = new EventEmitter<any>();

  // this property needs to have a setter, to dynamically get changes from parent component
  @Input() set tableData(data: any[]) {
    this.setTableDataSource(data);
  }

  constructor() {
  }

  ngOnInit(): void {
    const columnNames = this.tableColumns.map((tableColumn: TableColumn) => tableColumn.name);
    if (this.rowActionIcon) {
      this.displayedColumns = [this.rowActionIcon, ...columnNames]
    } else {
      this.displayedColumns = columnNames;
    }
  }

  // we need this, in order to make pagination work with *ngIf
  ngAfterViewInit(): void {
    this.tableDataSource.paginator = this.matPaginator;
  }


  setTableDataSource(data: any) {
    this.tableDataSource = new MatTableDataSource<any>(data);
    this.tableDataSource.paginator = this.matPaginator;
    this.tableDataSource.sort = this.matSort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableDataSource.filter = filterValue.trim().toLowerCase();
  }

  sortTable(sortParameters: Sort) {
    // defining name of data property, to sort by, instead of column name
    sortParameters.active = this.tableColumns.find(column => column.name === sortParameters.active).dataKey;
    this.sort.emit(sortParameters);
  }

  emitRowAction(row: any) {
    this.rowAction.emit(row);
  }

}
