import { Component, OnInit, Input } from '@angular/core';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';
import { UmanagementService } from '../../umanagement.service';
import { PermissionService } from 'src/app/components/shared/services/permission-data.service';
import { Observable } from 'rxjs';
import { PermissionData, permissionList } from 'src/app/models/permission-data';
import { PermissionManagementService } from 'src/app/components/permission-management/permission-management.service';
import { ReturnResult } from 'src/app/models/return-result';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-permission',
  templateUrl: './user-permission.component.html',
  styleUrls: ['./user-permission.component.css']
})

export class UserPermissionComponent implements OnInit {
  pagename: string = "EditUserPermission";
  @Input() userId: number;
  pageVisible: boolean = false;

  constructor(private accessService: AccessServicesService, private perm: PermissionManagementService,
    private permService: PermissionService, private router: Router) {
  }


  ngOnInit(): void {
    let permListData = this.accessService.getPermissionList(this.pagename);
    if (permListData.visible)
      this.pageVisible = true;
  }


  onSubmit() {
    this.router.navigate(['userpermission/' + this.userId]);
  }

}
