import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { FormService } from 'src/app/components/dynamic-forms/form.service';
import { CompanyinfoModel } from 'src/app/components/usermanagement/models/companyinfo-model';
import { ReturnResult } from 'src/app/models/return-result';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { UmanagementService } from 'src/app/components/usermanagement/umanagement.service';
import { SelectServicesService } from 'src/app/components/shared/services/select-services.service';
import { PermissionService } from 'src/app/components/shared/services/permission-data.service';
import { TokenStorageService } from 'src/app/services/auth/token-storage.service';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';

@Component({
  selector: 'app-company-info',
  templateUrl: './company-info.component.html',
  styleUrls: ['./company-info.component.css']
})
export class CompanyInfoComponent implements OnInit {

  @Input() pagename: string = "CompanyEdit";
  @Input() userId: number;
  form: FormGroup;

  companynameVisible: boolean = true;
  addressVisible: boolean = true;
  additionalAddressVisible: boolean = true;
  countryVisible: boolean = true;
  townVisible: boolean = true;
  postCodeVisible: boolean = true;

  successMessage: string;
  failMessage: string;

  userData: any;
  userModel: any;
  retModel: ReturnResult;
  retResult: ReturnResult;
  countrySelect: BasicSelectModel[];

  destroy$: Subject<BasicSelectModel> = new Subject();

  constructor(private formBuilder: FormBuilder, private userService: UmanagementService,
    private listService: SelectServicesService, private accessService: AccessServicesService, private tokenStorage: TokenStorageService) {
    this.userData = this.tokenStorage.getUser();

    this.form = new FormGroup({
      companyName: new FormControl("", Validators.required),
      address: new FormControl("", Validators.required),
      additionalAddress: new FormControl(""),
      town: new FormControl(""),
      postCode: new FormControl(""),
      countryId: new FormControl(""),
      userId: new FormControl(this.userId),
      id: new FormControl(0)
    });

    let permListData = this.accessService.getPermissionList(this.pagename);
    permListData["elements"].forEach(element => {
      if (element.name == this.getName(this.form.get(element.name))) {
        this[element.name.concat('Visible')] = element.visible;
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
    });
  }

  ngOnInit(): void {
    this.userService.getCompanyInfo(this.userId)
      .subscribe(
        data => {
          this.retModel = data;
          if (this.retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <CompanyinfoModel>this.retModel.body;
            // this.selectedValue = this.userModel.roleId.toString();
            this.getCountry();
            this.initForm();
            // console.log("returned userModel: ", this.userModel);
          }
        });
  }

  getCountry() {
    this.listService.getCountryList()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        console.log("returned result from get country list: ", this.retResult);
        if (this.retResult.result == 1) {

          this.countrySelect = <BasicSelectModel[]>this.retResult.body;
        }
        else
          this.countrySelect = <BasicSelectModel[]>this.retResult.body;
      });
  }


  private initForm() {
    // if (this.editMode) {
    if (this.userModel != null && this.userModel.id > 0) {
      this.form.patchValue({
        companyName: this.userModel.companyName,
        address: this.userModel.address,
        additionalAddress: this.userModel.additionalAddress,
        town: this.userModel.town,
        postCode: this.userModel.postCode,
        userId: this.userId,
        id: this.userModel.id
      });
      this.form.get('countryId').setValue(this.userModel.countryId.toString());
    }
    else
      this.form.get('userId').setValue(this.userId);
    // }
  }


  private getName(control: AbstractControl): string | null {
    let group = <FormGroup>control.parent;
    if (!group) {
      return null;
    }

    let name: string;
    Object.keys(group.controls).forEach(key => {
      let childControl = group.get(key);

      if (childControl !== control) {
        return;
      }
      name = key;
    });
    return name;
  }

  onSubmit() {
    // this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log("What is this form data: ", this.form.getRawValue());
    this.userService.updateCompanyInfo(this.form.getRawValue())
      .subscribe(resp => {
        this.retModel = resp

        if (this.retModel.result == 1) {
          this.successMessage = "Company details Updated Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the company details";
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
