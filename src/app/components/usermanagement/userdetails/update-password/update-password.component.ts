import { Component, Input } from '@angular/core';
import { ErrorStateMatcher } from '@angular/material/core';
import { FormControl, FormGroupDirective, NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/components/Authentication/authentication.service';
import { ReturnResult } from 'src/app/models/return-result';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}


@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css']
})
export class UpdatePasswordComponent {
  pagename: string = "UpdatePassword";

  @Input() userId: number;
  form: FormGroup;

  matcher = new MyErrorStateMatcher();
  isValidFormSubmitted = null;
  retModel: ReturnResult;
  successMessage: string;
  failMessage: string;

  constructor(private formBuilder: FormBuilder, private loginService: AuthenticationService) {
    this.form = this.formBuilder.group({
      oldPassword: ["", [Validators.required]],
      password: ['', [Validators.required,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&^])[A-Za-z\d$@$!%*?&].{7,}')
      ]
      ],
      confirmPassword: ['']
    }, { validator: this.checkPasswords });

  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true }
  }

  get password() {
    return this.form.get('password');
  }


  onSubmit() {
    this.failMessage = "";
    this.successMessage = "";
    this.isValidFormSubmitted = false;
    if (this.form.valid) {
      this.isValidFormSubmitted = true;

      console.log("What is this form data: ", this.form.value);
      let frm = this.form.value;
      console.log("What is this form data by element: ", frm.oldPassword);
      let newpwd: any = {
        oldPassword: frm.oldPassword,
        newPassword: frm.password,
        userId: this.userId
      };

      this.form.patchValue({
        oldPassword: "",
        password: "",
        confirmPassword: ""
      })

      this.loginService.ChangePassword(newpwd)
        .subscribe(resp => {
          this.retModel = resp

          if (this.retModel.result == 1) {
            this.successMessage = "The Password Was Changed Successfully";
          }
          else {
            this.failMessage = this.retModel.error;
          }
        });
    } else {
      return;
    }
  }

}
