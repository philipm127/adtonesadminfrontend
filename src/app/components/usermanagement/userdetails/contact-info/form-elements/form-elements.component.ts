import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBase } from 'src/app/components/dynamic-forms/form-base/form-base';

@Component({
  selector: 'app-form-elements',
  templateUrl: './form-elements.component.html',
  styleUrls: ['./form-elements.component.css']
})
export class FormElementsComponent {

  @Input() control: FormBase<string>;
  @Input() form: FormGroup;

  get isValid() { return this.form.controls[this.control.key].valid; }

}
