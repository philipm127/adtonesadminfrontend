import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ReturnResult } from 'src/app/models/return-result';
import { UmanagementService } from 'src/app/components/usermanagement/umanagement.service';
import { ContactinfoModel } from 'src/app/components/usermanagement/models/contactinfo-model';
import { PermissionData } from 'src/app/models/permission-data';
import { PermissionService } from 'src/app/components/shared/services/permission-data.service';
import { Subject } from 'rxjs';
import { TokenStorageService } from 'src/app/services/auth/token-storage.service';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';



@Component({
  selector: 'app-contact-info',
  templateUrl: './contact-info.component.html',
  styleUrls: ['./contact-info.component.css']

})
export class ContactInfoComponent implements OnInit {
  @Input() pagename: string = "ContactEdit";
  @Input() userId: number;
  form = new FormGroup({
    email: new FormControl("", Validators.required),
    mobileNumber: new FormControl("", Validators.required),
    fixedLine: new FormControl(""),
    phoneNumber: new FormControl(""),
    address: new FormControl(""),
    id: new FormControl(""),
    countryId: new FormControl()
  });

  emailVisible: boolean = true;
  mobileNumberVisible: boolean = true;
  fixedLineVisible: boolean = true;
  phoneNumberVisible: boolean = true;
  addressVisible: boolean = true;

  successMessage:string;
  failMessage:string;

  userData: any;
  editMode: boolean = true;
  userModel: ContactinfoModel;
  retModel: ReturnResult;

  contactData: PermissionData[];
  destroy$: Subject<ContactinfoModel> = new Subject();


  constructor(private formBuilder: FormBuilder, private userService: UmanagementService,
    private accessService: AccessServicesService, private tokenStorage: TokenStorageService) {
    this.userData = this.tokenStorage.getUser();

  }

  private initForm() {

    if (this.editMode && this.userModel != null) {
      console.log("What is this model: ",this.userModel);
      this.form.patchValue({
        mobileNumber: this.userModel.mobileNumber,
        fixedLine: this.userModel.fixedLine,
        email: this.userModel.email,
        phoneNumber: this.userModel.phoneNumber,
        address: this.userModel.address,
        id: this.userModel.id,
        countryId: this.userModel.countryId
      });

    }
  }



  ngOnInit() {
    console.log("the pagename is: ", this.pagename);
    let permListData = this.accessService.getPermissionList(this.pagename);
    permListData["elements"].forEach(element => {
      if (element.name == this.getName(this.form.get(element.name))) {
        this[element.name.concat('Visible')] = element.visible;
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
    });
    this.userService.getContactInfo(this.userId)
      .subscribe(
        data => {
          this.retModel = data
          if (this.retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <ContactinfoModel>this.retModel.body;
            // console.log("returned userModel: ", this.userModel);
            this.initForm();
          }
        });
  }


  onSubmit() {
    // this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log("What is this form data: ", this.form.getRawValue());
    this.userService.updateContactInfo(this.form.getRawValue())
      .subscribe(resp => {
        this.retModel = resp

        if (this.retModel.result == 1) {
          console.log("returned result: ", this.retModel.result);
          // this.userModel = <ContactinfoModel>this.retModel.body;
          // console.log("returned userModel: ", this.userModel);
          // this.initForm();
          this.successMessage = "The Contact Was Updated Successfully";
        }
        else{
          this.failMessage = "There was a problem updating the Contact";
        }
      });
  }


  private getName(control: AbstractControl): string | null {
    let group = <FormGroup>control.parent;
    if (!group) {
      return null;
    }

    let name: string;
    Object.keys(group.controls).forEach(key => {
      let childControl = group.get(key);

      if (childControl !== control) {
        return;
      }
      name = key;
    });
    return name;
  }

}
