import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenStorageService } from 'src/app/services/auth/token-storage.service';
import { PermissionService } from 'src/app/components/shared/services/permission-data.service';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent {
  pagename: string = "MyProfile";
  contactForm: string = "ContactProfileEdit";
  userForm: string = "UserProfileEdit";
  companyForm: string = "CompanyProfileEdit";

  contactVisible: boolean;
  companyVisible: boolean;
  userVisible: boolean;
  passwordVisible: boolean;

  userData: any;
  usersId: string;

  constructor(private route: ActivatedRoute, private router: Router, private tokenStorage: TokenStorageService,
    private accessService: AccessServicesService) {
    this.usersId = this.route.snapshot.paramMap.get('id');
    this.userData = this.tokenStorage.getUser();
    this.PermissionAssignment();
  }

  PermissionAssignment() {
    let conpermPageData = this.accessService.getPermissionList(this.contactForm);
    let comppermPageData = this.accessService.getPermissionList(this.companyForm);
    let userpermPageData = this.accessService.getPermissionList(this.userForm);

    //let conFind = conpermPageData["visible"];
    this.companyVisible = comppermPageData["visible"];
    //let userFind = userpermPageData["visible"];
    //console.log("what is conFind: ", conFind);
    // console.log("what is compFind: ", this.companyVisible);
  }

}
