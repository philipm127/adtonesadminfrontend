import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FormBase } from 'src/app/components/dynamic-forms/form-base/form-base';
import { Observable } from 'rxjs';
import { ContactService } from '../../tests/contact-info.service';

import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';


@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css'],
  providers: [ContactService]//,UserInfoService]
})


export class UserDetailsComponent {
  usersId: string;
  contactForm: string = "ContactEdit";
  userForm: string = "UserEdit";
  companyForm: string = "CompanyEdit";


  constructor(
    private route: ActivatedRoute, private router: Router) {
    this.usersId = this.route.snapshot.paramMap.get('id');

  }

}
