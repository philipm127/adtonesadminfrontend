import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ReturnResult } from 'src/app/models/return-result';
import { UmanagementService } from 'src/app/components/usermanagement/umanagement.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SelectServicesService } from 'src/app/components/shared/services/select-services.service';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { HttpResponse } from '@angular/common/http';
import { AdvertiserUser } from '../../models/users-applied-models';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})

export class UserInfoComponent implements OnInit {
  @Input() pagename: string;
  @Input() userId: number;

  emailDisabled: boolean;

  form = new FormGroup({
    email: new FormControl("", Validators.required),
    firstName: new FormControl("", Validators.required),
    lastName: new FormControl(""),
    organisation: new FormControl(""),
    outstandingdays: new FormControl(""),
    roleId: new FormControl(""),
    userId: new FormControl()
  });

  emailVisible: boolean = true;
  firstNameVisible: boolean = true;
  lastNameVisible: boolean = true;
  roleIdVisible: boolean = true;
  organisationVisible: boolean = true;
  outstandingdaysVisible: boolean = true;


  editMode: boolean = true;
  userModel: AdvertiserUser;
  retModel: ReturnResult;
  retResult: ReturnResult;
  roleSelect: BasicSelectModel[];
  selectedValue: any = "3";
  destroy$: Subject<BasicSelectModel> = new Subject();

  userData: any;

  successMessage: string;
  failMessage: string;


  constructor(private formBuilder: FormBuilder, private userService: UmanagementService,
    private accessService: AccessServicesService, private listService: SelectServicesService) {

    this.userData = this.accessService.getUser();

    if (this.pagename && this.pagename == "UserProfileEdit")
      this.emailDisabled = true;
  }

  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    permListData["elements"].forEach(element => {
      if (element.name == this.accessService.getName(this.form.get(element.name))) {
        this[element.name.concat('Visible')] = element.visible;
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
    });

    this.accessService.refreshToken();

    this.userService.getUserInfo(this.userId)
      .subscribe(
        data => {
          this.retModel = data;
          if (this.retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <AdvertiserUser>this.retModel.body;
            this.selectedValue = this.userModel.roleId.toString();
            this.getRoles();
            // console.log("returned userModel: ", this.userModel);
          }
        });
  }


  getRoles() {
    this.listService.getRoleList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;

        if (this.retResult.result == 1) {
          console.log("returned result from get role list: ", this.retResult);
          this.roleSelect = <BasicSelectModel[]>this.retResult.body;
          this.initForm();
        }
      });
  }



  private initForm() {
    if (this.editMode) {
      this.form.patchValue({
        firstName: this.userModel.firstName,
        lastName: this.userModel.lastName,
        email: this.userModel.email,
        outstandingdays: this.userModel.outstandingdays,
        organisation: this.userModel.organisation,
        userId: this.userModel.userId
      });
      this.form.get('roleId').setValue(this.userModel.roleId.toString());
    }
  }


  // private getName(control: AbstractControl): string | null {
  //   // console.log(" what is get name doing ", control);
  //   let group = <FormGroup>control.parent;
  //   if (!group) {
  //     return null;
  //   }
  //   let name: string;
  //   Object.keys(group.controls).forEach(key => {
  //     let childControl = group.get(key);

  //     if (childControl !== control) {
  //       return;
  //     }
  //     name = key;
  //   });
  //   // console.log(" what is get name doing with name ", name);
  //   return name;
  // }


  onSubmit() {
    // this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log("What is this form data: ", this.form.getRawValue());
    this.userService.updateUserInfo(this.form.getRawValue())
      .subscribe(resp => {
        this.retModel = resp

        if (this.retModel.result == 1) {
          this.successMessage = "The Details Were Updated Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the details";
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}

