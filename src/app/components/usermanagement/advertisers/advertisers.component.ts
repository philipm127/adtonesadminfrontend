import { Component, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { NgSwitch, NgSwitchCase } from '@angular/common';
import { ReturnResult } from 'src/app/models/return-result';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UmanagementService } from 'src/app/components/usermanagement/umanagement.service';
import { DatePipe } from '@angular/common';
import { AdvertiserUser } from 'src/app/components/usermanagement/models/users-applied-models';
import { TableSearchModel } from '../../shared/table-search-model';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { AccessServicesService } from '../../shared/services/access-services.service';


@Component({
  selector: 'app-advertisers',
  templateUrl: './advertisers.component.html',
  styleUrls: ['./advertisers.component.css']
})
export class AdvertisersComponent implements AfterViewInit, OnDestroy {
  pagename: string = "AdvertiserList";
  tableHeader: string = "Advertisers";
  permData: any;
  miscName: string = "Sales Exec";
  emailLink: boolean = false;
  ticketLink: boolean = false;
  creditLink: boolean = false;
  advertLink: boolean = false;
  campLink: boolean = false;
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];

  destroy$: Subject<AdvertiserUser> = new Subject();
  dataSource: MatTableDataSource<AdvertiserUser>;
  modelSource: any[];
  isLoading: boolean = true;

  // used for search by name
  fullName: string = "Advertiser";

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  filterValues = {};
  retResult: ReturnResult = null;
  retResultUpdate: ReturnResult = null;

  statusSelect: BasicSelectModel[] = [];
  countrySelect: BasicSelectModel[] = [];

  tableSearchModel: TableSearchModel;

  constructor(private userService: UmanagementService, private datePipe: DatePipe,
    private popService: PopSearchDdService, private accessService: AccessServicesService) {
    this.PermissionAssignment();
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  ngAfterViewInit() {
    this.userService.getUserList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.isLoading = false;
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <AdvertiserUser[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<AdvertiserUser>(this.modelSource);
        }
        this.statusSelect = this.popService.popStatus(this.modelSource);
        this.countrySelect = this.popService.popCountry(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("What is in filters: ",filter);
          const a = !filter.fullName || data.fullName.trim().toLowerCase().includes(filter.fullName.trim().toLowerCase());
          const b = !filter.email || data.email.trim().toLowerCase().includes(filter.email.trim().toLowerCase());
          const c = !filter.status || data.rStatus.trim().toLowerCase().includes(filter.status.trim().toLowerCase());
          const d = !filter.dateFrom || data.dateCreated >= this.datePipe.transform(filter.dateFrom, 'yyyy-MM-dd');
          const e = !filter.dateTo || data.dateCreated <= this.datePipe.transform(filter.dateTo, 'yyyy-MM-dd');
          const f = !filter.country || data.countryName.trim().toLowerCase().includes(filter.country.trim().toLowerCase());
          const g = !filter.name || data.salesExec.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          return a && b && c && d && e && f && g;
        }) as (AdvertiserElement, string) => boolean;

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
    let mailFind = permPageData["elements"].filter(j => j.name == "email" && j.type == "element");
    let creditFind = permPageData["elements"].filter(j => j.name == "creditlimit" && j.type == "element");
    this.emailLink = mailFind[0].enabled;
    this.ticketLink = this.campLink = this.advertLink = this.emailLink;
    this.creditLink = creditFind[0].enabled;
  }

  updatestatus(Id: number, status: number) {
    let newModel: AdvertiserUser;
    for (let i in this.modelSource) {
      if (this.modelSource[i].userId == Id) {
        this.modelSource[i].activated = status;
        newModel = this.modelSource[i];
        // console.log("What is userData for this id: ",this.modelSource[i]);
        break;
      }
    }

    this.userService.updateUserStatus(newModel)
      .subscribe(resp => {
        this.retResultUpdate = resp

        if (this.retResultUpdate.result == 1) {
          // console.log("returned result retResultUpdate: ", this.retResultUpdate.result);
        }
      });
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
