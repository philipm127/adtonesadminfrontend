import { Component, OnInit } from '@angular/core';
import { MyErrorStateMatcher } from '../userdetails/update-password/update-password.component';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AdvertiserUser } from '../models/users-applied-models';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { UmanagementService } from '../umanagement.service';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';
import { PermissionManagementService } from '../../permission-management/permission-management.service';
import { NewUserModel } from '../models/new-user-model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  pagename: string = "AddUser"
  roleId: any = 0;
  matcher = new MyErrorStateMatcher();
  isValidFormSubmitted = null;
  form: FormGroup;


  emailVisible: boolean = true;
  firstNameVisible: boolean = true;
  lastNameVisible: boolean = true;
  roleIdVisible: boolean = true;
  organisationVisible: boolean = true;
  outstandingdaysVisible: boolean = false;
  mobileNumberVisible: boolean = true;
  fixedLineVisible: boolean = true;
  phoneNumberVisible: boolean = true;
  addressVisible: boolean = true;
  countryVisible: boolean = true;
  operatorIdVisible: boolean = true;

  userModel: AdvertiserUser;
  roleSelect: BasicSelectModel[];
  operatorSelect: BasicSelectModel[];
  countrySelect: BasicSelectModel[];
  permissionsSelect: BasicSelectModel[];

  destroy$: Subject<BasicSelectModel> = new Subject();

  successMessage: string;
  failMessage: string;
  disableSubmit: boolean = false;


  constructor(private fb: FormBuilder, private userService: UmanagementService,
    private permService: PermissionManagementService,
    private accessService: AccessServicesService, private listService: SelectServicesService, private route: ActivatedRoute, private router: Router) {
    this.roleId = this.route.snapshot.paramMap.get('roleid');
    this.form = this.fb.group({
      email: new FormControl("", Validators.required),
      firstName: new FormControl("", Validators.required),
      lastName: new FormControl("", Validators.required),
      organisation: new FormControl(""),
      operatorId: new FormControl("0"),
      outstandingdays: new FormControl(0),
      roleId: new FormControl("6"),
      userId: new FormControl(0),
      mobileNumber: new FormControl("", Validators.required),
      fixedLine: new FormControl(""),
      phoneNumber: new FormControl(""),
      address: new FormControl(""),
      countryId: new FormControl("", Validators.required),
      password: ['', [Validators.required,
      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&^])[A-Za-z\d$@$!%*?&].{7,}')
      ]
      ],
      confirmPassword: new FormControl("")
    }, { validator: this.checkPasswords });

    if (this.roleId == 9)
      this.form.addControl('permissions', new FormControl(''));
    else
      this.form.addControl('permissions', new FormControl('', Validators.required));

  }


  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    permListData["elements"].forEach(element => {
      if (element.name == this.accessService.getName(this.form.get(element.name))) {
        this[element.name.concat('Visible')] = element.visible;
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
      if (this.roleId > 0) {
        this.form.get('roleId').setValue(this.roleId);
        this.form.get('roleId').disable();
      }
    });

    this.accessService.refreshToken();
    this.getRoles();
    this.getOperators();
    this.getCountry();
    this.getListUsersWPermissions();
    // this.form.get('roleId').disable();//this.userModel.roleId.toString());
  }

  get password() {
    return this.form.get('password');
  }

  getRoles() {
    this.listService.getRoleList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {
          // console.log("returned result from get role list: ", this.retResult);
          this.roleSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }


  getOperators() {
    this.listService.getOperatorList(0)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {

          this.operatorSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }


  getCountry() {
    this.listService.getCountryList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {

          this.countrySelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }


  getListUsersWPermissions() {
    this.listService.getUserPermissionList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {

          this.permissionsSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }


  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true }
  }


  getUserPermission(event: any) {
    this.permService.getUserPermission(event.value)
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          this.form.patchValue({
            permissions: retModel.body
          });
        }
        else {
          this.failMessage = "There was a problem updating the details";
        }
      });
  }




  onSubmit() {
    this.successMessage = "";
    this.failMessage = "";

    this.isValidFormSubmitted = false;
    if (this.form.valid) {
      this.isValidFormSubmitted = true;
      this.disableSubmit = true;

      console.log("What is this form data: ", this.form.getRawValue());
      let fm = this.form.getRawValue();
      let newModel = new NewUserModel();
      // newModel.userId = fm.userId;
      newModel.firstName = fm.firstName;
      newModel.lastName = fm.lastName;
      newModel.email = fm.email;
      newModel.organisation = fm.organisation;
      newModel.countryId = parseInt(fm.countryId);
      newModel.operatorId = fm.operatorId == null ? 0 : parseInt(fm.operatorId);
      newModel.roleId = parseInt(fm.roleId);
      newModel.mobileNumber = fm.mobileNumber;
      newModel.phoneNumber = fm.phoneNumber;
      newModel.fixedLine = fm.fixedLine;
      newModel.address = fm.address;
      newModel.password = fm.password;
      newModel.permissions = JSON.stringify(fm.permissions);
      console.log("what is new model: ", newModel);
      this.userService.insertNewUser(newModel)
        .subscribe(resp => {
          this.disableSubmit = false;
          let retModel = resp

          if (retModel.result == 1) {
            let sucMsg = retModel.body.toString();
            this.successMessage = sucMsg;
          }
          else {
            let erMsg = retModel.body == null ? retModel.error : retModel.body;
            this.failMessage = "There was a problem updating the details " + erMsg;
          }
        });
    } else {
      console.log("Form has failed");
      return;
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
