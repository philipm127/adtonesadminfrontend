import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ReturnResult } from '../../models/return-result';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class UmanagementService {
  myAppUrl: string;
  signUpUrl:string;
  controller: string = "usermanagement/";
  signUpController: string = "register/";

  constructor(private http: HttpClient) {
    this.myAppUrl = (environment.appUrl + this.controller);
    this.signUpUrl = (environment.appUrl + this.signUpController);
  }

  // Advertisers List of Users
  public getUserList() {
    // console.log("have entered ad constructor now hopefully in get ad list service")  ;
    let ctrAction = "v1/GetAdvertiserTable"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getSubscriberList(model: any) {
    // console.log("Search Params are: ",model);
    let ctrAction = "v1/GetSubscriberTable"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model, { observe: "response" });
  }

  public getOperatorList() {
    let ctrAction = "v1/LoadOperatorDataTable"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  public getAdminList() {
    let ctrAction = "v1/LoadAdminDataTable"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  public getUserInfo(Id: number): Observable<ReturnResult> {
    let ctrAction = "v1/GetUserProfile/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + Id);
  }

  public getContactInfo(Id: number): Observable<ReturnResult> {
    let ctrAction = "v1/GetContactDetails/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + Id);
  }


  public getCompanyInfo(userId: number): Observable<ReturnResult> {
    if (userId == null)
      userId = 0;
    let ctrAction = "v1/GetCompanyDetails/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + userId);
  }


  public updateCompanyInfo(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/UpdateCompanyDetails"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public updateContactInfo(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/UpdateContactDetails"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public updatePermissionInfo(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/UpdateUserPermission"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public updateUserInfo(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/UpdateUserProfile"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public updateUserStatus(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/UpdateUserStatus"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public getUserPermission(Id: number): Observable<ReturnResult> {
    let ctrAction = "v1/GetUserPermission/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + Id);
  }


  public insertNewUser(model: any): Observable<ReturnResult> {
    // console.log("ADD USER MODEL: ",model);
    let ctrAction = "v1/AddNewUser"
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public insertNewAdvertiser(model: any): Observable<ReturnResult> {
    // console.log("ADD USER MODEL: ", model);
    let ctrAction = "v1/AddNewUser"
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public newAdvertiserSignUp(model: any): Observable<ReturnResult> {
    // console.log("ADD USER MODEL: ", model);
    let ctrAction = "v1/AddNewUser"
    return this.http.post<ReturnResult>(this.signUpUrl + ctrAction, model);
  }

  public verifyEmail(code:string): Observable<ReturnResult> {
    // console.log("ADD USER MODEL: ", model);
    let ctrAction = "v1/VerifyEmail/"
    return this.http.get<ReturnResult>(this.signUpUrl + ctrAction + code);
  }


  public getClientInfo(Id: number): Observable<ReturnResult> {
    let ctrAction = "v1/GetClientProfile/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + Id);
  }

}
