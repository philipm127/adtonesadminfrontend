import { DatePipe } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Subscription, merge } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { IMessage, MessageService } from 'src/app/components/shared/services/message.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { SelectServicesService } from 'src/app/components/shared/services/select-services.service';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { IdCollectionModel } from 'src/app/models/IdCollectionViewModel';
import { PageSortSearchModel } from 'src/app/models/PageSortSearchModel';
import { ReturnResult } from 'src/app/models/return-result';
import { AdvertiserUser, SubscriberTableModel } from '../../models/users-applied-models';
import { SubscriberDatasourceService } from '../../subscriber-datasource.service';
import { UmanagementService } from '../../umanagement.service';

@Component({
  selector: 'app-subscriber-table',
  templateUrl: './subscriber-table.component.html',
  styleUrls: ['./subscriber-table.component.css']
})
export class SubscriberTableComponent implements OnInit {

  pagename: string = "SubscribersList";
  permData: any;
  userData: any;
  emailLink: boolean = true;
  fullName: string = "Subscribers Name";

  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];


  destroy$: Subject<SubscriberTableModel> = new Subject();
  //dataSource: MatTableDataSource<SubscriberTableModel>;
  dataSource: SubscriberDatasourceService;
  modelSource: SubscriberTableModel[];
  isLoading: boolean = true;
  filterValues: string;

  miscName: string = "MSISDN";
  typeName: string = "Subject";

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  retResult: ReturnResult = null;

  statusSelect: BasicSelectModel[] = [];
  countrySelect: BasicSelectModel[] = [];
  operatorSelect: BasicSelectModel[] = [];
  // typeSelect: BasicSelectModel[] = [];

  tableSearchModel: TableSearchModel;

  messages: IMessage[] = [];
  subscription: Subscription;
  successMessage: string;

  constructor(private userService: UmanagementService, private datePipe: DatePipe, private messService: MessageService,
    private popService: PopSearchDdService, private accessService: AccessServicesService,
    private route: ActivatedRoute, private router: Router, private selectList: SelectServicesService) {
    this.getStatusList();
    this.getCountryList();
    this.getOperatorList();
    // this.adUserId = this.route.snapshot.paramMap.get('id');

    this.userData = this.accessService.getUser();
    this.PermissionAssignment();
    // console.log("What is adUserId in Tickets: ",this.adUserId);

  }

  filtered(filter: string) {
    this.filterValues = filter;

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.paginator.pageIndex = 0;
    this.loadData();
    this.getStatusList();
    this.getCountryList();
    this.getOperatorList();
  }

  ngOnInit() {
    this.subscription = this.messService.getMessage().subscribe(message => {
      if (message) {
        this.messages.push(message.data);
        if (this.messages[0] != undefined) {
          console.log("this.messages.push(message.data); ", this.messages[0].text);
          this.successMessage = this.messages[0].text;
        }
      } else {
        // clear messages when empty message received
        this.messages = [];
      }
    });

    let paging = new PageSortSearchModel();
    paging.elementId = 0;
    paging.page = this.paginator.pageIndex;
    paging.pageSize = this.paginator.pageSize;
    paging.direction = this.sort.direction;
    paging.sort = this.sort.active;

    this.dataSource = new SubscriberDatasourceService(this.userService);

    this.dataSource.loadSubscribers(paging);
    console.log("this.dataSource.loadSubscribers(paging): ", this.dataSource.loading$);
    this.isLoading = this.dataSource.loading$._isScalar;

  }

  ngAfterViewInit() {
    // this.getClientList();


    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadData())
      )
      .subscribe();

  }


  updatestatus(Id: number, status: number) {
    let newModel: AdvertiserUser;
    for (let i in this.modelSource) {
      if (this.modelSource[i].userId == Id) {
        this.modelSource[i].activated = status;
        newModel.userId = Id; //this.modelSource[i];
        newModel.activated = status;
        // console.log("What is userData for this id: ",this.modelSource[i]);
        break;
      }
    }

    this.userService.updateUserStatus(newModel)
      .subscribe(resp => {
        let retResultUpdate = resp

        if (retResultUpdate.result == 1) {
          // console.log("returned result retResultUpdate: ", this.retResultUpdate.result);
        }
      });
  }


  loadData() {
    this.successMessage = "";

    let paging = new PageSortSearchModel();
    paging.elementId = 0;
    paging.page = this.paginator.pageIndex;
    paging.pageSize = this.paginator.pageSize;
    paging.direction = this.sort.direction;
    paging.sort = this.sort.active;
    paging.search = JSON.stringify(this.filterValues);
    console.log("load data for subscribers submit model : ", paging);
    console.log("load data for subscribers submit model search : ", paging.search);
    this.dataSource.loadSubscribers(paging);
    console.log("this.dataSource.loadSubscribers(paging): ", this.dataSource);

  }

  getCountryList() {
    this.selectList.getCountryList()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        // console.log("returned result from get subject list: ", retResult);
        if (retResult.result == 1) {
          this.countrySelect = <BasicSelectModel[]>retResult.body;
        }
        else
          this.countrySelect = <BasicSelectModel[]>retResult.body;
      });
  }


  getOperatorList() {
    this.selectList.getOperatorList(0)
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        // console.log("returned result from get subject list: ", retResult);
        if (retResult.result == 1) {
          this.operatorSelect = <BasicSelectModel[]>retResult.body;
        }
        else
          this.operatorSelect = <BasicSelectModel[]>retResult.body;
      });
  }


  getStatusList() {
    this.selectList.getUserStatusList()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        // console.log("returned result from getuser status list: ", retResult);
        if (retResult.result == 1) {
          this.statusSelect = <BasicSelectModel[]>retResult.body;
        }
        else
          this.statusSelect = <BasicSelectModel[]>retResult.body;
      });
  }



  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    console.log("let permPageData: ", permPageData);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
    let mailFind = permPageData["elements"].filter(j => j.name == "email" && j.type == "element");
    this.emailLink = mailFind[0].enabled;
    // console.log("what is this.emailLink: ", this.emailLink);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
