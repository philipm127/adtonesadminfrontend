import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { HttpResponse } from "@angular/common/http";
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { PageSortSearchModel } from "src/app/models/PageSortSearchModel";
import { ReturnResult } from "src/app/models/return-result";
import { SubscriberTableModel } from "./models/users-applied-models";
import { UmanagementService } from "./umanagement.service";


export class SubscriberDatasourceService {

  private userSubject = new BehaviorSubject<SubscriberTableModel[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  public resultCount = null;

  constructor(private userService: UmanagementService) { }

  loadSubscribers(paging: PageSortSearchModel) {

    this.loadingSubject.next(true);

    this.userService.getSubscriberList(paging)
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        this.resultCount = retResult.recordcount;
        this.loadingSubject.next(false);
        let data = <SubscriberTableModel[]>retResult.body;
        this.userSubject.next(data)
      });
    // .subscribe(tickets, this.userSubject.next(tickets));

  }

  connect(collectionViewer: CollectionViewer): Observable<SubscriberTableModel[]> {
    console.log("Connecting data source");
    return this.userSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.userSubject.complete();
    this.loadingSubject.complete();
  }

}
