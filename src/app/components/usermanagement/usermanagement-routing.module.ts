import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserDetailsComponent } from './userdetails/user-details/user-details.component';
import { AdvertisersComponent } from './advertisers/advertisers.component';
import { OperatorsComponent } from './operators/operators.component';
import { AuthguardService } from 'src/app/services/auth/authguard.service';
import { UserProfileComponent } from './userdetails/user-profile/user-profile.component';
import { AddUserComponent } from './add-user/add-user.component';
import { AdminsComponent } from './admins/admins.component';
import { AddAdvertiserComponent } from './add-advertiser/add-advertiser.component';
import { SubscriberTableComponent } from './subscribers/subscriber-table/subscriber-table.component';



const routes: Routes = [
  { path: 'advertisers', component: AdvertisersComponent, canActivate: [AuthguardService] },
  { path: 'adtoneadmins', component: AdminsComponent, canActivate: [AuthguardService] },
  { path: 'userdetails/:id', component: UserDetailsComponent },
  { path: 'operators', component: OperatorsComponent },
  { path: 'myprofile/:id', component: UserProfileComponent, canActivate: [AuthguardService] },
  { path: 'adduser', component: AddUserComponent },
  { path: 'adduser/:roleid', component: AddUserComponent },
  { path: 'addadvertiser', component: AddAdvertiserComponent },
  { path: 'subscribertable', component: SubscriberTableComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsermanagementRoutingModule { }
