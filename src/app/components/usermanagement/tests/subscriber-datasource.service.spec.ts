import { TestBed } from '@angular/core/testing';

import { SubscriberDatasourceService } from '../subscriber-datasource.service';

describe('SubscriberDatasourceService', () => {
  let service: SubscriberDatasourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubscriberDatasourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
