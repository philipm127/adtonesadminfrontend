import { TestBed } from '@angular/core/testing';

import { UmanagementService } from '../umanagement.service';

describe('UmanagementService', () => {
  let service: UmanagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UmanagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
