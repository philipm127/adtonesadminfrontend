import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserPermissionElementsComponent } from '../userdetails/user-permission/user-permission-elements/user-permission-elements.component';

describe('UserPermissionElementsComponent', () => {
  let component: UserPermissionElementsComponent;
  let fixture: ComponentFixture<UserPermissionElementsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPermissionElementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPermissionElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
