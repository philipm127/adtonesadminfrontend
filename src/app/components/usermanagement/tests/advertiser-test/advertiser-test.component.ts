import { Component, OnInit, ViewChild, OnDestroy,AfterViewInit } from '@angular/core';
import { NgSwitch, NgSwitchCase } from '@angular/common';
import { MockService } from 'src/app/services/mock.service';

import { ReturnResult } from 'src/app/models/return-result';
import { FormsModule } from '@angular/forms';
import * as _ from 'lodash';
import { BasicSelectModel } from 'src/app/models/basic-select';
import {  takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { AdvertiserUser } from 'src/app/components/usermanagement/models/users-applied-models';



@Component({
  selector: 'app-advertiser-test',
  templateUrl: './advertiser-test.component.html',
  styleUrls: ['./advertiser-test.component.css']
})
export class AdvertiserTestComponent  implements OnInit, OnDestroy {
  dtTrigger: Subject<AdvertiserUser> = new Subject();
  userResults: MatTableDataSource<AdvertiserUser>;
  // dataSource: MatTableDataSource<AdvertiserUserTableModel[]>;
  displayedColumns: string[] = ['userId', 'email', 'fullName', 'role'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  filterValues = {};


  constructor( private mockService: MockService ) { }

  ngOnInit(): void {
    this.getDataFromSource();

  }

    getDataFromSource() {
      this.mockService.getUserListLong()
            .pipe(takeUntil(this.dtTrigger))
            .subscribe((res: HttpResponse<AdvertiserUser[]>)=>{
        var model  = res.body;
        this.userResults = new MatTableDataSource<AdvertiserUser>(model);
        this.userResults.paginator = this.paginator;
        this.userResults.sort = this.sort;

        //function(data: AdvertiserUserTableModel, filterValue: string) {
          //return data['email'] /** replace this with the column name you want to filter */
        //     .trim()
        //     .toLocaleLowerCase().indexOf(filterValue.trim().toLocaleLowerCase()) >= 0;
        // };
        this.dtTrigger.next();
      }
      );
    }

    applyFilter(event: Event) {
      const filterValue = (event.target as HTMLInputElement).value;
      this.userResults.filter = filterValue.trim().toLowerCase();

      if (this.userResults.paginator) {
        this.userResults.paginator.firstPage();
      }
    }


    applyNewFilter(event: Event,col:string) {
      const filterValue = (event.target as HTMLInputElement).value;
      console.log("INSIDE NEW FILTER ");
      console.log("Value sent ON entry is: ",filterValue);
      // console.log("this.userResults: ",this.userResults);
      // console.log("this.userResults[email]: ",this.userResults['email']);
      this.filterValues['col'] = col;
      this.filterValues['search'] = (event.target as HTMLInputElement).value.toString();
      this.userResults.filter = JSON.stringify(this.filterValues);

      if (this.userResults.paginator) {
        this.userResults.paginator.firstPage();
      }
    }

      ngOnDestroy() {
        this.dtTrigger.next();
        // Unsubscribe from the subject
        this.dtTrigger.unsubscribe();
    }

	}


