import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdvertiserTestComponent } from './advertiser-test.component';

describe('AdvertiserTestComponent', () => {
  let component: AdvertiserTestComponent;
  let fixture: ComponentFixture<AdvertiserTestComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertiserTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
