import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdvertisersComponent } from '../advertisers/advertisers.component';

describe('AdvertisersComponent', () => {
  let component: AdvertisersComponent;
  let fixture: ComponentFixture<AdvertisersComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertisersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertisersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
