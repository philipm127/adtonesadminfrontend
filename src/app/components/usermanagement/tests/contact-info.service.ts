import { Injectable }       from '@angular/core';

import { FormDropdownComponent } from 'src/app/components/dynamic-forms/form-base/form-dropdown';
import { FormBase }     from 'src/app/components/dynamic-forms/form-base/form-base';
import { FormTextboxComponent }  from 'src/app/components/dynamic-forms/form-base/form-textbox';
import { of } from 'rxjs';

@Injectable()
export class ContactService {

  // TODO: get from a remote source of control metadata
  getcontrols() {

    let controls: FormBase<string>[] = [

      new FormDropdownComponent({
        key: 'brave',
        label: 'Bravery Rating',
        options: [
          {key: 'solid',  value: 'Solid'},
          {key: 'great',  value: 'Great'},
          {key: 'good',   value: 'Good'},
          {key: 'unproven', value: 'Unproven'}
        ],
        order: 3
      }),

      new FormTextboxComponent({
        key: 'firstName',
        label: 'First name',
        value: 'Bombasto',
        required: true,
        order: 1
      }),

      new FormTextboxComponent({
        key: 'emailAddress',
        label: 'Email',
        type: 'email',
        order: 2
      })
    ];

    return of(controls.sort((a, b) => a.order - b.order));
  }
}
