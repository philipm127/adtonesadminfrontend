import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';

import { Subject } from 'rxjs';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { TableSearchModel } from '../../shared/table-search-model';
import { OperatorUserTableModel } from 'src/app/components/usermanagement/models/users-applied-models';
import { UmanagementService } from 'src/app/components/usermanagement/umanagement.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';

@Component({
  selector: 'app-operators',
  templateUrl: './operators.component.html',
  styleUrls: ['./operators.component.css']
})

export class OperatorsComponent implements OnDestroy {
  pagename: string = "OperatorAdminList"
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];
  userData: any;
  destroy$: Subject<OperatorUserTableModel> = new Subject();
  dataSource: MatTableDataSource<OperatorUserTableModel>;
  modelSource: OperatorUserTableModel[];
  isLoading: boolean = true;

  countrySelect: BasicSelectModel[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  retResult: ReturnResult = null;

  tableSearchModel: TableSearchModel;


  constructor(private userService: UmanagementService, private datePipe: DatePipe, private popSearch: PopSearchDdService,
    private accessService: AccessServicesService) {

    this.userData = this.accessService.getUser();
    this.PermissionAssignment();

    this.userService.getOperatorList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.isLoading = false;
          this.modelSource = <OperatorUserTableModel[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<OperatorUserTableModel>(this.modelSource);
        }
        this.countrySelect = this.popSearch.popCountry(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          const a = !filter.fullName || data.fullName.trim().toLowerCase().includes(filter.fullName.trim().toLowerCase());
          const b = !filter.country || data.countryName.trim().toLowerCase().includes(filter.country.trim().toLowerCase());
          const c = !filter.email || data.email.trim().toLowerCase().includes(filter.email.trim().toLowerCase());
          return a && b && c;
        }) as (OperatorAdminElement, string) => boolean;

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }


  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
