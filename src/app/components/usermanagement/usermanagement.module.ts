import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { UsermanagementRoutingModule } from './usermanagement-routing.module';

import { UserDetailsComponent } from './userdetails/user-details/user-details.component';
import { UserInfoComponent } from './userdetails/user-info/user-info.component';
import { ContactInfoComponent } from './userdetails/contact-info/contact-info.component';
import { CompanyInfoComponent } from './userdetails/company-info/company-info.component';
import { AdvertisersComponent } from './advertisers/advertisers.component';

import { MaterialComponentsModule } from '../../material-components/material-components.module';
import { FormElementsComponent } from './userdetails/contact-info/form-elements/form-elements.component';
import { MatTableExporterModule } from 'mat-table-exporter';

import { TestDynamicComponent } from './test-dynamic/test-dynamic.component';
import { SubLevelComponent } from './test-dynamic/sub-level/sub-level.component';
import { ElementLevelComponent } from './test-dynamic/sub-level/element-level/element-level.component';
import { SharedModule } from '../shared/shared.module';
import { OperatorsComponent } from './operators/operators.component';
import { UpdatePasswordComponent } from './userdetails/update-password/update-password.component';
import { UserProfileComponent } from './userdetails/user-profile/user-profile.component';
import { UserPermissionComponent } from './userdetails/user-permission/user-permission.component';
import { AddUserComponent } from './add-user/add-user.component';
import { AdminsComponent } from './admins/admins.component';
import { AddAdvertiserComponent } from './add-advertiser/add-advertiser.component';
import { SubscriberTableComponent } from './subscribers/subscriber-table/subscriber-table.component';

@NgModule({
  declarations: [
    UserDetailsComponent,
    UserInfoComponent,
    ContactInfoComponent,
    CompanyInfoComponent,
    AdvertisersComponent,
    FormElementsComponent,
    TestDynamicComponent,
    SubLevelComponent,
    ElementLevelComponent,
    OperatorsComponent,
    UpdatePasswordComponent,
    UserProfileComponent,
    UserPermissionComponent,
    AddUserComponent,
    AdminsComponent,
    AddAdvertiserComponent,
    SubscriberTableComponent
  ],
  exports: [
    UserInfoComponent,
    ContactInfoComponent,
    CompanyInfoComponent
  ],
  imports: [
    CommonModule,
    UsermanagementRoutingModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule,
    SharedModule
  ]
})
export class UsermanagementModule { }
