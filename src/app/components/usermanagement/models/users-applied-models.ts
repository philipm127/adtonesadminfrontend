export interface User {
  userId: number;
  email: string;
  firstName: string;
  lastName: string;
  fullName: string;
  organisation: string;
  passwordHash: string;
  password: string;
  token: string;
  dateCreated: string;
  activated: number;
  rStatus: string;
  roleId: number;
  role: string;
  permissions: any;
}

export interface OperatorUserTableModel extends User {
  countryId: number;
  countryName: string;
  operatorId: number;
  operatorName: string;
}


export interface SubscriberTableModel extends User {
  countryId: number;
  countryName: string;
  operatorId: number;
  operatorName: string;
  msisdn: string;
}

export interface AdvertiserUser extends User {
  noOfactivecampaign: number;
  noOfunapprovedadverts: number;
  creditLimit: number;
  outstandinginvoice: number;
  ticketCount: number;
  outstandingdays: number;
  mobileNumber: string;
  fixedLine: string;
  phoneNumber: string;
  companyName: string;
  countryName: string;
  salesExec: string;
  sUserId: number;
  noAdvertisers: number;
  salesManager: string;
}

export interface maybeSubModel {
  userId: number;
  operatorId: number;
  email: string;
  firstName: string;
  lastName: string;
  fullName: string;
  passwordHash: string;
  token: string;
  dateCreated: string;
  organisation: string;
  activated: number;
  rStatus: string;
  roleId: number;
  role: string;
  verificationStatus: boolean;
  outstandingdays: number;
  isMsisdnMatch: boolean;
  isEmailVerfication: boolean;
  phoneticAlphabet: string;
  isMobileVerfication: boolean;
  organisationTypeId: number;
  userMatchTableName: string;
  adtoneServerUserId: number;
  tibcoMessageId: string;
}
