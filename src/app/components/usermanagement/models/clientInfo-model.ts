export interface ClientInfoModel {
  id: number;
  userId: number;
  name: string;
  description: string;
  email: string;
  contactPhone: string;
  countryId: number;
}
