export interface CompanyinfoModel{
    id: number;
    userId: number;
    companyName?: string;
    address: string;
    additionalAddress: string;
    town?: string;
    postCode?: string;
    countryId: number;
  }
