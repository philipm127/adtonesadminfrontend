export interface UserResultModelX{
    UserId:number;
    RoleId:number;
    FullName:string;
    Email:string;
    FirstName:string;
    LastName:string;
    NoOfactivecampaign:number;
    NoOfunapprovedadverts:number;
    CreditLimit:number;
    Outstandinginvoice:number;
    Activated:number;
    rStatus:string;
    DateCreated:string;
    TicketCount:number;
    Role:string;
}
