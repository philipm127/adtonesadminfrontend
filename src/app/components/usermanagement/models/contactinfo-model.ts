export interface ContactinfoModel{
    id: number;
    userId: number;
    mobileNumber?: string;
    fixedLine?: string;
    email: string;
    phoneNumber?: string;
    address?: string;
    countryId: number;
    currencyId?: number;
    adtoneServerContactId?:number;
  }
