export class UserinfoModelX{
    UserId: number ;
    OperatorId: number;
    Email: string;
    FirstName: string;
    LastName: string;
    FullName: string;
    PasswordHash: string;
    Token: string;
    DateCreated: string;
    organisation: string;
    Activated: number;
    rStatus: string;
    roleId: number;
    Role: string;
    VerificationStatus: boolean;
    outstandingdays: number;
    IsMsisdnMatch: boolean;
    IsEmailVerfication: boolean;
    PhoneticAlphabet: string;
    IsMobileVerfication: boolean;
    OrganisationTypeId: number;
    UserMatchTableName: string;
    AdtoneServerUserId: number;
    TibcoMessageId: string;
  }

