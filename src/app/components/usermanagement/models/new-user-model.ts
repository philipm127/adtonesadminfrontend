export class NewUserModel {
  userId: number;
  email: string;
  firstName: string;
  lastName: string;
  countryId: number;
  operatorId?: number;
  roleId: number;
  organisation: string;
  organisationTypeId: number;
  mobileNumber?: string;
  fixedLine?: string;
  phoneNumber?: string;
  address?: string;
  password: string;
  permissions: string;
  mailSuppression: boolean;
}
