import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FormBase } from 'src/app/components/dynamic-forms/form-base/form-base';
import { Observable }      from 'rxjs';
import { ContactService } from './sub-level/contact-info.service';


import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-test-dynamic',
  templateUrl: './test-dynamic.component.html',
  styleUrls: ['./test-dynamic.component.css'],
  providers:  [ContactService]
})
export class TestDynamicComponent implements OnInit {
  controls$: Observable<FormBase<any>[]>;

  constructor(contactService: ContactService) { this.controls$ = contactService.getcontrols();}

  ngOnInit(): void {
  }

}
