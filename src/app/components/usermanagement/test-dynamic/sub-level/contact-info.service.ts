import { Injectable }       from '@angular/core';

import { FormDropdownComponent } from 'src/app/components/dynamic-forms/form-base/form-dropdown';
import { FormBase }     from 'src/app/components/dynamic-forms/form-base/form-base';
import { FormTextboxComponent }  from 'src/app/components/dynamic-forms/form-base/form-textbox';
import { of } from 'rxjs';

@Injectable()
export class ContactService {

  // TODO: get from a remote source of control metadata
  getcontrols() {

    let controls: FormBase<string>[] = [

      new FormDropdownComponent({
        key: 'brave',
        label: 'Bravery Rating',
        options: [
          {key: 'solid',  value: 'Solid'},
          {key: 'great',  value: 'Great'},
          {key: 'good',   value: 'Good'},
          {key: 'unproven', value: 'Unproven'}
        ],
        order: 3
      }),

      new FormTextboxComponent({
        key: 'firstName',
        label: 'First name',
        value: 'Bombasto',
        required: true,
        order: 1
      }),

      new FormTextboxComponent({
        key: 'emailAddress',
        label: 'Email',
        type: 'email',
        order: 2
      })
    ];

    return of(controls.sort((a, b) => a.order - b.order));
  }
}



// @Injectable()
// export class UserInfoService {
// // TODO: get from a remote source of control metadata
//     retResult:ReturnResult = null;
//     dataSource:UserinfoModel;
//     model:IdCollectionModel;
//     controls: FormBase<string>[] = [

//       new FormDropdownComponent({
//         key: 'roles',
//         label: 'Change Role',
//         options: [
//           {key: 'solid',  value: 'Solid'},
//           {key: 'great',  value: 'Great'},
//           {key: 'good',   value: 'Good'},
//           {key: 'unproven', value: 'Unproven'}
//         ],
//         order: 6
//       }),

//       new FormTextboxComponent({
//         key: 'firstName',
//         label: 'First Name',
//         value: this.dataSource.FirstName,
//         required: true,
//         order: 2
//       }),

//       new FormTextboxComponent({
//         key: 'lastName',
//         label: 'Last Name',
//         value: this.dataSource.LastName,
//         required: true,
//         order: 3
//       }),

//       new FormTextboxComponent({
//         key: 'organisation',
//         label: 'Organisation',
//         value: this.dataSource.Organisation,
//         order: 4
//       }),

//       new FormTextboxComponent({
//         key: 'outstandingDuedateDays',
//         label: 'Outstanding Duedate Days',
//         value: this.dataSource.Outstandingdays,
//         required: true,
//         order: 5
//       }),

//       new FormTextboxComponent({
//         key: 'emailAddress',
//         label: 'Email',
//         type: 'email',
//         value:this.dataSource.Email,
//         order: 1
//       })
//     ];

// constructor(private userService:UmanagementService){
//   console.log("What is this model: ",this.dataSource);
//   this.model = {
//     id:1,
//     userId:20,
//     currencyId:1,
//     countryId:2,
//     Email:'bob',
//     billingId:2,
//     rStatus:'stat',
//     status:6
//   }
// //   this.userService.getUserInfo(this.model)
// //         .subscribe(
// //         data => {
// //           this.retResult = data

// //   if(this.retResult.result == 1){
// //     console.log(this.retResult.result);
// //     this.dataSource = <UserinfoModel>this.retResult.body;
// //   }
// // });
// }

// getusercontrols(userId:number) {
// //   this.userService.getUserInfo(this.model)
// //         .subscribe(
// //         data => {
// //           this.retResult = data

// //   if(this.retResult.result == 1){
// //     console.log(this.retResult.result);
// //     this.dataSource = <UserinfoModel>this.retResult.body;
// //     let controls = this.getusercontrols(userId);
// //     return controls;
// //   }
// // });

// }

// getcontrols(userId:number) {
//   //this.getusercontrols(7);
//   // let controls: FormBase<string>[] = [

//   //   new FormDropdownComponent({
//   //     key: 'roles',
//   //     label: 'Change Role',
//   //     options: [
//   //       {key: 'solid',  value: 'Solid'},
//   //       {key: 'great',  value: 'Great'},
//   //       {key: 'good',   value: 'Good'},
//   //       {key: 'unproven', value: 'Unproven'}
//   //     ],
//   //     order: 6
//   //   }),

//   //   new FormTextboxComponent({
//   //     key: 'firstName',
//   //     label: 'First Name',
//   //     value: this.dataSource.FirstName,
//   //     required: true,
//   //     order: 2
//   //   }),

//   //   new FormTextboxComponent({
//   //     key: 'lastName',
//   //     label: 'Last Name',
//   //     value: this.dataSource.LastName,
//   //     required: true,
//   //     order: 3
//   //   }),

//   //   new FormTextboxComponent({
//   //     key: 'organisation',
//   //     label: 'Organisation',
//   //     value: this.dataSource.Organisation,
//   //     order: 4
//   //   }),

//   //   new FormTextboxComponent({
//   //     key: 'outstandingDuedateDays',
//   //     label: 'Outstanding Duedate Days',
//   //     value: this.dataSource.Outstandingdays,
//   //     required: true,
//   //     order: 5
//   //   }),

//   //   new FormTextboxComponent({
//   //     key: 'emailAddress',
//   //     label: 'Email',
//   //     type: 'email',
//   //     value:this.dataSource.Email,
//   //     order: 1
//   //   })
//   // ];

//   //return of(this.controls.sort((a, b) => a.order - b.order));
// }
// }
