import { Component, Input, OnInit }  from '@angular/core';
import { FormGroup }                 from '@angular/forms';
import { FormBase } from 'src/app/components/dynamic-forms/form-base/form-base';
import { FormService } from 'src/app/components/dynamic-forms/form.service';
import { ContactinfoModel } from 'src/app/components/usermanagement/models/contactinfo-model';
import { Observable }      from 'rxjs';
import { ContactService } from './contact-info.service';

@Component({
  selector: 'app-sub-level',
  templateUrl: './sub-level.component.html',
  styleUrls: ['./sub-level.component.css'],
  providers: [ FormService ]
})
export class SubLevelComponent  implements OnInit {
  @Input() controls: FormBase<string>[] = [];
  form: FormGroup;
  payLoad = '';
  contactData: ContactinfoModel;

  control: FormBase<string>;
  get isValid() { return this.form.controls[this.control.key].valid; }


  constructor(private qcs: FormService) {
    //this.contactData = {
          // "Id": 73,
          // "UserId": 137,
          // "MobileNumber": "254708704119",
          // "FixedLine": "999999",
          // "Email": "Angela@adtones.co.ke",
          // "PhoneNumber": null,
          // "Address": "10 Rillington pl.",
          // "CountryId": 10,
          // "CurrencyId": 7,
          // "AdtoneServerContactId": null
      //}
  }

  ngOnInit() {
    this.form = this.qcs.toFormGroup(this.controls);
  }

  onSubmit() {
    this.payLoad = JSON.stringify(this.form.getRawValue());
  }


}
