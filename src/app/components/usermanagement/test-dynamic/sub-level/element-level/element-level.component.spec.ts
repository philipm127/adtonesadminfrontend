import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ElementLevelComponent } from './element-level.component';

describe('ElementLevelComponent', () => {
  let component: ElementLevelComponent;
  let fixture: ComponentFixture<ElementLevelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
