import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormBase } from 'src/app/components/dynamic-forms/form-base/form-base';

@Component({
  selector: 'app-element-level',
  templateUrl: './element-level.component.html',
  styleUrls: ['./element-level.component.css']
})
export class ElementLevelComponent {

  @Input() control: FormBase<string>;
  @Input() form: FormGroup;

  get isValid() { return this.form.controls[this.control.key].valid; }

}
