export interface PromoDashboardPlayListModel {
  // auditId: number;
  // totalCost: number;
  // playCost: number;
  // emailCost: number;
  // sMSCost: number;
  startTime: string;
  // endTime: string;
  playLength: number;
  // emailMsg: string;
  // sMS: string;
  // userId: number;
  // currencyCode: string;
  // advertName: string;
  msisdn: string;
  dtmfKey: string;
}
