export interface PromotionalCampaignResult
    {
        id: number;
        operatorID: number;
        countryID: number;
        campaignName: string;
        operatorName: string;
        batchID: string;
        maxDaily: number;
        maxWeekly: number;
        advertName: string;
        advertLocation: string;
        status: number;
        rStatus: string;
        files: File;
    }
