export interface CampaignPromoDashboardAuditModel {

  averageBid: number;
  totalSpend: number;
  totalPlayed: number;
  campaignName: string;
  advertName: string;
  currencyCode: string;
  averagePlayTime: number;
  freePlays: number;
  totalBudget: number;
  maxPlayLength: string;
  totalReach: number;
  reach: number;
  operatorName:string;
  advertLocation:string;
}
