import { Component, ViewChild } from '@angular/core';
import { CampaignPromoService } from '../camapaign-promo.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ReturnResult } from 'src/app/models/return-result';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-add-promo-user',
  templateUrl: './add-promo-user.component.html',
  styleUrls: ['./add-promo-user.component.css']
})
export class AddPromoUserComponent {

  pagename: string = "AddPromoUsers";
  userId: number;
  userData: any;
  @ViewChild('file', { static: false }) file;

  public files: Set<File> = new Set();

  form = new FormGroup({
    countryId: new FormControl("", Validators.required),
    operatorId: new FormControl("", Validators.required),
    batchID: new FormControl("", Validators.required)
  });


  userModel: any;
  countrySelect: BasicSelectModel[];
  operatorSelect: BasicSelectModel[];
  destroy$: Subject<BasicSelectModel> = new Subject();


  successMessage: string;
  failMessage: string;


  constructor(private formBuilder: FormBuilder, private campService: CampaignPromoService,
    private accessService: AccessServicesService, private listService: SelectServicesService) {

    this.userData = this.accessService.getUser();
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();


    this.getCountry();
    this.getOperator();
  }


  onFilesAdded() {
    console.log("Entered onFilesAdded()");
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (let key in files) {
      if (!isNaN(parseInt(key))) {
        this.files.add(files[key]);
      }
    }
    this.failMessage = "";
  }

  addFiles() {
    this.file.nativeElement.click();
  }

  getCountry() {
    this.listService.getCountryList()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        console.log("returned result from get country list: ", retResult);
        if (retResult.result == 1) {

          this.countrySelect = <BasicSelectModel[]>retResult.body;
        }
        else
        this.countrySelect = <BasicSelectModel[]>retResult.body;
      });
  }

  getOperator(){
    this.listService.getOperatorList(0)
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        console.log("returned result from get country list: ", retResult);
        if (retResult.result == 1) {

          this.operatorSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }


  onSubmit() {
    let fs: any;
    if (this.files.size > 0) {
      this.files.forEach(file => {
        fs = file;
      });
    }

    if (fs == null) {
      this.failMessage = "No file added";
    }
    else {
      // this.payLoad = JSON.stringify(this.form.getRawValue());
      console.log("What is this form data: ", this.form.value);
      this.campService.addPromoUsers(this.form.value, fs)
        .subscribe(resp => {
          let retModel = resp

          if (retModel.result == 1) {
            this.successMessage = "The camapaign users were added successfully.";
          }
          else {
            this.failMessage = "There was a problem adding the camapaign.";
          }
        });
    }
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
