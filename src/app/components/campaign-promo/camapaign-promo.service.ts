import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ReturnResult } from 'src/app/models/return-result';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CampaignPromoService {

  myAppUrl: string;
  controller: string = "promotionalcampaign/";

  repAppUrl: string;
  controllerRep: string = "campaignaudit/";



  constructor(private http: HttpClient) {
    this.myAppUrl = (environment.appUrl + this.controller),
      this.repAppUrl = (environment.appUrl + this.controllerRep);
  }

  public getPromoCampaignList() {
    let ctrAction = "v1/GetPromoCampaignDataTable"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getDashboardPlayList(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/GetPromoPlayDetailsByCampaign"
    // console.log("What is campaign id submitted parsed: ",id);
    return this.http.put<ReturnResult>(this.repAppUrl + ctrAction, model);
  }


  public getCampaignDashboardAudit(campId: string) {
     // console.log("What is campaign id submitted: ",campId);
    let ctrAction = "v1/GetPromoDashboardSummary/";
    let id = parseInt(campId);
    // console.log("What is campaign id submitted parsed: ",id);
    return this.http.get<ReturnResult>(this.repAppUrl + ctrAction + id, { observe: "response" });
  }


  public updatePromoCampaignStatus(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/UpdatePromoCampaignStatus"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public addPromoCampaign(model:any,file:File){//}: Observable<ReturnResult> {
console.log("What is THE model: ",model);
    let ctrAction = "v1/AddPromoCampaign";
    const formData: FormData = new FormData();
      if(file != null || file != undefined)
        formData.append('files', file, file.name);
      formData.append("BatchID",model.batchID);
      formData.append("OperatorId",model.operatorId);
      formData.append("AdvertName",model.advertName);
      formData.append("CampaignName",model.campaignName);
      formData.append("MaxDaily",model.maxDaily);
      formData.append("MaxWeekly",model.maxWeekly);
      return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, formData);

  }


  public addPromoUsers(model:any,file:File): Observable<ReturnResult> {
    let ctrAction = "v1/AddPromoUser";
    const formData: FormData = new FormData();
      if(file != null || file != undefined)
        formData.append('files', file, file.name);
      formData.append("BatchID",model.batchID.toString());
      formData.append("OperatorId",model.operatorId);
      formData.append("CountryId",model.countryId);

      return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, formData);

  }


  public getBatchList(id:number) {
    console.log("What is operator id submitted: ",id);
   let ctrAction = "v1/GetAvailableBatchId/";
   // console.log("What is campaign id submitted parsed: ",id);
   return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
 }

}
