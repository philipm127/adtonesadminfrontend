import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { PromotionalCampaignResult } from '../models/promo-campaign-table-model';
import { CampaignPromoService } from '../camapaign-promo.service';


@Component({
  selector: 'app-campaign-promo-table',
  templateUrl: './campaign-promo-table.component.html',
  styleUrls: ['./campaign-promo-table.component.css']
})

export class CampaignPromoTableComponent implements OnInit, OnDestroy {
  pagename: string = "CampaignPromoList";
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];

  destroy$: Subject<PromotionalCampaignResult> = new Subject();
  dataSource: MatTableDataSource<PromotionalCampaignResult>;
  modelSource: PromotionalCampaignResult[];
  isLoading:boolean = true;

  operatorSelect: BasicSelectModel[] = [];
  miscName: string = "Batch ID";
  userData: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  filterValues = {};
  retResult: ReturnResult = null;
  retResultUpdate: ReturnResult = null;

  tableSearchModel: TableSearchModel;


  constructor(private campaignService: CampaignPromoService, private datePipe: DatePipe,
    private popSearch: PopSearchDdService, private accessService: AccessServicesService) {

    this.userData = this.accessService.getUser();
    this.PermissionAssignment();
    this.campaignService.getPromoCampaignList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <PromotionalCampaignResult[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<PromotionalCampaignResult>(this.modelSource);
          this.isLoading = false;
        }
        this.operatorSelect = popSearch.popOperator(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("What is in filters: ",filter);
          // Object.keys(filter).forEach(key => console.log("What is key value: ",this.datePipe.transform(filter[key], 'yyyy-MM-dd')));
          const a = !filter.operator || data.operatorName.trim().toLowerCase().includes(filter.operator.trim().toLowerCase());
          const b = !filter.name || data.batchID.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          return a && b;
        }) as (CamapaignPromoElement, string) => boolean;


        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    // console.log("In Permission assignment what is value of permPageData: ",permPageData);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
    // console.log("what is this.emailLink: ", this.emailLink);
  }

  ngOnInit(): void { }

  updateStatus(campaignId: number, status: number) {
    let newModel: PromotionalCampaignResult;
    for (let i in this.modelSource) {
      if (this.modelSource[i].id == campaignId) {
        this.modelSource[i].status = status;
        newModel = this.modelSource[i];
        console.log("What is userData for this id: ", this.modelSource[i]);
        break;
      }
    }

    this.campaignService.updatePromoCampaignStatus(newModel)
      .subscribe(resp => {
        this.retResultUpdate = resp

        if (this.retResultUpdate.result == 1) {
          console.log("returned result retResultUpdate: ", this.retResultUpdate.result);
        }
      });
    console.log("What is status after update: ", this.modelSource);
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
