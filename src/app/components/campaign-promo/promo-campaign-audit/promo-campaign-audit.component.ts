import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-promo-campaign-audit',
  templateUrl: './promo-campaign-audit.component.html',
  styleUrls: ['./promo-campaign-audit.component.css']
})
export class PromoCampaignAuditComponent {

  campId: string;
  dashboardType: string = "campaign";

  constructor(private route: ActivatedRoute,
    private router: Router) {
    this.campId = this.route.snapshot.paramMap.get('id');

  }

}
