import { Component, AfterViewInit, Input } from '@angular/core';
import { ReturnResult } from 'src/app/models/return-result';
import { Subject } from 'rxjs';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { CampaignPromoService } from '../../camapaign-promo.service';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { CampaignPromoDashboardAuditModel } from '../../models/promo-campaign-dashboard-audit-model';

@Component({
  selector: 'app-promo-dashboard',
  templateUrl: './promo-dashboard.component.html',
  styleUrls: ['./promo-dashboard.component.css']
})

export class PromoDashboardComponent implements AfterViewInit {
  pagename: string = "CampaignPromoDashboardAudit";
  permData: any;
  retResult: ReturnResult = null;
  @Input() elementId: string = "0";
  destroy$: Subject<CampaignPromoDashboardAuditModel> = new Subject();
  modelSource: CampaignPromoDashboardAuditModel;

  constructor(private campaignService: CampaignPromoService, private accessService: AccessServicesService) { }

  ngAfterViewInit() {
    this.campaignService.getCampaignDashboardAudit(this.elementId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        // console.log("Returned res result for campaign audit dashboard is: ", res);
        this.retResult = res.body;
        console.log("Returned body for campaign audit dashboard is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <CampaignPromoDashboardAuditModel>this.retResult.body;
        }
      });

  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
