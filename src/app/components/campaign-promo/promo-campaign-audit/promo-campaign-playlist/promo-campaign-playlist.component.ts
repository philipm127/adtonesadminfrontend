import { Component, AfterViewInit, OnDestroy, Input, ViewChild } from '@angular/core';
import { ReturnResult } from 'src/app/models/return-result';
import { Subject, merge } from 'rxjs';
import { PromoDashboardPlayListModel } from '../../models/promo-dashboard-playlist-model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { DatePipe } from '@angular/common';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { tap, takeUntil } from 'rxjs/operators';
import { PageSortSearchModel } from 'src/app/models/PageSortSearchModel';
import { CampaignPromoService } from '../../camapaign-promo.service';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-promo-campaign-playlist',
  templateUrl: './promo-campaign-playlist.component.html',
  styleUrls: ['./promo-campaign-playlist.component.css']
})

export class PromoCampaignPlaylistComponent implements AfterViewInit, OnDestroy {

  pagename: string = "PromoDashboardPlayList";
  permData: any;
  retResult: ReturnResult = null;
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];

  userData: any;
  miscName: string = "MSISDN";
  fullName: string = "DTMF";
  numberName: string = "Play Length";
  responseName: string = "Play Date"
  @Input() campId: string = "0";

  destroy$: Subject<PromoDashboardPlayListModel> = new Subject();
  // dataSource: MatTableDataSource<CampaignAdminResult>;
  modelSource: PromoDashboardPlayListModel[];
  dataSource: MatTableDataSource<PromoDashboardPlayListModel>;
  resultsLength: number;
  isLoading: boolean = true;
  isLoadingResults: boolean = false;


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  filterValues: string;

  tableSearchModel: TableSearchModel;

  constructor(private campaignService: CampaignPromoService,
    private datePipe: DatePipe, private popSearch: PopSearchDdService,
    private accessService: AccessServicesService) {

    this.userData = this.accessService.getUser();
    this.PermissionAssignment();

  }

  filtered(filter: string) {
    // console.log("What is in filters: ",filter);
    this.filterValues = filter;
    this.paginator.pageIndex = 0;
    // console.log("What is in filters: ",this.filterValues);
    this.loadData();
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    // console.log("In Permission assignment what is value of permPageData: ", permPageData);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }


  ngAfterViewInit() {
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.loadData();

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadData())
      )
      .subscribe();
  }

  loadData() {
    let paging = new PageSortSearchModel();
    paging.elementId = parseInt(this.campId);
    paging.page = this.paginator.pageIndex;
    paging.pageSize = this.paginator.pageSize;
    paging.direction = this.sort.direction;
    paging.sort = this.sort.active;
    paging.search = JSON.stringify(this.filterValues);

    // Hidden until needed
    this.campaignService.getDashboardPlayList(paging)
      .subscribe(
        data => {
          this.retResult = data
          if (this.retResult.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.modelSource = <PromoDashboardPlayListModel[]>this.retResult.body;
            this.isLoading = false;
            // console.log("returned userModel: ", this.modelSource);
            this.resultsLength = this.retResult.recordcount;
          }
        });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
