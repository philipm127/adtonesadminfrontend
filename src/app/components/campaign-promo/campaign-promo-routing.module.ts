import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthguardService } from 'src/app/services/auth/authguard.service';
import { CampaignPromoTableComponent } from './campaign-promo-table/campaign-promo-table.component';
import { PromoCampaignAuditComponent } from './promo-campaign-audit/promo-campaign-audit.component';
import { AddPromoCampaignComponent } from './add-promo-campaign/add-promo-campaign.component';
import { AddPromoUserComponent } from './add-promo-user/add-promo-user.component';


const campignpromoRoutes: Routes = [
  { path: 'promotable', component: CampaignPromoTableComponent },
  { path: 'promocampaignaudit/:id', component: PromoCampaignAuditComponent },
  { path: 'addpromocampaign', component: AddPromoCampaignComponent },
  { path: 'addpromouser', component: AddPromoUserComponent }
];

@NgModule({
  imports: [RouterModule.forChild(campignpromoRoutes)],
  exports: [RouterModule]
})
export class CampaignPromoRoutingModule { }
