import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CampaignPromoService } from '../camapaign-promo.service';
import { ReturnResult } from 'src/app/models/return-result';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-add-promo-campaign',
  templateUrl: './add-promo-campaign.component.html',
  styleUrls: ['./add-promo-campaign.component.css']
})

export class AddPromoCampaignComponent implements OnInit {
  pagename: string = "AddPromoCampaign";
  @ViewChild('file', { static: false }) file;
  retModel: ReturnResult = null;
  successMessage: string;
  failMessage: string;

  batchSelect: BasicSelectModel[];
  operatorSelect: BasicSelectModel[];

  destroy$: Subject<BasicSelectModel> = new Subject();

  form = new FormGroup({
    batchID: new FormControl("", Validators.required),
    operatorId: new FormControl("", Validators.required),
    maxDaily: new FormControl("", Validators.required),
    maxWeekly: new FormControl("", Validators.required),
    campaignName: new FormControl("", Validators.required),
    advertName: new FormControl("", Validators.required)
  });

  public files: Set<File> = new Set();

  constructor(private campaignService: CampaignPromoService, private accessService: AccessServicesService,
    private listService: SelectServicesService) {
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();
    this.getOperator();
  }

  ngOnInit(): void {
  }

  onFilesAdded() {
    console.log("Entered onFilesAdded()");
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (let key in files) {
      if (!isNaN(parseInt(key))) {
        this.files.add(files[key]);
      }
    }
    this.failMessage = "";
  }

  addFiles() {
    this.file.nativeElement.click();
  }

  getOperator() {
    this.listService.getOperatorList(0)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {
          console.log("returned result from get role list: ", retResult);
          this.operatorSelect = <BasicSelectModel[]>retResult.body;
          // this.initForm();
        }
      });
  }


  popBatch(event: any) {
    console.log("popCampaign(event:any) ", event.value);
    this.getBatch(event.value);
  }

  getBatch(id: number) {
    this.campaignService.getBatchList(id)
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        console.log("returned result from get campaign list: ", retResult);
        if (retResult.result == 1) {

          this.batchSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }


  onSubmit() {
    let fs: any;
    if (this.files.size > 0) {
      this.files.forEach(file => {
        fs = file;
      });
    }

    // this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log("What is this form data: ", this.form.value);
    if (fs == null) {
      this.failMessage = "No file added";
    }
    else {
      this.campaignService.addPromoCampaign(this.form.value, fs)
        .subscribe(resp => {
          let retModel = resp

          if (retModel.result == 1) {
            this.successMessage = "The camapaign Was Added Successfully";
          }
          else {
            this.failMessage = retModel.error;//"There was a problem adding the camapaign";
          }
        });
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
