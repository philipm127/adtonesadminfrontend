import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignPromoRoutingModule } from './campaign-promo-routing.module';
import { CampaignPromoTableComponent } from './campaign-promo-table/campaign-promo-table.component';
import { AddPromoCampaignComponent } from './add-promo-campaign/add-promo-campaign.component';
import { PromoCampaignAuditComponent } from './promo-campaign-audit/promo-campaign-audit.component';
import { SharedModule } from '../shared/shared.module';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatTableExporterModule } from 'mat-table-exporter';
import { PromoDashboardComponent } from './promo-campaign-audit/promo-dashboard/promo-dashboard.component';
import { PromoCampaignPlaylistComponent } from './promo-campaign-audit/promo-campaign-playlist/promo-campaign-playlist.component';
import { AddPromoUserComponent } from './add-promo-user/add-promo-user.component';


@NgModule({
  declarations: [
    CampaignPromoTableComponent,
    AddPromoCampaignComponent,
    PromoCampaignAuditComponent,
    PromoDashboardComponent,
    PromoCampaignPlaylistComponent,
    AddPromoUserComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule,
    CampaignPromoRoutingModule
  ]
})
export class CamapaignPromoModule { }
