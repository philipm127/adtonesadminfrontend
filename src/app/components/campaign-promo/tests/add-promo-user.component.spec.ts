import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddPromoUserComponent } from '../add-promo-user/add-promo-user.component';

describe('AddPromoUserComponent', () => {
  let component: AddPromoUserComponent;
  let fixture: ComponentFixture<AddPromoUserComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPromoUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPromoUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
