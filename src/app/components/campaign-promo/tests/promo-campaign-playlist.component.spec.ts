import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PromoCampaignPlaylistComponent } from '../promo-campaign-audit/promo-campaign-playlist/promo-campaign-playlist.component';

describe('PromoCampaignPlaylistComponent', () => {
  let component: PromoCampaignPlaylistComponent;
  let fixture: ComponentFixture<PromoCampaignPlaylistComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoCampaignPlaylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoCampaignPlaylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
