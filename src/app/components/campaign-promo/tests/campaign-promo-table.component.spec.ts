import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CampaignPromoTableComponent } from '../campaign-promo-table/campaign-promo-table.component';

describe('CampaignPromoTableComponent', () => {
  let component: CampaignPromoTableComponent;
  let fixture: ComponentFixture<CampaignPromoTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignPromoTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignPromoTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
