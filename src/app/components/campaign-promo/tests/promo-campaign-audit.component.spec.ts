import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { PromoCampaignAuditComponent } from '../promo-campaign-audit/promo-campaign-audit.component';


describe('PromoCampaignAuditComponent', () => {
  let component: PromoCampaignAuditComponent;
  let fixture: ComponentFixture<PromoCampaignAuditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoCampaignAuditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoCampaignAuditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
