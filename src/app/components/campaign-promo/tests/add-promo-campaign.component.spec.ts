import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AddPromoCampaignComponent } from '../add-promo-campaign/add-promo-campaign.component';


describe('AddPromoCampaignComponent', () => {
  let component: AddPromoCampaignComponent;
  let fixture: ComponentFixture<AddPromoCampaignComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPromoCampaignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPromoCampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
