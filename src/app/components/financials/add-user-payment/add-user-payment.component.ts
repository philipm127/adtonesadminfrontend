import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { FinancialService } from '../financial.service';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';
import { ActivatedRoute } from '@angular/router';
// import { parse } from 'path';
import { UserPaymentDetailModel } from '../models/user-payment-model';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-add-user-payment',
  templateUrl: './add-user-payment.component.html',
  styleUrls: ['./add-user-payment.component.css']
})
export class AddUserPaymentComponent implements OnInit {
  pagename: string = "AddUserPayment";

  outstandingAmount: number = 1;
  form = new FormGroup({
    fullName: new FormControl(""),
    campaignName: new FormControl(""),
    campaignProfileId: new FormControl(""),
    billingId: new FormControl(""),
    amount: new FormControl(['', [Validators.min(1),Validators.pattern(/^\d*\.?\d*$/), Validators.max(this.outstandingAmount)]]),
    outstandingAmount: new FormControl(""),
    description: new FormControl("", Validators.required),
    invoiceNumber: new FormControl(""),
    userId: new FormControl("")
  });
  get f() { return this.form.controls; }


  // invoiceSelect: BasicSelectModel[];
  // campaignSelect: BasicSelectModel[];
  // userSelect: BasicSelectModel[];
  outstandingVisible: boolean = false;
  invoiceId: number = 0;
  model: UserPaymentDetailModel;


  destroy$: Subject<BasicSelectModel[]> = new Subject();

  submitEnabled: boolean = true;

  userData: any;

  successMessage: string;
  failMessage: string;



  constructor(private formBuilder: FormBuilder, private finService: FinancialService,
    private accessService: AccessServicesService, private listService: SelectServicesService,
    private route: ActivatedRoute) {
      console.log("get billing id string and is: ",this.route.snapshot.paramMap.get('id'));
      console.log("get billing id int and is: ",parseInt(this.route.snapshot.paramMap.get('id')));
    this.invoiceId = parseInt(this.route.snapshot.paramMap.get('id'));
    this.userData = this.accessService.getUser();

    let permListData = this.accessService.getPermissionList(this.pagename);
    // permListData["elements"].forEach(element => {
    //   if (element.type == "element" && element.name == this.accessService.getName(this.form.get(element.name))) {
    //     this[element.name.concat('Visible')] = element.visible;
    //     if (element.enabled == false)
    //       this.form.get(element.name).disable();
    //   }
    //   else if (element.type == "button") {
    //     this.submitEnabled = element.enabled;
    //   }
    // });
  }

  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();
    this.getInvoiceDetails();
  }

  getInvoiceDetails() {
    this.finService.GetInvoiceDetails(this.invoiceId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        // console.log("Returned body is: ", retResult);
        if (retResult.result == 1) {
          this.model = <UserPaymentDetailModel>retResult.body;
          this.initForm();
        }
      })
  }



  initForm() {
    let outs = this.model.outstandingAmount;
    this.form.patchValue
      ({
        outstandingAmount: this.model.outstandingAmount,
        fullName: this.model.fullName,
        invoiceNumber: this.model.invoiceNumber,
        billingId: this.model.billingId,
        campaignName: this.model.campaignName,
        campaignProfileId: this.model.campaignProfileId,
        userId: this.model.userId
      })
    this.form.get('outstandingAmount').disable();
    const validators = [Validators.required, Validators.max(outs)];
    this.form.get('amount').setValidators([Validators.required, Validators.max(outs)]);
    this.form.get('amount').updateValueAndValidity();
    this.form.get('fullName').disable();
    this.form.get('invoiceNumber').disable();
    this.form.get('campaignName').disable();
  }



  onSubmit() {
    let frmData = this.form.getRawValue();
    //this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log("What is this form data: ", this.form.value);
    console.log("What is this raw form data: ", frmData);
    let newModel = {
      billingId: frmData.billingId,
      userId: frmData.userId,
      description: frmData.description,
      campaignProfileId: frmData.campaignProfileId,
      amount: frmData.amount
    }
    this.finService.addUserPayment(newModel)
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          this.successMessage = "The amount was added successfully";
        }
        else {
          this.failMessage = "There was a problem adding the payment";
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
