import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ReturnResult } from 'src/app/models/return-result';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { FinancialService } from 'src/app/components/financials/financial.service';
import { SelectServicesService } from 'src/app/components/shared/services/select-services.service';
import { HttpResponse } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';

import { UserCreditDetailModel } from '../../models/user-credit-detail-model';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';

@Component({
  selector: 'app-advertisercredit-details',
  templateUrl: './advertisercredit-details.component.html',
  styleUrls: ['./advertisercredit-details.component.css']
})

export class AdvertiserCreditDetailsComponent implements OnInit {

  pagename: string = "UpdateAdvertiserCreditDetails";
  pageHeader: string = "Advertiser Credit Details";
  pageheaderAdd: string = "Add Advertiser Credit";
  submitButton: string = "Update";
  usersId: any;
  editForm: boolean = true;

  form = new FormGroup({
    assignCredit: new FormControl("", Validators.required),
    currencyId: new FormControl("", Validators.required),
    countryId: new FormControl(""),
    userId: new FormControl(0, Validators.required),
    id: new FormControl(0),
    submit: new FormControl()
  });


  submitEnabled: boolean = true;

  userModel: UserCreditDetailModel;
  userSelect: BasicSelectModel[];
  currencySelect: BasicSelectModel[];
  countrySelect: BasicSelectModel[];
  destroy$: Subject<BasicSelectModel> = new Subject();

  displayedColumns: string[] = ['invoiceNumber', 'amount', 'createdDate'];

  userData: any;

  successMessage: string;
  failMessage: string;



  constructor(private formBuilder: FormBuilder, private creditService: FinancialService,
    private listService: SelectServicesService, private accessService: AccessServicesService,
    private route: ActivatedRoute, private router: Router) {

    this.usersId = this.route.snapshot.paramMap.get('id');
    if (this.usersId == "0") {
      this.editForm = false;
      this.pagename = "AddAdvertiserCreditDetails";
      this.pageHeader = this.pageheaderAdd;
      this.submitButton = "Submit";
    }
    this.userData = this.accessService.getUser();

    this.listService.getUserCreditDetailList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body
        if (retResult.result == 1) {
          console.log("returned retResult: ", retResult);
          let selectables = retResult.body;
          // console.log("returned selectables: ", selectables);
          if (this.editForm)
            this.userSelect = selectables["users"];
          else
            this.userSelect = selectables["addUser"];

          this.countrySelect = selectables["country"];
          this.currencySelect = selectables["currency"];
        }
      });

    let permListData = this.accessService.getPermissionList(this.pagename);
    permListData["elements"].forEach(element => {
      if (element.type == "element" && element.name == this.accessService.getName(this.form.get(element.name))) {
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
      else if (element.type == "button") {
        this.submitEnabled = element.enabled;
      }
    });

    this.accessService.refreshToken();
  }

  ngOnInit() {
    if (this.editForm) {
      this.creditService.GetUserCreditDetails(this.usersId)
        .pipe(takeUntil(this.destroy$))
        .subscribe((res: HttpResponse<ReturnResult>) => {
          let retModel = res.body
          if (retModel.result == 1) {
            this.userModel = <UserCreditDetailModel>retModel.body;
            console.log("returned userModel: ", this.userModel);
            this.initForm();
          }
        });
    }
  }


  private initForm() {
    // console.log("What is this model: ",this.userModel);
    this.form.patchValue({
      id: this.userModel.id,
      assignCredit: this.userModel.assignCredit,
    });
    this.form.get('userId').setValue(this.userModel.userId.toString());
    this.form.get('countryId').setValue(this.userModel.countryId.toString());
    this.form.get('currencyId').setValue(this.userModel.currencyId.toString());
  }


  onSubmit() {
    console.log("What is this form data: ", this.form.getRawValue());
    let addCredit = null;
    // if (this.editForm)
    //   addCredit = this.creditService.updateAdvertiserCreditLimit(this.form.value);
    // else
    addCredit = this.creditService.addAdvertiserCreditLimit(this.form.getRawValue());
    addCredit
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          console.log("returned result: ", retModel.result);
          // this.userModel = <ContactinfoModel>retModel.body;
          // console.log("returned userModel: ", this.userModel);
          // this.initForm();
          this.successMessage = "The available credit Was added Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the credit";
        }
      });
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
