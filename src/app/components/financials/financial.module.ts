import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinancialRoutingModule } from './financial-routing.module';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatTableExporterModule } from 'mat-table-exporter';

import { AdvertiserCreditTableComponent } from './usercredit/advertisercredit-table/advertisercredit-table.component';
import { SharedModule } from '../shared/shared.module';
import { FinancialTablesComponent } from './financial-tables/financial-tables.component';
import { OutstandingInvoiceTableComponent } from './outstanding-invoice-table/outstanding-invoice-table.component';
import { InvoicesTableComponent } from './invoices-table/invoices-table.component';
import { AdvertiserCreditDetailsComponent } from './usercredit/advertisercredit-details/advertisercredit-details.component';
import { CampaignCreditPeriodTableComponent } from './campaign-credit-period-table/campaign-credit-period-table.component';
import { AddCampaignCreditPeriodComponent } from './add-campaign-credit-period/add-campaign-credit-period.component';
import { AddUserPaymentComponent } from './add-user-payment/add-user-payment.component';
import { AddBillingPaymentComponent } from './add-billing-payment/add-billing-payment.component';
import { UserCreditPaymentComponent } from './add-billing-payment/user-credit-payment/user-credit-payment.component';



@NgModule({
  declarations: [
    AdvertiserCreditTableComponent,
    FinancialTablesComponent,
    OutstandingInvoiceTableComponent,
    InvoicesTableComponent,
    AdvertiserCreditDetailsComponent,
    CampaignCreditPeriodTableComponent,
    AddCampaignCreditPeriodComponent,
    AddUserPaymentComponent,
    AddBillingPaymentComponent,
    UserCreditPaymentComponent
  ],
  imports: [
    CommonModule,
    FinancialRoutingModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule,
    SharedModule
  ],
  exports:[
    AdvertiserCreditTableComponent
  ]
})
export class FinancialModule { }
