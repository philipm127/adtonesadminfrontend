import { Component, OnInit } from '@angular/core';
import { FinancialService } from '../financial.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { RewardResult } from '../../config-system/models/reward-table-model';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';
import { CampaignCreditPeriodModel } from '../models/campaign-credit-model';

@Component({
  selector: 'app-add-campaign-credit-period',
  templateUrl: './add-campaign-credit-period.component.html',
  styleUrls: ['./add-campaign-credit-period.component.css']
})
export class AddCampaignCreditPeriodComponent implements OnInit {

  pagename: string = "AddCampaignCredit";
  campId: string = "0";

  form:FormGroup;


  editMode: boolean = false;
  userModel: CampaignCreditPeriodModel;

  userSelect: BasicSelectModel[];
  campaignSelect: BasicSelectModel[];

  destroy$: Subject<RewardResult> = new Subject();

  submitEnabled: boolean = true;

  userData: any;

  successMessage: string;
  failMessage: string;

  inputvalue = "";


  constructor(private formBuilder: FormBuilder, private finService: FinancialService,
    private accessService: AccessServicesService, private listService: SelectServicesService,
    private route: ActivatedRoute, private router: Router) {

    this.campId = this.route.snapshot.paramMap.get('id');
    if (parseInt(this.campId) > 0) {
      this.pagename = "EditCampaignCredit";
      this.editMode = true;
    }

    this.form = this.formBuilder.group({
      campaignCreditPeriodId: new FormControl(""),
      userId: new FormControl(""),
      campaignProfileId: new FormControl("", Validators.required),
      creditPeriod: new FormControl("", [Validators.required,Validators.max(90)])
    });

    this.userData = this.accessService.getUser();
    this.getUsers();

    let permListData = this.accessService.getPermissionList(this.pagename);
    permListData["elements"].forEach(element => {
      if (element.type == "element" && element.name == this.accessService.getName(this.form.get(element.name))) {
        this[element.name.concat('Visible')] = element.visible;
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
      else if (element.type == "button") {
        this.submitEnabled = element.enabled;
      }
    });
  }

  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();

    if (this.editMode) {
      this.finService.getCampaignCreditList(this.campId)
        .subscribe((res: HttpResponse<ReturnResult>) => {
          let retModel = res.body;
          if (retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <CampaignCreditPeriodModel>retModel.body[0];
            console.log("returned userModel: ", this.userModel);
            this.initForm();
          }
        });
    }
  }

  getUsers() {
    this.listService.getUserPaymentList()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        console.log("returned result from get users list: ", retResult);
        if (retResult.result == 1) {

          this.userSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }


  popCampaign(event: any) {
    console.log("popCampaign(event:any) ", event.value);
    this.getCampaign(event.value);
  }

  getCampaign(id: number) {
    this.listService.getCampaignList(id)
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        console.log("returned result from get campaign list: ", retResult);
        if (retResult.result == 1) {

          this.campaignSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }

  private initForm() {
    if (this.editMode) {
      this.getCampaign(this.userModel.userId);//this.userModel.userId);
      this.form.patchValue({
        campaignCreditPeriodId: this.userModel.campaignCreditPeriodId,
        creditPeriod: this.userModel.creditPeriod
      });
      this.form.get('userId').setValue(this.userModel.userId.toString());
      this.form.get('campaignProfileId').setValue(this.userModel.campaignProfileId.toString());
      this.form.get('userId').disable();
      this.form.get('campaignProfileId').disable();
    }
  }

  onSubmit() {
    //this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log("What is this form data: ", this.form.value);
    let serviceResult = null;
    if (this.editMode)
      serviceResult = this.finService.updateCampaignCreditPeriod(this.form.value);
    else
      serviceResult = this.finService.addCampaignCreditPeriod(this.form.value);
    serviceResult
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          this.successMessage = "The Details Were Updated Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the details";
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
