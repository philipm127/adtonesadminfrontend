import { HttpResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';
import { CampaignService } from '../../camapign/campaign.service';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { MessageService } from '../../shared/services/message.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { FinancialService } from '../financial.service';
import { BuyCreditModel } from '../models/user-payment-model';
import { CurrencyEnum } from 'src/app/models/currencyEnum';

@Component({
  selector: 'app-add-billing-payment',
  templateUrl: './add-billing-payment.component.html',
  styleUrls: ['./add-billing-payment.component.css']
})
export class AddBillingPaymentComponent implements OnInit {

  destroy$: Subject<BasicSelectModel> = new Subject();
  campaignSelect: BasicSelectModel[];
  currencySelect: BasicSelectModel[];
  frmStepOne: FormGroup;
  campaignId: number;
  userData: any;
  modelSource: BuyCreditModel;
  failMessage: string;

  appliedCredit: number = 0;
  maxFundAmount: number = 0;
  advertiserId: number = 0;

  currencySymbol = CurrencyEnum;

  submitted: boolean = false;

  constructor(private accessService: AccessServicesService,
    private route: ActivatedRoute, private router: Router, private changeDetect: ChangeDetectorRef,
    private campaignService: CampaignService, private formBuilder: FormBuilder, private listService: SelectServicesService,
    private finService: FinancialService, private messService: MessageService) {
    this.userData = this.accessService.getUser();
    this.campaignId = parseInt(this.route.snapshot.paramMap.get('id'));
    this.advertiserId = parseInt(this.route.snapshot.paramMap.get('userId'));
    this.getCampaign(this.advertiserId);
    this.getCurrency();
    // this.PermissionAssignment();

  }


  ngOnInit(): void {
    this.frmStepOne = this.formBuilder.group({
      campaignProfileId: new FormControl("", Validators.required),
      currencyId: new FormControl("", Validators.required),
      fundAmount: new FormControl(""),
      poNumber: new FormControl("")
    });
  }


  initForm() {

    this.frmStepOne.controls['fundAmount'].setValidators([Validators.required, Validators.pattern(/^\d*\.?\d*$/), Validators.min(1), Validators.max(this.maxFundAmount)]);
    if (this.advertiserId != this.userData.userId) {
      this.frmStepOne.get('campaignProfileId').disable();
    }
    this.frmStepOne.get('campaignProfileId').setValue(this.modelSource.campaignProfileId.toString());
    this.frmStepOne.get('currencyId').setValue(this.modelSource.currencyId.toString());

    this.frmStepOne.get('currencyId').disable();

  }


  applyTax(event: any) {
    let x: number = this.frmStepOne.get('fundAmount').value;
    this.appliedCredit = x * ((this.modelSource.taxPercantage / 100) + 1);
    console.log("applyTax(event:any) let x: ", x);
  }


  onSubmit() {
    this.modelSource.fundAmount = this.frmStepOne.get('fundAmount').value;
    this.modelSource.poNumber = this.frmStepOne.get('poNumber').value;
    this.modelSource.campaignProfileId = this.frmStepOne.get('campaignProfileId').value;
    this.modelSource.currencyId = this.frmStepOne.get('currencyId').value;
    this.modelSource.totalAmount = this.appliedCredit;

    this.submitted = true;

    this.finService.submitPayment(this.modelSource)
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          let message = retModel.body;
          this.messService.sendMessage({ text: message.toString(), category: 'success' });
          this.router.navigate(['/salescampaign']);
        }
        else {
          this.failMessage = "There was a problem adding the payment. " + retModel.error;
        }
      });
  }



  getBillingDetails(campaign: number) {
    this.finService.getBillingPaymentDetails(campaign, this.advertiserId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        console.log("Returned BuyCreditModel body is: ", retResult);
        if (retResult.result == 1) {
          this.modelSource = <BuyCreditModel>retResult.body;
          if (this.modelSource) {
            this.maxFundAmount = (this.modelSource.availableCredit / (this.modelSource.taxPercantage / 100 + 1));
            this.initForm();
          }
          else
            this.failMessage = "The Advertiser has no Credit setup.<br /> Ask Admin to allot some credit for the Advertiser OR<br />The Advertiser can fund the campaign using any approved payment method through their account on <b>my.adtones.com<b>";
        }
        else
          this.failMessage = "There was a problem adding credit to the Campaign " + retResult.error;
      })
  }

  getCampaign(event: number) {
    this.listService.getCampaignList(event)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        if (retResult.result == 1) {
          console.log("Returned getCampaign body is: ", retResult);
          this.campaignSelect = <BasicSelectModel[]>retResult.body;
          this.getBillingDetails(this.campaignId);
        }
      });
  }


  getCurrency() {
    this.listService.getCurrencyList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {
          this.currencySelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }


}
