import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UserCreditPaymentComponent } from './user-credit-payment.component';

describe('UserCreditPaymentComponent', () => {
  let component: UserCreditPaymentComponent;
  let fixture: ComponentFixture<UserCreditPaymentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCreditPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCreditPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
