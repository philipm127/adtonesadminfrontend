import { Component, ViewChild, AfterViewInit, ElementRef, HostListener } from '@angular/core';
import { Subject } from 'rxjs';
import { OutstandingInvoiceResult } from '../models/invoices-table-model';
import { MatTableDataSource } from '@angular/material/table';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TableSearchModel } from '../../shared/table-search-model';
import { FinancialService } from '../financial.service';
import { DatePipe } from '@angular/common';
import { PopSearchDdService } from '../../shared/services/pop-search-dd.service';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';
import { CurrencyEnum } from 'src/app/models/currencyEnum';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PdfModalComponent } from '../../shared/pdf-modal/pdf-modal.component';
import { SelectServicesService } from '../../shared/services/select-services.service';


@Component({
  selector: 'app-outstanding-invoice-table',
  templateUrl: './outstanding-invoice-table.component.html',
  styleUrls: ['./outstanding-invoice-table.component.css']
})
export class OutstandingInvoiceTableComponent implements AfterViewInit {
  pagename: string = "OutstandingInvoicesTable";
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = []
  userData: any;
  // Sent to set filter names.
  createdDateName: string = "Invoice Date";
  numberName: string = "Paid Amount";
  miscName: string = "Invoice Number";

  destroy$: Subject<OutstandingInvoiceResult> = new Subject();
  dataSource: MatTableDataSource<OutstandingInvoiceResult>;
  modelSource: OutstandingInvoiceResult[];
  isLoading: boolean = true;

  statusSelect: BasicSelectModel[] = [];
  clientSelect: BasicSelectModel[] = [];
  paymentSelect: BasicSelectModel[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  tableSearchModel: TableSearchModel;
  currencySymbol = CurrencyEnum;


  constructor(private financialService: FinancialService, private datePipe: DatePipe,
    private popSearch: PopSearchDdService, private accessService: AccessServicesService,
    private matDialog: MatDialog, private pdfService: SelectServicesService) {

    this.userData = this.accessService.getUser();
    this.PermissionAssignment();

  }

  ngAfterViewInit() {
    this.financialService.getOutstandingInvoiceList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        // console.log("Returned body is: ", retResult);
        if (retResult.result == 1) {
          this.isLoading = false;
          this.modelSource = <OutstandingInvoiceResult[]>retResult.body;
          this.dataSource = new MatTableDataSource<OutstandingInvoiceResult>(this.modelSource);
        }
        this.statusSelect = this.popSearch.popStatus(this.modelSource);
        this.clientSelect = this.popSearch.popClient(this.modelSource);
        this.paymentSelect = this.popSearch.popPayment(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("What is in filters: ",filter);
          // Object.keys(filter).forEach(key => console.log("What is key value: ",this.datePipe.transform(filter[key], 'yyyy-MM-dd')));
          const a = !filter.fullName || data.fullName.trim().toLowerCase().includes(filter.fullName.trim().toLowerCase());
          const b = !filter.client || data.clientName.trim().toLowerCase().includes(filter.client.trim().toLowerCase());
          const c = !filter.status || data.status.includes(filter.status);
          const d = !filter.dateFrom || data.createdDate >= this.datePipe.transform(filter.dateFrom, 'yyyy-MM-dd');
          const e = !filter.dateTo || data.createdDate <= this.datePipe.transform(filter.dateTo, 'yyyy-MM-dd');
          const f = !filter.name || data.invoiceNumber.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          const j = !filter.numberFrom || data.paidAmount >= parseInt(filter.numberFrom);
          const k = !filter.numberTo || data.paidAmount <= parseInt(filter.numberTo);
          return a && b && c && d && e && f && j && k;
        }) as (OutstandingInvoiceElement, string) => boolean;


        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }


  getsPDFFile(script: string) {
    this.pdfService.downloadPDF(script).subscribe((res: any) => {
      var file = new Blob([res], { type: 'application/pdf' });
      var fileURL = URL.createObjectURL(file);
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = fileURL;
      dialogConfig.height = "550";
      dialogConfig.width = "650";
      let dialogRef = this.matDialog.open(PdfModalComponent, dialogConfig);
    });
  }




  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
