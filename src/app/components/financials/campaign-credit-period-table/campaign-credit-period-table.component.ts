import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { CampaignCreditPeriodModel } from '../models/campaign-credit-model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TableSearchModel } from '../../shared/table-search-model';
import { FinancialService } from '../financial.service';
import { DatePipe } from '@angular/common';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';

@Component({
  selector: 'app-campaign-credit-period-table',
  templateUrl: './campaign-credit-period-table.component.html',
  styleUrls: ['./campaign-credit-period-table.component.css']
})
export class CampaignCreditPeriodTableComponent implements OnInit {

  pagename: string = "CampaignCreditTable";
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = []
  userData: any;
  // Sent to set filter names.
  fullName: string = "Advertiser";
  miscName: string = "Campaign";

  destroy$: Subject<CampaignCreditPeriodModel> = new Subject();
  dataSource: MatTableDataSource<CampaignCreditPeriodModel>;
  modelSource: CampaignCreditPeriodModel[];
  isLoading: boolean = true;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  tableSearchModel: TableSearchModel;

  constructor(private financialService: FinancialService, private datePipe: DatePipe, private accessService: AccessServicesService) {

    this.userData = this.accessService.getUser();
    this.PermissionAssignment();

  }

  ngOnInit(): void {
    this.financialService.getCampaignCreditList(0)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        // console.log("Returned body is: ", retResult);
        if (retResult.result == 1) {
          this.isLoading = false;
          this.modelSource = <CampaignCreditPeriodModel[]>retResult.body;
          this.dataSource = new MatTableDataSource<CampaignCreditPeriodModel>(this.modelSource);
        }

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("What is in filters: ", filter);
          const a = !filter.fullName || data.userName.trim().toLowerCase().includes(filter.fullName.trim().toLowerCase());
          const b = !filter.name || data.campaignName.trim().toLowerCase().includes(filter.name.trim().toLowerCase());

          return a && b;
        }) as (CampaignCreditElement, string) => boolean;


        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
