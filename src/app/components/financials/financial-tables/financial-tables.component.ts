import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-financial-tables',
  templateUrl: './financial-tables.component.html',
  styleUrls: ['./financial-tables.component.css']
})
export class FinancialTablesComponent implements OnInit {
  @Input() displayedColumns: string[]=[];
  @Input() createdDateDisplay:string='Created Date';
  @Input() dataSource: MatTableDataSource<any>;
  constructor() { }

  ngOnInit(): void {
  }

}
