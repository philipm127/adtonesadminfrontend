import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdvertiserCreditTableComponent } from './usercredit/advertisercredit-table/advertisercredit-table.component';
import { InvoicesTableComponent } from './invoices-table/invoices-table.component';
import { AdvertiserCreditDetailsComponent } from './usercredit/advertisercredit-details/advertisercredit-details.component';
import { OutstandingInvoiceTableComponent } from './outstanding-invoice-table/outstanding-invoice-table.component';
import { CampaignCreditPeriodTableComponent } from './campaign-credit-period-table/campaign-credit-period-table.component';
import { AddCampaignCreditPeriodComponent } from './add-campaign-credit-period/add-campaign-credit-period.component';
import { AddUserPaymentComponent } from './add-user-payment/add-user-payment.component';
import { AddBillingPaymentComponent } from './add-billing-payment/add-billing-payment.component';


const financialRoutes: Routes = [
  { path: 'advertisercredit', component: AdvertiserCreditTableComponent },
  { path: 'advertisercreditdetails/:id', component: AdvertiserCreditDetailsComponent },
  { path: 'invoicelist', component: InvoicesTableComponent },
  { path: 'outstandinginvoicelist', component: OutstandingInvoiceTableComponent },
  { path: 'creditperiod', component: CampaignCreditPeriodTableComponent },
  { path: 'creditperioddetail/:id', component: AddCampaignCreditPeriodComponent },
  { path: 'adduserpayment/:id', component: AddUserPaymentComponent },
  { path: 'buycredit/:id/:userId', component: AddBillingPaymentComponent }
];

@NgModule({
  imports: [RouterModule.forChild(financialRoutes)],
  exports: [RouterModule]
})
export class FinancialRoutingModule { }
