import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ReturnResult } from '../../models/return-result';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class FinancialService {
  myAppUrl: string;
  controller: string = "financials/";
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  };

  constructor(private http: HttpClient) {
    this.myAppUrl = (environment.appUrl + this.controller);
  }

  public getUserCreditList() {
    let ctrAction = "v1/GetCreditData"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  public getUserCreditListForSalesExec(id: number) {
    let ctrAction = "v1/GetAdvertiserCreditListForSales/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public getInvoiceListForSalesExec(id: number) {
    let ctrAction = "v1/GetInvoicListForSales/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getInvoiceListForAdvertiser(id: number) {
    let ctrAction = "v1/GetInvoicListForAdvertiser/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getOutstandingInvoiceListForsalesExec(id: number) {
    let ctrAction = "v1/GetOutstandingInvoicListForSales/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getCampaignCreditList(Id: any) {
    if (Id == null)
      Id = "0";
    let id = parseInt(Id);
    let ctrAction = "v1/GetCampaignCreditPeriodData/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getInvoiceList() {
    let ctrAction = "v1/GetInvoiceData"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  public GetInvoiceDetails(id: number) {
    let ctrAction = "v1/GetToPayDetails/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getOutstandingInvoiceList() {
    let ctrAction = "v1/GetOutstandingInvoiceData"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public GetUserCreditDetails(Id: any) {
    if (Id == null)
      Id = "0";
    let id = parseInt(Id);
    let ctrAction = "v1/GetCreditDetails/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public updateCampaignCreditPeriod(model: any) {
    let ctrAction = "v1/UpdateCampaignCreditPeriod";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public addCampaignCreditPeriod(model: any) {
    let ctrAction = "v1/InsertCampaignCreditPeriod";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public sendInvoiceEmail(model: any) {
    let ctrAction = "v1/SendInvoice";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  // public getListInvoices(Id: any) {
  //   if (Id == null)
  //     Id = "0";
  //   let id = parseInt(Id);
  //   let ctrAction = "v1/GetInvoiceDropdown/";
  //   return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  // }


  // public getOutstandingAmount(Id: any) {
  //   if (Id == null)
  //     Id = "0";
  //   let id = parseInt(Id);
  //   let ctrAction = "v1/GetOutstandingBalance/";
  //   return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  // }


  public addUserPayment(model: any) {
    let ctrAction = "v1/ReceivePayment";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  // public updateAdvertiserCreditLimit(model: any) {
  //   let ctrAction = "v1/ReceivePayment";
  //   return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  // }


  public addAdvertiserCreditLimit(model: any) {
    let ctrAction = "v1/AddCredit";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public getBillingPaymentDetails(id: number, userid: number) {
    let ctrAction = '';
    if (id > 0)
      ctrAction = "v1/GetPaymentData/";
    else {
      ctrAction = "v1/GetPaymentDataAdvertiser/";
      id = userid;
    }
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public submitPayment(model: any) {
    let ctrAction = "v1/PayWithUserCredit";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

}
