export interface InvoiceBase {
  billingId: number;
  userId: number;
  invoiceNumber: string;
  fullName: string;
  organisation: string;
  email: string;
  clientName: string;
  campaignName: string;
  campaignProfileId: number;
  currencyCode: string;
  status: number;
  salesExec: string;
  sUserId: number;
  invoicePath: string;
}


export interface InvoiceResult extends InvoiceBase {

  poNumber: string;
  invoiceDate: string;
  invoiceTotal: number;
  rStatus: string;
  settledDate: string;
  paymentMethod: string;
  usersCreditPaymentId: number;
  }

export interface OutstandingInvoiceResult extends InvoiceBase {

  paidAmount: number;
  creditAmount: number;
  outstandingAmount: number;
  description: string;

}
