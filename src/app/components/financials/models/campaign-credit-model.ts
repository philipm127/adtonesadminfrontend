export interface CampaignCreditPeriodModel {
  campaignCreditPeriodId: number;
  userId: number;
  userName: string;
  campaignId: number;
  campaignName: string;
  creditPeriod: number;
  createdDate: string;
  campaignProfileId:number;
}
