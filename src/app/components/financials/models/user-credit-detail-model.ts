import { BasicSelectModel } from 'src/app/models/basic-select';

export interface UserCreditDetailModel {
  id: number;
  userId: number;
  countryId: number;
  currencyId: number;
  assignCredit: number;
  availableCredit: number;
  paymentHistory: UserCreditPaymentHistory[];
}

export interface UserCreditPaymentHistory {
  id: number;
  amount: number;
  createdDate: string;
}
