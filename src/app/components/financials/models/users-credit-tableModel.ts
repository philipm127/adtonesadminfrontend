export interface UserCreditTableModel {
  Id: number;
  UserId: number;
  Email: string;
  FullName: string;
  Organisation: string;
  Credit: number;
  AvailableCredit: number;
  TotalUsed: number;
  TotalPaid: number;
  RemainingAmount: number;
  CreatedDate: string;
  countryName: string;
  currencyCode: string;
  salesExec: string;
}
