export interface UserPaymentDetailModel {
  userId: number;
  billingId: number;
  amount: number;
  description: string;
  status: number;
  campaignName: number;
  outstandingAmount: number;
  invoiceNumber: string;
  fullName: string;
  campaignProfileId: number;
}


export interface BuyCreditModel {
  advertiserId: number;
  fundAmount: number;
  totalAmount: number;
  assignedCredit: number;
  availableCredit: number;
  outstandingAmount: number;
  poNumber: string;
  countryId: number;
  campaignProfileId: number;
  totalFundAmount: number;
  taxPercantage: number;
  currencyId: number;
  currencyCode: string;
}

