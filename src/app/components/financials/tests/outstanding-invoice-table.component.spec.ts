import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OutstandingInvoiceTableComponent } from '../outstanding-invoice-table/outstanding-invoice-table.component';

describe('OutstandingInvoiceTableComponent', () => {
  let component: OutstandingInvoiceTableComponent;
  let fixture: ComponentFixture<OutstandingInvoiceTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OutstandingInvoiceTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutstandingInvoiceTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
