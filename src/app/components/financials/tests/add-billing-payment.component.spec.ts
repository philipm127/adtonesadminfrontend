import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddBillingPaymentComponent } from '../add-billing-payment/add-billing-payment.component';

describe('AddBillingPaymentComponent', () => {
  let component: AddBillingPaymentComponent;
  let fixture: ComponentFixture<AddBillingPaymentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBillingPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBillingPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
