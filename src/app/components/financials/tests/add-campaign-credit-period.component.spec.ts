import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddCampaignCreditPeriodComponent } from '../add-campaign-credit-period/add-campaign-credit-period.component';

describe('AddCampaignCreditPeriodComponent', () => {
  let component: AddCampaignCreditPeriodComponent;
  let fixture: ComponentFixture<AddCampaignCreditPeriodComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCampaignCreditPeriodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCampaignCreditPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
