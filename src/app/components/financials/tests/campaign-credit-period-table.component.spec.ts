import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CampaignCreditPeriodTableComponent } from '../campaign-credit-period-table/campaign-credit-period-table.component';

describe('CampaignCreditPeriodTableComponent', () => {
  let component: CampaignCreditPeriodTableComponent;
  let fixture: ComponentFixture<CampaignCreditPeriodTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignCreditPeriodTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignCreditPeriodTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
