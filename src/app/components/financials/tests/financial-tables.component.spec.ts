import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FinancialTablesComponent } from '../financial-tables/financial-tables.component';

describe('FinancialTablesComponent', () => {
  let component: FinancialTablesComponent;
  let fixture: ComponentFixture<FinancialTablesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FinancialTablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
