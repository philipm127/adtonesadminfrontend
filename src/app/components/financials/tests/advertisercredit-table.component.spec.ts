import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdvertiserCreditTableComponent } from '../usercredit/advertisercredit-table/advertisercredit-table.component';

describe('AdvertiserCreditTableComponent', () => {
  let component: AdvertiserCreditTableComponent;
  let fixture: ComponentFixture<AdvertiserCreditTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertiserCreditTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserCreditTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
