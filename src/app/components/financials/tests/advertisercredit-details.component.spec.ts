import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdvertiserCreditDetailsComponent } from '../usercredit/advertisercredit-details/advertisercredit-details.component';

describe('AdvertiserCreditDetailsComponent', () => {
  let component: AdvertiserCreditDetailsComponent;
  let fixture: ComponentFixture<AdvertiserCreditDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertiserCreditDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserCreditDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
