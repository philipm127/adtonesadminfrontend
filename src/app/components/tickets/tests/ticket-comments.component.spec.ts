import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TicketCommentsComponent } from '../ticket-detail/ticket-comments/ticket-comments.component';

describe('TicketCommentsComponent', () => {
  let component: TicketCommentsComponent;
  let fixture: ComponentFixture<TicketCommentsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
