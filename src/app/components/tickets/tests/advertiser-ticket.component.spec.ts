import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertiserTicketComponent } from '../user-type/advertiser-ticket/advertiser-ticket.component';

describe('AdvertiserTicketComponent', () => {
  let component: AdvertiserTicketComponent;
  let fixture: ComponentFixture<AdvertiserTicketComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvertiserTicketComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
