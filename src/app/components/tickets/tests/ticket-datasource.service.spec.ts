import { TestBed } from '@angular/core/testing';

import { TicketDatasourceService } from '../ticket-datasource.service';

describe('TicketDatasourceService', () => {
  let service: TicketDatasourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TicketDatasourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
