import { Component, OnInit, Input } from '@angular/core';
import { TicketCommentModel } from '../../models/ticket-comment-model';
import { MatDialog } from '@angular/material/dialog';
import { ImageModalComponent } from './image-modal/image-modal.component';

@Component({
  selector: 'app-ticket-comments',
  templateUrl: './ticket-comments.component.html',
  styleUrls: ['./ticket-comments.component.css']
})
export class TicketCommentsComponent implements OnInit {
  @Input() comment: TicketCommentModel;


  constructor(public dialog: MatDialog) {

  }

  ngOnInit(): void {
  }

  GetCommentImages(imagePath: string) {
    this.dialog.open(ImageModalComponent, {
      data: {
        src: imagePath
      }
    });
  }

}
