import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ReturnResult } from 'src/app/models/return-result';
import { PermissionData } from 'src/app/models/permission-data';
import { Subject } from 'rxjs';
import { TicketTableModel } from '../models/ticket-table-model';
import { TicketService } from 'src/app/components/tickets/ticket.service';
import { HttpResponse } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { TicketCommentModel } from '../models/ticket-comment-model';
import { MatDialog } from '@angular/material/dialog';
import { AddCommentModalComponent } from './add-comment-modal/add-comment-modal.component';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { IdCollectionModel } from 'src/app/models/IdCollectionViewModel';
import { MessageService } from '../../shared/services/message.service';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.css']
})
export class TicketDetailComponent implements OnInit {
  pagename: string = "TicketDetails";
  userData: any;
  isError: boolean = false;
  errorMessage: string;

  ticketId: string;
  ticketImage: string;

  userModel: any;
  comments: TicketCommentModel[];
  retModel: ReturnResult;
  retResultUpdate: ReturnResult;

  permData: PermissionData[];
  destroy$: Subject<TicketTableModel> = new Subject();

  constructor(private formBuilder: FormBuilder, private ticketService: TicketService, public dialog: MatDialog,
    private accessService: AccessServicesService, private route: ActivatedRoute, private router: Router, private messService: MessageService) {
    this.userData = this.accessService.getUser();
    this.ticketId = this.route.snapshot.paramMap.get('id');
    this.accessService.refreshToken();
  }


  ngOnInit() {
    this.ticketService.getTicketDetails(this.ticketId).pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retModel = res.body;
        if (this.retModel.result == 1) {
          this.userModel = <TicketTableModel>this.retModel.body;
          // console.log("returned userModel: ", this.userModel);
        }
        else {
          this.isError = true;
          this.errorMessage = this.retModel.error + " for the selected ticket.";
        }
      });
  }


  updateTicket(status: number) {
    let statusType: string;
    let model = new IdCollectionModel();
    model.id = parseInt(this.ticketId);
    model.status = status;
    this.ticketService.updateTicketStatus(model)
      .subscribe(resp => {
        let retModel = resp
        if (retModel.result == 1) {
          if (model.status == 3)
            statusType = "closed";
          else if (model.status == 4)
            statusType = "archived";

          let message = "The Ticket " + this.userModel.qNumber + " was succesfully " + statusType;
          this.messService.sendMessage({ text: message.toString(), category: 'success' });
          this.router.navigate(['/tickets']);
        }
        else {
          this.isError = true;
          this.errorMessage = this.retModel.error + " for the selected ticket.";
        }
      });
  }


  addCommentModal() {
    let dialogRef = this.dialog.open(AddCommentModalComponent, {
      width: '500px',
      data: {
        ticketId: parseInt(this.ticketId),
        description: "",
        userId: this.userModel.userId
      }
    });
  }

}
