import { Component, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { forkJoin } from 'rxjs';
import { TicketService } from 'src/app/components/tickets/ticket.service';
import { TicketCommentModel } from '../../models/ticket-comment-model';
import { ReturnResult } from 'src/app/models/return-result';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-comment-modal',
  templateUrl: './add-comment-modal.component.html',
  styleUrls: ['./add-comment-modal.component.css']
})
export class AddCommentModalComponent {
  @ViewChild('file', { static: false }) file;
  ticketId: any;
  public files: Set<File> = new Set();

  resultMessage: string;
  retModel: ReturnResult;

  constructor(public dialogRef: MatDialogRef<AddCommentModalComponent>, private router:Router,
    @Inject(MAT_DIALOG_DATA) public data: any, private ticketService: TicketService) { }

  cancelClose: string = "Cancel"
  errorVisible: boolean = false;
  succesVisible: boolean = false;

  onFilesAdded() {
    console.log("Entered onFilesAdded()");
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (let key in files) {
      if (!isNaN(parseInt(key))) {
        this.files.add(files[key]);
      }
    }
  }

  addFiles() {
    this.file.nativeElement.click();
  }


  closeDialog(data: any) {
    console.log("Entered closeDialog() with data value of ", data.ticketId);
    // if everything was uploaded already, just close the dialog

    this.ticketId = data.ticketId;
    let newModel = new TicketCommentModel;
    newModel.questionId = data.ticketId;
    newModel.description = data.description;
    newModel.userId = data.userId;
    // console.log("What is in model submitted to service: ", newModel);
    // console.log("Ready to upload what is in files ", this.files);
    if (this.files.size > 0) {
      this.files.forEach(file => {
        newModel.commentImage = file;
      });
    }
    this.uploadComment(newModel);
  }


  uploadComment(newModel: TicketCommentModel) {
    // console.log("Entered uploadCommentWithImage");
    this.ticketService.addCommentWithImage(newModel)
      .subscribe(resp => {
        this.retModel = resp
        if (this.retModel.result == 1) {
          console.log("returned result: ", this.retModel.result);
          this.dialogRef.disableClose = true;

          this.succesVisible = true;
          this.resultMessage = "The Comment Was Added Successfully";
          this.cancelClose = "Close";
          this.reload();
          return this.dialogRef.close();
        }
        else {
          this.errorVisible = true;
          this.resultMessage = this.retModel.error;
          this.cancelClose = "Close";
        }
      });
  }

  reload() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate(["/ticketdetails/" + this.ticketId]);
  }
}
