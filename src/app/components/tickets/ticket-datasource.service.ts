import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { HttpResponse } from "@angular/common/http";
import { Observable, BehaviorSubject, of } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { PageSortSearchModel } from "src/app/models/PageSortSearchModel";
import { ReturnResult } from "src/app/models/return-result";
import { TicketTableModel } from "./models/ticket-table-model";
import { TicketService } from "./ticket.service";


export class TicketDatasourceService {

  private ticketSubject = new BehaviorSubject<TicketTableModel[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  public resultCount = null;

  constructor(private ticketService: TicketService) { }

  loadTickets(paging: PageSortSearchModel) {

    this.loadingSubject.next(true);

    this.ticketService.getTicketListAsync(paging)
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        this.resultCount = retResult.recordcount;
        this.loadingSubject.next(false);
        let data = <TicketTableModel[]>retResult.body;
        this.ticketSubject.next(data)
      });
    // .subscribe(tickets, this.ticketSubject.next(tickets));

  }

  connect(collectionViewer: CollectionViewer): Observable<TicketTableModel[]> {
    console.log("Connecting data source");
    return this.ticketSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.ticketSubject.complete();
    this.loadingSubject.complete();
  }

}
