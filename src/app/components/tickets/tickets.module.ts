import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TicketsRoutingModule } from './tickets-routing.module';
import { TicketTableComponent } from './ticket-table/ticket-table.component';
import { TicketTableCopyComponent } from './ticket-table copy/ticket-table-copy.component';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatTableExporterModule } from 'mat-table-exporter';
import { SharedModule } from '../shared/shared.module';
import { TicketDetailComponent } from './ticket-detail/ticket-detail.component';
import { TicketCommentsComponent } from './ticket-detail/ticket-comments/ticket-comments.component';
import { ImageModalComponent } from './ticket-detail/ticket-comments/image-modal/image-modal.component';
import { AddCommentModalComponent } from './ticket-detail/add-comment-modal/add-comment-modal.component';
import { AdvertiserTicketComponent } from './user-type/advertiser-ticket/advertiser-ticket.component';


@NgModule({
  declarations: [TicketTableComponent,TicketTableCopyComponent, TicketDetailComponent, TicketCommentsComponent, ImageModalComponent, AddCommentModalComponent, AdvertiserTicketComponent],
  imports: [
    CommonModule,
    TicketsRoutingModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule,
    SharedModule
  ]
})
export class TicketsModule { }
