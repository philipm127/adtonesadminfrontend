import { Component, ViewChild, OnDestroy, AfterViewInit, OnInit } from '@angular/core';
import { ReturnResult } from 'src/app/models/return-result';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { takeUntil, tap } from 'rxjs/operators';
import { fromEvent, merge, Subject, Subscription } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DatePipe } from '@angular/common';
import { TableSearchModel } from '../../shared/table-search-model';
import { TicketTableModel } from '../models/ticket-table-model';
import { TicketService } from 'src/app/components/tickets/ticket.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { TicketDatasourceService } from '../ticket-datasource.service';
import { PageSortSearchModel } from 'src/app/models/PageSortSearchModel';
import { IdCollectionModel } from 'src/app/models/IdCollectionViewModel';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { IMessage, MessageService } from '../../shared/services/message.service';



@Component({
  selector: 'app-ticket-table',
  templateUrl: './ticket-table.component.html',
  styleUrls: ['./ticket-table.component.css']
})

export class TicketTableComponent implements OnInit, AfterViewInit, OnDestroy {
  pagename: string = "TicketList";
  permData: any;
  userData: any;
  emailLink: boolean = true;
  adUserId: string = "0";

  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];


  destroy$: Subject<TicketTableModel> = new Subject();
  //dataSource: MatTableDataSource<TicketTableModel>;
  dataSource: TicketDatasourceService;
  modelSource: TicketTableModel[];
  isLoading: boolean = true;
  filterValues: string;

  miscName: string = "Ticket ID";
  typeName: string = "Subject";

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  retResult: ReturnResult = null;

  statusSelect: BasicSelectModel[] = [];
  clientSelect: BasicSelectModel[] = [];
  paymentSelect: BasicSelectModel[] = [];
  typeSelect: BasicSelectModel[] = [];

  tableSearchModel: TableSearchModel;

  messages: IMessage[] = [];
  subscription: Subscription;
  successMessage: string;

  constructor(private ticketService: TicketService, private datePipe: DatePipe, private messService: MessageService,
    private popService: PopSearchDdService, private accessService: AccessServicesService,
    private route: ActivatedRoute, private router: Router, private selectList: SelectServicesService) {
    this.getStatusList();
    this.getSubjectList();
    this.adUserId = this.route.snapshot.paramMap.get('id');

    this.userData = this.accessService.getUser();
    this.PermissionAssignment();
    // console.log("What is adUserId in Tickets: ",this.adUserId);

  }

  filtered(filter: string) {
    this.filterValues = filter;

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.paginator.pageIndex = 0;
    this.loadData();
    this.getStatusList();
    this.getSubjectList();
  }

  ngOnInit() {
    this.subscription = this.messService.getMessage().subscribe(message => {
      if (message) {
        this.messages.push(message.data);
        if (this.messages[0] != undefined) {
          // console.log("this.messages.push(message.data); ", this.messages[0].text);
          this.successMessage = this.messages[0].text;
        }
      } else {
        // clear messages when empty message received
        this.messages = [];
      }
    });

    let addOverideId: number = 0;
    if (this.adUserId != null)
      addOverideId = parseInt(this.adUserId);
    // console.log("What is the ad Id: ", this.adUserId);
    let paging = new PageSortSearchModel();
    paging.elementId = addOverideId;
    paging.page = this.paginator.pageIndex;
    paging.pageSize = this.paginator.pageSize;
    paging.direction = this.sort.direction;
    paging.sort = this.sort.active;

    this.dataSource = new TicketDatasourceService(this.ticketService);

    this.dataSource.loadTickets(paging);
  }

  ngAfterViewInit() {
    // this.getClientList();


    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadData())
      )
      .subscribe();

  }

  archiveQuestion(qId: number) {
    let model = new IdCollectionModel();
    model.id = qId;
    model.status = 4;
    this.ticketService.updateTicketStatus(model)
      .subscribe(resp => {
        let retModel = resp
        // console.log("archiveQuestion(qId: number) let retModel = resp: ", retModel);
        if (retModel.result == 1) {
          this.successMessage = "Your ticket was succesfully archived";
        }
        else {
          this.successMessage = "Therewas an issue archiving your ticket";
        }
      });
  }


  loadData() {
    this.successMessage = "";
    let addOverideId: number = 0;
    if (this.adUserId != null)
      addOverideId = parseInt(this.adUserId);
    let paging = new PageSortSearchModel();
    paging.elementId = addOverideId;
    paging.page = this.paginator.pageIndex;
    paging.pageSize = this.paginator.pageSize;
    paging.direction = this.sort.direction;
    paging.sort = this.sort.active;
    paging.search = JSON.stringify(this.filterValues);

    this.dataSource.loadTickets(paging);

  }

  getSubjectList() {
    this.selectList.getTicketSubject()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        // console.log("returned result from get subject list: ", retResult);
        if (retResult.result == 1) {
          this.typeSelect = <BasicSelectModel[]>retResult.body;
        }
        else
          this.typeSelect = <BasicSelectModel[]>retResult.body;
      });
  }


  getStatusList() {
    this.selectList.getTicketStatus()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        // console.log("returned result from get payment list: ", retResult);
        if (retResult.result == 1) {
          this.statusSelect = <BasicSelectModel[]>retResult.body;
        }
        else
          this.statusSelect = <BasicSelectModel[]>retResult.body;
      });
  }



  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
    let mailFind = permPageData["elements"].filter(j => j.name == "email" && j.type == "element");
    this.emailLink = mailFind[0].enabled;
    // console.log("what is this.emailLink: ", this.emailLink);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
