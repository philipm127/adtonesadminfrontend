import { DatePipe } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';
import { TicketTableModel } from '../../models/ticket-table-model';
import { TicketService } from '../../ticket.service';

@Component({
  selector: 'app-advertiser-ticket',
  templateUrl: './advertiser-ticket.component.html',
  styleUrls: ['./advertiser-ticket.component.css']
})
export class AdvertiserTicketComponent implements AfterViewInit, OnDestroy {

  pagename: string = "TicketList";
  permData: any;
  userData: any;
  salesId: number = 0;
  salesExecName: string;
  emailLink: boolean = true;

  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];
  permPageData:any;
  destroy$: Subject<TicketTableModel> = new Subject();
  dataSource: MatTableDataSource<TicketTableModel>;
  modelSource: TicketTableModel[];
  isLoading: boolean = true;

  miscName: string = "Ticket ID";
  typeName: string = "Subject";

  retResult: ReturnResult = null;

  tableSearchModel: TableSearchModel;

  constructor(private ticketService: TicketService,
    private accessService: AccessServicesService,
    private route: ActivatedRoute, private router: Router) {
    this.userData = this.accessService.getUser();
    this.salesId = this.userData.userId;
  }


  ngAfterViewInit() {
    this.ticketService.getTicketListForAdvertiser(this.salesId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.isLoading = false;
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <TicketTableModel[]>this.retResult.body;
          this.permPageData = this.accessService.getPermissionList(this.pagename);
        }
      });
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
