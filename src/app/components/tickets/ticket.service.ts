import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReturnResult } from '../../models/return-result';

import { environment } from 'src/environments/environment';
import { TokenStorageService } from '../../services/auth/token-storage.service';
import { Observable } from 'rxjs';
import { TicketTableModel } from './models/ticket-table-model';
import { map } from "rxjs/operators";
import { IdCollectionModel } from 'src/app/models/IdCollectionViewModel';


@Injectable({
  providedIn: 'root'
})
export class TicketService {

  myAppUrl: string;
  controller: string = "ticket/";

  constructor(private http: HttpClient, private tokenService: TokenStorageService) {
    this.myAppUrl = (environment.appUrl + this.controller);
  }

  public getTicketList(adUserId: string) {
    console.log("Entered ticket list service: ");
    if (adUserId == null)
      adUserId = "0";
    let ctrAction = "v1/GetTicketList/"
    let id = parseInt(adUserId);
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public getTicketListAsync(model: any) {
    // console.log("Search Params are: ",model);
    let ctrAction = "v1/GetTicketListAsync"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model, { observe: "response" });
  }


  public getTicketListForSalesExec(id: number) {
    let ctrAction = "v1/GetTicketListForSales/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public getTicketListForAdvertiser(id: number) {
    let ctrAction = "v1/GetTicketList/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public getTicketDetails(ticketId: string) {
    let ctrAction = "v1/GetTicketDetails/"
    let id = parseInt(ticketId);
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public addComment(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/AddTicketComment"
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public addCommentWithImage(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/AddTicketComment";
    const formData: FormData = new FormData();
    if (model.commentImage != null || model.commentImage != undefined)
      formData.append('commentImage', model.commentImage, model.commentImage.name);
    formData.append("UserId", model.userId);
    formData.append("QuestionId", model.questionId);
    formData.append("Description", model.description);
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, formData);

  }


  public updateTicketStatus(model: IdCollectionModel): Observable<ReturnResult> {
    let ctrAction = "v1/UpdateTicketStatus";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

}
