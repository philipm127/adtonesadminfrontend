import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TicketTableComponent } from './ticket-table/ticket-table.component';
import { TicketDetailComponent } from './ticket-detail/ticket-detail.component';
import { AuthguardService } from 'src/app/services/auth/authguard.service';


const ticketRoutes: Routes = [
  { path: 'tickets', component: TicketTableComponent, canActivate: [AuthguardService] },
  { path: 'tickets/:id', component: TicketTableComponent },
  { path: 'ticketdetails/:id', component: TicketDetailComponent }
];

@NgModule({
  imports: [RouterModule.forChild(ticketRoutes)],
  exports: [RouterModule]
})
export class TicketsRoutingModule { }
