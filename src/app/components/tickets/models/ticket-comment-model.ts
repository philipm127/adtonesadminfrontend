export class TicketCommentModel {
  commentId: number;
  userId: number;
  questionId: number;
  commentTitle: string;
  description: string;
  commentDate: string;
  ticketCode: string;
  userName: string;
  imageFile: string;
  commentImage:File;
}
