import { TicketCommentModel } from './ticket-comment-model';

export interface TicketTableModel {
  id: number;
  userId: number;
  userName: string;
  email: string;
  organisation: string;
  qNumber: string;
  clientId: number;
  clientName: string;
  campaignProfileId: number;
  campaignName: string;
  createdDate: string;
  questionTitle: string;
  questionSubjectId: number;
  description: string;
  questionSubject: string;
  status: number;
  rStatus: string;
  lastResponseDatetime: string;
  lastResponseDateTimeByUser: string;
  paymentMethodId: number;
  paymentMethod: string;
  updatedBy: number;
  comments: TicketCommentModel[];
  salesExec: string;
}
