import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { OperatorResult } from 'src/app/components/operator/models/operator-result-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { OperatorService } from 'src/app/components/operator/operator.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';

@Component({
  selector: 'app-operator-table',
  templateUrl: './operator-table.component.html',
  styleUrls: ['./operator-table.component.css']
})

export class OperatorTableComponent implements OnDestroy {
  pagename: string = "OperatorsList";
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];
  userData: any;

  destroy$: Subject<OperatorResult> = new Subject();
  dataSource: MatTableDataSource<OperatorResult>;
  modelSource: OperatorResult[];
  isLoading: boolean = true;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  retResult: ReturnResult = null;

  countrySelect: BasicSelectModel[] = [];

  tableSearchModel: TableSearchModel;
  // This is to appear in Search Box of numberFrom/numberTo
  numberName: string = "Credit Limit";

  constructor(private operatorService: OperatorService, private datePipe: DatePipe, private popSearch: PopSearchDdService,
    private accessService: AccessServicesService) {

    this.userData = this.accessService.getUser();
    this.PermissionAssignment();
    this.operatorService.getOperatorList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.isLoading = false;
          this.modelSource = <OperatorResult[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<OperatorResult>(this.modelSource);
        }
        this.countrySelect = this.popSearch.popCountry(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          const a = !filter.name || data.operatorName.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          const b = !filter.country || data.countryName.trim().toLowerCase().includes(filter.country.trim().toLowerCase());
          return a && b;
        }) as (UserCreditElement, string) => boolean;

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}

