import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OperatorRoutingModule } from './operator-routing.module';
import { OperatorTableComponent } from './operator-table/operator-table.component';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatTableExporterModule } from 'mat-table-exporter';
import { SharedModule } from '../shared/shared.module';
import { MaxAdvertTableComponent } from './max-advert-table/max-advert-table.component';
import { OperatorConfigTableComponent } from './operator-config-table/operator-config-table.component';
import { AddOperatorComponent } from './add-operator/add-operator.component';
import { AddMaxAdvertRuleComponent } from './add-max-advert-rule/add-max-advert-rule.component';
import { AddOperatorConfigurationComponent } from './add-operator-configuration/add-operator-configuration.component';


@NgModule({
  declarations: [OperatorTableComponent, MaxAdvertTableComponent, OperatorConfigTableComponent, AddOperatorComponent, AddMaxAdvertRuleComponent, AddOperatorConfigurationComponent],
  imports: [
    CommonModule,
    OperatorRoutingModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule,
    SharedModule
  ]
})
export class OperatorModule { }
