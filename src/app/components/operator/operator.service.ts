import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ReturnResult } from '../../models/return-result';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OperatorService {

  myAppUrl: string;
  controller: string = "operator/";

  constructor(private http: HttpClient) {
    this.myAppUrl = (environment.appUrl + this.controller);
  }

  public getOperatorList() {
    let ctrAction = "v1/GetOperators"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getOperatorMaxAdList() {
    let ctrAction = "v1/LoadOperatorMaxAdvertDataTable"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getOperatorConfigList() {
    let ctrAction = "v1/LoadOperatorConfigurationDataTable"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getOperatorDetails(Id:string) {
    let id = parseInt(Id);
    let ctrAction = "v1/GetOperator/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public updateOperator(model:any) {
    let ctrAction = "v1/UpdateOperator";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public addOperator(model:any) {
    let ctrAction = "v1/AddOperator";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public getOperatorMaxAdsDetails(Id:string) {
    let id = parseInt(Id);
    let ctrAction = "v1/GetOperatorMaxAdvert/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public updateOperatorMaxAdvert(model:any) {
    let ctrAction = "v1/UpdateOperatorMaxAdverts";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public addOperatorMaxAdvert(model:any) {
    let ctrAction = "v1/AddOperatorMaxAdverts";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public getOperatorConfigDetails(Id:string) {
    let id = parseInt(Id);
    let ctrAction = "v1/GetOperatorConfig/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }


  public updateOperatorConfig(model:any) {
    let ctrAction = "v1/UpdateOperatorConfig";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public addOperatorConfig(model:any) {
    let ctrAction = "v1/AddOperatorConfig";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

}
