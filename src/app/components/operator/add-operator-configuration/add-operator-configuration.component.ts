import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { OperatorConfigurationResult, OperatorMaxAdverts } from '../models/operator-result-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { OperatorService } from '../operator.service';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-add-operator-configuration',
  templateUrl: './add-operator-configuration.component.html',
  styleUrls: ['./add-operator-configuration.component.css']
})
export class AddOperatorConfigurationComponent implements OnInit {
  pagename: string = "AddOperatorConfig";
  opId: string = "0";

  form = new FormGroup({
    operatorConfigurationId: new FormControl(0),
    operatorId: new FormControl("", Validators.required),
    days: new FormControl("", Validators.required),
    isActive: new FormControl("true")
  });


  editMode: boolean = false;
  userModel: OperatorConfigurationResult;

  operatorSelect: BasicSelectModel[];

  destroy$: Subject<OperatorConfigurationResult> = new Subject();

  submitEnabled: boolean = true;


  userData: any;

  successMessage: string;
  failMessage: string;

  inputvalue = "";


  constructor(private formBuilder: FormBuilder, private opService: OperatorService,
    private accessService: AccessServicesService, private listService: SelectServicesService,
    private route: ActivatedRoute, private router: Router) {

    this.opId = this.route.snapshot.paramMap.get('id');
    if (parseInt(this.opId) > 0) {
      this.pagename = "EditOperatorConfig";
      this.editMode = true;
    }

    this.userData = this.accessService.getUser();
    this.getOperator();

    let permListData = this.accessService.getPermissionList(this.pagename);
    permListData["elements"].forEach(element => {
      if (element.type == "element" && element.name == this.accessService.getName(this.form.get(element.name))) {
        this[element.name.concat('Visible')] = element.visible;
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
      else if (element.type == "button") {
        this.submitEnabled = element.enabled;
      }
    });
  }

  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();

    if (this.editMode) {
      this.opService.getOperatorConfigDetails(this.opId)
        .subscribe((res: HttpResponse<ReturnResult>) => {
          let retModel = res.body;
          if (retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <OperatorConfigurationResult>retModel.body;
            // console.log("returned userModel: ", this.userModel);
            this.initForm();
          }
        });
    }
  }

  getOperator(){
    this.listService.getOperatorList(0)
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        console.log("returned result from get country list: ", retResult);
        if (retResult.result == 1) {

          this.operatorSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }


  private initForm() {
    if (this.editMode) {
      this.form.patchValue({
        operatorConfigurationId: this.userModel.operatorConfigurationId,
        days: this.userModel.days
      });
      this.form.get('operatorId').setValue(this.userModel.operatorId.toString());
      this.form.get('isActive').setValue(this.userModel.isActive.toString());
      this.form.get('operatorId').disable;
    }
  }

  onSubmit() {
    // this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log("What is this form data: ", this.form.value);
    let serviceResult = null;
    if (this.editMode)
      serviceResult = this.opService.updateOperatorConfig(this.form.value);
    else
      serviceResult = this.opService.addOperatorConfig(this.form.value);
    serviceResult
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          this.successMessage = "The Details Were Updated Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the details";
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
