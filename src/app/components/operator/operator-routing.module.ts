import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OperatorTableComponent } from './operator-table/operator-table.component';
import { MaxAdvertTableComponent } from './max-advert-table/max-advert-table.component';
import { OperatorConfigTableComponent } from './operator-config-table/operator-config-table.component';
import { AddOperatorComponent } from './add-operator/add-operator.component';
import { AddMaxAdvertRuleComponent } from './add-max-advert-rule/add-max-advert-rule.component';
import { AddOperatorConfigurationComponent } from './add-operator-configuration/add-operator-configuration.component';



const operatorRoutes: Routes = [
  { path: 'operator', component: OperatorTableComponent },
  { path: 'addoperator/:id', component: AddOperatorComponent },
  { path: 'addoperatormaxadvert/:id', component: AddMaxAdvertRuleComponent },
  { path: 'operatormax', component: MaxAdvertTableComponent },
  { path: 'operatorconfig', component: OperatorConfigTableComponent },
  { path: 'addoperatorconfig/:id', component: AddOperatorConfigurationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(operatorRoutes)],
  exports: [RouterModule]
})
export class OperatorRoutingModule { }
