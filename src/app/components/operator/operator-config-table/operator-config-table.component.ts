import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { OperatorService } from 'src/app/components/operator/operator.service';
import { OperatorConfigurationResult } from '../models/operator-result-model';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import _ from 'lodash';

@Component({
  selector: 'app-operator-config-table',
  templateUrl: './operator-config-table.component.html',
  styleUrls: ['./operator-config-table.component.css']
})

export class OperatorConfigTableComponent implements OnDestroy {
  pagename: string = "OperatorConfigTable";
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];
  userData: any;
  destroy$: Subject<OperatorConfigurationResult> = new Subject();
  dataSource: MatTableDataSource<OperatorConfigurationResult>;
  modelSource: OperatorConfigurationResult[];
  isLoading: boolean = true;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  retResult: ReturnResult = null;

  countrySelect: BasicSelectModel[] = [];
  operatorSelect: BasicSelectModel[] = [];

  tableSearchModel: TableSearchModel;

  constructor(private operatorService: OperatorService, private datePipe: DatePipe, private popSearch: PopSearchDdService,
    private accessService: AccessServicesService) {

    this.userData = this.accessService.getUser();
    this.PermissionAssignment();
    this.operatorService.getOperatorConfigList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let resresult = res.body;
        // console.log("Returned body is: ", resresult);
        if (resresult.result == 1) {
          this.isLoading = false;
          this.modelSource = <OperatorConfigurationResult[]>resresult.body;
          this.dataSource = new MatTableDataSource<OperatorConfigurationResult>(this.modelSource);
        }
        this.countrySelect = this.popSearch.popCountry(this.modelSource);
        this.operatorSelect = this.popSearch.popOperator(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("What is in filters: ",filter);
          // Object.keys(filter).forEach(key => console.log("What is key value: ",this.datePipe.transform(filter[key], 'yyyy-MM-dd')));
          const a = !filter.name || data.operatorName.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          const b = !filter.country || data.countryName.trim().toLowerCase().includes(filter.country.trim().toLowerCase());
          const c = !filter.operator || data.operatorName.trim().toLowerCase().includes(filter.operator.trim().toLowerCase());
          return a && b && c;
        }) as (UserCreditElement, string) => boolean;


        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}

