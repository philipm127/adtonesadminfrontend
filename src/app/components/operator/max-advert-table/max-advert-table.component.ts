import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { OperatorService } from 'src/app/components/operator/operator.service';
import { OperatorMaxAdverts } from '../models/operator-result-model';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import _ from 'lodash';

@Component({
  selector: 'app-max-advert-table',
  templateUrl: './max-advert-table.component.html',
  styleUrls: ['./max-advert-table.component.css']
})

export class MaxAdvertTableComponent implements OnDestroy {
  pagename: string = "MaxAdvertTable";
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];

  userData: any;

  destroy$: Subject<OperatorMaxAdverts> = new Subject();
  dataSource: MatTableDataSource<OperatorMaxAdverts>;
  modelSource: OperatorMaxAdverts[];
  isLoading: boolean = true;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  retResult: ReturnResult = null;

  operatorSelect: BasicSelectModel[] = [];

  tableSearchModel: TableSearchModel;

  constructor(private operatorService: OperatorService, private datePipe: DatePipe, private popSearch: PopSearchDdService,
    private accessService: AccessServicesService) {
    this.userData = this.accessService.getUser();
    this.PermissionAssignment();
    this.operatorService.getOperatorMaxAdList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.isLoading = false;
          this.modelSource = <OperatorMaxAdverts[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<OperatorMaxAdverts>(this.modelSource);
        }
        this.operatorSelect = this.popSearch.popOperator(this.modelSource);
        this.dataSource.filterPredicate = ((data, filter) => {
          const a = !filter.name || data.keyName.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          const b = !filter.operator || data.operatorName.trim().toLowerCase().includes(filter.operator.trim().toLowerCase());
          return a && b;
        }) as (UserCreditElement, string) => boolean;

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}

