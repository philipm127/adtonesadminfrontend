import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { OperatorMaxAdverts } from '../models/operator-result-model';
import { ReturnResult } from 'src/app/models/return-result';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { OperatorService } from '../operator.service';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-add-max-advert-rule',
  templateUrl: './add-max-advert-rule.component.html',
  styleUrls: ['./add-max-advert-rule.component.css']
})
export class AddMaxAdvertRuleComponent implements OnInit {
  pagename: string = "AddOperatorMaxAdvert";
  opId: string = "0";

  form = new FormGroup({
    operatorMaxAdvertId: new FormControl(0),
    operatorId: new FormControl("", Validators.required),
    keyName: new FormControl("", Validators.required),
    keyValue: new FormControl("", Validators.required)
  });


  editMode: boolean = false;
  userModel: OperatorMaxAdverts;
  retModel: ReturnResult;
  operatorSelect: BasicSelectModel[];

  destroy$: Subject<BasicSelectModel> = new Subject();

  submitEnabled: boolean = true;


  userData: any;

  successMessage: string;
  failMessage: string;

  inputvalue = "";


  constructor(private formBuilder: FormBuilder, private opService: OperatorService,
    private accessService: AccessServicesService, private listService: SelectServicesService,
    private route: ActivatedRoute, private router: Router) {

    this.opId = this.route.snapshot.paramMap.get('id');
    if (parseInt(this.opId) > 0) {
      this.pagename = "EditOperatorMaxAdvert";
      this.editMode = true;
    }

    this.userData = this.accessService.getUser();
    this.getOperator();

    let permListData = this.accessService.getPermissionList(this.pagename);
    permListData["elements"].forEach(element => {
      if (element.type == "element" && element.name == this.accessService.getName(this.form.get(element.name))) {
        this[element.name.concat('Visible')] = element.visible;
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
      else if (element.type == "button") {
        this.submitEnabled = element.enabled;
      }
    });
  }

  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();

    if (this.editMode) {
      this.opService.getOperatorMaxAdsDetails(this.opId)
        .subscribe((res: HttpResponse<ReturnResult>) => {
          this.retModel = res.body;
          if (this.retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <OperatorMaxAdverts>this.retModel.body;
            // console.log("returned userModel: ", this.userModel);
            this.initForm();
          }
        });
    }
  }

  getOperator(){
    this.listService.getOperatorList(0)
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        console.log("returned result from get country list: ", retResult);
        if (retResult.result == 1) {

          this.operatorSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }

  private initForm() {
    if (this.editMode) {
      this.form.patchValue({
        operatorMaxAdvertId: this.userModel.operatorMaxAdvertId,
        keyName: this.userModel.keyName,
        keyValue: this.userModel.keyValue
      });
      this.form.get('operatorId').setValue(this.userModel.operatorId.toString());
      this.form.get('operatorId').disable();
      this.form.get('keyName').disable();
    }
  }

  onSubmit() {
    // this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log("What is this form data: ", this.form.value);
    let serviceResult = null;
    if (this.editMode)
      serviceResult = this.opService.updateOperatorMaxAdvert(this.form.getRawValue());
    else
      serviceResult = this.opService.addOperatorMaxAdvert(this.form.getRawValue());
    serviceResult
      .subscribe(resp => {
        this.retModel = resp

        if (this.retModel.result == 1) {
          this.successMessage = "The Details Were Updated Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the details";
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
