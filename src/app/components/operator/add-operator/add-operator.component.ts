import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { OperatorResult } from '../models/operator-result-model';
import { ReturnResult } from 'src/app/models/return-result';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject, Observable } from 'rxjs';
import { OperatorService } from '../operator.service';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-add-operator',
  templateUrl: './add-operator.component.html',
  styleUrls: ['./add-operator.component.css']
})
export class AddOperatorComponent implements OnInit {

  pagename: string = "AddOperator";
  opId: string = "0";
  form:FormGroup;

  // form = new FormGroup({
  //   operatorId: new FormControl(""),
  //   operatorName: new FormControl("", Validators.required),
  //   countryId: new FormControl("", Validators.required),
  //   currencyId: new FormControl("", Validators.required),
  //   emailCost: new FormControl("", Validators.required),
  //   smsCost: new FormControl("", Validators.required),
  //   isActive: new FormControl("true", Validators.required)
  // });


  editMode: boolean = false;
  userModel: OperatorResult;

  countrySelect: BasicSelectModel[];
  currencySelect: BasicSelectModel[];
  destroy$: Subject<BasicSelectModel> = new Subject();

  submitEnabled: boolean = true;
  areaNameVisible: boolean = true;
  countryIdVisible: boolean = true;

  userData: any;

  successMessage: string;
  failMessage: string;

  inputvalue = "";


  constructor(private formBuilder: FormBuilder, private opService: OperatorService,
    private accessService: AccessServicesService, private listService: SelectServicesService,
    private route: ActivatedRoute, private router: Router) {

    this.opId = this.route.snapshot.paramMap.get('id');
    if (parseInt(this.opId) > 0) {
      this.pagename = "EditOperator";
      this.editMode = true;
    }

    this.userData = this.accessService.getUser();
    this.getCountry();
    this.getCurrency();

    this.form = new FormGroup({
      operatorId: new FormControl(0),
      operatorName: new FormControl("", Validators.required),
      countryId: new FormControl("", Validators.required),
      currencyId: new FormControl("", Validators.required),
      emailCost: new FormControl("", Validators.required),
      smsCost: new FormControl("", Validators.required),
      isActive: new FormControl("true", Validators.required)
    });

    let permListData = this.accessService.getPermissionList(this.pagename);
    permListData["elements"].forEach(element => {
      if (element.type == "element" && element.name == this.accessService.getName(this.form.get(element.name))) {
        this[element.name.concat('Visible')] = element.visible;
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
      else if (element.type == "button") {
        this.submitEnabled = element.enabled;
      }
    });
  }

  getCountry() {
    this.listService.getCountryList()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        console.log("returned result from get country list: ", retResult);
        if (retResult.result == 1) {

          this.countrySelect = <BasicSelectModel[]>retResult.body;
        }
        else
        this.countrySelect = <BasicSelectModel[]>retResult.body;
      });
  }

  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();

    if (this.editMode) {
      this.opService.getOperatorDetails(this.opId)
        .subscribe((res: HttpResponse<ReturnResult>) => {
          let retModel = res.body;
          if (retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <OperatorResult>retModel.body;
            // console.log("returned userModel: ", this.userModel);
            this.initForm();
          }
        });
    }
  }


  getCurrency() {
    this.listService.getCurrencyList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {
          console.log("returned result from get currency list: ", retResult);
          this.currencySelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }



  private initForm() {
    if (this.editMode) {
      this.form.patchValue({
        operatorId: this.userModel.operatorId,
        operatorName: this.userModel.operatorName,
        emailCost: this.userModel.emailCost,
        smsCost: this.userModel.smsCost
      });
      this.form.get('countryId').setValue(this.userModel.countryId.toString());
      this.form.get('currencyId').setValue(this.userModel.currencyId.toString());
      this.form.get('isActive').setValue(this.userModel.isActive.toString());
      this.form.get('countryId').disable();
      this.form.get('currencyId').disable();
      this.form.get('operatorName').disable();
    }
  }

  onSubmit() {
    this.successMessage = "";
    this.failMessage = "";
    // this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log("What is this form data: ", this.form.value);
    let serviceResult = null;
    if (this.editMode)
      serviceResult = this.opService.updateOperator(this.form.value);
    else
      serviceResult = this.opService.addOperator(this.form.value);
    serviceResult
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          this.successMessage = "The Details Were Updated Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the details";
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
