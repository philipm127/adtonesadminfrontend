import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddMaxAdvertRuleComponent } from '../add-max-advert-rule/add-max-advert-rule.component';

describe('AddMaxAdvertRuleComponent', () => {
  let component: AddMaxAdvertRuleComponent;
  let fixture: ComponentFixture<AddMaxAdvertRuleComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMaxAdvertRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMaxAdvertRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
