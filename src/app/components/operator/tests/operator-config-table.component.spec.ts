import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OperatorConfigTableComponent } from '../operator-config-table/operator-config-table.component';

describe('OperatorConfigTableComponent', () => {
  let component: OperatorConfigTableComponent;
  let fixture: ComponentFixture<OperatorConfigTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OperatorConfigTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperatorConfigTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
