import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddOperatorConfigurationComponent } from '../add-operator-configuration/add-operator-configuration.component';

describe('AddOperatorConfigurationComponent', () => {
  let component: AddOperatorConfigurationComponent;
  let fixture: ComponentFixture<AddOperatorConfigurationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOperatorConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOperatorConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
