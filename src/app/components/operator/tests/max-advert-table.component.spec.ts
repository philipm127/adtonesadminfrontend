import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MaxAdvertTableComponent } from '../max-advert-table/max-advert-table.component';

describe('MaxAdvertTableComponent', () => {
  let component: MaxAdvertTableComponent;
  let fixture: ComponentFixture<MaxAdvertTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MaxAdvertTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaxAdvertTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
