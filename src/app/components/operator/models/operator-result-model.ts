export interface OperatorResult {
  operatorId: number;
  operatorName: string;
  countryId: number;
  countryName: string;
  isActive: string;
  emailCost: number;
  smsCost: number;
  currency: string;
  currencyId:number;
  createdDate: string;
}

export interface OperatorMaxAdverts extends OperatorResult {
  operatorMaxAdvertId: number;
  keyName: string;
  keyValue: string;
}

export interface OperatorConfigurationResult extends OperatorResult {
  operatorConfigurationId: number;
  days: number;
}
