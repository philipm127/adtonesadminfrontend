import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const crmRoutes: Routes = [
  // { path: 'countries', component: CountryTableComponent },
  // { path: 'areas', component: AreaTableComponent },
  // { path: 'areadetails/:id', component: AddAreaComponent },
  // { path: 'countrydetails/:id', component: AddCountryComponent }
];

@NgModule({
  imports: [RouterModule.forChild(crmRoutes)],
  exports: [RouterModule]
})
export class CrmRoutingModule { }
