import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PermissionManagementMenuComponent } from './permission-management-menu/permission-management-menu.component';
import { PermissionManagementRoutingModule } from './permission-management-routing.module';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { PermissionElementsComponent } from './permission-elements/permission-elements.component';
import { PermissionManagementAddComponent } from './permission-management-add/permission-management-add.component';
import { AddRangeComponent } from './add-range/add-range.component';
import { PermissionManagementChangesComponent } from './permission-management-changes/permission-management-changes.component';
import { PermissionManagementUsersComponent } from './permission-management-users/permission-management-users.component';
import { PermissionUsersElementsComponent } from './permission-management-users/permission-users-elements/permission-users-elements.component';
import { PermissionsRawDataComponent } from './permissions-raw-data/permissions-raw-data.component';
import { AddeditElementComponent } from './addedit-element/addedit-element.component';
import { PermissionManagementComponent } from './permission-management/permission-management.component';
import { ComparePermissionsComponent } from './compare-permissions/compare-permissions.component';



@NgModule({
  declarations: [
    PermissionManagementMenuComponent,
    PermissionManagementAddComponent,
    PermissionElementsComponent,
    AddRangeComponent,
    PermissionManagementChangesComponent,
    PermissionManagementUsersComponent,
    PermissionUsersElementsComponent,
    PermissionsRawDataComponent,
    AddeditElementComponent,
    PermissionManagementComponent,
    ComparePermissionsComponent
  ],
  imports: [
    CommonModule,
    PermissionManagementRoutingModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule
  ]
})
export class PermissionManagementModule { }
