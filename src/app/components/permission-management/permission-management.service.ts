import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ReturnResult } from '../../models/return-result';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class PermissionManagementService {
  myAppUrl: string;
  controller: string = "permissionmanagement/";

  constructor(private http: HttpClient) {
    this.myAppUrl = (environment.appUrl + this.controller);
  }

  public getPermissionPageList(roleid: number) {
    if (roleid == null)
      roleid = 0;
    let ctrAction = "v1/GetPermissionPagesList/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + roleid, { observe: "response" });
  }


  public getUserPermission(id: number): Observable<ReturnResult> {
    let ctrAction = "v1/GetPermissionsByUser/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id);
  }


  public updateUserPermission(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/UpdateUserPermissions"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public addNewPage(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/AddNewPage"
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public addNewElement(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/AddNewElement"
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public comparePermissions(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/ComparePermissionPages"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public getPermissionMenuList(roleid: number) {
    if (roleid == null)
      roleid = 0;
    let ctrAction = "v1/GetPermissionMenuList/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + roleid, { observe: "response" });
  }


  public changeMenu(model: any): Observable<ReturnResult> {
    console.log("changeMenu(model: any) ", model);
    let ctrAction = "v1/UpdateMenu"
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


}
