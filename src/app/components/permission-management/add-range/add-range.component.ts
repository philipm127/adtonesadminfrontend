import { Component, Input } from '@angular/core';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-range',
  templateUrl: './add-range.component.html',
  styleUrls: ['./add-range.component.css']
})
export class AddRangeComponent {
  @Input() pageForm: FormGroup;
  roleSelect: BasicSelectModel[];
  userSelect: BasicSelectModel[];
  retModel: any;
  destroy$: Subject<BasicSelectModel> = new Subject();

  constructor(private selectService: SelectServicesService) {
    this.selectService.getRoleList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retModel = res.body;
        if (this.retModel.result == 1) {
          console.log("returned result: ", this.retModel.result);
          this.roleSelect = <BasicSelectModel[]>this.retModel.body;
        }
      });

    // this.selectService.getRealUsersRoles()
    //   .pipe(takeUntil(this.destroy$))
    //   .subscribe((res: HttpResponse<ReturnResult>) => {
    //     this.retModel = res.body;
    //     if (this.retModel.result == 1) {
    //       console.log("returned result: ", this.retModel.result);
    //       this.userSelect = <BasicSelectModel[]>this.retModel.body;
    //     }
    //   });
  }

  allRoleCheck(event: any) {
    let chk = this.pageForm.get('roles');
    console.log("let chk =this.pageForm.get('roles'); ", chk.value.length);
    if (chk.value.length == 0)
      this.pageForm.get('all').setValue(true);
    else
      this.pageForm.get('all').setValue(true);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
