import { Component, OnInit, Input } from '@angular/core';
import { PermissionData } from 'src/app/models/permission-data';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { PermissionManagementService } from '../permission-management.service';
import { PermissionService } from '../../shared/services/permission-data.service';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';

@Component({
  selector: 'app-permissions-raw-data',
  templateUrl: './permissions-raw-data.component.html',
  styleUrls: ['./permissions-raw-data.component.css']
})
export class PermissionsRawDataComponent implements OnInit {

  pagename: string = "UserPermissionRaw";
  successMessage: string;
  failMessage: string;
  userId: number;
  userPerms: PermissionData[];
  userSelect: BasicSelectModel[];
  destroy$: Subject<BasicSelectModel> = new Subject();

  form: FormGroup;

  constructor(private fb: FormBuilder, private userlist: SelectServicesService,
    private accessService: AccessServicesService, private perm: PermissionManagementService,
    private permService: PermissionService) {
    this.form = new FormGroup({
      permissions: new FormControl(""),
      userId: new FormControl("")
    });
  }

  ngOnInit(): void {
    this.getUsersWPermissions();
  }


  getUserPermission(event: any) {
    this.successMessage = null;
    this.failMessage = null;
    this.userId = event.value;
    this.perm.getUserPermission(event.value)
      .subscribe(resp => {
        let retModel = resp
        if (retModel.result == 1) {
          this.userPerms = <PermissionData[]>retModel.body;
          this.form.patchValue({
            permissions: this.userPerms
          });

        }
        else {
          this.failMessage = "There was a problem getting the users permission details";
        }
      });
  }

  // Populate the list of users
  getUsersWPermissions() {

    this.userlist.getUserPermissionList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {
          this.userSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }



  onSubmit() {
    console.log("What is this form data: ", this.form.getRawValue());
    let permJobject = null;
    let getRaw = this.form.getRawValue();
    let permRaw = getRaw.permissions;
    try {
      permJobject = JSON.parse(permRaw);
      // console.log("Parse passed try ");
    }
    catch (e) {
      // console.log("Parse caught in catch ");
      permJobject = permRaw;
    }

    let newString = JSON.stringify(permJobject, null, 0);
    // console.log("What is newString: ", newString);

    let newModel: any = ({
      "userId": this.userId,
      "permissions": newString
    })
    this.perm.updateUserPermission(newModel)
      .subscribe(resp => {
        let retModel = resp
        if (retModel.result == 1)
          this.successMessage = "The permissions were updated successfully";
        else
          this.failMessage = "There was a problem updating the permissions";
      });

  }



  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
