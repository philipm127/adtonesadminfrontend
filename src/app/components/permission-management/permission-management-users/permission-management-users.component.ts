import { Component, OnInit, Input } from '@angular/core';
import { PermissionData, permissionList } from 'src/app/models/permission-data';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { UmanagementService } from '../../usermanagement/umanagement.service';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { PermissionManagementService } from '../permission-management.service';
import { PermissionService } from '../../shared/services/permission-data.service';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { AdvertiserUser } from '../../usermanagement/models/users-applied-models';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-permission-management-users',
  templateUrl: './permission-management-users.component.html',
  styleUrls: ['./permission-management-users.component.css']
})
export class PermissionManagementUsersComponent implements OnInit {
  pagename: string = "UserPermission";
  userId: any;
  userPerms: PermissionData[];
  testPermissionOutput: boolean = environment.testPermissionOutput;
  userModel: AdvertiserUser;
  userSelect: BasicSelectModel[];
  showDD: boolean = false;
  showUserData: boolean = false;
  form: FormGroup;

  destroy$: Subject<BasicSelectModel> = new Subject();

  constructor(private fb: FormBuilder, private userService: UmanagementService, private route: ActivatedRoute,
    private accessService: AccessServicesService, private perm: PermissionManagementService, private userlist: SelectServicesService,
    private permService: PermissionService) {
    this.userId = this.route.snapshot.paramMap.get('id');

  }

  get pageData() {
    return (<FormArray>this.form.get('pageData')).controls;
  }

  getTopElementsFor(index) {
    return (<FormArray>(<FormArray>this.form.get('pageData')).controls[index].get('elements')).controls;
  }

  ngOnInit(): void {

    if (this.userId != null && this.userId != 0)
      this.loadPermissionData()
    else {
      this.getUsers();
      this.showDD = true;
    }
  }

  getUserInfo() {
    this.userService.getUserInfo(this.userId)
      .subscribe(
        data => {
          let retModel = data;
          if (retModel.result == 1) {
            // console.log("getUserInfo() inside PermManUser: ", retModel.body);
            this.userModel = <AdvertiserUser>retModel.body;
            // console.log("returned userModel: ", this.userModel);
            this.showUserData = true;
          }
        });
  }

  getUserPermission(event: any) {
    this.userId = event.value;
    this.loadPermissionData();
  }

  loadPermissionData() {
    this.getUserInfo();
    // This is used for testing how it looks
    if (this.testPermissionOutput) {
      let serviceData = null;
      //   console.log("Entered if(this.testPermissionOutput");
      // serviceData = this.permService.getAllpermissionData();
      serviceData = this.permService.getAllpermissionDataOperator()

      serviceData
        .subscribe(
          data => {
            this.userPerms = data
            // console.log("What is the new this.userPerms: ", this.userPerms);
            this.setForm();
          });
    } // This is to be used when live and data in DB
    else if (!this.testPermissionOutput) {
      this.perm.getUserPermission(this.userId)
        .subscribe(
          data => {
            let retModel = data
            if (retModel.result == 1) {
              this.userPerms = <PermissionData[]>retModel.body;
              // console.log("returned userPerms: ", this.userPerms);
              this.setForm();
            }
          });
    }
  }


  setForm() {
    this.form = this.fb.group({
      pageData: this.fb.array(this.getPageData())
    });
  }


  getPageData() {
    let general: boolean = false;
    for (let i in this.userPerms) {
      // console.log("getPageData ",this.userPerms[i].pageName);
    }
    return this.userPerms.map(page => this.fb.group({
      pageName: page.pageName,
      visible: page.visible,
      elements: this.fb.array(this.getTopElements(page.elements, page.pageName))
    }));
  }


  getTopElements(groups: any[], page: string) {
    if (page != null && page != undefined) {
      return groups.map(group => this.fb.group({
        name: group.name,
        visible: group.visible,
        enabled: group.enabled,
        route: group.route,
        arrayId: [group.arrayId],
        description: group.description,
        type: group.type,
        elements: this.fb.array(this.getElements(group.elements))
      }));
    }
    else
      return [];

  }


  getElements(eles: any[]) {
    // console.log("value of eles perms: ", eles);
    if (eles != null) {
      return eles.map(els => this.fb.group({
        name: els.name,
        visible: els.visible,
        enabled: els.enabled,
        route: els.route,
      }));
    }
    return [];
  }


  getUsers() {
    this.userlist.getRealUsersRoles()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {
          this.userSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }


  onSubmit() {
    // this.payLoad = JSON.stringify(this.form.getRawValue());
    // console.log("What is this form data: ", this.form.value);

    let permlist: permissionList = <permissionList>this.form.value;;
    let modelSource = permlist.pageData;
    for (let i in modelSource) {
      if (modelSource[i].pageName == "GeneralAccess") {
        for (let j in modelSource[i].elements) {
          if (!Array.isArray(modelSource[i].elements[j].arrayId))
            if (modelSource[i].elements[j].arrayId.length > 0) {
              modelSource[i].elements[j].arrayId = modelSource[i].elements[j].arrayId.toString().split(',').map(Number);
            }
            else
              modelSource[i].elements[j].arrayId = [];
        }
      }
    }

    // console.log("What is the new this.userPerms: ", modelSource);
    let newModel: any = ({
      "userId": this.userId,
      "permissions": JSON.stringify(modelSource)
    })
    this.perm.updateUserPermission(newModel)
      .subscribe(resp => {
      });
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
