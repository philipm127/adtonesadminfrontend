import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'app-permission-users-elements',
  templateUrl: './permission-users-elements.component.html',
  styleUrls: ['./permission-users-elements.component.css']
})
export class PermissionUsersElementsComponent implements OnInit {

  constructor() { }

  @Input() formmain: FormGroup;
  @Input() parentForm: FormGroup;
  @Input() taskIndex;
  @Input() groupIndex;

  ngOnInit() {
  }

  getElementsFor() {
    // console.log(this.taskIndex);
    return (<FormArray>(<FormArray>(<FormArray>this.formmain.get('pageData'))
      .controls[this.taskIndex].get('elements'))
      .controls[this.groupIndex].get('elements'))
      .controls;
  }

}
