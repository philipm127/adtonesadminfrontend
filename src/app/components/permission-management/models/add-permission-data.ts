import { element } from 'src/app/models/permission-data';

export interface PermissionList{
  pages:AddPermissionData[];
}


export interface AddPermissionData{
  pageName:string;
  visible:boolean;
  elements:element[];
  users:number[];
  roles:number[];
  all:boolean;
}
