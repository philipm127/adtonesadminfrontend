import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { PermissionData } from 'src/app/models/permission-data';
import { ReturnResult } from 'src/app/models/return-result';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { PermissionService } from '../../shared/services/permission-data.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { PermissionManagementService } from '../permission-management.service';

@Component({
  selector: 'app-compare-permissions',
  templateUrl: './compare-permissions.component.html',
  styleUrls: ['./compare-permissions.component.css']
})
export class ComparePermissionsComponent implements OnInit {

  pagename: string = "CompareUserPermission";
  successMessage: string;
  failMessage: string;
  userId: number = 0;
  id: number = 0;
  userPerms: string[];
  userSelect: BasicSelectModel[];
  destroy$: Subject<BasicSelectModel> = new Subject();

  form: FormGroup;

  constructor(private fb: FormBuilder, private userlist: SelectServicesService,
    private accessService: AccessServicesService, private perm: PermissionManagementService,
    private permService: PermissionService) {
    this.form = new FormGroup({
      id: new FormControl(""),
      userId: new FormControl(""),
      comp: new FormControl("")
    });
  }

  ngOnInit(): void {
    this.getUsersWPermissions();
  }

  getUserPermission2(event: any) {
    this.id = event.value;
  }


  getUserPermission(event: any) {
    this.userId = event.value;
  }

  onSubmit() {
    if (this.id != 0 && this.userId != 0) {
      let newModel = {
        id: this.id,
        userId: this.userId
      }
      this.perm.comparePermissions(newModel)
        .subscribe(resp => {
          let retModel = resp
          if (retModel.result == 1) {
            this.userPerms = <string[]>retModel.body;
          }
          else {
            this.failMessage = "There was a problem updating the details";
          }
        });
    }
    else
      this.failMessage = "You need 2 users to ";
  }

  getUsersWPermissions() {
    this.userlist.getUserPermissionList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {
          this.userSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }




  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
