import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { PermissionManagementService } from '../permission-management.service';


@Component({
  selector: 'app-permission-management-add',
  templateUrl: './permission-management-add.component.html',
  styleUrls: ['./permission-management-add.component.css']
})

export class PermissionManagementAddComponent implements OnInit {

  public pageForm: FormGroup;

  messageView: boolean = false;
  successMessage: string = "";
  failMessage: string;

  constructor(private fb: FormBuilder, private permService: PermissionManagementService) { }

  ngOnInit(): void {
    this.pageForm = this.fb.group({
      pageName: this.fb.control[''],
      visible: [true],
      users: this.fb.control[''],
      roles: this.fb.control[''],
      all: [true],
      elements: this.fb.array([
        this.fb.group({
          name: ['', Validators.required],
          visible: [true],
          enabled: [true],
          type: ['element'],
          route: ['']
        })
      ])
    })
  }


  onSubmit() {
    this.messageView = false;
    let newarray = this.pageForm.value;
    // console.log("What is this form data: ", this.pageForm.value);
    if (newarray.users == null && newarray.roles == null && newarray.all === false) {
      this.failMessage = "You need to select how you want the page applied";
      //this.messageView = true)
      this.messageView = true;
    }
    else {
      let eles = newarray.elements;
      if (newarray.users == null)
        newarray.users = [];
      if (newarray.roles == null)
        newarray.roles = [];

      // console.log("let eles = newarray.elements; ", eles);
      eles = this.checkElements(eles);
      let newModel = {
        users: newarray.users,
        roles: newarray.roles,
        all: newarray.all,
        pageName: newarray.pageName,
        visible: newarray.visible,
        elements: eles
      }

      console.log("let newModel = {; ", newModel);
      this.permService.addNewPage(newModel)
        .subscribe(resp => {
          let retModel = resp

          if (retModel.result == 1) {
            this.messageView = true;
            this.successMessage = "The Details Were Updated Successfully";
          }
          else {
            this.messageView = true;
            if (retModel.error == null || retModel.error.length == 0)
              this.failMessage = "There was a problem updating the details";
            else
              this.failMessage = retModel.error;
          }
        });
    }
  }

  checkElements(eles: any) {
    for (let i = 0; i < eles.length; i++) {
      if (eles[i].name.length == 0) {
        eles.splice(i, 1)
        i -= 1
      }
    }
    return eles;
  }

  removeElement(i) {
    let elementsArray = this.pageForm.controls.elements as FormArray;
    elementsArray.removeAt(i);
  }

  addElement() {
    let elementsArray = this.pageForm.controls.elements as FormArray;
    let arrayLength = elementsArray.length;
    let newElementGroup: FormGroup = this.fb.group({
      name: ['', Validators.required],
      visible: ['true'],
      enabled: ['true'],
      type: [''],
      route: ['']
    });
    elementsArray.insert(arrayLength, newElementGroup);
  }

}
