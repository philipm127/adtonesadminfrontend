import { Component, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-permission-elements',
  templateUrl: './permission-elements.component.html',
  styleUrls: ['./permission-elements.component.css']
})
export class PermissionElementsComponent {
  @Input() pageForm: FormGroup;

  constructor(private fb: FormBuilder) { }


  removeElement(i) {
    let elementsArray = this.pageForm.controls.elements as FormArray;
    elementsArray.removeAt(i);
  }

  addElement() {
    let elementsArray = this.pageForm.controls.elements as FormArray;
    let arrayLength = elementsArray.length;
    let newElementGroup: FormGroup = this.fb.group({
      name: ['', Validators.required],
      visible: ['true'],
      enabled: ['true'],
      type: [''],
      route: ['']
    });
    elementsArray.insert(arrayLength, newElementGroup);
  }

}
