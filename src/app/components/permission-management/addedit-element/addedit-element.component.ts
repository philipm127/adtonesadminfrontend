import { Component, OnInit } from '@angular/core';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { PermissionManagementService } from '../permission-management.service';
import { SelectServicesService } from '../../shared/services/select-services.service';

@Component({
  selector: 'app-addedit-element',
  templateUrl: './addedit-element.component.html',
  styleUrls: ['./addedit-element.component.css']
})
export class AddeditElementComponent implements OnInit {

  public pageForm: FormGroup;
  pageSelect: BasicSelectModel[];
  roleSelect: BasicSelectModel[];
  destroy$: Subject<BasicSelectModel> = new Subject();
  successMessage: string = "";
  failMessage: string;
  messageView: boolean = false;
  roleid: number;

  exelements: string[];

  constructor(private fb: FormBuilder, private permService: PermissionManagementService,
    private selectService: SelectServicesService) {
    this.getRoles();
  }

  ngOnInit(): void {
    this.pageForm = this.fb.group({
      pageName: this.fb.control[''],
      users: this.fb.control[''],
      roles: this.fb.control[''],
      all: [false],
      elements: this.fb.array([
        this.fb.group({
          name: ['', Validators.required],
          visible: [true],
          enabled: [true],
          type: ['element'],
          route: ['']
        })
      ])
    })
  }


  getPagePermission(event: any) {
    this.messageView = false;
    this.failMessage = null;
    this.pageForm.patchValue({
      pageName: event.value
    });
  }


  getPageList(event: any) {
    this.messageView = false;
    this.failMessage = null;
    this.roleid = parseInt(event.value);
    console.log("getPageList(event: any): ", this.roleid);
    this.pageForm.get('roles').setValue([this.roleid.toString()]);
    this.permService.getPermissionPageList(this.roleid)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {
          this.pageSelect = <BasicSelectModel[]>retResult.body;
        }
        else {
          this.failMessage = "There are no permissions for the selected role";
          this.messageView = true;
        }
      });
  }

  onSubmit() {
    this.messageView = false;
    this.failMessage = null;
    this.successMessage = null;
    let newarray = this.pageForm.value;
    // console.log("What is this form data: ", this.pageForm.value);
    if (newarray.users == null && newarray.roles == null && newarray.all === false) {
      this.failMessage = "No role was selected";
      this.messageView = true;
    }
    else if (newarray.pageName == null || newarray.pageName.length == 0) {
      this.failMessage = "No Page was selected";
      this.messageView = true;
      console.log("Entered Failed Page");
    }
    else if (newarray.eles == undefined || newarray.eles == null || newarray.eles.count == 0) {
      this.failMessage = "No Element name was added";
      this.messageView = true;
    }
    else {
      if (newarray.users == null)
        newarray.users = [];
      if (newarray.roles == null)
        newarray.roles = [];

      let eles = newarray.elements;
      // console.log("let eles = newarray.elements; ", eles);
      eles = this.checkElements(eles);
      let newModel = {
        users: newarray.users,
        roles: newarray.roles,
        all: newarray.all,
        pageName: newarray.pageName,
        elements: eles
      }

      console.log("let newModel = {; ", newModel);
      this.permService.addNewElement(newModel)
        .subscribe(resp => {
          let retModel = resp

          if (retModel.result == 1) {
            this.messageView = true;
            this.successMessage = "The Details Were Updated Successfully";
          }
          else {
            this.messageView = true;
            this.failMessage = "There was a problem updating the details";
          }
        });
    }
  }

  checkElements(eles: any) {
    for (let i = 0; i < eles.length; i++) {
      if (eles[i].name.length == 0) {
        eles.splice(i, 1)
        i -= 1
      }
    }
    return eles;
  }

  removeElement(i) {
    let elementsArray = this.pageForm.controls.elements as FormArray;
    elementsArray.removeAt(i);
  }

  addElement() {
    let elementsArray = this.pageForm.controls.elements as FormArray;
    let arrayLength = elementsArray.length;
    let newElementGroup: FormGroup = this.fb.group({
      name: ['', Validators.required],
      visible: ['true'],
      enabled: ['true'],
      type: [''],
      route: ['']
    });
    elementsArray.insert(arrayLength, newElementGroup);
  }


  getRoles() {
    this.selectService.getRoleList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retModel = res.body;
        if (retModel.result == 1) {
          console.log("returned result: ", retModel.result);
          this.roleSelect = <BasicSelectModel[]>retModel.body;
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
