import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { PermissionManagementService } from '../permission-management.service';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { PermissionData } from 'src/app/models/permission-data';

@Component({
  selector: 'app-permission-management-menu',
  templateUrl: './permission-management-menu.component.html',
  styleUrls: ['./permission-management-menu.component.css']
})

export class PermissionManagementMenuComponent implements OnInit {
  pageName: string = "MenuPermission";
  public pageForm: FormGroup;
  pageSelect: BasicSelectModel[];
  roleSelect: BasicSelectModel[];
  destroy$: Subject<BasicSelectModel> = new Subject();
  successMessage: string = "";
  failMessage: string = "You need to select how you want the page applied";
  messageView: boolean = false;
  roleid: number;
  userPerms: PermissionData;

  constructor(private fb: FormBuilder, private permService: PermissionManagementService,
    private selectService: SelectServicesService) {
    this.getRoles();
  }


  ngOnInit(): void {
    // this.pageForm = this.fb.group({
    //   pageName: this.fb.control[''],
    //   users: this.fb.control[''],
    //   roles: this.fb.control[''],
    //   all: [true],
    //   elements: this.fb.array([
    //     this.fb.group({
    //       name: ['', Validators.required],
    //       visible: [true],
    //       enabled: [true],
    //       type: ['element'],
    //       route: ['']
    //     })
    //   ])
    // })
  }


  // getPagePermission(event: any) {
  //   this.pageForm.patchValue({
  //     pageName: event.value
  //   });
  // }


  getMenuList(event: any) {
    this.roleid = parseInt(event.value);
    this.permService.getPermissionMenuList(this.roleid)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {
          this.userPerms = <PermissionData>retResult.body;
          console.log("this.userPerms = : ", this.userPerms);
          this.setForm();
        }
      });
  }


  onSubmit() {
    let newarray = this.pageForm.value;
    // console.log("What is this form data: ", this.pageForm.value);
    if (newarray.users == null && newarray.roles == null && newarray.all === false) {
      this.failMessage = "You need to select how you want the page applied";
      //this.messageView = true)
      this.messageView = true;
    }


    let eles = newarray.elements;
    // console.log("let eles = newarray.elements; ", eles);
    eles = this.checkElements(eles);
    let newModel = {
      users: newarray.users,
      roles: [this.roleid],
      all: newarray.all,
      pageName: "MainMenu",
      elements: eles
    }

    console.log("let newModel = {; ", newModel);
    this.permService.changeMenu(newModel)
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          this.successMessage = "The Details Were Updated Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the details";
        }
      });
  }

  checkElements(eles: any) {
    for (let i = 0; i < eles.length; i++) {
      if (eles[i].name.length == 0) {
        eles.splice(i, 1)
        i -= 1
      }
    }
    return eles;
  }

  // removeElement(i) {
  //   let elementsArray = this.pageForm.controls.elements as FormArray;
  //   elementsArray.removeAt(i);
  // }

  addElement(index: number) {
    let elementsArray = this.pageForm.get('elements') as FormArray;
    console.log("addElement elementsArray: ", elementsArray);
    let arrayLength = elementsArray.length;
    let newElementGroup: FormGroup = this.fb.group({
      name: ['', Validators.required],
      visible: ['true'],
      enabled: ['true'],
      type: [''],
      route: [''],
      arrayId: [""],
      elements: [""]
    });
    console.log("addElement elementsArray: ", elementsArray);
    elementsArray.insert(arrayLength, newElementGroup);
  }


  addElement2(index: number) {
    let elementsArray = (<FormArray>(<FormArray>this.pageForm.get('elements'))
      .controls[index].get('elements')) as FormArray;
    console.log("addElement2 elementsArray: ", elementsArray);
    let arrayLength = elementsArray.length;
    let newElementGroup: FormGroup = this.fb.group({
      name: ['', Validators.required],
      visible: ['true'],
      enabled: ['true'],
      type: [''],
      route: [''],
      arrayId: [""]
    });
    console.log("addElement2 elementsArray: ", elementsArray);
    elementsArray.insert(arrayLength, newElementGroup);
  }

  getRoles() {
    this.selectService.getRoleList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retModel = res.body;
        if (retModel.result == 1) {
          console.log("returned result: ", retModel.result);
          this.roleSelect = <BasicSelectModel[]>retModel.body;
        }
      });
  }


  // get pageData() {
  //   return (<FormArray>this.pageForm.get('pageData')).controls;
  // }

  getTopElementsFor(index) {
    console.log("getTopElementsFor: ", (<FormArray>this.pageForm.get('elements')).controls);
    return (<FormArray>this.pageForm.get('elements')).controls;
  }

  getElementsFor2(groupIndex: number) {
    // console.log(this.taskIndex);
    return (<FormArray>(<FormArray>this.pageForm.get('elements'))
      .controls[groupIndex].get('elements'))
      .controls;
  }


  setForm() {
    this.pageForm = this.fb.group({
      pageName: this.userPerms.pageName,
      visible: this.userPerms.visible,
      elements: this.fb.array(this.getTopElements(this.userPerms.elements, this.userPerms.pageName))
    });
  }

  // getPageData() {
  //   let general: boolean = false;
  //   for (let i in this.userPerms) {
  //     // console.log("getPageData ",this.userPerms[i].pageName);
  //   }
  //   let model =  this.fb.group({
  //     pageName: this.userPerms.pageName,
  //     visible: this.userPerms.visible,
  //     elements: this.fb.array(this.getTopElements(this.userPerms.elements, this.userPerms.pageName))
  //   });
  //   return model;
  // }

  getTopElements(groups: any[], page: string) {
    if (page != null && page != undefined) {
      return groups.map(group => this.fb.group({
        name: group.name,
        visible: group.visible,
        enabled: group.enabled,
        route: group.route,
        arrayId: [group.arrayId],
        description: group.description,
        type: group.type,
        elements: this.fb.array(this.getElements(group.elements))
      }));
    }
    else
      return [];

  }


  getElements(eles: any[]) {
    // console.log("value of eles perms: ", eles);
    if (eles != null) {
      return eles.map(els => this.fb.group({
        name: els.name,
        visible: els.visible,
        enabled: els.enabled,
        route: els.route,
      }));
    }
    return [];
  }

  move(shift, currentIndex) {
    const rules = this.pageForm.get('elements') as FormArray;

    let newIndex: number = currentIndex + shift;
    if (newIndex === -1) {
      newIndex = rules.length - 1;
    } else if (newIndex == rules.length) {
      newIndex = 0;
    }

    const currentGroup = rules.at(currentIndex);
    rules.removeAt(currentIndex);;
    rules.insert(newIndex, currentGroup)
  }

  move2(shift, parentIndex, currentIndex) {
    const rules = (<FormArray>(<FormArray>this.pageForm.get('elements'))
      .controls[parentIndex].get('elements')) as FormArray;

    let newIndex: number = currentIndex + shift;
    if (newIndex === -1) {
      newIndex = rules.length - 1;
    } else if (newIndex == rules.length) {
      newIndex = 0;
    }


    const currentGroup = rules.at(currentIndex);
    rules.removeAt(currentIndex);;
    rules.insert(newIndex, currentGroup)
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
