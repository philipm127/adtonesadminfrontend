import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { PermissionManagementService } from '../permission-management.service';
import { PermissionData } from 'src/app/models/permission-data';
import { ReturnResult } from 'src/app/models/return-result';
import { PermissionList } from '../models/add-permission-data';

@Component({
  selector: 'app-permission-management-changes',
  templateUrl: './permission-management-changes.component.html',
  styleUrls: ['./permission-management-changes.component.css']
})
export class PermissionManagementChangesComponent implements OnInit, AfterViewInit {
  public pageForm: FormGroup;

  messageView: boolean = false;
  successMessage: string="";
  failMessage: string = "You need to select how you want the page applied";
  userPerms: PermissionList;
  retModel: ReturnResult;
  constructor(private fb: FormBuilder, private perm: PermissionManagementService) {
    this.pageForm = this.fb.group({
      pages:this.fb.array([
      this.fb.group({
    pageName: this.fb.control[''],
    visible: [true],
    elements: this.fb.array([
      this.fb.group({
        name: ['', Validators.required],
        visible: [''],
        enabled: [''],
        type: [''],
        route: [''],
        description: ['']
      })
    ])
  })
  ])
});
    // this.perm.getUserPermission(19)
    //   .subscribe(
    //     data => {
    //       this.retModel = data
    //       if (this.retModel.result == 1) {
    //         // console.log("returned result data: ", data);
    //         // console.log("returned converted result: ", this.retModel);
    //         // const permU:PermissionData[] = JSON.parse(this.retModel.body.toString());
    //         // console.log("returned result: ", this.retModel.result);
    //         this.userPerms = <PermissionData[]>this.retModel.body;
    //         console.log("returned userPerms: ", this.userPerms);
    //       }
    //     });
   }

  ngOnInit(): void {
    // this.pageForm = this.fb.array([
    //     this.fb.group({
    //   pageName: this.fb.control[''],
    //   visible: [true],
    //   elements: this.fb.array([
    //     this.fb.group({
    //       name: ['', Validators.required],
    //       visible: [''],
    //       enabled: [''],
    //       type: [''],
    //       route: [''],
    //       description: ['']
    //     })
    //   ])
    // })
    // ])
    //})

  }

  ngAfterViewInit(){
    this.perm.getUserPermission(19)
      .subscribe(
        data => {
          this.retModel = data
          if (this.retModel.result == 1) {
            // console.log("returned result data: ", data);
            // console.log("returned converted result: ", this.retModel);
            // const permU:PermissionData[] = JSON.parse(this.retModel.body.toString());
            // console.log("returned result: ", this.retModel.result);
            this.userPerms = <PermissionList>this.retModel.body;
            console.log("returned userPerms: ", this.userPerms);
            this.pageForm.patchValue(this.userPerms);
          }
        });
    // this.pageForm.patchValue(this.userPerms);
  }


  onSubmit() {
    let newarray = this.pageForm.value;
    console.log("What is this form data: ", this.pageForm.value);
    console.log("What is this form RAW data: ", this.pageForm.getRawValue());
    // if (newarray.users == null && newarray.roles == null && newarray.all === false) {
    //   this.failMessage = "You need to select how you want the page applied";
    //   //this.messageView = true)
    //   this.messageView = true;
    // }
    // else
    //   console.log("What is roles: ", newarray.pageName);
    // this.userService.updateUserInfo(this.form.value)
    //   .subscribe(resp => {
    //     this.retModel = resp

    //     if (this.retModel.result == 1) {
    //       this.successMessage = "The Details Were Updated Successfully";
    //     }
    //     else {
    //       this.failMessage = "There was a problem updating the details";
    //     }
    //   });
  }

  onChange(event: any, name: string, type: string, page: string) {
    console.log("What is this form data event : ", event);
    console.log("What is this form data name : ", name);
    console.log("What is this form data type : ", type);
    console.log("What is this form data page : ", page);
    // for (let i in this.userPerms) {
    //   if (this.userPerms[i].pageName == page) {
    //     for (let j in this.userPerms[i].elements) {
    //       if (this.userPerms[i].elements[j].name == name) {
    //         if (type == 'visible')
    //           this.userPerms[i].elements[j].visible = event;
    //         if (type == 'enabled')
    //           this.userPerms[i].elements[j].enabled = event;
    //         break;
    //       }
    //     }
    //   }
    // }
  }

}
