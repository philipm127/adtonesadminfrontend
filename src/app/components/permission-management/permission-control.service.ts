import { Injectable } from '@angular/core';
import { UserPermissionBase } from './permission-management-users/user-permission-base';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Injectable()
export class PermissionControlService {
  constructor() { }

  toFormGroup(questions: UserPermissionBase<string>[] ) {
    const group: any = {};

    questions.forEach(question => {
      group[question.key] = question.required ? new FormControl(question.value || '', Validators.required)
                                              : new FormControl(question.value || '');
    });
    return new FormGroup(group);
  }
}
