import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PermissionManagementMenuComponent } from './permission-management-menu/permission-management-menu.component';
import { AuthguardService } from 'src/app/services/auth/authguard.service';
import { PermissionManagementChangesComponent } from './permission-management-changes/permission-management-changes.component';
import { PermissionManagementAddComponent } from './permission-management-add/permission-management-add.component';
import { PermissionManagementUsersComponent } from './permission-management-users/permission-management-users.component';
import { PermissionsRawDataComponent } from './permissions-raw-data/permissions-raw-data.component';
import { AddeditElementComponent } from './addedit-element/addedit-element.component';
import { PermissionManagementComponent } from './permission-management/permission-management.component';
import { ComparePermissionsComponent } from './compare-permissions/compare-permissions.component';


const routes: Routes = [
  { path: 'permissionmanagement', component: PermissionManagementComponent },
  { path: 'permissionmenu', component: PermissionManagementMenuComponent, canActivate: [AuthguardService] },
  { path: 'permissionchange', component: PermissionManagementChangesComponent },
  { path: 'permissionpageadd', component: PermissionManagementAddComponent },
  { path: 'permissionelementadd', component: AddeditElementComponent },
  { path: 'userpermission/:id', component: PermissionManagementUsersComponent },
  { path: 'rawpermissions', component: PermissionsRawDataComponent },
  { path: 'comparepermissions', component: ComparePermissionsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PermissionManagementRoutingModule { }
