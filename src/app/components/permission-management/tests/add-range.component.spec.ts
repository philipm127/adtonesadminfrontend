import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddRangeComponent } from '../add-range/add-range.component';

describe('AddRangeComponent', () => {
  let component: AddRangeComponent;
  let fixture: ComponentFixture<AddRangeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
