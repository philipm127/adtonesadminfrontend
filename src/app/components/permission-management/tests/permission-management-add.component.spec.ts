import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PermissionManagementAddComponent } from '../permission-management-add/permission-management-add.component';

describe('PermissionManagementAddComponent', () => {
  let component: PermissionManagementAddComponent;
  let fixture: ComponentFixture<PermissionManagementAddComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionManagementAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionManagementAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
