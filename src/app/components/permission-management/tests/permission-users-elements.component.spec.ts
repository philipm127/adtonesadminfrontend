import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PermissionUsersElementsComponent } from '../permission-management-users/permission-users-elements/permission-users-elements.component';

describe('PermissionUsersElementsComponent', () => {
  let component: PermissionUsersElementsComponent;
  let fixture: ComponentFixture<PermissionUsersElementsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionUsersElementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionUsersElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
