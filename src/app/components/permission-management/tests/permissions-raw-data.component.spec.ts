import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PermissionsRawDataComponent } from './permissions-raw-data.component';

describe('PermissionsRawDataComponent', () => {
  let component: PermissionsRawDataComponent;
  let fixture: ComponentFixture<PermissionsRawDataComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionsRawDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionsRawDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
