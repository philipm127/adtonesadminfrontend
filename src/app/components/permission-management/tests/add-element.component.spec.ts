import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PermissionElementsComponent } from '../add-element/permission-elements.component';

describe('AddElementComponent', () => {
  let component: PermissionElementsComponent;
  let fixture: ComponentFixture<PermissionElementsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionElementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
