import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ComparePermissionsComponent } from '../compare-permissions/compare-permissions.component';

describe('ComparePermissionsComponent', () => {
  let component: ComparePermissionsComponent;
  let fixture: ComponentFixture<ComparePermissionsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ComparePermissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparePermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
