import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PermissionManagementChangesComponent } from '../permission-management-changes/permission-management-changes.component';

describe('PermissionManagementChangesComponent', () => {
  let component: PermissionManagementChangesComponent;
  let fixture: ComponentFixture<PermissionManagementChangesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionManagementChangesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionManagementChangesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
