import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PermissionManagementUsersComponent } from './permission-management-users.component';

describe('PermissionManagementUsersComponent', () => {
  let component: PermissionManagementUsersComponent;
  let fixture: ComponentFixture<PermissionManagementUsersComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionManagementUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionManagementUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
