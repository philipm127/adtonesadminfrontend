import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddeditElementComponent } from '../addedit-element/addedit-element.component';

describe('AddeditElementComponent', () => {
  let component: AddeditElementComponent;
  let fixture: ComponentFixture<AddeditElementComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
