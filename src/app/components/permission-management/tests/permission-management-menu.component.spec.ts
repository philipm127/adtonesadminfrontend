import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PermissionManagementMenuComponent } from '../permission-management-menu/permission-management-menu.component';

describe('PermissionManagementMenuComponent', () => {
  let component: PermissionManagementMenuComponent;
  let fixture: ComponentFixture<PermissionManagementMenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PermissionManagementMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionManagementMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
