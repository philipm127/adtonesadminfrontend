import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdvertRoutingModule } from './adverts-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatTableExporterModule } from 'mat-table-exporter';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { SharedModule } from '../shared/shared.module';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { AddeditAdvertcategoryComponent } from './addedit-advertcategory/addedit-advertcategory.component';
import { AdvertCategoryTableComponent } from './addedit-advertcategory/advert-category-table/advert-category-table.component';
import { AdvertTableComponent } from './advert-table/advert-table.component';
import { RejectDialogueComponent } from './advert-table/reject-dialogue/reject-dialogue.component';
import { ScriptDialogueComponent } from './advert-table/script-dialogue/script-dialogue.component';


@NgModule({
  declarations: [
    AdvertTableComponent,
    RejectDialogueComponent,
    ScriptDialogueComponent,
    AddeditAdvertcategoryComponent,
    AdvertCategoryTableComponent
  ],
  imports: [
    CommonModule,
    AdvertRoutingModule,
    SharedModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule
  ],
  exports: [
    ScriptDialogueComponent,
    RejectDialogueComponent
  ],
  providers: [
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } }
  ]
})
export class AdvertsModule { }
