import { Component, OnInit } from '@angular/core';
import { SelectServicesService } from 'src/app/components/shared/services/select-services.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ReturnResult } from 'src/app/models/return-result';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AdvertCategoryTableModel } from '../models/advert-category-table';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { AdvertService } from '../../adverts/advert.service';

@Component({
  selector: 'app-addedit-advertcategory',
  templateUrl: './addedit-advertcategory.component.html',
  styleUrls: ['./addedit-advertcategory.component.css']
})
export class AddeditAdvertcategoryComponent implements OnInit {

  pagename: string = "AddAdvertCategory";
  catId: string = "0";

  form = new FormGroup({
    advertCategoryId: new FormControl(0),
    categoryName: new FormControl("", Validators.required),
    countryId: new FormControl("", Validators.required),
  });


  editMode: boolean = false;
  userModel: any;
  retModel: ReturnResult;
  retResult: ReturnResult;
  countrySelect: BasicSelectModel[];
  destroy$: Subject<BasicSelectModel> = new Subject();

  userData: any;

  successMessage: string;
  failMessage: string;


  constructor(private formBuilder: FormBuilder, private advertService: AdvertService,
    private accessService: AccessServicesService, private listService: SelectServicesService,
    private route: ActivatedRoute, private router: Router) {

    this.catId = this.route.snapshot.paramMap.get('id');
    if (parseInt(this.catId) > 0) {
      this.pagename = "EditAdvertCategory";
      this.editMode = true;
    }

    this.userData = this.accessService.getUser();
    this.getCountry();
  }

  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();

    if (this.editMode) {
      this.advertService.getAdvertCategoryDetails(this.catId)
        .subscribe((res: HttpResponse<ReturnResult>) => {
          this.retModel = res.body;
          if (this.retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <AdvertCategoryTableModel>this.retModel.body;
            // console.log("returned userModel: ", this.userModel);
            this.initForm();
          }
        });
    }
  }

  getCountry() {
    this.listService.getCountryList()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        console.log("returned result from get country list: ", this.retResult);
        if (this.retResult.result == 1) {

          this.countrySelect = <BasicSelectModel[]>this.retResult.body;
        }
        else
          this.countrySelect = <BasicSelectModel[]>this.retResult.body;
      });
  }

  private initForm() {
    if (this.editMode) {
      this.form.patchValue({
        advertCategoryId: this.userModel.advertCategoryId,
        categoryName: this.userModel.categoryName
      });
      this.form.get('countryId').setValue(this.userModel.countryId.toString());
    }
  }

  onSubmit() {
    this.successMessage = "";
    this.failMessage = "";
    // this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log("What is this form data: ", this.form.value);
    let serviceResult = null;
    if (this.editMode)
      serviceResult = this.advertService.updateAdvertCategory(this.form.value);
    else
      serviceResult = this.advertService.addAdvertCategory(this.form.value);
    serviceResult
      .subscribe(resp => {
        this.retModel = resp

        if (this.retModel.result == 1) {
          this.successMessage = "The Details Were Updated Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the details";
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
