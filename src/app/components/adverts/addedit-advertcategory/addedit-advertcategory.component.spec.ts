import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditAdvertcategoryComponent } from './addedit-advertcategory.component';

describe('AddeditAdvertcategoryComponent', () => {
  let component: AddeditAdvertcategoryComponent;
  let fixture: ComponentFixture<AddeditAdvertcategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddeditAdvertcategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditAdvertcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
