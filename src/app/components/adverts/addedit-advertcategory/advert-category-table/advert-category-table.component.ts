import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { AdvertCategoryTableModel } from 'src/app/components/adverts/models/advert-category-table';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { PermissionData } from 'src/app/models/permission-data';
import { AdvertService } from '../../advert.service';

@Component({
  selector: 'app-advert-category-table',
  templateUrl: './advert-category-table.component.html',
  styleUrls: ['./advert-category-table.component.css']
})
export class AdvertCategoryTableComponent implements OnInit, OnDestroy {
  pagename: string = "AdvertCategoryList";
  userData: any;
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];
  destroy$: Subject<AdvertCategoryTableModel> = new Subject();
  dataSource: MatTableDataSource<AdvertCategoryTableModel>;
  modelSource: AdvertCategoryTableModel[];
  isLoading:boolean = true;

  countrySelect: BasicSelectModel[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  filterValues = {};
  retResult: ReturnResult = null;
  retResultUpdate: ReturnResult = null;


  tableSearchModel: TableSearchModel;

  constructor(private advertService: AdvertService, private datePipe: DatePipe, private popSearch: PopSearchDdService,
    private accessService: AccessServicesService) {
    this.userData = this.accessService.getUser();
    this.PermissionAssignment();
    this.getList();
  }

  getList(){
    this.advertService.getAdvertCategoryList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.isLoading = false;
          this.modelSource = <AdvertCategoryTableModel[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<AdvertCategoryTableModel>(this.modelSource);
        }
        this.countrySelect = this.popSearch.popCountry(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("Advert category searches are: ", filter);
          const a = !filter.name || data.categoryName.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          const b = !filter.country || data.countryName.trim().toLowerCase().includes(filter.country.trim().toLowerCase());
          return a && b;
        }) as (AdvertCategoryElement, string) => boolean;

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  ngOnInit(): void { }

  PermissionAssignment() {
    let permPageData = <PermissionData>this.accessService.getPermissionList(this.pagename);
    // console.log("In Permission assignment what is value of permPageData: ",permPageData);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }


  DeleteCategory(id: number, countryId: number) {
    this.advertService.deleteAdvertCategory(id, countryId)
      .subscribe(resp => {
        this.retResultUpdate = resp

        if (this.retResultUpdate.result == 1) {
          for (let i in this.modelSource) {
            if (this.modelSource[i].advertCategoryId == id) {
              this.modelSource.splice(parseInt(i),1);
              break;
            }
          }
          this.dataSource = new MatTableDataSource<AdvertCategoryTableModel>(this.modelSource);
        }
      });
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
