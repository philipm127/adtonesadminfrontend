import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertCategoryTableComponent } from './advert-category-table.component';

describe('AdvertCategoryTableComponent', () => {
  let component: AdvertCategoryTableComponent;
  let fixture: ComponentFixture<AdvertCategoryTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvertCategoryTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertCategoryTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
