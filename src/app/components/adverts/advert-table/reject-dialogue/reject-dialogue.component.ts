import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-reject-dialogue',
  templateUrl: './reject-dialogue.component.html',
  styleUrls: ['./reject-dialogue.component.css']
})
export class RejectDialogueComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<RejectDialogueComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

}
