import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectDialogueComponent } from './reject-dialogue.component';

describe('RejectDialogueComponent', () => {
  let component: RejectDialogueComponent;
  let fixture: ComponentFixture<RejectDialogueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RejectDialogueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectDialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
