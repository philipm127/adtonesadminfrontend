import { Component, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { DatePipe } from '@angular/common';
import { takeUntil, filter } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { AdvertTableModel } from 'src/app/components/adverts/models/advert-table-model';
import { AdvertService } from 'src/app/components/adverts/advert.service';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ScriptDialogueComponent } from './script-dialogue/script-dialogue.component';
import { RejectDialogueComponent } from './reject-dialogue/reject-dialogue.component';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';


@Component({
  selector: 'app-advert-table',
  templateUrl: './advert-table.component.html',
  styleUrls: ['./advert-table.component.css']
})

export class AdvertTableComponent implements AfterViewInit, OnDestroy {
  pagename: string = "AdvertsList";
  permData: any;
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];

  userData: any;
  adUserId: string = "0";
  adId: string = "0";
  miscName: string = "Advert";
  // Current UserId
  currentUser: number;
  emailLink: boolean = false;
  rejectAdvertId: number;
  rejectReason: string;

  destroy$: Subject<AdvertTableModel> = new Subject();
  isLoading: boolean = true;
  dataSource: MatTableDataSource<AdvertTableModel>;
  modelSource: AdvertTableModel[];

  statusSelect: BasicSelectModel[] = [];
  clientSelect: BasicSelectModel[] = [];
  countrySelect: BasicSelectModel[] = [];
  operatorSelect: BasicSelectModel[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  filterValues = {};
  retResult: ReturnResult = null;
  retResultUpdate: ReturnResult = null;

  successMessage: string;
  failMessage: string;

  tableSearchModel: TableSearchModel;


  constructor(private advertService: AdvertService, private datePipe: DatePipe,
    private popSearch: PopSearchDdService, private route: ActivatedRoute,
    private router: Router, private accessService: AccessServicesService, public dialog: MatDialog) {

    this.adUserId = this.route.snapshot.paramMap.get('id');
    this.adId = this.route.snapshot.paramMap.get("adId");
    this.userData = this.accessService.getUser();
    this.currentUser = this.userData.userId;
    // console.log("The current user is: ", this.currentUser);
    this.PermissionAssignment();
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  ngAfterViewInit() {
    this.loadData();
  }

  loadData() {
    this.successMessage = undefined;
    this.failMessage = undefined;
    let adService = null;
    console.log("this.adUserId: ", this.adUserId);
    console.log("this.adId: ", this.adId);
    if (this.adId == "0" || this.adId == null || this.adId == undefined)
      adService = this.advertService.GetAdvertList(this.adUserId);
    else
      adService = this.advertService.GetAdvertListById(this.adId);

    adService
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        this.isLoading = false;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <AdvertTableModel[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<AdvertTableModel>(this.modelSource);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
        this.statusSelect = this.popSearch.popStatus(this.modelSource);
        this.clientSelect = this.popSearch.popClient(this.modelSource);
        this.countrySelect = this.popSearch.popCountry(this.modelSource);
        this.operatorSelect = this.popSearch.popOperator(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("What is in filters: ",filter);
          // Object.keys(filter).forEach(key => console.log("What is key value: ",this.datePipe.transform(filter[key], 'yyyy-MM-dd')));
          const a = !filter.fullName || data.userName.trim().toLowerCase().includes(filter.fullName.trim().toLowerCase());
          const b = !filter.client || data.clientName.trim().toLowerCase().includes(filter.client.trim().toLowerCase());
          const c = !filter.status || data.rStatus.trim().toLowerCase().includes(filter.status.trim().toLowerCase());
          const d = !filter.dateFrom || data.createdDate >= this.datePipe.transform(filter.dateFrom, 'yyyy-MM-dd');
          const e = !filter.dateTo || data.createdDate <= this.datePipe.transform(filter.dateTo, 'yyyy-MM-dd');
          const f = !filter.name || data.advertName.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          const g = !filter.country || data.countryName.trim().toLowerCase().includes(filter.country.trim().toLowerCase());
          const h = !filter.operator || data.operatorName.trim().toLowerCase().includes(filter.operator.trim().toLowerCase());
          return a && b && c && d && e && f && g && h;
        }) as (AdvertElement, string) => boolean;
      });
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    // console.log("In Permission assignment what is value of permPageData: ", permPageData);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    // console.log("this.displayedColumns: ", this.displayedColumns);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
    let mailFind = permPageData["elements"].filter(j => j.name == "email" && j.type == "element");
    this.emailLink = mailFind[0].enabled;
    // console.log("what is this.emailLink: ", this.emailLink);
  }

  getscripts(type: string, advert: string, script: string) {
    this.dialog.open(ScriptDialogueComponent, {
      data: {
        type: type,
        advertName: advert,
        script: script
      }
    });
  }



  rejectAdvertModal(adId: number, advert: string) {
    this.rejectAdvertId = adId;
    let dialogRef = this.dialog.open(RejectDialogueComponent, {
      width: '500px',
      data: {
        id: this.rejectAdvertId,
        advertName: advert,
        reason: this.rejectReason
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.updatestatus(result.id, 5, result.reason);
    });
  }

  updatestatus(Id: number, status: number, reason: any) {
    this.successMessage = null;
    this.failMessage = null;
    console.log("What is the submitted status: ", status);
    let newModel: AdvertTableModel;
    for (let i in this.modelSource) {
      if (this.modelSource[i].advertId == Id) {
        this.modelSource[i].prevStatus = this.modelSource[i].status;
        this.modelSource[i].status = status;
        this.modelSource[i].updatedBy = this.userData.userId;
        this.modelSource[i].rejectionReason = reason;
        newModel = this.modelSource[i];
        break;
      }
    }

    console.log("What is the newModel data: ", newModel);
    this.advertService.updateAdvertStatus(newModel)
      .subscribe(resp => {
        let retResultUpdate = resp

        if (retResultUpdate.result == 1) {
          this.successMessage = retResultUpdate.body.toString();
          setTimeout(function () {
            this.loadData();
          }, 5000);

          console.log("returned result retResultUpdate: ", retResultUpdate.result);
        }
        else {
          this.failMessage = retResultUpdate.body.toString();
        }
      });
    console.log("What is status after update: ", this.modelSource);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
