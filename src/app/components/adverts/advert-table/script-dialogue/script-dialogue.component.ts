import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-script-dialogue',
  templateUrl: './script-dialogue.component.html',
  styleUrls: ['./script-dialogue.component.css']
})
export class ScriptDialogueComponent {

  constructor(public dialogRef: MatDialogRef<ScriptDialogueComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
