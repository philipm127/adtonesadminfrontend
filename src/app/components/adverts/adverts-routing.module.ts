import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthguardService } from 'src/app/services/auth/authguard.service';
import { AddeditAdvertcategoryComponent } from './addedit-advertcategory/addedit-advertcategory.component';
import { AdvertCategoryTableComponent } from './addedit-advertcategory/advert-category-table/advert-category-table.component';
import { AdvertTableComponent } from './advert-table/advert-table.component';


const advertRoutes: Routes = [
  { path: 'adverttable', component: AdvertTableComponent, canActivate: [AuthguardService] },
  { path: 'adverttable/:id', component: AdvertTableComponent },
  { path: 'adverttable/:id/:adId', component: AdvertTableComponent },
  { path: 'advertcategorytable', component: AdvertCategoryTableComponent },
  { path: 'advertcategory/:id', component: AddeditAdvertcategoryComponent }
];

@NgModule({
  imports: [RouterModule.forChild(advertRoutes)],
  exports: [RouterModule]
})
export class AdvertRoutingModule { }
