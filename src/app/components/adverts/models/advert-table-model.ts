export interface AdvertTableModel {
  advertId: number;
  advertName: string;
  brand: string;
  // MediaFileLocation: string;
  mediaFile: string;
  // ClientId: number;
  clientName: string;
  scripts: string;
  scriptFileLocation: string;
  userId: number;
  userName: string;
  email: string;
  createdDate: string;
  status: number;
  prevStatus: number;
  rStatus: string;
  rejectionReason: string;
  updatedBy: number;
  smsBody: string;
  emailBody: string;
  salesExec: string;
  sUserId: number;
  // CampaignProfileId:number;
  // OperatorId:number;
  // UploadedToMediaServer:boolean;
}
