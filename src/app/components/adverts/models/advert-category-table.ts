export interface AdvertCategoryTableModel {
  advertCategoryId: number;
  categoryName: string;
  countryId: number;
  countryName: string;
  createdDate: string;
}


export interface CampaignCategoryTableModel {
  campaignCategoryId: number;
  categoryName: string;
  description: string;
  active: boolean;
}
