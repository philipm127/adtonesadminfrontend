export interface AdvertTableModel {
  AdvertId: number;
  AdvertName: string;
  Brand: string;
  // MediaFileLocation: string;
  MediaFile: string;
  // ClientId: number;
  ClientName: string;
  Scripts: string;
  ScriptFileLocation: string;
  UserId: number;
  UserName: string;
  Email: string;
  CreatedDate: string;
  Status: number;
  PrevStatus:number;
  rStatus: string;
  RejectionReason: string;
  UpdatedBy: number;
  SmsBody: string;
  EmailBody: string;
  // CampaignProfileId:number;
  // OperatorId:number;
  // UploadedToMediaServer:boolean;
}
