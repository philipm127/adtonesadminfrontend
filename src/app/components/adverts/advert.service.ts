import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { ReturnResult } from '../../models/return-result';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { IdCollectionModel } from 'src/app/models/IdCollectionViewModel';

@Injectable({
  providedIn: 'root'
})
export class AdvertService {

  myAppUrl: string;
  controller: string = "advert/";

  constructor(private http: HttpClient) {
    this.myAppUrl = (environment.appUrl + this.controller)
  }


  public GetAdvertList(adUserId: string) {
    if (adUserId == null)
      adUserId = "0";
    let ctrAction = "v1/GetAdvertList/";
    let id = parseInt(adUserId);
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public GetAdvertListForSales(id: number) {
    let ctrAction = "v1/GetAdvertListForSales/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public GetAdvertListForAdvertisers(id: number) {
    let ctrAction = "v1/GetAdvertListForAdvertiser/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public GetAdvertListForAdvertisersSummary(id: number) {
    let ctrAction = "v1/GetAdvertListForAdvertiserSummary/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  // Gets a single Advert into list from camapaign screen
  public GetAdvertListById(adId: string) {
    let ctrAction = "v1/GetAdvertListById/";
    let id = parseInt(adId);
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public getAdvertCategoryList() {
    let ctrAction = "v1/GetAdvertCategoryList";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getAdvertCategoryDetails(Id: string) {
    let id = parseInt(Id);
    let ctrAction = "v1/GetAdvertCategoryDetails/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public deleteAdvertCategory(id: number, countryId: number) {
    let model: IdCollectionModel = new IdCollectionModel();
    model.id = id;
    model.countryId = countryId;
    let ctrAction = "v1/DeleteAdvertCategory";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public updateAdvertCategory(model: any) {
    let ctrAction = "v1/UpdateAdvertCategory";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public addAdvertCategory(model: any) {
    let ctrAction = "v1/AddAdvertCategory";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public updateAdvertStatus(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/UpdateAdvertStatus"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public checkIfAdvertNameExists(model: any): Observable<ReturnResult> {

    let ctrAction = "v1/CheckIfAdvertNameExists/";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public createCampaign_Advert(model: any): Observable<ReturnResult> {//, file: File, file2: File): Observable<ReturnResult> {
    console.log("Model Data for Advert CreateAdvert: ", model);
    console.log("advertName Data for Advert CreateAdvert: ", model.advertName);
    let ctrAction = "v1/CreateCampaignAdvert";
    const formData: FormData = new FormData();
    formData.append("campaignProfileId", model.campaignProfileId);
    formData.append("advertName", model.advertName);
    formData.append("advertCategoryId", model.advertCategoryId);
    // formData.append("clientId", null);
    formData.append("advertiserId", model.advertiserId);
    formData.append("brand", model.brand);
    formData.append("operatorId", model.operatorId);
    formData.append("script", model.script);
    formData.append("mediaFile", model.mediaFile);
    formData.append("scriptFile", model.scriptFile);
    // console.log("FormData for Advert CreateAdvert: ", formData);
    // console.log("FormData for Advert CreateAdvert: ", formData.get('advertName'));
    // console.log("FormData for Advert CreateAdvert: ", formData.get('mediaFile'));
    // console.log("FormData for Advert CreateAdvert  CampaignId: ", formData.get('campaignProfileId'));

    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, formData);
  }

  public updateAdvert(model: FormData): Observable<ReturnResult> {
    let ctrAction = "v1/UpdateAdvert";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public getAdvertDetailsByCampaignId(id: number) {
    let ctrAction = "v1/GetAdvertDetailByCampaignId/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public getAdvertDetailsByAdvertId(id: number) {
    let ctrAction = "v1/GetAdvertDetail/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

}
