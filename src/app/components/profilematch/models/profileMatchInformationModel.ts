export interface ProfileMatchInformationFormModel{
    id:number;
    profileName:string;
    isActive:boolean;
    active:string;
    countryId:number;
    countryName:string;
    profileType:string;
    createdDate:string;
    profileMatchLabelFormModels:ProfileMatchLabelFormModel[];
}

export interface ProfileMatchLabelFormModel
{
    id:number;
    profileMatchInformationId:number;
    profileLabel:string;
    createdDate:string;
}

export class MatchLabelModel
{
    id=0;
    profileMatchInformationId=0;
    profileLabel="";
    createdDate="";
}
