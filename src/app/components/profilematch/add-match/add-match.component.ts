import { Component, OnInit } from '@angular/core';
import { ProfileMatchService } from '../profile-match.service';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { ProfileMatchInformationFormModel, MatchLabelModel, ProfileMatchLabelFormModel } from '../models/profileMatchInformationModel';
import { ReturnResult } from 'src/app/models/return-result';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-match',
  templateUrl: './add-match.component.html',
  styleUrls: ['./add-match.component.css']
})
export class AddMatchComponent implements OnInit {
  pagename: string = "AddProfileMatch";
  matchId: string = "0";

  labelArray: FormArray;
  form = new FormGroup({
    id: new FormControl(0),
    profileName: new FormControl("", Validators.required),
    profileType: new FormControl("", Validators.required),
    countryId: new FormControl("", Validators.required),
    isActive: new FormControl(true),
    profileMatchLabelFormModels: this.fb.array([
      this.fb.group({
        id: new FormControl(0),
        profileMatchInformationId: [0],
        profileLabel: ["", Validators.required],
        createdDate: new FormControl({ value: this.datePipe.transform(Date.now(), 'd/M/yyyy'), disabled: true })
      })
    ])
  });


  editMode: boolean = false;
  userModel: ProfileMatchInformationFormModel;
  retResult: ReturnResult;
  countrySelect: BasicSelectModel[];
  typeSelect: BasicSelectModel[];
  destroy$: Subject<BasicSelectModel> = new Subject();

  submitEnabled: boolean = true;
  areaNameVisible: boolean = true;
  countryIdVisible: boolean = true;

  userData: any;

  successMessage: string;
  failMessage: string;

  inputvalue = "";


  constructor(private fb: FormBuilder, private profileService: ProfileMatchService,
    private accessService: AccessServicesService, private listService: SelectServicesService,
    private route: ActivatedRoute, private router: Router, private datePipe: DatePipe) {

    this.matchId = this.route.snapshot.paramMap.get('id');
    if (parseInt(this.matchId) > 0) {
      this.pagename = "EditProfileMatch";
      this.editMode = true;
    }


    this.userData = this.accessService.getUser();
    this.getCountry();
    this.getTypes();

    let permListData = this.accessService.getPermissionList(this.pagename);
    permListData["elements"].forEach(element => {
      if (element.type == "element" && element.name == this.accessService.getName(this.form.get(element.name))) {
        this[element.name.concat('Visible')] = element.visible;
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
      else if (element.type == "button") {
        this.submitEnabled = element.enabled;
      }
    });
  }

  getCountry() {
    this.listService.getCountryList()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        console.log("returned result from get country list: ", this.retResult);
        if (this.retResult.result == 1) {

          this.countrySelect = <BasicSelectModel[]>this.retResult.body;
        }
        else
          this.countrySelect = <BasicSelectModel[]>this.retResult.body;
      });
  }

  getTypes() {
    this.profileService.getProfileTypeList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;

        if (this.retResult.result == 1) {
          //console.log("returned result from get types list: ", this.retResult);
          this.typeSelect = <BasicSelectModel[]>this.retResult.body;
        }
      });
  }

  get labels(): FormArray {
    return this.form.get('profileMatchLabelFormModels') as FormArray;
  }

  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();

    if (this.editMode) {
      this.profileService.getProfileMatchDetails(this.matchId)
        .subscribe((res: HttpResponse<ReturnResult>) => {
          let retModel = res.body;
          if (retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <ProfileMatchInformationFormModel>retModel.body;
            // console.log("returned userModel: ", this.userModel);
            this.initForm();
          }
        });
    }
  }


  private initForm() {
    if (this.editMode) {
      this.form.patchValue({
        id: this.userModel.id,
        profileName: this.userModel.profileName
      });

      this.form.get('countryId').setValue(this.userModel.countryId.toString());
      this.form.get('profileType').setValue(this.userModel.profileType.toString());
      this.form.get('isActive').setValue(this.userModel.isActive.toString());

      this.form.get('countryId').disable();
      this.form.get('profileName').disable();


      this.form.controls['profileMatchLabelFormModels'] = this.fb.array(this.userModel.profileMatchLabelFormModels.map(data => {
        const group = this.fb.group({
          id: [0],
          profileMatchInformationId: [0],
          profileLabel: new FormControl([{ value: '', disabled: true }, Validators.required]),
          createdDate: new FormControl({ value: this.datePipe.transform(Date.now(), 'd/M/yyyy'), disabled: true })
        })
        group.patchValue(data);
        return group;
      })
      )
    }
  }

  onSubmit() {
    let serviceResult = null;
    console.log("this.form.value: ",this.form.getRawValue());
    if (this.editMode)
      serviceResult = this.profileService.updateProfileMatch(this.form.getRawValue());
    else
      serviceResult = this.profileService.addProfileMatch(this.form.getRawValue());

    serviceResult
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          if (this.editMode)
            this.successMessage = "The Details Were Updated Successfully";
          else
            this.successMessage = "The Details Were Added Successfully";
        }
        else {
          this.failMessage = retModel.error;
          ;
        }
      });
  }

  removeElement(i: number) {
    console.log("removeElement(i): ", i);
    let elementsArray = this.form.controls.profileMatchLabelFormModels as FormArray;
    console.log("let elementsArray: ", elementsArray);
    console.log("llet labId: ", elementsArray.controls[i]["controls"].id.value);
    let labId = elementsArray.controls[i]["controls"].id.value;

    if (labId != 0)
      this.profileService.deleteMatchLabel(labId)
      .subscribe(
        data => {
          let retModel = data;
          if (retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            elementsArray.removeAt(i);
          }
          else
          this.failMessage = retModel.error;
        });

  }

  addElement() {
    let elementsArray = this.form.controls.profileMatchLabelFormModels as FormArray;
    let arrayLength = elementsArray.length;
    let newElementGroup: FormGroup = this.fb.group({
      id: 0,
      profileLabel: ['', Validators.required],
      profileMatchInformationId: parseInt(this.matchId),
      createdDate: this.datePipe.transform(Date.now(), 'd/M/yyyy')
    });
    elementsArray.insert(arrayLength, newElementGroup);
  }



  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
