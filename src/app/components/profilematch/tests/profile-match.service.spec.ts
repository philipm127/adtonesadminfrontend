import { TestBed } from '@angular/core/testing';

import { ProfileMatchService } from '../profile-match.service';

describe('ProfileMatchService', () => {
  let service: ProfileMatchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProfileMatchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
