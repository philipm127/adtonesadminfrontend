import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProfilematchTableComponent } from '../profilematch-table/profilematch-table.component';

describe('ProfilematchTableComponent', () => {
  let component: ProfilematchTableComponent;
  let fixture: ComponentFixture<ProfilematchTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilematchTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilematchTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
