import { Component, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';

import { Subject } from 'rxjs';
import { ProfileMatchInformationFormModel } from 'src/app/components/profilematch/models/profileMatchInformationModel';
import { ProfileMatchService } from 'src/app/components/profilematch/profile-match.service';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { TableSearchModel } from '../../shared/table-search-model';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { uniqBy } from 'lodash';


@Component({
  selector: 'app-profilematch-table',
  templateUrl: './profilematch-table.component.html',
  styleUrls: ['./profilematch-table.component.css']
})

export class ProfilematchTableComponent implements AfterViewInit, OnDestroy {
  pagename: string = "ProfileMatchTable";
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];
  userData: any;

  destroy$: Subject<ProfileMatchInformationFormModel> = new Subject();
  dataSource: MatTableDataSource<ProfileMatchInformationFormModel>;
  modelSource: ProfileMatchInformationFormModel[];
  isLoading: boolean = true;
  uniqueProfileData$: any;
  profileSelect: BasicSelectModel[] = [];
  countrySelect: BasicSelectModel[] = [];
  miscName: string = "Name"

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  filterValues = {};
  retResult: ReturnResult = null;

  tableSearchModel: TableSearchModel;


  constructor(private profileService: ProfileMatchService, private datePipe: DatePipe,
    private popSearch: PopSearchDdService, private accessService: AccessServicesService) {
    this.userData = this.accessService.getUser();
    this.PermissionAssignment();


  }

  ngAfterViewInit() {
    this.profileService.getProfileMatchList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <ProfileMatchInformationFormModel[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<ProfileMatchInformationFormModel>(this.modelSource);
          this.isLoading = false;
        }
        this.popProfile();
        this.countrySelect = this.popSearch.popCountry(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          const a = !filter.name || data.profileName.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          const b = !filter.typeName || data.profileType.trim().toLowerCase().includes(filter.typeName.trim().toLowerCase());
          const c = !filter.country || data.countryName.trim().toLowerCase().includes(filter.country.trim().toLowerCase());
          return a && b && c;
        }) as (ProfileMatchElement, string) => boolean;


        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }


  popProfile() {

    this.uniqueProfileData$ = uniqBy(this.modelSource, 'profileType');
    // console.log("What is in status ",this.uniqueProfileData$);

    for (let i = 0; i < this.uniqueProfileData$.length; i++) {
      let tmp = { text: this.uniqueProfileData$[i].profileType, value: this.uniqueProfileData$[i].profileType };
      // console.log("What is in status select ", tmp);
      this.profileSelect.push(tmp);
    }
  }


  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
