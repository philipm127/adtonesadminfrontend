import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfilematchRoutingModule } from './profilematch-routing.module';


import { ProfilematchTableComponent } from './profilematch-table/profilematch-table.component';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatTableExporterModule } from 'mat-table-exporter';
import { SharedModule } from '../shared/shared.module';
import { AddMatchComponent } from './add-match/add-match.component';



@NgModule({
  declarations: [ProfilematchTableComponent, AddMatchComponent],
  imports: [
    CommonModule,
    ProfilematchRoutingModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule,
    SharedModule
  ]
})
export class ProfilematchModule { }
