import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfilematchTableComponent } from './profilematch-table/profilematch-table.component';
import { AddMatchComponent } from './add-match/add-match.component';


const profileMatchRoutes: Routes = [
  {path: 'profilematch', component: ProfilematchTableComponent},
  {path: 'profilematchdetails/:id', component: AddMatchComponent }
];

@NgModule({
  imports: [RouterModule.forChild(profileMatchRoutes)],
  exports: [RouterModule]
})
export class ProfilematchRoutingModule { }
