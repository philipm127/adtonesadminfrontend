import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ReturnResult } from '../../models/return-result';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ProfileMatchService {

  myAppUrl: string;
  controller:string="profilematch/";


  constructor(private http: HttpClient)
  {
      this.myAppUrl = (environment.appUrl+this.controller);
  }
// { headers: this.httpOptions }
  public getProfileMatchList() {
    let ctrAction = "v1/GetProfileMatchData"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getProfileMatchDetails(Id:string) {
    let id = parseInt(Id);
    let ctrAction = "v1/GetProfileMatchInfoById/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public updateProfileMatch(model:any) {
    let ctrAction = "v1/UpdateProfileMatchInfo";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public addProfileMatch(model:any) {
    let ctrAction = "v1/AddProfileMatchInfo";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public getProfileTypeList() {
    let ctrAction = "v1/GetProfileTypeList"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  public deleteMatchLabel(id:number): Observable<ReturnResult> {
    let ctrAction = "v1/DeleteProfileMatchLabel/";
    return this.http.delete<ReturnResult>(this.myAppUrl + ctrAction + id);
  }

  public getUserPreferences(model:any) {
    let ctrAction = "v1/GetUserProfilePreference";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

}
