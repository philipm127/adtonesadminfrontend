import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportDashboardComponent } from './report-dashboard/report-dashboard.component';


const reportRoutes: Routes = [
  { path: 'reportdashboard', component: ReportDashboardComponent }
];

@NgModule({
  imports: [RouterModule.forChild(reportRoutes)],
  exports: [RouterModule]
})
export class ManagementReportsRoutingModule { }
