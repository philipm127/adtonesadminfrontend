import { Component, OnInit } from '@angular/core';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReportDashboardModel } from '../models/report-dashboard-model';
import { ReportTableSearchModel } from '../../shared/table-search-model';
import { ManagementReportsService } from '../management-reports.service';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { ManagementReportSearchModel } from 'src/app/models/PageSortSearchModel';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { saveAs as importedSaveAs } from "file-saver-es";


@Component({
  selector: 'app-report-dashboard',
  templateUrl: './report-dashboard.component.html',
  styleUrls: ['./report-dashboard.component.css']
})
export class ReportDashboardComponent implements OnInit {
  pagename: string = "ReportDashboard";

  sharedModel: ReportTableSearchModel;
  operatorSelect: BasicSelectModel[] = [];
  countrySelect: BasicSelectModel[] = [];
  currencySelect: BasicSelectModel[] = [];
  modelSource: ReportDashboardModel;
  campModelSource: ReportDashboardModel;
  isLoading: boolean = true;
  isCampLoading: boolean = true;
  resultsLength: number;


  filterValues: string;

  destroy$: Subject<BasicSelectModel> = new Subject();

  minDate = new Date(2013, 1, 1);
  maxDate = new Date();


  form: FormGroup;

  constructor(private reportService: ManagementReportsService, private formBuilder: FormBuilder,
    private accessService: AccessServicesService, private listService: SelectServicesService) {
    this.getOperators();
    this.getCountry();
    this.getCurrency();
    this.PermissionAssignment();
  }

  setDate(date: string) {
    // this.date = date;
    return new Date(date.toString()).toUTCString();
  }


  getSearchModel() {
    let x = this.filterValues;
    let fsm = JSON.stringify(x);
    console.log("let fsm = JSON.stringify(x);: ", fsm);
    let fsmStr: string = null;
    let paging = new ManagementReportSearchModel();

    const today = new Date();
    const tomorrow = new Date(today.setDate(today.getDate() + 2));
    const previoustime = new Date(today.setDate(today.getDate() - 2000));

    if (x != null && x != undefined) {
      // if (fsm.indexOf("operator") != -1) {
      //   fsmStr = JSON.stringify(this.filterValues).replace("operator", "operators");
      //   x = JSON.parse(fsmStr);
      //   console.log("what is value of x ", x);
      if (!Array.isArray(x["operators"]))
        if (x["operators"] != null && x["operators"].length > 0)
          paging.operators = x["operators"].toString().split(',').map(Number);
        //}
        else
          paging.operators = [];

      if (fsm.indexOf("dateFrom") != -1 && x["dateFrom"] != null) {
        paging.dateFrom = this.getFormattedTime(x["dateFrom"]);
        console.log("paging.dateFrom: ", paging.dateFrom);
      }
      else
        paging.dateFrom = this.getFormattedTime(previoustime);

      if (fsm.indexOf("dateTo") != -1 && x["dateTo"] != null)
        paging.dateTo = this.getFormattedTime(x["dateTo"]);
      else
        paging.dateTo = this.getFormattedTime(tomorrow);

      if (fsm.indexOf("country") != -1)
        paging.country = parseInt(x["country"]);
      else
        paging.country = 0;

      if (fsm.indexOf("currency") != -1)
        paging.currency = parseInt(x["currency"]);
      else
        paging.currency = 0;
    }

    console.log("what is paging object ", paging);
    return paging;
  }

  loadData() {
    this.isLoading = true;
    this.reportService.getDashboardReportData(this.getSearchModel())
      .subscribe(
        data => {
          this.isLoading = false;
          let retResult = data
          console.log("this.retResult : ", retResult);
          if (retResult.result == 1) {
            this.isLoading = false;
            this.modelSource = <ReportDashboardModel>retResult.body;
            console.log("this.modelSource : ", this.modelSource);
          }
        });

  }


  generateReport() {
    const today = new Date();
    let filename = "Management_Report_" + this.getFormattedTime(today) + ".xlsx";
    var DocFile = filename.slice(0, -5);
    this.reportService.downloadtDashboardReportData(this.getSearchModel())
      .subscribe(response => {
        importedSaveAs(response, filename);
      })
  }

  getFormattedTime(date: Date) {

    const day = date && date.getDate() || -1;
    const dayWithZero = day.toString().length > 1 ? day : '0' + day;
    const month = date && date.getMonth() + 1 || -1;
    const monthWithZero = month.toString().length > 1 ? month : '0' + month;
    const year = date && date.getFullYear() || -1;

    return `${year}-${monthWithZero}-${dayWithZero}`;

  }

  // downloadFile(data: any) {
  //   const blobs = new Blob([data], { type: 'application/octet-stream' });
  //   console.log("downloadFile(data: any) returned excel file in raw return : ",blobs);
  //   const url= window.URL.createObjectURL(blobs);
  //   window.open(url);
  // }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    let searchData = null;
    let tableSearch = new ReportTableSearchModel()
    // Pulls out the data from the page elements that relate to the search bar elements
    searchData = permPageData.elements.filter(j => j.type == "search")

    // has a function on the class that sets the visibility of the filter elements
    tableSearch.setFromJSON(searchData);
    this.sharedModel = tableSearch;
    // console.log("In Permission assignment what is value of permPageData: ",permPageData);
  }


  ngOnInit(): void {
    this.form = this.formBuilder.group({
      operators: new FormControl(""),
      country: new FormControl(""),
      currency: new FormControl(""),
      dateFrom: new FormControl(""),
      dateTo: new FormControl("")
    });

    this.form.valueChanges.subscribe(value => {
      Object.keys(value).forEach(key => value[key] === '' ? delete value[key] : key);
      this.filterValues = { ...value } as string;
      this.loadData();
    });
    this.loadData();
  }

  getOperators() {
    this.listService.getOperatorList(0)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        if (retResult.result == 1) {
          let elect = <BasicSelectModel[]>retResult.body;
          let x = elect.unshift({ text: "All", value: "" });
          this.operatorSelect = elect;
        }
      });
  }


  getCountry() {
    this.listService.getCountryList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        if (retResult.result == 1) {
          let elect = <BasicSelectModel[]>retResult.body;
          this.countrySelect = elect;
        }
      });
  }

  getCurrency() {
    this.listService.getCurrencyList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        if (retResult.result == 1) {
          let elect = <BasicSelectModel[]>retResult.body;
          this.currencySelect = elect;
        }
      });
  }

}
