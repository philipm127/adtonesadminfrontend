import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ReturnResult } from 'src/app/models/return-result';

@Injectable({
  providedIn: 'root'
})
export class ManagementReportsService {
  myAppUrl: string;
  controller: string = "campaignaudit/";

  constructor(private http: HttpClient) {
    this.myAppUrl = (environment.appUrl + this.controller);
  }

  public getDashboardReportData(model: any): Observable<ReturnResult> {
    // console.log("what is value of model: ", model);
    let ctrAction = "v1/GetManagementReport/"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public downloadtDashboardReportData(model: any) {
    let ctrAction = "v1/GenerateManReport/"
    console.log("entered downloadtDashboardReportData with the search model : ", model);
    return this.http.put(this.myAppUrl + ctrAction, model, { responseType: 'blob' });
  }



}
