import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagementReportsRoutingModule } from './management-reports-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatTableExporterModule } from 'mat-table-exporter';
import { ReportDashboardComponent } from './report-dashboard/report-dashboard.component';


@NgModule({
  declarations: [ReportDashboardComponent],
  imports: [
    CommonModule,
    ManagementReportsRoutingModule,
    SharedModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule
  ]
})
export class ManagementReportsModule { }
