// export interface ReportDashboardModel {
//   numOfTotalUser: number;
//   numOfRemovedUser: number;
//   numberOfAdsProvisioned: number;
//   numOfUpdateToAudit: number;
//   numOfCancel: number;
//   numOfLiveCampaign: number;
//   numOfEmail: number;
//   numOfTextFile: number;
//   numOfTextLine: number;
//   numOfSMS: number;
//   numOfPlay: number;
//   numOfPlayUnder6secs: number;
//   averagePlaysPerUser: number;
//   totalCredit: number;
//   totalSpend: number;
//   averagePlayLength: number;
//   currencyCode: string;
// }


export interface ReportDashboardModel {
  totalUsers: number;
  totalListened: number;
  totalRemovedUser: number;
  addedUsers: number;
  numListened: number;
  totalSpend: number;
  totalCredit: number;
  amountSpent: number;
  amountCredit: number;
  amountPayment: number;
  total6Over: number;
  totalUnder6: number;
  num6Over: number;
  numUnder6: number;
  totalAvgPlayLength: number;
  numAvgPlayLength: number;
  totalAvgPlays: number;
  totalAvgPlaysListened: number;
  numAvgPlays: number;
  numAvgPlaysListened: number;
  totalCampaigns: number;
  totalAdverts: number;
  totalCancelled: number;
  numCancelled: number;
  campaignsAdded: number;
  advertsProvisioned: number;
  totalSMS: number;
  totalEmail: number;
  numSMS: number;
  numEmail: number;
  totalRewards: number;
  totRewardUsers: number;
  numRewards: number;
  numRewardUsers: number;
  totAvgRewards: number;
  numAvgRewards: number;
  currencyCode: string;
}
