import { TestBed } from '@angular/core/testing';

import { ManagementReportsService } from './management-reports.service';

describe('ManagementReportsService', () => {
  let service: ManagementReportsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManagementReportsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
