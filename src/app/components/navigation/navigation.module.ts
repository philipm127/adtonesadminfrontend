import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Used to control which menu is displyed dependent upon role.
import { NavigationControlComponent } from './navigation-control.component';

// TODO: replace this with new menu when I work out through trial and error.
// currently is a straight copy of existing.

// Trial and error version of MainNavigationComponent
import { MenuItemComponent } from './navigation-builder/menu-item/menu-item.component';

// This is the dropdown on mouse over menu with its three parts.
// Few things to figure out with it.
// import { NavigationMenuComponent } from './navigation-menu/navigation-menu.component';
// import {NavBarComponent} from './navigation-menu/nav-bar/nav-bar.component';
// import {SubMenuComponent} from './navigation-menu/nav-bar/sub-menu/sub-menu.component';

import { MaterialComponentsModule } from '../../material-components/material-components.module';
import { RouterModule } from '@angular/router';

import { NavigationBuilderComponent } from './navigation-builder/navigation-builder.component';
import { ProfileMenuComponent } from './navigation-builder/profile-menu/profile-menu.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    NavigationControlComponent,
    MenuItemComponent,
    NavigationBuilderComponent,
    ProfileMenuComponent
  ],
  imports: [
    CommonModule,
    MaterialComponentsModule,
    SharedModule,
    RouterModule
  ],
  exports: [
    NavigationControlComponent
  ]
})
export class NavigationModule { }
