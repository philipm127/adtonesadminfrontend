import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/services/auth/token-storage.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-navigation-control',
  templateUrl: './navigation-control.component.html',
  styleUrls: ['./navigation-control.component.css']
})
export class NavigationControlComponent implements OnInit {
  userData: any;
  userId: any;
  // MenuId: 1 is Admin, 2 is Operator, 3 is userAdmin, 4 is advertAdmin
  menuId: number;
  welcomeHeader: string;
  pagename: string;

  //else if (role.toLowerCase().indexOf("operator") !== -1)
  constructor(private tokenStorage: TokenStorageService) { }

  ngOnInit(): void {
    this.userData = this.tokenStorage.getUser();
    this.userId = this.userData.userId;
    let role = this.userData.role.toLowerCase();
    let roleid = this.userData.roleId;
    if (roleid == 1)
      this.welcomeHeader = "Admin";
    else if (roleid == 6)
      this.welcomeHeader = "Operator Admin";
    else if (roleid == 7)
      this.welcomeHeader = "Profile Admin";
    else if (roleid == 8 || roleid == 9)
      this.welcomeHeader = "Sales Management";
    else if (roleid == 5)
      this.welcomeHeader = "Advert Management";
    else if (roleid == 5)
      this.welcomeHeader = "Subscriber Management";
    else if (roleid == 3)
      this.welcomeHeader = "Advertiser Management";

  }
}
