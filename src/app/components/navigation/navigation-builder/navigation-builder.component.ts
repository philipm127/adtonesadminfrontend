import { Component, ViewEncapsulation, OnInit, Output, EventEmitter, Renderer2, Input } from '@angular/core';
import { NavItemModel } from '../models/nav-item-model';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';

@Component({
  selector: 'app-navigation-builder',
  templateUrl: './navigation-builder.component.html',
  styleUrls: ['./navigation-builder.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NavigationBuilderComponent implements OnInit {

  navItems: NavItemModel[] = [];
  pagename: string = "MainMenu";

  @Output()
  menuItem = new EventEmitter();
  enteredButton = false;
  isMatMenuOpen = false;
  isMatMenu2Open = false;
  prevButtonTrigger;

  constructor(private ren: Renderer2, private accessService: AccessServicesService) {

  }

  ngOnInit(): void {
    // console.log("menu item in builder: ", this.pagename);

    let menus = this.accessService.getPermissionList(this.pagename);
    this.navItems = menus["elements"];
    // console.log("this.accessService.getPermissionList(this.pagename);: ", this.navItems);
  }


  // Dropdown menu generation
  menuItemClicked(child: {}) {
    this.menuItem.emit(child);
  }
  // new
  buttonEnter(trigger) {
    // /*
    console.log('trigger ',trigger)
    setTimeout(() => {
      if (this.prevButtonTrigger && this.prevButtonTrigger != trigger) {

        this.prevButtonTrigger.closeMenu();
        this.prevButtonTrigger = trigger;
        this.isMatMenuOpen = false;
        this.isMatMenu2Open = false;
        trigger.openMenu();
        // console.log(1);
      }
      else if (!this.isMatMenuOpen) {

        this.enteredButton = true;
        this.prevButtonTrigger = trigger
        trigger.openMenu()
        // console.log(2);

      }
      else {
        this.enteredButton = true;
        this.prevButtonTrigger = trigger
        // console.log(3);
      }

    })
    // */
    //console.log("enter event");
  }

  buttonLeave(trigger, button) {
    // /*
    setTimeout(() => {
      if (this.enteredButton && !this.isMatMenuOpen) {
        trigger.closeMenu();
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-focused');
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-program-focused');
      } if (!this.isMatMenuOpen) {
        trigger.closeMenu();
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-focused');
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-program-focused');
      } else {
        this.enteredButton = false;
      }
    }, 100)
    // */
    // console.log("leave event");
  }

  menuenter() {
    this.isMatMenuOpen = true;
    if (this.isMatMenu2Open) {
      this.isMatMenu2Open = false;
    }
    // console.log("menuEnter");
  }

  menuLeave(trigger, button) {
    setTimeout(() => {
      if (!this.isMatMenu2Open && !this.enteredButton) {
        this.isMatMenuOpen = false;
        trigger.closeMenu();
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-focused');
        this.ren.removeClass(button['_elementRef'].nativeElement, 'cdk-program-focused');
      } else {
        this.isMatMenuOpen = false;
      }
    }, 80)
    // console.log("menuLeave");
  }

}
