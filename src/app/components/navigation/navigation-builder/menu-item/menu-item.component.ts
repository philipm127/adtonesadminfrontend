import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { NavItemModel } from 'src/app/components/navigation/models/nav-item-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css']
})
export class MenuItemComponent implements OnInit {

  @Input() items: NavItemModel[];
  @ViewChild('childMenu', {static: true}) public childMenu: any;

  @Output()
  mouseenter2 = new EventEmitter();
  @Output()
  mouseleave2 = new EventEmitter();


  constructor(public router: Router) {
  }

  ngOnInit(): void {

  }

}
