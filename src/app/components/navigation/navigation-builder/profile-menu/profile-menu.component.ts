import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profile-menu',
  templateUrl: './profile-menu.component.html',
  styleUrls: ['./profile-menu.component.css']
})
export class ProfileMenuComponent implements OnInit {
  @Input() userId:any;
  loginStr:string="login";
  logoutStr:string="logout";
  profileStr:string="/myprofile/";
  constructor() { }

  ngOnInit(): void {
  }

}
