import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NavigationBuilderComponent } from '../navigation-builder/navigation-builder.component';

describe('NavigationBuilderComponent', () => {
  let component: NavigationBuilderComponent;
  let fixture: ComponentFixture<NavigationBuilderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
