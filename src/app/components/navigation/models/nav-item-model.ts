export interface NavItemModel {
  name: string;
  // displayName: string;
  disabled?: boolean;
  visible?: boolean;
  iconName?: string;
  route?: string;
  elements?: NavItemModel[];
}
