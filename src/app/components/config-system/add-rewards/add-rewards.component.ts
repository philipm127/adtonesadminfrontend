import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { RewardResult } from '../models/reward-table-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { ConfigSystemService } from '../config-system.service';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';

@Component({
  selector: 'app-add-rewards',
  templateUrl: './add-rewards.component.html',
  styleUrls: ['./add-rewards.component.css']
})
export class AddRewardsComponent implements OnInit {
  pagename: string = "AddReward";
  confId: string = "0";

  form = new FormGroup({
    rewardId: new FormControl(0),
    rewardName: new FormControl("", Validators.required),
    rewardValue: new FormControl("", Validators.required),
    operatorId: new FormControl(true, Validators.required)
  });


  editMode: boolean = false;
  userModel: RewardResult;

  operatorSelect: BasicSelectModel[];

  destroy$: Subject<RewardResult> = new Subject();

  submitEnabled: boolean = true;


  userData: any;

  successMessage: string;
  failMessage: string;

  inputvalue = "";


  constructor(private formBuilder: FormBuilder, private configService: ConfigSystemService,
    private accessService: AccessServicesService, private listService: SelectServicesService,
    private route: ActivatedRoute, private router: Router) {

    this.confId = this.route.snapshot.paramMap.get('id');
    if (parseInt(this.confId) > 0) {
      this.pagename = "EditReward";
      this.editMode = true;
    }

    this.userData = this.accessService.getUser();
    this.getOperator();

    let permListData = this.accessService.getPermissionList(this.pagename);
    // console.log("matching value of permissions: ",element.name);
    permListData["elements"].forEach(element => {
      if (element.type == "element" && element.name == this.accessService.getName(this.form.get(element.name))) {
        this[element.name.concat('Visible')] = element.visible;
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
      else if (element.type == "button") {
        // console.log("matching value of submit type: ",element.name);
        this.submitEnabled = element.enabled;
      }
    });
  }

  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();

    if (this.editMode) {
      this.configService.getRewardDetails(this.confId)
        .subscribe((res: HttpResponse<ReturnResult>) => {
          let retModel = res.body;
          if (retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <RewardResult>retModel.body;
            // console.log("returned userModel: ", this.userModel);
            this.initForm();
          }
        });
    }
  }

  getOperator() {
    this.listService.getOperatorList(0)
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        console.log("returned result from get country list: ", retResult);
        if (retResult.result == 1) {

          this.operatorSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }

  private initForm() {
    if (this.editMode) {
      this.form.patchValue({
        rewardId: this.userModel.rewardId,
        rewardName: this.userModel.rewardName,
        rewardValue: this.userModel.rewardValue
      });
      this.form.get('operatorId').setValue(this.userModel.operatorId.toString());
    }
  }

  onSubmit() {
    // this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log("What is this form data: ", this.form.value);
    let serviceResult = null;
    if (this.editMode)
      serviceResult = this.configService.updateReward(this.form.value);
    else
      serviceResult = this.configService.addReward(this.form.value);
    serviceResult
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          this.successMessage = "The Details Were Updated Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the details";
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
