import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemConfigComponent } from './system-config-table/system-config.component';
import { RewardsComponent } from './rewards-table/rewards.component';
import { AddSystemConfigComponent } from './add-system-config/add-system-config.component';
import { AddRewardsComponent } from './add-rewards/add-rewards.component';


const configRoutes: Routes = [
  { path: 'systemconfig', component:SystemConfigComponent },
  { path: 'addsystemconfig/:id', component: AddSystemConfigComponent },
  { path: 'rewards', component: RewardsComponent },
  { path: 'addreward/:id', component: AddRewardsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(configRoutes)],
  exports: [RouterModule]
})
export class ConfigSystemRoutingModule { }
