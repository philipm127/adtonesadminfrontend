import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigSystemRoutingModule } from './config-system-routing.module';
import { SystemConfigComponent } from './system-config-table/system-config.component';
import { RewardsComponent } from './rewards-table/rewards.component';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MatTableExporterModule } from 'mat-table-exporter';
import { SharedModule } from '../shared/shared.module';
import { AddSystemConfigComponent } from './add-system-config/add-system-config.component';
import { AddRewardsComponent } from './add-rewards/add-rewards.component';


@NgModule({
  declarations: [SystemConfigComponent, RewardsComponent, AddSystemConfigComponent, AddRewardsComponent],
  imports: [
    CommonModule,
    ConfigSystemRoutingModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule,
    SharedModule
  ]
})
export class ConfigSystemModule { }
