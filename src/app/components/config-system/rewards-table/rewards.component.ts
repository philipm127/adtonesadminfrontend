import { Component, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import * as _ from 'lodash';
import { ConfigSystemService } from 'src/app/components/config-system/config-system.service';
import { RewardResult } from '../models/reward-table-model';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';

@Component({
  selector: 'app-rewards',
  templateUrl: './rewards.component.html',
  styleUrls: ['./rewards.component.css']
})
export class RewardsComponent implements AfterViewInit, OnDestroy {
  pagename: string = "RewardsList";
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];

  destroy$: Subject<RewardResult> = new Subject();
  dataSource: MatTableDataSource<RewardResult>;
  modelSource: RewardResult[];
  isLoading: boolean = true;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  filterValues = {};
  retResult: ReturnResult = null;
  userData: any;
  operatorSelect: BasicSelectModel[] = [];

  tableSearchModel: TableSearchModel;
  // This is to appear in Search Box of numberFrom/numberTo
  numberName: string = "Credit Limit";

  constructor(private systemService: ConfigSystemService, private popSearch: PopSearchDdService,
    private datePipe: DatePipe, private accessService: AccessServicesService) {
    this.userData = this.accessService.getUser();
    this.PermissionAssignment();

  }

  ngAfterViewInit() {
    this.systemService.getRewardsList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <RewardResult[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<RewardResult>(this.modelSource);
          this.isLoading = false;
        }
        this.operatorSelect = this.popSearch.popOperator(this.modelSource);
        this.dataSource.filterPredicate = ((data, filter) => {
          const a = !filter.name || data.rewardName.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          const b = !filter.operator || data.operatorName.trim().toLowerCase().includes(filter.operator.trim().toLowerCase());
          return a && b;
        }) as (RewardElement, string) => boolean;

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }


  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }

  DeleteReward(id: number) {
    this.systemService.deleteReward(id)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResultUpdate = res.body;

        if (retResultUpdate.result == 1) {
          for (let i in this.modelSource) {
            if (this.modelSource[i].rewardId == id) {
              this.modelSource.splice(parseInt(i), 1);
              break;
            }
          }
          this.dataSource = new MatTableDataSource<RewardResult>(this.modelSource);
        }
      });
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
