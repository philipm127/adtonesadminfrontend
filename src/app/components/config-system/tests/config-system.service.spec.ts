import { TestBed } from '@angular/core/testing';

import { ConfigSystemService } from '../config-system.service';

describe('ConfigSystemService', () => {
  let service: ConfigSystemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigSystemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
