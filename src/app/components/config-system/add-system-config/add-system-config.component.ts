import { Component, OnInit } from '@angular/core';
import { ConfigSystemService } from '../config-system.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { OperatorConfigurationResult } from '../../operator/models/operator-result-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { Subject } from 'rxjs';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { SelectServicesService } from '../../shared/services/select-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { ReturnResult } from 'src/app/models/return-result';
import { SystemConfigResult } from '../models/system-config-table';

@Component({
  selector: 'app-add-system-config',
  templateUrl: './add-system-config.component.html',
  styleUrls: ['./add-system-config.component.css']
})
export class AddSystemConfigComponent implements OnInit {
  pagename: string = "AddSystemConfig";
  confId: string = "0";

  form = new FormGroup({
    systemConfigId: new FormControl(0),
    systemConfigKey: new FormControl("", Validators.required),
    systemConfigValue: new FormControl("", Validators.required),
    systemConfigType: new FormControl(true, Validators.required)
  });


  editMode: boolean = false;
  userModel: SystemConfigResult;

  configSelect: BasicSelectModel[];

  destroy$: Subject<OperatorConfigurationResult> = new Subject();

  submitEnabled: boolean = true;

  userData: any;

  successMessage: string;
  failMessage: string;

  inputvalue = "";

  constructor(private formBuilder: FormBuilder, private configService: ConfigSystemService,
    private accessService: AccessServicesService, private listService: SelectServicesService,
    private route: ActivatedRoute, private router: Router) {

    this.confId = this.route.snapshot.paramMap.get('id');
    if (parseInt(this.confId) > 0) {
      this.pagename = "EditSystemConfig";
      this.editMode = true;
    }

    this.userData = this.accessService.getUser();
    this.getTypes();

    let permListData = this.accessService.getPermissionList(this.pagename);
    permListData["elements"].forEach(element => {
      if (element.type == "element" && element.name == this.accessService.getName(this.form.get(element.name))) {
        this[element.name.concat('Visible')] = element.visible;
        if (element.enabled == false)
          this.form.get(element.name).disable();
      }
      else if (element.type == "button") {
        this.submitEnabled = element.enabled;
      }
    });
  }

  ngOnInit() {
    let permListData = this.accessService.getPermissionList(this.pagename);
    this.accessService.refreshToken();

    if (this.editMode) {
      this.configService.getSystemConfigDetails(this.confId)
        .subscribe((res: HttpResponse<ReturnResult>) => {
          let retModel = res.body;
          if (retModel.result == 1) {
            // console.log("returned result: ", this.retModel.result);
            this.userModel = <SystemConfigResult>retModel.body;
            // console.log("returned userModel: ", this.userModel);
            this.initForm();
          }
        });
    }
  }

  getTypes() {
    this.configService.getConfigTypes()
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        console.log("returned result from get country list: ", retResult);
        if (retResult.result == 1) {
          this.configSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }

  private initForm() {
    if (this.editMode) {
      this.form.patchValue({
        systemConfigId: this.userModel.systemConfigId,
        systemConfigKey: this.userModel.systemConfigKey,
        systemConfigValue: this.userModel.systemConfigValue
      });
      this.form.get('systemConfigType').setValue(this.userModel.systemConfigType.toString());
      this.form.get('systemConfigKey').disable();
    }
  }

  onSubmit() {
    this.successMessage = "";
    this.failMessage = "";
    // this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log("What is this form data: ", this.form.value);
    let serviceResult = null;
    if (this.editMode)
      serviceResult = this.configService.updateSystemConfig(this.form.getRawValue());
    else
      serviceResult = this.configService.addSystemConfig(this.form.getRawValue());
    serviceResult
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          this.successMessage = "The Details Were Updated Successfully";
        }
        else {
          this.failMessage = "There was a problem updating the details";
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
