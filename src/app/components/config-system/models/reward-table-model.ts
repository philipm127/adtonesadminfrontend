export interface RewardResult {
  rewardId: number;
  rewardName: string;
  rewardValue: number;
  createdDate: string;
  operatorId: number;
  operatorName: string;
}
