export interface SystemConfigResult {
  systemConfigId: number;
  systemConfigKey: string;
  systemConfigValue: string;
  systemConfigType: string;
  createdDate: string;

}
