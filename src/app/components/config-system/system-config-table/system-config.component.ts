import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ReturnResult } from 'src/app/models/return-result';
import { DatePipe } from '@angular/common';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ConfigSystemService } from 'src/app/components/config-system/config-system.service';
import { SystemConfigResult } from '../models/system-config-table';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';

@Component({
  selector: 'app-system-config',
  templateUrl: './system-config.component.html',
  styleUrls: ['./system-config.component.css']
})
export class SystemConfigComponent implements OnDestroy {
  pagename: string = "SystemConfigList";
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];
  userData: any;
  destroy$: Subject<SystemConfigResult> = new Subject();
  dataSource: MatTableDataSource<SystemConfigResult>;
  modelSource: SystemConfigResult[];
  isLoading: boolean = true;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  filterValues = {};
  retResult: ReturnResult = null;


  tableSearchModel: TableSearchModel;

  fullName: string = "Name";
  miscName: string = "Value";

  constructor(private popSearch: PopSearchDdService, private accessService: AccessServicesService, private systemService: ConfigSystemService, private datePipe: DatePipe) {

    this.userData = this.accessService.getUser();
    this.PermissionAssignment();
    this.systemService.getSystemConfigList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <SystemConfigResult[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<SystemConfigResult>(this.modelSource);
          this.isLoading = false;
        }

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("What is in filters: ",filter);
          // Object.keys(filter).forEach(key => console.log("What is key value: ",this.datePipe.transform(filter[key], 'yyyy-MM-dd')));
          const a = !filter.fullName || data.systemConfigKey.trim().toLowerCase().includes(filter.fullName.trim().toLowerCase());
          const c = !filter.dateFrom || data.createdDate >= this.datePipe.transform(filter.dateFrom, 'yyyy-MM-dd');
          const d = !filter.dateTo || data.createdDate <= this.datePipe.transform(filter.dateTo, 'yyyy-MM-dd');
          const b = !filter.name || data.systemConfigValue.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          return a && b && c && d;
        }) as (SystemConfigElement, string) => boolean;


        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);

  }

  DeleteConfig(id: number) {
    this.systemService.deleteConfig(id)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResultUpdate = res.body;

        if (retResultUpdate.result == 1) {
          for (let i in this.modelSource) {
            if (this.modelSource[i].systemConfigId == id) {
              this.modelSource.splice(parseInt(i), 1);
              break;
            }
          }
          this.dataSource = new MatTableDataSource<SystemConfigResult>(this.modelSource);
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}

