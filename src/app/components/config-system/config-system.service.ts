import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ReturnResult } from '../../models/return-result';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ConfigSystemService {

  myAppUrl: string;
  controller: string = "systemconfig/";
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    })
  };

  constructor(private http: HttpClient) {
    this.myAppUrl = (environment.appUrl + this.controller);
  }

  public getSystemConfigList() {
    let ctrAction = "v1/LoadSystemConfigurationDataTable"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  public getRewardsList() {
    let ctrAction = "v1/LoadRewardsDataTable"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  public getSystemConfigDetails(Id: string) {
    let id = parseInt(Id);
    let ctrAction = "v1/GetSystemConfig/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public getConfigTypes() {
    let ctrAction = "v1/GetConfigTypeList"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }

  public updateSystemConfig(model: any) {
    let ctrAction = "v1/UpdateSystemConfig";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public addSystemConfig(model: any) {
    let ctrAction = "v1/AddSystemConfig";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }

  public deleteConfig(id: number) {
    let ctrAction = "v1/DeleteSystemConfig/";
    return this.http.delete<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public getRewardDetails(Id: string) {
    let id = parseInt(Id);
    let ctrAction = "v1/GetReward/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public updateReward(model: any) {
    console.log("What is updatereward model: ",model);
    let ctrAction = "v1/UpdateReward";
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public addReward(model: any) {
    console.log("What is addreward model: ",model);
    let ctrAction = "v1/AddReward";
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public deleteReward(id: number) {
    let ctrAction = "v1/DeleteReward/";
    return this.http.delete<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

}
