import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';
import { AllocationListModel } from '../models/AllocationListModel';
import { SalesManagementService } from '../sales-management.service';

@Component({
  selector: 'app-allocate-advertisers',
  templateUrl: './allocate-advertisers.component.html',
  styleUrls: ['./allocate-advertisers.component.css']
})

export class AllocateAdvertisersComponent implements OnInit {
  pagename: string = "ManageSalesAllocations"
  destroy$: Subject<AllocationListModel> = new Subject();
  userSelect: BasicSelectModel[];
  user1: number;
  user2: number;

  unallocatedList: AllocationListModel[];
  allocatedList1: AllocationListModel[];
  allocatedList2: AllocationListModel[];

  successMessage: string;
  failMessage: string;


  form = new FormGroup({
    id: new FormControl(0),
    userId: new FormControl(0)
  });

  constructor(private salesService: SalesManagementService) {
    this.getUsers();
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    console.log("How does unallocated look: ", this.unallocatedList);
  }

  ngOnInit(): void {
    this.loadUnallocated();
  }

  loadUnallocated() {
    this.salesService.getAllocationList(0)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        // console.log("Returned body is: ", retResult);
        if (retResult.result == 1) {
          this.unallocatedList = <AllocationListModel[]>retResult.body;
        }
      })
  }

  getUsers() {
    this.salesService.getSalesExecDDList()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;

        if (retResult.result == 1) {
          this.userSelect = <BasicSelectModel[]>retResult.body;
        }
      });
  }


  getUserPermission2(event: any) {
    this.successMessage = "";
    this.failMessage = "";
    this.user2 = event.value;
    this.salesService.getAllocationList(this.user2)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        // console.log("Returned body is: ", retResult);
        if (retResult.result == 1) {
          this.allocatedList2 = <AllocationListModel[]>retResult.body;
        }
      })
  }


  getUserPermission(event: any) {
    this.successMessage = "";
    this.failMessage = "";
    this.user1 = event.value;
    this.salesService.getAllocationList(this.user1)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        // console.log("Returned body is: ", retResult);
        if (retResult.result == 1) {
          this.allocatedList1 = <AllocationListModel[]>retResult.body;
        }
      })
  }

  onSubmit() {
    this.successMessage = "";
    this.failMessage = "";
    if (this.user1 == this.user2)
      this.failMessage = "You must select different Sales Execs"
    else {
      let newModel = {
        user1: this.user1,
        user2: this.user2,
        user1array: this.allocatedList1,
        user2array: this.allocatedList2
      }

      this.salesService.updateAllocationInfo(newModel)
        .subscribe(resp => {
          let retModel = resp

          if (retModel.result == 1) {
            this.successMessage = "The Details Were Updated Successfully";
            this.unallocatedList = null;
            this.loadUnallocated();
          }
          else {
            this.failMessage = "There was a problem updating the details";
          }
        });
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
