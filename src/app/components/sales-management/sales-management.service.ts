import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ReturnResult } from 'src/app/models/return-result';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SalesManagementService {

  myAppUrl: string;
  controller: string = "usermanagement/";

  constructor(private http: HttpClient) {
    this.myAppUrl = (environment.appUrl + this.controller);
  }

  // Advertisers List of Users
  public getAllocationList(id: number) {
    let ctrAction = "v1/GetAllocatedUnallocated/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }

  public getSalesExecDDList() {
    let ctrAction = "v1/GetSalesExecDDList"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public getSalesExecList() {
    let ctrAction = "v1/GetSalesExecDataList"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }



  public getSalesExecForAdminList() {
    let ctrAction = "v1/GetSalesExecForAdminDataList"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction, { observe: "response" });
  }


  public updateAllocationInfo(model: any): Observable<ReturnResult> {
    let ctrAction = "v1/UpdateAllocationInfo"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public getAdvertiserDashListForSalesExec(id: number) {
    let ctrAction = "v1/GetAdvertiserDataListForSales/"
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + id, { observe: "response" });
  }
}
