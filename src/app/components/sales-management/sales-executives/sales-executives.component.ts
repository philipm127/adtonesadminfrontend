import { DatePipe } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { PopSearchDdService } from '../../shared/services/pop-search-dd.service';
import { TableSearchModel } from '../../shared/table-search-model';
import { AdvertiserUser } from '../../usermanagement/models/users-applied-models';
import { UmanagementService } from '../../usermanagement/umanagement.service';
import { SalesManagementService } from '../sales-management.service';

@Component({
  selector: 'app-sales-executives',
  templateUrl: './sales-executives.component.html',
  styleUrls: ['./sales-executives.component.css']
})
export class SalesExecutivesComponent implements AfterViewInit, OnDestroy {
  pagename: string = "SalesExecList";
  miscName: string = "Sales Exec";
  // used for search by name
  fullName: string = "Sales Exec";
  emailLink: boolean = false;
  ticketLink: boolean = false;
  creditLink: boolean = false;
  advertLink: boolean = false;
  campLink: boolean = false;
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];

  destroy$: Subject<AdvertiserUser> = new Subject();
  dataSource: MatTableDataSource<AdvertiserUser>;
  modelSource: any[];
  isLoading: boolean = true;

  userModel: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  statusSelect: BasicSelectModel[] = [];
  countrySelect: BasicSelectModel[] = [];

  tableSearchModel: TableSearchModel;

  constructor(private userService: UmanagementService, private salesService: SalesManagementService, private datePipe: DatePipe,
    private popService: PopSearchDdService, private accessService: AccessServicesService) {
    this.userModel = this.accessService.getUser();
    this.PermissionAssignment();
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  ngAfterViewInit() {
    let execList: any;
    if (this.userModel.role == "Admin")
      execList = this.salesService.getSalesExecForAdminList();
    else
      execList = this.salesService.getSalesExecList();
    execList
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.isLoading = false;
        let retResult = res.body;
        // console.log("Returned body is: ", retResult);
        if (retResult.result == 1) {
          this.modelSource = <AdvertiserUser[]>retResult.body;
          this.dataSource = new MatTableDataSource<AdvertiserUser>(this.modelSource);
        }
        this.statusSelect = this.popService.popStatus(this.modelSource);
        this.countrySelect = this.popService.popCountry(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("What is in filters: ",filter);
          const a = !filter.fullName || data.fullName.trim().toLowerCase().includes(filter.fullName.trim().toLowerCase());
          const b = !filter.email || data.email.trim().toLowerCase().includes(filter.email.trim().toLowerCase());
          const c = !filter.status || data.rStatus.trim().toLowerCase().includes(filter.status.trim().toLowerCase());
          const d = !filter.dateFrom || data.dateCreated >= this.datePipe.transform(filter.dateFrom, 'yyyy-MM-dd');
          const e = !filter.dateTo || data.dateCreated <= this.datePipe.transform(filter.dateTo, 'yyyy-MM-dd');
          const f = !filter.country || data.countryName.trim().toLowerCase().includes(filter.country.trim().toLowerCase());
          const g = !filter.name || data.salesExec.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          return a && b && c && d && e && f && g;
        }) as (AdvertiserElement, string) => boolean;

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
    let mailFind = permPageData["elements"].filter(j => j.name == "email" && j.type == "element");
    this.emailLink = mailFind[0].enabled;
    this.ticketLink = this.campLink = this.advertLink = this.emailLink;
  }

  updatestatus(Id: number, status: number) {
    let newModel: AdvertiserUser;
    for (let i in this.modelSource) {
      if (this.modelSource[i].userId == Id) {
        this.modelSource[i].activated = status;
        newModel = this.modelSource[i];
        // console.log("What is userData for this id: ",this.modelSource[i]);
        break;
      }
    }

    this.userService.updateUserStatus(newModel)
      .subscribe(resp => {
        let retResultUpdate = resp

        if (retResultUpdate.result == 1) {
          // console.log("returned result retResultUpdate: ", this.retResultUpdate.result);
        }
      });
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
