import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SalesCampaignDashboardComponent } from '../campaign/sales-campaign-dashboard/sales-campaign-dashboard.component';

describe('SalesCampaignDashboardComponent', () => {
  let component: SalesCampaignDashboardComponent;
  let fixture: ComponentFixture<SalesCampaignDashboardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesCampaignDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesCampaignDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
