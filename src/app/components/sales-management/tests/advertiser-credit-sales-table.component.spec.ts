import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdvertiserCreditSalesTableComponent } from '../finances/advertiser-credit-sales-table/advertiser-credit-sales-table.component';

describe('AdvertiserCreditSalesTableComponent', () => {
  let component: AdvertiserCreditSalesTableComponent;
  let fixture: ComponentFixture<AdvertiserCreditSalesTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertiserCreditSalesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserCreditSalesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
