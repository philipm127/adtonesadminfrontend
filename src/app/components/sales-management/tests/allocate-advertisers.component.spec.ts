import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AllocateAdvertisersComponent } from '../allocate-advertisers/allocate-advertisers.component';

describe('AllocateAdvertisersComponent', () => {
  let component: AllocateAdvertisersComponent;
  let fixture: ComponentFixture<AllocateAdvertisersComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AllocateAdvertisersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocateAdvertisersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
