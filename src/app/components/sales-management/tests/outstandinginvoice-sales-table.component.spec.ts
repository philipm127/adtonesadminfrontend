import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OutstandinginvoiceSalesTableComponent } from '../finances/outstandinginvoice-sales-table/outstandinginvoice-sales-table.component';

describe('OutstandinginvoiceSalesTableComponent', () => {
  let component: OutstandinginvoiceSalesTableComponent;
  let fixture: ComponentFixture<OutstandinginvoiceSalesTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OutstandinginvoiceSalesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutstandinginvoiceSalesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
