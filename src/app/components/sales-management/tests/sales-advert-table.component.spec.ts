import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SalesAdvertTableComponent } from '../adverts/sales-advert-table/sales-advert-table.component';

describe('SalesAdvertTableComponent', () => {
  let component: SalesAdvertTableComponent;
  let fixture: ComponentFixture<SalesAdvertTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesAdvertTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesAdvertTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
