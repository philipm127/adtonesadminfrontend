import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InvoiceSalesTableComponent } from '../finances/invoice-sales-table/invoice-sales-table.component';

describe('InvoiceSalesTableComponent', () => {
  let component: InvoiceSalesTableComponent;
  let fixture: ComponentFixture<InvoiceSalesTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceSalesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceSalesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
