import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdvertiserTableComponent } from '../advertiser-table/advertiser-table.component';

describe('AdvertiserTableComponent', () => {
  let component: AdvertiserTableComponent;
  let fixture: ComponentFixture<AdvertiserTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertiserTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertiserTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
