import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SalesCampaignAuditDashboardComponent } from '../campaign/sales-campaign-audit-dashboard/sales-campaign-audit-dashboard.component';

describe('SalesCampaignAuditDashboardComponent', () => {
  let component: SalesCampaignAuditDashboardComponent;
  let fixture: ComponentFixture<SalesCampaignAuditDashboardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesCampaignAuditDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesCampaignAuditDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
