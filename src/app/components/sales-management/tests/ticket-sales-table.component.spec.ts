import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TicketSalesTableComponent } from '../tickets/ticket-sales-table/ticket-sales-table.component';

describe('TicketSalesTableComponent', () => {
  let component: TicketSalesTableComponent;
  let fixture: ComponentFixture<TicketSalesTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketSalesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketSalesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
