import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SalesCampaignTableComponent } from '../campaign/sales-campaign-table/sales-campaign-table.component';

describe('SalesCampaignTableComponent', () => {
  let component: SalesCampaignTableComponent;
  let fixture: ComponentFixture<SalesCampaignTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesCampaignTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesCampaignTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
