import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SalesExecutivesComponent } from '../sales-executives/sales-executives.component';

describe('SalesExecutivesComponent', () => {
  let component: SalesExecutivesComponent;
  let fixture: ComponentFixture<SalesExecutivesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesExecutivesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesExecutivesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
