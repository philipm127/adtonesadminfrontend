import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthguardService } from 'src/app/services/auth/authguard.service';
import { AdvertiserTableComponent } from './advertiser-table/advertiser-table.component';
import { SalesAdvertTableComponent } from './adverts/sales-advert-table/sales-advert-table.component';
import { AllocateAdvertisersComponent } from './allocate-advertisers/allocate-advertisers.component';
import { AdvertiserCreditSalesTableComponent } from './finances/advertiser-credit-sales-table/advertiser-credit-sales-table.component';
import { InvoiceSalesTableComponent } from './finances/invoice-sales-table/invoice-sales-table.component';
import { OutstandinginvoiceSalesTableComponent } from './finances/outstandinginvoice-sales-table/outstandinginvoice-sales-table.component';
import { SalesExecutivesComponent } from './sales-executives/sales-executives.component';
import { TicketSalesTableComponent } from './tickets/ticket-sales-table/ticket-sales-table.component';


const routes: Routes = [
  { path: 'manageallocation', component: AllocateAdvertisersComponent },
  { path: 'salesexec', component: SalesExecutivesComponent },
  { path: 'advertisersforsales', component: AdvertiserTableComponent },
  { path: 'advertisersforsales/:id/:name', component: AdvertiserTableComponent },
  { path: 'salesexec/advertisersforsales/:id/:name', component: AdvertiserTableComponent },
  // { path: 'salesexec/salescampaign/:id/:name/:type', component: SalesCampaignDashboardComponent },
  // { path: 'salescampaign/:id/:name/:type', component: SalesCampaignDashboardComponent },
  // { path: 'salescampaign', component: SalesCampaignDashboardComponent },
  { path: 'salesexec/salesadverttable/:id/:name', component: SalesAdvertTableComponent },
  { path: 'salesadverttable/:id/:name', component: SalesAdvertTableComponent },
  { path: 'salesadverttable', component: SalesAdvertTableComponent },
  { path: 'salestickettable/:id/:name', component: TicketSalesTableComponent },
  { path: 'salestickettable', component: TicketSalesTableComponent },
  { path: 'salesadvertisercredit/:id/:name', component: AdvertiserCreditSalesTableComponent },
  { path: 'salesadvertisercredit', component: AdvertiserCreditSalesTableComponent },
  { path: 'salesinvoicelist/:id/:name', component: InvoiceSalesTableComponent },
  { path: 'salesinvoicelist', component: InvoiceSalesTableComponent },
  { path: 'outstandinginvoicesaleslist/:id/:name', component: OutstandinginvoiceSalesTableComponent },
  { path: 'outstandinginvoicesaleslist', component: OutstandinginvoiceSalesTableComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesManagementRoutingModule { }
