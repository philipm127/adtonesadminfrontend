import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesManagementRoutingModule } from './sales-management-routing.module';
import { AllocateAdvertisersComponent } from './allocate-advertisers/allocate-advertisers.component';
import { SharedModule } from '../shared/shared.module';
import { MaterialComponentsModule } from 'src/app/material-components/material-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SalesExecutivesComponent } from './sales-executives/sales-executives.component';
import { RouterModule } from '@angular/router';
import { MatTableExporterModule } from 'mat-table-exporter';
import { AdvertiserTableComponent } from './advertiser-table/advertiser-table.component';
import { SalesAdvertTableComponent } from './adverts/sales-advert-table/sales-advert-table.component';
import { TicketSalesTableComponent } from './tickets/ticket-sales-table/ticket-sales-table.component';
import { InvoiceSalesTableComponent } from './finances/invoice-sales-table/invoice-sales-table.component';
import { OutstandinginvoiceSalesTableComponent } from './finances/outstandinginvoice-sales-table/outstandinginvoice-sales-table.component';
import { AdvertiserCreditSalesTableComponent } from './finances/advertiser-credit-sales-table/advertiser-credit-sales-table.component';


@NgModule({
  declarations: [
    AllocateAdvertisersComponent,
    SalesExecutivesComponent,
    AdvertiserTableComponent,
    SalesAdvertTableComponent,
    TicketSalesTableComponent,
    InvoiceSalesTableComponent,
    OutstandinginvoiceSalesTableComponent,
    AdvertiserCreditSalesTableComponent
  ],
  imports: [
    CommonModule,
    SalesManagementRoutingModule,
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTableExporterModule,
    SharedModule
  ]
})
export class SalesManagementModule { }


