import { DatePipe } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FinancialService } from 'src/app/components/financials/financial.service';
import { OutstandingInvoiceResult } from 'src/app/components/financials/models/invoices-table-model';
import { PdfModalComponent } from 'src/app/components/shared/pdf-modal/pdf-modal.component';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { SelectServicesService } from 'src/app/components/shared/services/select-services.service';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { CurrencyEnum } from 'src/app/models/currencyEnum';
import { ReturnResult } from 'src/app/models/return-result';

@Component({
  selector: 'app-outstandinginvoice-sales-table',
  templateUrl: './outstandinginvoice-sales-table.component.html',
  styleUrls: ['./outstandinginvoice-sales-table.component.css']
})
export class OutstandinginvoiceSalesTableComponent implements AfterViewInit {
  pagename: string = "OutstandingInvoicesTable";
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = []
  userData: any;
  salesId: number = 0;
  salesExecName: string;
  // Sent to set filter names.
  createdDateName: string = "Invoice Date";
  numberName: string = "Paid Amount";
  miscName: string = "Invoice Number";
  fullName: string = "Advertiser";

  destroy$: Subject<OutstandingInvoiceResult> = new Subject();
  dataSource: MatTableDataSource<OutstandingInvoiceResult>;
  modelSource: OutstandingInvoiceResult[];
  isLoading: boolean = true;

  statusSelect: BasicSelectModel[] = [];
  clientSelect: BasicSelectModel[] = [];
  paymentSelect: BasicSelectModel[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  tableSearchModel: TableSearchModel;
  currencySymbol = CurrencyEnum;


  constructor(private financialService: FinancialService, private datePipe: DatePipe,
    private popSearch: PopSearchDdService, private accessService: AccessServicesService, private route: ActivatedRoute,
    private pdfService: SelectServicesService, private matDialog: MatDialog) {

    this.userData = this.accessService.getUser();
    let sid = this.route.snapshot.paramMap.get('id');
    this.salesId = sid == null ? 0 : parseInt(this.route.snapshot.paramMap.get('id'));
    if (this.userData.roleId == 9)
      this.salesId = this.userData.userId;
    this.salesExecName = this.route.snapshot.paramMap.get('name');
    this.PermissionAssignment();

  }

  ngAfterViewInit() {
    this.financialService.getOutstandingInvoiceListForsalesExec(this.salesId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        let retResult = res.body;
        // console.log("Returned body is: ", retResult);
        if (retResult.result == 1) {
          this.isLoading = false;
          this.modelSource = <OutstandingInvoiceResult[]>retResult.body;
          this.dataSource = new MatTableDataSource<OutstandingInvoiceResult>(this.modelSource);
        }
        this.statusSelect = this.popSearch.popStatus(this.modelSource);
        this.clientSelect = this.popSearch.popClient(this.modelSource);
        this.paymentSelect = this.popSearch.popPayment(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("What is in filters: ",filter);
          // Object.keys(filter).forEach(key => console.log("What is key value: ",this.datePipe.transform(filter[key], 'yyyy-MM-dd')));
          const a = !filter.fullName || data.fullName.trim().toLowerCase().includes(filter.fullName.trim().toLowerCase());
          const b = !filter.client || data.clientName.trim().toLowerCase().includes(filter.client.trim().toLowerCase());
          const c = !filter.status || data.status.includes(filter.status);
          const d = !filter.dateFrom || data.createdDate >= this.datePipe.transform(filter.dateFrom, 'yyyy-MM-dd');
          const e = !filter.dateTo || data.createdDate <= this.datePipe.transform(filter.dateTo, 'yyyy-MM-dd');
          const f = !filter.name || data.invoiceNumber.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          const j = !filter.numberFrom || data.paidAmount >= parseInt(filter.numberFrom);
          const k = !filter.numberTo || data.paidAmount <= parseInt(filter.numberTo);
          const l = !filter.salesExec || data.salesExec.trim().toLowerCase().includes(filter.salesExec.trim().toLowerCase());
          return a && b && c && d && e && f && j && k && l;
        }) as (OutstandingInvoiceElement, string) => boolean;


        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }

  getsPDFFile(script: string) {
    this.pdfService.downloadPDF(script).subscribe((res: any) => {
      var file = new Blob([res], { type: 'application/pdf' });
      var fileURL = URL.createObjectURL(file);
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = fileURL;
      dialogConfig.height = "550";
      dialogConfig.width = "650";
      let dialogRef = this.matDialog.open(PdfModalComponent, dialogConfig);
    });
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
