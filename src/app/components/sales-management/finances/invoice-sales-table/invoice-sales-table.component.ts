import { DatePipe } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FinancialService } from 'src/app/components/financials/financial.service';
import { InvoiceResult } from 'src/app/components/financials/models/invoices-table-model';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { CurrencyEnum } from 'src/app/models/currencyEnum';
import { IdCollectionModel } from 'src/app/models/IdCollectionViewModel';
import { ReturnResult } from 'src/app/models/return-result';

@Component({
  selector: 'app-invoice-sales-table',
  templateUrl: './invoice-sales-table.component.html',
  styleUrls: ['./invoice-sales-table.component.css']
})
export class InvoiceSalesTableComponent implements AfterViewInit, OnDestroy {
  pagename: string = "InvoicesTable";
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = []
  userData: any;
  salesId: number = 0;
  salesExecName: string;
  // Sent to set filter names.
  createdDateName: string = "Invoice Date";
  responseName: string = "Settled Date";
  numberName: string = "Invoice Total";
  miscName: string = "Invoice Number";

  destroy$: Subject<InvoiceResult> = new Subject();
  dataSource: MatTableDataSource<InvoiceResult>;
  modelSource: InvoiceResult[];
  isLoading: boolean = true;

  statusSelect: BasicSelectModel[] = [];
  clientSelect: BasicSelectModel[] = [];
  paymentSelect: BasicSelectModel[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  retResult: ReturnResult = null;

  tableSearchModel: TableSearchModel;

  currencySymbol = CurrencyEnum;


  constructor(private financialService: FinancialService, private datePipe: DatePipe, private popSearch: PopSearchDdService,
    private accessService: AccessServicesService, private route: ActivatedRoute) {

    this.userData = this.accessService.getUser();
    let sid = this.route.snapshot.paramMap.get('id');
    this.salesId = sid == null ? 0 : parseInt(this.route.snapshot.paramMap.get('id'));
    if (this.userData.roleId == 9)
      this.salesId = this.userData.userId;
    this.salesExecName = this.route.snapshot.paramMap.get('name');
    this.PermissionAssignment();

  }

  ngAfterViewInit() {
    this.financialService.getInvoiceListForSalesExec(this.salesId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <InvoiceResult[]>this.retResult.body;
          this.isLoading = false;
          this.dataSource = new MatTableDataSource<InvoiceResult>(this.modelSource);
        }
        this.statusSelect = this.popSearch.popStatus(this.modelSource);
        this.clientSelect = this.popSearch.popClient(this.modelSource);
        this.paymentSelect = this.popSearch.popPayment(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("What is in filters: ",filter);
          const a = !filter.fullName || data.userName.trim().toLowerCase().includes(filter.fullName.trim().toLowerCase());
          const b = !filter.client || data.clientName.trim().toLowerCase().includes(filter.client.trim().toLowerCase());
          const c = !filter.status || data.rStatus.trim().toLowerCase().includes(filter.status.trim().toLowerCase());
          const d = !filter.dateFrom || data.createdDate >= this.datePipe.transform(filter.dateFrom, 'yyyy-MM-dd');
          const e = !filter.dateTo || data.createdDate <= this.datePipe.transform(filter.dateTo, 'yyyy-MM-dd');
          const f = !filter.name || data.invoiceNumber.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          const g = !filter.responseFrom || data.settledDate >= this.datePipe.transform(filter.responseFrom, 'yyyy-MM-dd');
          const h = !filter.responseTo || data.settledDate <= this.datePipe.transform(filter.responseTo, 'yyyy-MM-dd');
          const i = !filter.payment || data.paymentMethod.trim().toLowerCase().includes(filter.payment.trim().toLowerCase());
          const j = !filter.numberFrom || data.invoiceTotal >= parseInt(filter.numberFrom);
          const k = !filter.numberTo || data.invoiceTotal <= parseInt(filter.numberTo);
          const l = !filter.salesExec || data.salesExec.trim().toLowerCase().includes(filter.salesExec.trim().toLowerCase());
          return a && b && c && d && e && f && g && h && i && j && k&& l;
        }) as (InvoiceElement, string) => boolean;


        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
  }

  SendEmail(billing: number, user: number, pay: number) {
    let model: IdCollectionModel = new IdCollectionModel();

    model.id = pay;
    model.billingId = billing;
    model.userId = user;

    this.financialService.sendInvoiceEmail(model)
      .subscribe(resp => {
        let retModel = resp

        if (retModel.result == 1) {
          //this.successMessage = "The Details Were Updated Successfully";
        }
        else {
          //this.failMessage = "There was a problem updating the details";
        }
      });
  }


  getsPDFFile(script: string) {
    window.open(script, '_blank');
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
