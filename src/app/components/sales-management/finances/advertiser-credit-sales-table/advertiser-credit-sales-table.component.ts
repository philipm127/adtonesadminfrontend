import { DatePipe } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FinancialService } from 'src/app/components/financials/financial.service';
import { UserCreditTableModel } from 'src/app/components/financials/models/users-credit-tableModel';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { CurrencyEnum } from 'src/app/models/currencyEnum';
import { ReturnResult } from 'src/app/models/return-result';

@Component({
  selector: 'app-advertiser-credit-sales-table',
  templateUrl: './advertiser-credit-sales-table.component.html',
  styleUrls: ['./advertiser-credit-sales-table.component.css']
})
export class AdvertiserCreditSalesTableComponent implements AfterViewInit, OnDestroy {
  pagename: string = "AdvertiserCreditTable";
  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];
  userData: any;
  salesId: number = 0;
  salesExecName: string;
  destroy$: Subject<UserCreditTableModel> = new Subject();
  dataSource: MatTableDataSource<UserCreditTableModel>;
  modelSource: UserCreditTableModel[];
  isLoading: boolean = true;

  countrySelect: BasicSelectModel[] = [];

  currencySymbol = CurrencyEnum;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  retResult: ReturnResult = null;
  createdDateDisplay: string = 'Created Date';

  tableSearchModel: TableSearchModel;
  // This is to appear in Search Box of numberFrom/numberTo
  numberName: string = "Credit Limit";

  addCredit: boolean = true;

  constructor(private userService: FinancialService, private datePipe: DatePipe, private popSearch: PopSearchDdService,
    private accessService: AccessServicesService, private route: ActivatedRoute) {

    this.userData = this.accessService.getUser();
    let sid = this.route.snapshot.paramMap.get('id');
    this.salesId = sid == null ? 0 : parseInt(this.route.snapshot.paramMap.get('id'));
    if (this.userData.roleId == 9)
      this.salesId = this.userData.userId;
    this.salesExecName = this.route.snapshot.paramMap.get('name');
    this.PermissionAssignment();

  }

  ngAfterViewInit() {
    this.userService.getUserCreditListForSalesExec(this.salesId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.isLoading = false;
          this.modelSource = <UserCreditTableModel[]>this.retResult.body;
          this.dataSource = new MatTableDataSource<UserCreditTableModel>(this.modelSource);
        }
        this.countrySelect = this.popSearch.popCountry(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("What is in filters: ",filter);
          // Object.keys(filter).forEach(key => console.log("What is key value: ",this.datePipe.transform(filter[key], 'yyyy-MM-dd')));
          const a = !filter.fullName || data.fullName.trim().toLowerCase().includes(filter.fullName.trim().toLowerCase());
          const b = !filter.email || data.email.trim().toLowerCase().includes(filter.email.trim().toLowerCase());
          const c = !filter.numberFrom || data.credit >= parseInt(filter.numberFrom);
          const d = !filter.numberTo || data.credit <= parseInt(filter.numberTo);
          const e = !filter.dateFrom || data.createdDate >= this.datePipe.transform(filter.dateFrom, 'yyyy-MM-dd');
          const f = !filter.dateTo || data.createdDate <= this.datePipe.transform(filter.dateTo, 'yyyy-MM-dd');
          const g = !filter.country || data.countryName.trim().toLowerCase().includes(filter.country.trim().toLowerCase());
          return a && b && c && d && e && f && g;
        }) as (UserCreditElement, string) => boolean;


        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
    let addFind = permPageData["elements"].filter(j => j.name == "add" && j.type == "addedit");
    this.addCredit = addFind[0].visible;
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
