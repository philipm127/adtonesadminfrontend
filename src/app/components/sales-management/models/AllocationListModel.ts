export interface AllocationListModel {
  fullName: string;
  userId: number;
}
