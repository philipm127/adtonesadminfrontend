import { DatePipe } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AccessServicesService } from 'src/app/components/shared/services/access-services.service';
import { PopSearchDdService } from 'src/app/components/shared/services/pop-search-dd.service';
import { TableSearchModel } from 'src/app/components/shared/table-search-model';
import { TicketTableModel } from 'src/app/components/tickets/models/ticket-table-model';
import { TicketService } from 'src/app/components/tickets/ticket.service';
import { BasicSelectModel } from 'src/app/models/basic-select';
import { ReturnResult } from 'src/app/models/return-result';

@Component({
  selector: 'app-ticket-sales-table',
  templateUrl: './ticket-sales-table.component.html',
  styleUrls: ['./ticket-sales-table.component.css']
})
export class TicketSalesTableComponent implements AfterViewInit, OnDestroy {

  pagename: string = "TicketList";
  permData: any;
  userData: any;
  salesId: number = 0;
  salesExecName: string;
  emailLink: boolean = true;

  // This determines the order and what is displayed not what is in the html.
  displayedColumns: string[] = [];


  destroy$: Subject<TicketTableModel> = new Subject();
  dataSource: MatTableDataSource<TicketTableModel>;
  modelSource: TicketTableModel[];
  isLoading: boolean = true;

  miscName: string = "Ticket ID";
  typeName: string = "Subject";

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  retResult: ReturnResult = null;

  statusSelect: BasicSelectModel[] = [];
  clientSelect: BasicSelectModel[] = [];
  paymentSelect: BasicSelectModel[] = [];
  typeSelect: BasicSelectModel[] = [];


  tableSearchModel: TableSearchModel;

  constructor(private ticketService: TicketService, private datePipe: DatePipe,
    private popService: PopSearchDdService, private accessService: AccessServicesService,
    private route: ActivatedRoute, private router: Router) {
    this.userData = this.accessService.getUser();
    let sid = this.route.snapshot.paramMap.get('id');
    this.salesId = sid == null ? 0 : parseInt(this.route.snapshot.paramMap.get('id'));
    if (this.userData.roleId == 9)
      this.salesId = this.userData.userId;
    this.salesExecName = this.route.snapshot.paramMap.get('name');
    this.PermissionAssignment();
    // console.log("What is adUserId in Tickets: ",this.adUserId);

  }

  filtered(filter: string) {
    this.dataSource.filter = filter;
  }


  ngAfterViewInit() {
    this.ticketService.getTicketListForSalesExec(this.salesId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: HttpResponse<ReturnResult>) => {
        this.isLoading = false;
        this.retResult = res.body;
        // console.log("Returned body is: ", this.retResult);
        if (this.retResult.result == 1) {
          this.modelSource = <TicketTableModel[]>this.retResult.body;
          // console.log("this.modelSource: ", this.modelSource.length);
          this.dataSource = new MatTableDataSource<TicketTableModel>(this.modelSource);
        }
        this.statusSelect = this.popService.popStatus(this.modelSource);
        this.clientSelect = this.popService.popClient(this.modelSource);
        this.paymentSelect = this.popService.popPayment(this.modelSource);
        this.typeSelect = this.popService.popSubjectType(this.modelSource);

        this.dataSource.filterPredicate = ((data, filter) => {
          // console.log("What is in filters: ",filter);
          // Object.keys(filter).forEach(key => console.log("What is key value: ",this.datePipe.transform(filter[key], 'yyyy-MM-dd')));
          const a = !filter.fullName || data.userName.trim().toLowerCase().includes(filter.fullName.trim().toLowerCase());
          const b = !filter.name || data.qNumber.trim().toLowerCase().includes(filter.name.trim().toLowerCase());
          const c = !filter.status || data.rStatus.trim().toLowerCase().includes(filter.status.trim().toLowerCase());
          const d = !filter.dateFrom || data.createdDate >= this.datePipe.transform(filter.dateFrom, 'yyyy-MM-dd');
          const e = !filter.dateTo || data.createdDate <= this.datePipe.transform(filter.dateTo, 'yyyy-MM-dd');
          const f = !filter.responseFrom || data.lastResponseDatetime >= this.datePipe.transform(filter.responseFrom, 'yyyy-MM-dd');
          const g = !filter.responseTo || data.lastResponseDatetime <= this.datePipe.transform(filter.responseTo, 'yyyy-MM-dd');
          const h = !filter.client || data.clientName.trim().toLowerCase().includes(filter.client.trim().toLowerCase());
          const i = !filter.payment || data.paymentMethod.trim().toLowerCase().includes(filter.payment.trim().toLowerCase());
          const j = !filter.typeName || data.questionSubject.trim().toLowerCase().includes(filter.typeName.trim().toLowerCase());
          return a && b && c && d && e && f && g && h && i && j;
        }) as (TicketElement, string) => boolean;

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.displayedColumns = this.accessService.TablePermissionAssignment(permPageData);
    this.tableSearchModel = this.accessService.FilterPermissionAssignment(permPageData);
    let mailFind = permPageData["elements"].filter(j => j.name == "email" && j.type == "element");
    this.emailLink = mailFind[0].enabled;
    // console.log("what is this.emailLink: ", this.emailLink);
  }


  ngOnDestroy() {
    this.destroy$.next();
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
