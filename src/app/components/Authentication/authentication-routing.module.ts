import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { SuccessMessageComponent } from './forgot-password/success-message/success-message.component';
import { AdvertiserLoginComponent } from './advertiser-login/advertiser-login.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent},
  { path: 'ResetPassword', component:ResetPasswordComponent},
  { path: 'forgotpassword', component:ForgotPasswordComponent},
  { path: 'forgotpassword/login', component:LoginComponent},
  { path: 'login/forgotpassword', component:ForgotPasswordComponent},
  { path: 'successmessage', component: SuccessMessageComponent },
  { path: 'adlogin', component: AdvertiserLoginComponent },
  { path: 'adlogin/forgotpassword/login', component: AdvertiserLoginComponent },
  { path: 'adlogin/forgotpassword', component:ForgotPasswordComponent},
  { path: 'adlogin/forgotpassword/login/forgotpassword', component:ForgotPasswordComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
