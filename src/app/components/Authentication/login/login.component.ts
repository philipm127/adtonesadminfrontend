import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/components/Authentication/authentication.service';
import { User } from '../../usermanagement/models/users-applied-models';
import { ReturnResult } from 'src/app/models/return-result';
import { TokenStorageService } from 'src/app/services/auth/token-storage.service';
import { MatFormFieldControl } from '@angular/material/form-field';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { VisitorService } from '../visitor.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  pagename: string = "Login";
  errorMessage: string;
  form: FormGroup;
  model: User;
  userModel: User;
  userData: any;
  retModel: ReturnResult;
  loginStr: string = "forgotpassword";
  public loginInvalid: boolean;
  private formSubmitAttempt: boolean;
  private returnUrl: string;
  submitting = false;
  ipaddress:string = '';
  latitude:string= '';
   longitude:string= '';
   currency:string = '';
   currencysymbol:string = '';
   isp:string= '';
   city:string = '';
   country:string ='';
   languages:string ='';


  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router,
    private authService: AuthenticationService, private tokenStorage: TokenStorageService,
      private accessService: AccessServicesService, private visitorsService: VisitorService) { }

  async ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/game';
    this.form = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(5), Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]]
    });
    // this.visitorsService.getIpAddress().subscribe(res => {
    //   this.ipaddress = res['ip'];
    //   this.visitorsService.getGEOLocation(this.ipaddress).subscribe(res => {

    //     // this.latitude = res['latitude'];
    //     // this.longitude = res['longitude'];
    //     // this.currency = res['currency']['code'];
    //     this.currencysymbol = res['currency']['symbol'];
    //     // this.city = res['city'];
    //     this.country = res['country_code3'];
    //     // this.isp = res['isp'];
    //     this.languages = res['languages'];
    //     console.log(res);
    //   });
    //   //console.log(res);
    // });
  }


  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.returnUrl = permPageData["elements"].filter(j => j.name == "defaultroute")[0].route;

  }


  async onSubmit() {

    this.loginInvalid = false;
    this.formSubmitAttempt = false;
    if (this.form.valid) {
      this.submitting = true;
      try {

        this.authService.Login(this.form.get('username').value, this.form.get('password').value)
          .subscribe(
            data => {
              this.retModel = data
              if (this.retModel.result == 1) {
                // console.log("returned result: ",this.retModel.result);
                this.userModel = <User>this.retModel.body;
                // console.log("about to be saved token value is: ",this.userModel["token"]);
                // console.log("about to be saved as user value is: ", this.userModel);
                this.tokenStorage.saveToken(this.userModel["token"]);
                this.tokenStorage.saveUser(this.userModel);
                this.userData = this.tokenStorage.getUser();
                this.PermissionAssignment();
                this.router.navigate([this.returnUrl]);

              }
              else {
                this.errorMessage = this.retModel.error;
                this.loginInvalid = true;
              }
            });
      } catch (err) {
        this.loginInvalid = true;
        this.errorMessage = "Invalid Login Attempt"
      }
    } else {
      this.formSubmitAttempt = true;
    }
    this.submitting = false;
  }

}
