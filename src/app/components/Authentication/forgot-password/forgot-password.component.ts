import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  loginStr: string = "login";
  form: FormGroup;
  retModel: any;
  errorMessage: string;
  mailInvalid: boolean;

  constructor(private fb: FormBuilder, private authService: AuthenticationService,
              private router: Router) { }

  async ngOnInit() {
    this.form = this.fb.group({
      email: ['', Validators.required]
    });
  }

  onSubmit() {

    if (this.form.valid) {
      // console.log("Entered submit & form valid and model is: ");
      try {
        this.authService.ForgotPassword(this.form.get('email').value)
          .subscribe(
            data => {
              this.retModel = data.body
              console.log("returned result: ",this.retModel);
              if (this.retModel.result == 1) {
                console.log("returned result: ",this.retModel);
                this.mailInvalid = true;
                this.errorMessage = "An email has been sent to your email account";
                this.router.navigate(['successmessage']);
              }
              else {
                this.mailInvalid = true;
                this.errorMessage = this.retModel.error;
              }
            });
      } catch (err) {
        this.mailInvalid = true;
        this.errorMessage = "Email has no match"
      }
    } else {
      this.mailInvalid = true;
      this.errorMessage = "There is a problem speak to admin"
    }
  }

}
