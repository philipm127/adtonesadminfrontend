import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/components/Authentication/authentication.service';
import { MyErrorStateMatcher } from '../../usermanagement/userdetails/update-password/update-password.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  loginStr: string = "login";
  form: FormGroup;
  retModel: any;
  errorMessage: string;
  public mailInvalid: boolean;
  matcher = new MyErrorStateMatcher();
  isValidFormSubmitted = null;

  constructor(private fb: FormBuilder, private authService: AuthenticationService, private router: Router) { }

  async ngOnInit() {

    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', [Validators.required,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&^])[A-Za-z\d$@$!%*?&].{7,}')
      ]
      ],
      confirmPassword: ['']
    }, { validator: this.checkPasswords });
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true }
  }

  get password() {
    return this.form.get('password');
  }


  onSubmit() {
    this.isValidFormSubmitted = false;
    if (this.form.valid) {
      this.isValidFormSubmitted = true;
      // console.log("Entered submit & form valid and model is: ",this.model);
      try {
        console.log("What is this form data: ", this.form.value);
        let frm = this.form.value;
        let newpwd: any = {
          newPassword: frm.password,
          email: frm.email
        };
        this.authService.ResetPassword(newpwd)
          .subscribe(resp => {
            this.retModel = resp

            if (this.retModel.result == 1) {
              this.mailInvalid = true;
              this.errorMessage = "The Password Was Changed Successfully";
              setTimeout(() => {
                this.router.navigate(["login"]);
              }, 1500);
            }
            else {
              this.mailInvalid = true;
              this.errorMessage = this.retModel.error;
            }
          });
      } catch (err) {
        this.mailInvalid = true;
        this.errorMessage = "Email has no match"
      }
    } else {
      return;
    }
  }


}
