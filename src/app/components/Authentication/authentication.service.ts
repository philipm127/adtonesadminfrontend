import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ReturnResult } from '../../models/return-result';
import { TokenStorageService } from 'src/app/services/auth/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  myAppUrl: string;
  controller: string = "login/";

  constructor(private http: HttpClient, private tokenStorage: TokenStorageService) {
    this.myAppUrl = (environment.appUrl + this.controller);
  }


  public Login(user: any, pass: any) {
    let model = { "Email": user, "PasswordHash": pass }
    let ctrAction = "v1/Login"
    console.log("this.myAppUrl ",this.myAppUrl);
    console.log("this.myAppUrl + ctrAction ",this.myAppUrl + ctrAction);
    return this.http.post<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public ChangePassword(model: any) {
    let ctrAction = "v1/UpdatePassword"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public ResetPassword(model: any) {
    console.log("Entered reset password");
    let ctrAction = "v1/ResetPassword"
    return this.http.put<ReturnResult>(this.myAppUrl + ctrAction, model);
  }


  public ForgotPassword(email: string) {
    let ctrAction = "v1/ForgotPassword/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + email, { observe: "response" });
  }

  public RefreshToken(email: string) {
    let ctrAction = "v1/RefreshAccessToken/";
    return this.http.get<ReturnResult>(this.myAppUrl + ctrAction + email, { observe: "response" });
  }

}
