import { Component } from '@angular/core';
import { TokenStorageService } from 'src/app/services/auth/token-storage.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent {
  isLoggedIn: Observable<boolean>;

  constructor(private tokenStorage: TokenStorageService) {
    this.tokenStorage.signOut();
    this.isLoggedIn = this.tokenStorage.isLoggedIn;
  }

}
