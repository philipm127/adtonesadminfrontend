import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ReturnResult } from 'src/app/models/return-result';
import { TokenStorageService } from 'src/app/services/auth/token-storage.service';
import { AccessServicesService } from '../../shared/services/access-services.service';
import { User } from '../../usermanagement/models/users-applied-models';
import { AuthenticationService } from '../authentication.service';
import { VisitorService } from '../visitor.service';

@Component({
  selector: 'app-advertiser-login',
  templateUrl: './advertiser-login.component.html',
  styleUrls: ['./advertiser-login.component.css']
})
export class AdvertiserLoginComponent implements OnInit {
  pagename: string = "Login";
  errorMessage: string;
  loginForm: FormGroup;
  model: User;
  userModel: User;
  userData: any;
  retModel: ReturnResult;
  loginStr: string = "forgotpassword";

  submitting = false;

  public loginInvalid: boolean;
  private formSubmitAttempt: boolean;
  private returnUrl: string;
  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router,
    private authService: AuthenticationService, private tokenStorage: TokenStorageService,
      private accessService: AccessServicesService, private visitorsService: VisitorService) { }

  ngOnInit(): void {
    var equalHeightColumnElements = { "element-groups": { "1": { "selector": ".row-1 .block", "breakpoint": 660 }, "2": { "selector": ".row-2 .block", "breakpoint": 660 }, "3": { "selector": ".row-3 .block", "breakpoint": 660 }, "4": { "selector": ".row-4 .block", "breakpoint": 660 }, "5": { "selector": ".row-5 .block", "breakpoint": 660 }, "6": { "selector": ".row-6 .block", "breakpoint": 660 }, "7": { "selector": ".row-7 .block", "breakpoint": 660 }, "8": { "selector": ".row-8 .block", "breakpoint": 660 }, "9": { "selector": ".row-9 .block", "breakpoint": 660 }, "10": { "selector": ".row-10 .block", "breakpoint": 660 } } };
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/game';
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.minLength(5), Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  PermissionAssignment() {
    let permPageData = this.accessService.getPermissionList(this.pagename);
    this.returnUrl = permPageData["elements"].filter(j => j.name == "defaultroute")[0].route;

  }

  advertiserSignUp(){
    this.router.navigate(["advertisersignup"]);
  }

  async onSubmit() {

    this.loginInvalid = false;
    this.formSubmitAttempt = false;
    if (this.loginForm.valid) {
      this.submitting = true;
      try {

        this.authService.Login(this.loginForm.get('username').value, this.loginForm.get('password').value)
          .subscribe(
            data => {
              this.retModel = data
              if (this.retModel.result == 1) {
                // console.log("returned result: ",this.retModel.result);
                this.userModel = <User>this.retModel.body;
                // console.log("about to be saved token value is: ",this.userModel["token"]);
                // console.log("about to be saved as user value is: ", this.userModel);
                this.tokenStorage.saveToken(this.userModel["token"]);
                this.tokenStorage.saveUser(this.userModel);
                this.userData = this.tokenStorage.getUser();
                // console.log("LOGIN user model: ",this.userModel);
                this.PermissionAssignment();
                this.router.navigate([this.returnUrl]);

              }
              else {
                this.errorMessage = this.retModel.error;
                this.loginInvalid = true;
              }
            });
      } catch (err) {
        this.loginInvalid = true;
        this.errorMessage = "Invalid Login Attempt"
      }
    } else {
      this.formSubmitAttempt = true;
    }
    this.submitting = false;
  }

}
