import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './services/auth/token-storage.service';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AdtonesAdminWeb';
  isLoggedIn: Observable<boolean>;
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
  public href: string = "";
  route: string = "asdf";
  accessPage: string = "";


  constructor(private tokenStorage: TokenStorageService, private router: Router, location: Location)
  // private idle: Idle, private keepalive: Keepalive) {
  {
    let routePath = window.location.pathname;
    this.accessPage = environment.accessPage
    this.route = window.location.origin;
    // if (this.route.indexOf(this.accessPage) == -1 && routePath.indexOf('ResetPassword') == -1
    // && routePath.indexOf('Verify') == -1)
    //   this.router.navigate(['adlogin'])


    this.isLoggedIn = this.tokenStorage.isLoggedIn;
    // sets an idle timeout of 5 seconds, for testing purposes.
    // idle.setIdle(5);
    // // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    // idle.setTimeout(5);
    // // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    // idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    // idle.onIdleEnd.subscribe(() => {
    //   this.idleState = 'No longer idle.'
    //   console.log(this.idleState);
    //   this.reset();
    // });

    // idle.onTimeout.subscribe(() => {
    //   this.idleState = 'Timed out!';
    //   this.timedOut = true;
    //   console.log(this.idleState);
    //   this.router.navigate(['/']);
    // });

    // idle.onIdleStart.subscribe(() => {
    //   this.idleState = 'You\'ve gone idle!'
    //   console.log(this.idleState);
    //   //this.childModal.show();
    // });

    // idle.onTimeoutWarning.subscribe((countdown) => {
    //   this.idleState = 'You will time out in ' + countdown + ' seconds!'
    //   console.log(this.idleState);
    // });

    // // sets the ping interval to 15 seconds
    // keepalive.interval(15);

    // keepalive.onPing.subscribe(() => this.lastPing = new Date());

    // this.reset();
  }

  ngOnInit() {
    this.href = this.router.url;

  }

}
