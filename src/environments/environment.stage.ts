export const environment = {
  production: true,
  // appUrl: 'http://localhost:86/api/',
  appUrl: 'http://77.68.112.245:86/api/',
  // appUrl: 'https://admin.adtones.com:8086/api/',
  // Retrieves json file to be used for populating user permissions if false gets from DB
  accessPage: '88',
  testPermissionOutput: false,
  // Retrieves json file used on individual pages if false gets from DB
  testPermissionInput: false
};

// ng build --configuration=staging
