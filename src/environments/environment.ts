export const environment = {
  production: false,
  // appUrl: 'http://localhost:86/api/'
  appUrl: 'https://localhost:7298/api/'
  // appUrl: 'https://admin.adtones.com:8086/api/'
  // appUrl: 'http://77.68.112.245:86/api/'
  ,
  accessPage: 'bobo',
  // Retrieves json file to be used for populating user permissions if false gets from DB
  testPermissionOutput: false,
  // Retrieves json file used on individual pages if false gets from DB
  testPermissionInput: false

};
