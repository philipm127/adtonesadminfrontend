export const environment = {
  production: true,
  appUrl: 'https://admin.adtones.com:8086/api/',
  // Retrieves json file to be used for populating user permissions if false gets from DB
  accessPage: 'admin',
  testPermissionOutput: false,
  // Retrieves json file used on individual pages if false gets from DB
  testPermissionInput: false
};

